package com.utilitechnology.mystockplannerandroid;

import android.content.Context;

import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Bibaswann on 26-06-2016.
 */
public class CheckTradingToken {

    private static String API_KEY;
    private static String ACCESS_TOKEN;
    TradingAccountSettingsAPI tras;
    Context mContext;

    public CheckTradingToken(Context context) {
        mContext = context;
        tras = new TradingAccountSettingsAPI(mContext);
        ACCESS_TOKEN = tras.readSetting("access_token");
        API_KEY = mContext.getString(R.string.ZERODHA_API_KEY);
    }

    public boolean isZerodhaValid() {
        if (!tras.readSetting("access_token").equals("na")) {
            try {
                URL url = new URL("https://api.kite.trade/user/margins/equity?api_key=" + API_KEY + "&access_token=" + ACCESS_TOKEN);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");

                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.contains("TokenException")) {
                        invalidateZerodhaSession();
                        stream.close();
                        return false;
                    }
                }
            } catch (Exception e) {
                invalidateZerodhaSession();
                return false;
            }
        }
        return true;
    }

    private void invalidateZerodhaSession() {
        URL url = null;
        try {
            url = new URL("https://api.kite.trade/session/token?api_key=" + API_KEY + "&access_token=" + ACCESS_TOKEN);
        } catch (MalformedURLException exception) {
            exception.printStackTrace();
        }
        HttpsURLConnection conn = null;
        try {
            conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("DELETE");
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                //Log.e("important", "success");
                tras.deleteAllSettings();
            }
        } catch (IOException ignored) {
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public String getUserName() {
        if (!tras.readSetting("access_token").equals("na"))
            return tras.readSetting("name");
        else
            return "na";
    }

    public String getUserID() {
        if (!tras.readSetting("access_token").equals("na"))
            return tras.readSetting("id");
        else
            return "na";
    }
}
