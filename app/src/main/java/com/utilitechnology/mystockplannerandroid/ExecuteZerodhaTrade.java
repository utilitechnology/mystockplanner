package com.utilitechnology.mystockplannerandroid;

import android.os.Build;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Bibaswann on 18-06-2016.
 */
public class ExecuteZerodhaTrade {

    private static String REGULAR_ORDER_URL = "https://api.kite.trade/orders/regular";
    private static String MODIFY_URL = "https://api.kite.trade/orders/regular/";
    private static String DELETE_URL = "https://api.kite.trade/orders/regular/";
    HashMap<String, String> postDataParams;

    public volatile boolean orderProcessingIncomplete = true;
    public volatile boolean orderProcessingSuccess;

    public void placeBuySellOrder(HashMap<String, String> tradata,String apiKey, String accessToken) {
        REGULAR_ORDER_URL+="?api_key=" + apiKey+"&access_token=" + accessToken;
        postDataParams = tradata;
        performBuySellPostCall();
    }

    public void placeModifyOrder(@NonNull HashMap<String, String> modata, @NonNull String orderId,String apiKey, String accessToken) {
        MODIFY_URL += orderId+ "?api_key=" + apiKey + "&access_token=" + accessToken;
        postDataParams = modata;
        performModifyPostCall();
    }

    public void placeDeleteOrder(@NonNull String orderId, @NonNull String apiKey, @NonNull String accessToken) {
        DELETE_URL += orderId + "?api_key=" + apiKey + "&access_token=" + accessToken;
        performDeleteOperation();
    }

    private String getPostData(HashMap<String, String> params) {
        StringBuilder result = new StringBuilder();
        try {
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return result.toString();
    }

    public void performBuySellPostCall() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        URL url= new URL(REGULAR_ORDER_URL+"&"+getPostData(postDataParams));
                        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                        conn.setReadTimeout(15000);
                        conn.setConnectTimeout(15000);
                        conn.setRequestMethod("POST");
                        conn.setDoInput(true);
                        conn.setDoOutput(true);

                        OutputStream os = conn.getOutputStream();
    //                    conn.connect();

                        int responseCode = conn.getResponseCode();

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String line;
                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            if ((line = br.readLine()) != null) {
//                                Log.e("important", "lines are " + line);
                                ParseOrderResultDataJSON podj = new ParseOrderResultDataJSON();
                                podj.parseJSONAndStoreIt(line);
                                while (podj.parsingInComplete) ;
                                if (podj.getStatus().equals("success")) {
                                    orderProcessingIncomplete = false;
                                    orderProcessingSuccess = true;
                                } else {
                                    orderProcessingIncomplete = false;
                                    orderProcessingSuccess = false;
                                }
                            }
                        } else {
    //                        Log.e("important", "responsecode is " + responseCode);
                            orderProcessingIncomplete = false;
                            orderProcessingSuccess = false;
                        }
                        os.close();

                    } catch (Exception e) {
    //                    Log.e("important",Log.getStackTraceString(e));
                        orderProcessingIncomplete = false;
                        orderProcessingSuccess = false;
                    }
//
                }
            }).start();
    }

    public void performModifyPostCall() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(MODIFY_URL+"&"+getPostData(postDataParams));
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("PUT");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStream os = conn.getOutputStream();
//                    conn.connect();


                    int responseCode = conn.getResponseCode();
                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        if ((line = br.readLine()) != null) {
                            //Log.e("important", "lines are " + line);
                            ParseOrderResultDataJSON podj = new ParseOrderResultDataJSON();
                            podj.parseJSONAndStoreIt(line);
                            while (podj.parsingInComplete) ;
                            if (podj.getStatus().equals("success")) {
                                orderProcessingIncomplete = false;
                                orderProcessingSuccess = true;
                            } else {
                                orderProcessingIncomplete = false;
                                orderProcessingSuccess = false;
                            }
                        }
                    } else {
                        //Log.e("important", "responsecode is " + responseCode);
                        orderProcessingIncomplete = false;
                        orderProcessingSuccess = false;
                    }

                    os.close();
                } catch (Exception e) {
                    //Log.e("important",Log.getStackTraceString(e));
                    orderProcessingIncomplete = false;
                    orderProcessingSuccess = false;
                }
            }
        }).start();
    }

    public void performDeleteOperation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(DELETE_URL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("DELETE");
                    conn.setDoInput(true);
                    OutputStream os = conn.getOutputStream();
                    if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        if ((line = br.readLine()) != null) {
                            //Log.e("important", "lines are " + line);
                            ParseOrderResultDataJSON podj = new ParseOrderResultDataJSON();
                            podj.parseJSONAndStoreIt(line);
                            while (podj.parsingInComplete) ;
                            if (podj.getStatus().equals("success")) {
                                orderProcessingIncomplete = false;
                                orderProcessingSuccess = true;
                            } else {
                                orderProcessingIncomplete = false;
                                orderProcessingSuccess = false;
                            }
                        }
                    } else {
                        orderProcessingIncomplete = false;
                        orderProcessingSuccess = false;
                    }
                    os.close();
                } catch (IOException e) {
                    orderProcessingIncomplete = false;
                    orderProcessingSuccess = false;
                }
            }
        }).start();
    }

    private class ParseOrderResultDataJSON {
        String status, ordrId;
        public volatile boolean parsingInComplete = true;

        public String getStatus() {
            return status;
        }

        public String getOrdrId() {
            return ordrId;
        }

        private void parseJSONAndStoreIt(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                status = jsonObject.optString("status");
                String dataString = jsonObject.optString("data");
                if (status.equals("success")) {
                    JSONObject jsonDataObject = new JSONObject(dataString);
                    ordrId = jsonDataObject.optString("order_id");//No need for the id, just taking
                }

                parsingInComplete = false;
            } catch (JSONException e) {
                //Log.e("important", "exception " + Log.getStackTraceString(e));
            }
        }
    }

}
