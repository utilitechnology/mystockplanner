package com.utilitechnology.mystockplannerandroid;

import android.content.Context;

import com.utilitechnology.mystockplannerandroid.caching.IndexCachingAPI;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * Created by Bibaswann on 06-03-2016.
 */
public class SaveIndexData {
    Context mContext;
    private String BSENow, NSENow, BSEchange, NSEchange, BSEChangePercent, NSEChangePercent, BSELastUpdate, NSELastUpdate;
    private String openVNS, PrevCloseNS, dayHighNS, dayLowNS, yrHighNS, yrLowNS;
    private String openVBS, PrevCloseBS, dayHighBS, dayLowBS, yrHighBS, yrLowBS;
    private String bseUp, nseUp;

    private String BSE_RESOURCE_WEB = Urls.MSN_INDEX_PREFIX + "?symbol=1";
    private String NSE_RESOURCE_WEB = Urls.MSN_INDEX_PREFIX + "fi-138.10.NIFTY50?symbol=NIFTY";
    private final String BSE_NSE_RESOURCE_GOOGLE = Urls.GOOGLE_JSON_PREFIX + "INDEXBOM:SENSEX,NSE:NIFTY";
    //    private final String BSE_NSE_RESOURCE_YAHOO = "http://finance.yahoo.com/webservice/v1/symbols/^bsesn,^nsei/quote?format=xml&mView=detail";
    //ParseXML prx;
//    ParseJSON prj;
    IndexCachingAPI ica;
    SettingsAPI set;

    public SaveIndexData(Context context) {
        mContext = context;
        ica = new IndexCachingAPI(mContext);
        set = new SettingsAPI(mContext);
    }

    public void downloadCharts(String uri) {
        String fileName = uri.replace("/", "").replace(";", "").replace("=", "").replace("^", "").replace("https:chartapi.finance.yahoo.cominstrument1.0", "");
        fileName += ".csv";
        try {
            URL url = new URL(uri);
            InputStream stream = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            StringBuilder data = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                data.append(line).append("\r\n");
            }
            ica.deleteOldCharts(fileName);
            ica.saveChart(fileName, data.toString());
        } catch (Exception ignored) {
            ica.deleteOldCharts(fileName);
        }
    }

    public void downloadValues() {
//Method 0: Parse from MSN
        //BSE first
        try {
            Document bsdoc = Jsoup.parse(new URL(BSE_RESOURCE_WEB), 10000);
            Elements bstopicList = bsdoc.select("span[class=current-price]");
            for (org.jsoup.nodes.Element topic : bstopicList)
                BSENow = topic.text().trim();
            Elements bstopicListChange = bsdoc.select("div[data-role=change]");
            BSEchange = bstopicListChange.get(0).text().trim();
            Elements bstopicListChangeP = bsdoc.select("div[data-role=percentchange]");
            BSEChangePercent = bstopicListChangeP.get(0).text().trim();
            Elements bstopicListLastUpdate = bsdoc.select("span[class=update-datetime]");
            for (org.jsoup.nodes.Element topic : bstopicListLastUpdate)
                BSELastUpdate = "Last update: " + topic.text().trim();
            if (BSEchange.contains("+"))
                bseUp = "up";
            else
                bseUp = "down";
            try {
                Elements topicListHelper = bsdoc.select("ul[class=today-trading-container]").select("li").select("span").select("p");
//                    for (Element t:topicListHelper
//                         ) {
//                        Log.e("important", t.text());
//                    }
                //There are also day hi-lo and yr hi-lo in separate tags
                PrevCloseBS = topicListHelper.get(3).text().trim();
                openVBS = topicListHelper.get(1).text().trim();
                dayHighBS = topicListHelper.get(5).text().split(Pattern.quote("-"))[1].trim();
                dayLowBS = topicListHelper.get(5).text().split(Pattern.quote("-"))[0].trim();
                yrHighBS = topicListHelper.get(7).text().split(Pattern.quote("-"))[1].trim();
                yrLowBS = topicListHelper.get(7).text().split(Pattern.quote("-"))[0].trim();
            } catch (IndexOutOfBoundsException ioob) {
                PrevCloseBS = mContext.getString(R.string.global_not_found);
                openVBS = mContext.getString(R.string.global_not_found);
                dayHighBS = mContext.getString(R.string.global_not_found);
                dayLowBS = mContext.getString(R.string.global_not_found);
                yrHighBS = mContext.getString(R.string.global_not_found);
                yrLowBS = mContext.getString(R.string.global_not_found);
            }

            //Then NSE

            Document nsdoc = Jsoup.parse(new URL(NSE_RESOURCE_WEB), 10000);
            Elements nstopicList = nsdoc.select("span[class=current-price]");
            for (org.jsoup.nodes.Element topic : nstopicList)
                NSENow = topic.text().trim();
            Elements nstopicListChange = nsdoc.select("div[data-role=change]");
            NSEchange = nstopicListChange.get(0).text().trim();
            Elements nstopicListChangeP = nsdoc.select("div[data-role=percentchange]");
            NSEChangePercent = nstopicListChangeP.get(0).text().trim();
            Elements nstopicListLastUpdate = nsdoc.select("span[class=update-datetime]");
            for (org.jsoup.nodes.Element topic : nstopicListLastUpdate)
                NSELastUpdate = "Last update: " + topic.text().trim();
            if (NSEchange.contains("+"))
                nseUp = "up";
            else
                nseUp = "down";
            try {
                Elements topicListHelper = nsdoc.select("ul[class=today-trading-container]").select("li").select("span").select("p");
                PrevCloseNS = topicListHelper.get(3).text().trim();
                openVNS = topicListHelper.get(1).text().trim();
                dayHighNS = topicListHelper.get(5).text().split(Pattern.quote("-"))[1].trim();
                dayLowNS = topicListHelper.get(5).text().split(Pattern.quote("-"))[0].trim();
                yrHighNS = topicListHelper.get(7).text().split(Pattern.quote("-"))[1].trim();
                yrLowNS = topicListHelper.get(7).text().split(Pattern.quote("-"))[0].trim();
            } catch (IndexOutOfBoundsException ioob) {
                PrevCloseNS = mContext.getString(R.string.global_not_found);
                openVNS = mContext.getString(R.string.global_not_found);
                dayHighNS = mContext.getString(R.string.global_not_found);
                dayLowNS = mContext.getString(R.string.global_not_found);
                yrHighNS = mContext.getString(R.string.global_not_found);
            }
        } catch (IOException ignored) {
        }

        //Method 1: Get from yahoo and google finance api: use yahoo resource with ParseXML to fetch from yahoo

//        prj = new ParseJSON(BSE_NSE_RESOURCE_GOOGLE);
//        prj.fetchJSON();
//        while (prj.parsingInComplete) ;
//
//        //Log.e("important","google done");
//        //I don't remember why I removed XML parsing, there must be a reason. Will check later
//        try {
//            BSENow = prj.getPrice().get(0);
//            NSENow = prj.getPrice().get(1);
//
//            BSEchange = prj.getChange().get(0);
//            NSEchange = prj.getChange().get(1);
//
//            BSEChangePercent = prj.getChangePercent().get(0);
//            NSEChangePercent = prj.getChangePercent().get(1);
//
//            BSELastUpdate = prj.getUpdate();
//            NSELastUpdate = prj.getUpdate();//Double here if in future requirements arise to save different update values in xml
//
//            bseUp = prj.getUpsNDowns().get(0);
//            nseUp = prj.getUpsNDowns().get(1);
//
//            dayHighBS = prj.getDayHigh().get(0);
//            dayHighNS = prj.getDayHigh().get(1);
//
//            dayLowBS = prj.getDayLow().get(0);
//            dayLowNS = prj.getDayLow().get(1);
//
//            yrHighBS = prj.getYearHigh().get(0);
//            yrHighNS = prj.getYearHigh().get(1);
//
//            yrLowBS = prj.getYearLow().get(0);
//            yrLowNS = prj.getYearLow().get(1);
//
//            PrevCloseBS = prj.getP_closes().get(0);
//            PrevCloseNS = prj.getP_closes().get(1);
//
//            openVBS = prj.getOpens().get(0);
//            openVNS = prj.getOpens().get(1);
//        } catch (IndexOutOfBoundsException iobx) {
//            // TODO: 10-05-2017 tags changed, check later for new tags
//            //Method 2: Screen scrape from yahoo
//        }

        //Then save along with time
        ica.addNewValue("BSENow", BSENow);
        ica.addNewValue("NSENow", NSENow);
        ica.addNewValue("BSEChange", BSEchange);
        ica.addNewValue("NSEChange", NSEchange);
        ica.addNewValue("BSEChangePercent", BSEChangePercent);
        ica.addNewValue("NSEChangePercent", NSEChangePercent);
        ica.addNewValue("BSELastUpdate", BSELastUpdate);
        ica.addNewValue("NSELastUpdate", NSELastUpdate);
        ica.addNewValue("bseUp", bseUp);
        ica.addNewValue("nseUp", nseUp);

        ica.addNewValue("openVBS", openVBS);
        ica.addNewValue("openVNS", openVNS);

        ica.addNewValue("prevCloseNS", PrevCloseNS);
        ica.addNewValue("prevCloseBS", PrevCloseBS);

        ica.addNewValue("dayHighNS", dayHighNS);
        ica.addNewValue("dayHighBS", dayHighBS);

        ica.addNewValue("dayLowNS", dayLowNS);
        ica.addNewValue("dayLowBS", dayLowBS);

        ica.addNewValue("yrHighNS", yrHighNS);
        ica.addNewValue("yrHighBS", yrHighBS);

        ica.addNewValue("yrLowNS", yrLowNS);
        ica.addNewValue("yrLowBS", yrLowBS);

        set.addUpdateSettings("indexTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
    }

    public String getBSENow() {
        return ica.getValue("BSENow");
    }

    public String getNSENow() {
        return ica.getValue("NSENow");
    }

    public String getBSEchange() {
        return ica.getValue("BSEChange");
    }

    public String getNSEchange() {
        return ica.getValue("NSEChange");
    }

    public String getBSEChangePercent() {
        return "(" + ica.getValue("BSEChangePercent") + ")";
    }

    public String getNSEChangePercent() {
        return "(" + ica.getValue("NSEChangePercent") + ")";
    }

    public String getBSELastUpdate() {
        return ica.getValue("BSELastUpdate");
    }

    public String getNSELastUpdate() {
        return ica.getValue("NSELastUpdate");
    }

    public String getBseUp() {
        return ica.getValue("bseUp");
    }

    public String getNseUp() {
        return ica.getValue("nseUp");
    }

    public String getOpenVBS() {
        return ica.getValue("openVBS");
    }

    public String getOpenVNS() {
        return ica.getValue("openVNS");
    }

    public String getPrevCloseBS() {
        return ica.getValue("prevCloseBS");
    }

    public String getPrevCloseNS() {
        return ica.getValue("prevCloseNS");
    }

    public String getDayHighBS() {
        return ica.getValue("dayHighBS");
    }

    public String getDayLowBS() {
        return ica.getValue("dayLowBS");
    }

    public String getDayHighNS() {
        return ica.getValue("dayHighNS");
    }

    public String getDayLowNS() {
        return ica.getValue("dayLowNS");
    }

    public String getYrHighBS() {
        return ica.getValue("yrHighBS");
    }

    public String getYrHighNS() {
        return ica.getValue("yrHighNS");
    }

    public String getYrLowBS() {
        return ica.getValue("yrLowBS");
    }

    public String getYrLowNS() {
        return ica.getValue("yrLowNS");
    }
}
