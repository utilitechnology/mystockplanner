package com.utilitechnology.mystockplannerandroid;

import android.content.Context;

import com.utilitechnology.mystockplannerandroid.caching.MoverCachingAPI;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseJSON;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 10-04-2016.
 */
public class SaveMovers {

    Context mContext;
    SettingsAPI set;
    ScriptHelper sch;
    ParseJSON prj;
    List<String> companies, scripts, ltps, changes;
    List<Float> realChangePercentList;
    MoverCachingAPI mca;

    private List<String> companiesG = new ArrayList<>();
    private List<String> scriptsG = new ArrayList<>();
    private List<String> ltpsG = new ArrayList<>();
    private List<String> changesG = new ArrayList<>();
    private List<String> upsNDownsG = new ArrayList<>();

    private List<String> companiesL = new ArrayList<>();
    private List<String> scriptsL = new ArrayList<>();
    private List<String> ltpsL = new ArrayList<>();
    private List<String> changesL = new ArrayList<>();
    private List<String> upsNDownsL = new ArrayList<>();

    String[] b30 = {"adaniports.bo", "asianpaint.bo", "axisbank.bo", "bajaj-auto.bo", "bhartiartl.bo", "bhel.bo", "cipla.bo", "coalindia.bo", "drreddy.bo", "gail.bo", "hdfc.bo", "hdfcbank.bo", "heromotoco.bo", "hindunilvr.bo", "icicibank.bo", "infy.bo", "itc.bo", "lt.bo", "lupin.bo", "m&m.bo", "maruti.bo", "ntpc.bo", "ongc.bo", "reliance.bo", "sbin.bo", "sunpharma.bo", "tatamotors.bo", "tatasteel.bo", "tcs.bo", "wipro.bo"};

    String[] n50 = {"acc.ns", "adaniports.ns", "ambujacem.ns", "asianpaint.ns", "axisbank.ns", "bajaj-auto.ns", "bankbaroda.ns", "bhel.ns", "bpcl.ns", "bhartiartl.ns", "boschltd.ns", "cairn.ns", "cipla.ns", "coalindia.ns", "drreddy.ns", "gail.ns", "grasim.ns", "hcltech.ns", "hdfcbank.ns", "heromotoco.ns", "hindalco.ns", "hindunilvr.ns", "hdfc.ns", "itc.ns", "icicibank.ns", "idea.ns", "indusindbk.ns", "infy.ns", "kotakbank.ns", "lt.ns", "lupin.ns", "m&m.ns", "maruti.ns", "ntpc.ns", "ongc.ns", "powergrid.ns", "pnb.ns", "reliance.ns", "sbin.ns", "sunpharma.ns", "tcs.ns", "tatamotors.ns", "tatapower.ns", "tatasteel.ns", "techm.ns", "ultracemco.ns", "vedl.ns", "wipro.ns", "yesbank.ns", "zeel.ns"};

//    private final String YAHOO_BSE_RESOURCE = "http://finance.yahoo.com/webservice/v1/symbols/adaniports.bo,asianpaint.bo,axisbank.bo,bajaj-auto.bo,bhartiartl.bo,bhel.bo,cipla.bo,coalindia.bo,drreddy.bo,gail.bo,hdfc.bo,hdfcbank.bo,heromotoco.bo,hindunilvr.bo,icicibank.bo,infy.bo,itc.bo,lt.bo,lupin.bo,m&m.bo,maruti.bo,ntpc.bo,ongc.bo,reliance.bo,sbin.bo,sunpharma.bo,tatamotors.bo,tatasteel.bo,tcs.bo,wipro.bo/quote?format=xml&mView=detail";
//
//    private final String YAHOO_NSE_RESOURCE = "http://finance.yahoo.com/webservice/v1/symbols/acc.ns,adaniports.ns,ambujacem.ns,asianpaint.ns,axisbank.ns,bajaj-auto.ns,bankbaroda.ns,bhel.ns,bpcl.ns,bhartiartl.ns,boschltd.ns,cairn.ns,cipla.ns,coalindia.ns,drreddy.ns,gail.ns,grasim.ns,hcltech.ns,hdfcbank.ns,heromotoco.ns,hindalco.ns,hindunilvr.ns,hdfc.ns,itc.ns,icicibank.ns,idea.ns,indusindbk.ns,infy.ns,kotakbank.ns,lt.ns,lupin.ns,m&m.ns,maruti.ns,ntpc.ns,ongc.ns,powergrid.ns,pnb.ns,reliance.ns,sbin.ns,sunpharma.ns,tcs.ns,tatamotors.ns,tatapower.ns,tatasteel.ns,techm.ns,ultracemco.ns,vedl.ns,wipro.ns,yesbank.ns,zeel.ns/quote?format=xml&mView=detail";

    private final String GOOGLE_BSE_RESOURCE = Urls.GOOGLE_JSON_PREFIX + "BOM:532921,BOM:500820,BOM:532215,BOM:532977,BOM:532454,BOM:500103,BOM:500087,BOM:533278,BOM:500124,BOM:532155,BOM:500010,BOM:500180,BOM:500182,BOM:500696,BOM:532174,BOM:500209,BOM:500875,BOM:500510,BOM:500257,BOM:500520,BOM:532500,BOM:532555,BOM:500312,BOM:500325,BOM:500112,BOM:524715,BOM:500570,BOM:500470,BOM:532540,BOM:507685";

    private final String GOOGLE_NSE_RESOURCE = Urls.GOOGLE_JSON_PREFIX + "NSE:ACC,NSE:ADANIPORTS,NSE:AMBUJACEM,NSE:ASIANPAINT,NSE:AXISBANK,NSE:BAJAJ-AUTO,NSE:BANKBARODA,NSE:BHEL,NSE:BPCL,NSE:BHARTIARTL,NSE:BOSCHLTD,NSE:CAIRN,NSE:CIPLA,NSE:COALINDIA,NSE:DRREDDY,NSE:GAIL,NSE:GRASIM,NSE:HCLTECH,NSE:HDFCBANK,NSE:HEROMOTOCO,NSE:HINDALCO,NSE:HINDUNILVR,NSE:HDFC,NSE:ITC,NSE:ICICIBANK,NSE:IDEA,NSE:INDUSINDBK,NSE:INFY,NSE:KOTAKBANK,NSE:LT,NSE:LUPIN,NSE:M%26M,NSE:MARUTI,NSE:NTPC,NSE:ONGC,NSE:POWERGRID,NSE:PNB,NSE:RELIANCE,NSE:SBIN,NSE:SUNPHARMA,NSE:TCS,NSE:TATAMOTORS,NSE:TATAPOWER,NSE:TATASTEEL,NSE:TECHM,NSE:ULTRACEMCO,NSE:VEDL,NSE:WIPRO,NSE:YESBANK,NSE:ZEEL";

    SaveMovers(Context context) {
        mContext = context;
        companies = new ArrayList<>();
        scripts = new ArrayList<>();
        ltps = new ArrayList<>();
        changes = new ArrayList<>();
        realChangePercentList = new ArrayList<>();

        set = new SettingsAPI(mContext);
        sch = new ScriptHelper(mContext);
        mca = new MoverCachingAPI(mContext);
    }

    public void downloadMovers() {
        //Log.e("important","savemovers called");
        try {
            if (set.readSetting("currentIndex").equals("^bsesn")) {
                filList("bse");
                getGainerList();
                getLoserList();
                //fillGainerLoserList();
                mca.saveBSEData(companiesG, scriptsG, ltpsG, changesG, upsNDownsG, companiesL, scriptsL, ltpsL, changesL, upsNDownsL);
                set.addUpdateSettings("bseMoverTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
                //Log.e("important","setting bsemovertimestamp");
            } else {
                filList("nse");
                getGainerList();
                getLoserList();
                //fillGainerLoserList();
                mca.saveNSEData(companiesG, scriptsG, ltpsG, changesG, upsNDownsG, companiesL, scriptsL, ltpsL, changesL, upsNDownsL);
                set.addUpdateSettings("nseMoverTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
                //Log.e("important","setting nsemovertimestamp");
            }
        } catch (Exception ex) {
            //Log.e("important", Log.getStackTraceString(ex));
            companiesL.clear();
            scriptsL.clear();
            ltpsL.clear();
            changesL.clear();
            upsNDownsL.clear();
            companiesL.add("No items here");
            scriptsL.add("");
            ltpsL.add("");
            changesL.add("");
            upsNDownsL.add("up");

            companiesG.clear();
            scriptsG.clear();
            ltpsG.clear();
            changesG.clear();
            upsNDownsG.clear();
            companiesG.add("No items here");
            scriptsG.add("");
            ltpsG.add("");
            changesG.add("");
            upsNDownsG.add("up");
        }
    }

    private void fillGainerLoserList() {
        companiesG.clear();
        scriptsG.clear();
        ltpsG.clear();
        changesG.clear();
        upsNDownsG.clear();

        companiesL.clear();
        scriptsL.clear();
        ltpsL.clear();
        changesL.clear();
        upsNDownsL.clear();

        List<String> unsortedCompaniesG = new ArrayList<>();
        List<String> unsortedScriptsG = new ArrayList<>();
        List<String> unsortedLtpsG = new ArrayList<>();
        List<String> unsortedChangesG = new ArrayList<>();
        List<Float> unsortedRealChangePercentListG = new ArrayList<>();
        List<Float> unsortedRealChangePercentListFixedG = new ArrayList<>();

        List<String> unsortedCompaniesL = new ArrayList<>();
        List<String> unsortedScriptsL = new ArrayList<>();
        List<String> unsortedLtpsL = new ArrayList<>();
        List<String> unsortedChangesL = new ArrayList<>();
        List<Float> unsortedRealChangePercentListL = new ArrayList<>();
        List<Float> unsortedRealChangePercentListFixedL = new ArrayList<>();

        int index;
        for (String oneScript : scripts) {
            index = scripts.indexOf(oneScript);
            if (realChangePercentList.get(index) > 0) {
                unsortedCompaniesG.add(companies.get(index));
                unsortedScriptsG.add(scripts.get(index));
                unsortedLtpsG.add(ltps.get(index));
                unsortedChangesG.add(changes.get(index));
                unsortedRealChangePercentListG.add(realChangePercentList.get(index));
                unsortedRealChangePercentListFixedG.add(realChangePercentList.get(index));
            } else {
                unsortedCompaniesL.add(companies.get(index));
                unsortedScriptsL.add(scripts.get(index));
                unsortedLtpsL.add(ltps.get(index));
                unsortedChangesL.add(changes.get(index));
                unsortedRealChangePercentListL.add(realChangePercentList.get(index));
                unsortedRealChangePercentListFixedL.add(realChangePercentList.get(index));
            }
        }

        Comparator com = Collections.reverseOrder();
        Collections.sort(unsortedRealChangePercentListG, com);

        int sortIndex;
        for (Float real : unsortedRealChangePercentListG) {
            sortIndex = unsortedRealChangePercentListFixedG.indexOf(real);

            companiesG.add(unsortedCompaniesG.get(sortIndex));
            scriptsG.add(unsortedScriptsG.get(sortIndex));
            ltpsG.add(unsortedLtpsG.get(sortIndex));
            changesG.add(unsortedChangesG.get(sortIndex));
            upsNDownsG.add("up");
        }

        Collections.sort(unsortedRealChangePercentListL);

        for (Float real : unsortedRealChangePercentListL) {
            sortIndex = unsortedRealChangePercentListFixedL.indexOf(real);

            companiesL.add(unsortedCompaniesL.get(sortIndex));
            scriptsL.add(unsortedScriptsL.get(sortIndex));
            ltpsL.add(unsortedLtpsL.get(sortIndex));
            changesL.add(unsortedChangesL.get(sortIndex));
            upsNDownsL.add("down");
        }
    }

    private void getGainerList() {
        int numOfPositiveItems = 0;
        for (Float real : realChangePercentList) {
            if (real >= 0)
                numOfPositiveItems++;
        }
        while (numOfPositiveItems > 0) {
            int indexToRemove = -1;
            Float highest = 0.0f;
            for (Float real : realChangePercentList) {
                if (real >= highest) {
                    highest = real;
                    indexToRemove = realChangePercentList.indexOf(real);
                    //Log.e("important","index of change "+highest+ " is "+index);
                }
            }
            if (indexToRemove != -1) {
                //Log.e("important","gainer found: "+companies.get(index)+" changes "+changes.get(index)+" index "+index);
                companiesG.add(companies.get(indexToRemove));
                scriptsG.add(scripts.get(indexToRemove));
                ltpsG.add(ltps.get(indexToRemove));
                changesG.add(changes.get(indexToRemove));
                upsNDownsG.add("up");

                realChangePercentList.remove(indexToRemove);
                companies.remove(indexToRemove);
                scripts.remove(indexToRemove);
                ltps.remove(indexToRemove);
                changes.remove(indexToRemove);

                numOfPositiveItems--;
            }
        }
    }

    private void getLoserList() {
        int numOfNegativeItems = 0;
        for (Float real : realChangePercentList) {
            if (real < 0)
                numOfNegativeItems++;
        }
        while (numOfNegativeItems > 0) {
            int indexToRemove = -1;
            Float lowest = 0.0f;
            for (Float real : realChangePercentList) {
                if (real <= lowest) {
                    lowest = real;
                    indexToRemove = realChangePercentList.indexOf(real);
                }
            }
            if (indexToRemove != -1) {
                //Log.e("important","loser found: "+companies.get(index));
                companiesL.add(companies.get(indexToRemove));
                scriptsL.add(scripts.get(indexToRemove));
                ltpsL.add(ltps.get(indexToRemove));
                changesL.add(changes.get(indexToRemove));
                upsNDownsL.add("down");

                realChangePercentList.remove(indexToRemove);
                companies.remove(indexToRemove);
                scripts.remove(indexToRemove);
                ltps.remove(indexToRemove);
                changes.remove(indexToRemove);

                numOfNegativeItems--;
            }
        }
    }

    private void filList(String index) {
        scripts.clear();
        companies.clear();
        ltps.clear();
        changes.clear();
        realChangePercentList.clear();
        if (index.equals("nse")) {
            for (String scr : n50) {
                scripts.add(scr);
                companies.add(sch.scriptToCompany(scr));
            }
            prj = new ParseJSON(GOOGLE_NSE_RESOURCE);
        } else {
            for (String scr : b30) {
                scripts.add(sch.unconvertScript(scr));
                companies.add(sch.scriptToCompany(scr));
            }
            prj = new ParseJSON(GOOGLE_BSE_RESOURCE);
        }

        prj.fetchJSON();
        while (prj.parsingInComplete) ;
        for (String oneChange : prj.getRealChangePercent()) {
            realChangePercentList.add(stringToIndianFloat(oneChange));
        }
        ltps = prj.getPrice();
        changes = prj.getChangeString();

//        for (String comp:companies) {
//            int i=0;
//            Log.e("important","company "+comp+" change_percent "+realChangePercentList.get(i));
//            i++;
//        }
    }

    private float stringToIndianFloat(String text) {
        NumberFormat format = NumberFormat.getInstance(new Locale("en", "IN"));
        float formattedFloat = 0.00f;
        try {
            Number number = format.parse(text);
            formattedFloat = number.floatValue();
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        //Log.e("important","returning "+formattedFloat);
        return formattedFloat;
    }
}
