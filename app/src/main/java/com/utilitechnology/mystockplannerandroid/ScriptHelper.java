package com.utilitechnology.mystockplannerandroid;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.Pattern;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by Bibaswann on 02-02-2016.
 */
public class ScriptHelper {

    Context mContext;

    public ScriptHelper(Context context) {
        mContext = context;
    }

    //Todo use this before showing stock data and graph in HandleChartValues and HandleEquity
    public String convertScript(String bseScript) {
        String scriptPart, extensionPart;
        if (bseScript.contains(".")) {
            scriptPart = bseScript.split(Pattern.quote("."))[0];
            extensionPart = "." + bseScript.split(Pattern.quote("."))[1];
        } else {
            scriptPart = bseScript;
            extensionPart = "";
        }
        return companyToScriptNseFirst(scriptToCompany(scriptPart)) + extensionPart;
    }

    public String makeMSNString(String unconvertedScriptWithExtension) {
        if(unconvertedScriptWithExtension.equals("^bsesn"))
            return "?symbol=1";
        if(unconvertedScriptWithExtension.equals("^nsei"))
            return "fi-138.10.NIFTY50?symbol=NIFTY";
        String scriptPart = unconvertedScriptWithExtension.split(Pattern.quote("."))[0].toUpperCase();
        String extensionPart = unconvertedScriptWithExtension.split(Pattern.quote("."))[1];
        if (extensionPart.equals("bo"))
            return "fi-139.1." + unconvertedScriptWithExtension.replace(".bo", ".BOM") + "?symbol=" + scriptPart;
        else
            return "fi-138.1." + unconvertedScriptWithExtension.replace(".ns", ".NSE") + "?symbol=" + scriptPart;
    }

    public String unconvertScript(String nseScript) {
        String scriptPart, extensionPart;
        if (nseScript.contains(".")) {
            scriptPart = nseScript.split(Pattern.quote("."))[0];
            extensionPart = "." + nseScript.split(Pattern.quote("."))[1];
        } else {
            scriptPart = nseScript;
            extensionPart = "";
        }
        return companyToScriptBseFirst(scriptToCompany(scriptPart)) + extensionPart;
    }

    public String getGoogleScript(String unconvertedScriptWithExtension) {
        String googleScript;
        switch (unconvertedScriptWithExtension) {
            case "^gspc":
                googleScript = "INDEXSP:.INX";
                break;
            case "^dji":
                googleScript = "INDEXDJX:.DJI";
                break;
            case "^ixic":
                googleScript = "INDEXNASDAQ:.IXIC";
                break;
            case "^gsptse":
                googleScript = "INDEXTSI:OSPTX";
                break;
            case "^bvsp":
                googleScript = "INDEXBVMF:IBOV";
                break;
            case "^mxx":
                googleScript = "INDEXBMV:ME";
                break;
            case "^gdaxi":
                googleScript = "INDEXDB:DAX";
                break;
            case "^ftse":
                googleScript = "INDEXFTSE:UKX";
                break;
            case "^fchi":
                googleScript = "INDEXEURO:PX1";
                break;
            case "^jkse":
                googleScript = "IDX:COMPOSITE";
                break;
            case "^n225":
                googleScript = "INDEXNIKKEI:NI225";
                break;
            case "^axjo":
                googleScript = "INDEXASX:XJO";
                break;
            case "000001.ss":
                googleScript = "SHA:000001";
                break;
            case "^hsi":
                googleScript = "INDEXHANGSENG:HSI";
                break;
            case "^ks11":
                googleScript = "KRX:KOSPI";
                break;
            case "^bsesn":
                googleScript = "INDEXBOM:SENSEX";
                break;
            case "BSE-100.BO":
                googleScript = "INDEXBOM:BSE-100";
                break;
            case "BSE-500.BO":
                googleScript = "INDEXBOM:BSE-500";
                break;
            case "ALLCAP.BO":
                googleScript = "INDEXBOM:DOLLEX30";// TODO: 18-05-2017 this is wrong
                break;
            case "LRGCAP.BO":
                googleScript = "INDEXBOM:DOLLEX30";// TODO: 18-05-2017 this is wrong
                break;
            case "BSE-MIDCAP.BO":
                googleScript = "INDEXBOM:BSE-MIDCAP";
                break;
            case "BSE-SMLCAP.BO":
                googleScript = "INDEXBOM:BSE-SMLCAP";
                break;
            case "BSE-IT.BO":
                googleScript = "INDEXBOM:BSE-IT";
                break;
            case "BSE-OILGAS.BO":
                googleScript = "INDEXBOM:BSE-OILGAS";
                break;
            case "BSE-METAL.BO":
                googleScript = "INDEXBOM:BSE-METAL";
                break;
            case "BSE-POWER.BO":
                googleScript = "INDEXBOM:BSE-POWER";
                break;
            case "BSE-AUTO.BO":
                googleScript = "INDEXBOM:BSE-AUTO";
                break;
            case "BSE-BANK.BO":
                googleScript = "INDEXBOM:BSE-BANK";
                break;
            case "INFRASTRUCTURE.BO":
                googleScript = "INDEXBOM:DOLLEX200";// TODO: 18-05-2017 this is wrong
                break;
            case "TELCOM.BO":
                googleScript = "INDEXBOM:DOLLEX200";// TODO: 18-05-2017 this is wrong
                break;
            case "^CNX100":
                googleScript = "NSE:CNX100";
                break;
            case "^CRSLDX":
                googleScript = "NSE:CNX500";
                break;
            case "^NSEBANK":
                googleScript = "NSE:BANKNIFTY";
                break;
            case "^CNXMETAL":
                googleScript = "NSE:METAL";// TODO: 18-05-2017 this is wrong
                break;
            case "^CNXPHARMA":
                googleScript = "NSE:CNXPHARMA";
                break;
            case "^CNXAUTO":
                googleScript = "NSE:AUTO";// TODO: 18-05-2017 this is wrong
                break;
            case "^CNXIT":
                googleScript = "NSE:CNXIT";
                break;
            case "^CNXINFRA":
                googleScript = "NSE:CNXINFRA";
                break;
            case "^nsei":
                googleScript = "NSE:NIFTY";
                break;
            default:
                try {
                    if (unconvertedScriptWithExtension.split("\\.")[1].equalsIgnoreCase("ns"))
                        googleScript = "NSE:" + unconvertedScriptWithExtension.split("\\.")[0].replace("&", "%26").toUpperCase();
                    else
                        googleScript = "BOM:" + unconvertedScriptWithExtension.split("\\.")[0].replace("&", "%26");
                } catch (IndexOutOfBoundsException iobe) {
                    return null;
                }
                break;
        }
        return googleScript;
    }

    public HashMap<String, String> breakTotalString(String totalString) {
        HashMap<String, String> result = new HashMap<>();
        String stockName = totalString.split(Pattern.quote("["))[0].trim();
        String companyMeta = totalString.split(Pattern.quote("["))[1].trim();
        String exchange = companyMeta.split(Pattern.quote(":"))[0].trim();
        String companyScript = companyMeta.split(Pattern.quote(":"))[1].replaceAll("\\]", "").trim().toLowerCase();
        result.put("name", stockName);
        result.put("script", companyScript);
        result.put("exchange", exchange);
        return result;
    }

    public String wholeStringToScriptWithExtension(String wholeString) {
        HashMap<String, String> hashMap = breakTotalString(wholeString);
        if (hashMap.get("exchange").equals("BSE"))
            return hashMap.get("script").toLowerCase() + ".bo";
        else
            return hashMap.get("script").toLowerCase() + ".ns";
    }

    public String scriptToCompany(String script) {
        script=script.replace("%26", "&");
        if (script.contains("."))
            script = script.split(Pattern.quote("."))[0];
        String next[];
        CSVReader reader = null;
        try {
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("nsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[1].toLowerCase().equals(script.toLowerCase()))
                    return next[0];
            }
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("bsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[1].toLowerCase().equals(script.toLowerCase()))
                    return next[0];
            }
        } catch (IOException e) {
        }
        return null;
    }

    public String companyToScriptNseFirst(String company) {
        String next[];
        CSVReader reader = null;
        try {
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("nsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[0].toLowerCase().equals(company.toLowerCase()))
                    return next[1];
            }
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("bsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[0].toLowerCase().equals(company.toLowerCase()))
                    return next[1];
            }
        } catch (IOException e) {
        }
        return null;
    }

    public String companyToScriptBseFirst(String company) {
        String next[];
        CSVReader reader = null;
        try {
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("bsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[0].toLowerCase().equals(company.toLowerCase()))
                    return next[1];
            }
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("nsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[0].toLowerCase().equals(company.toLowerCase()))
                    return next[1];
            }
        } catch (IOException e) {
        }
        return null;
    }

    public String unconvertedScriptToWholeString(String script) {
        if (script.contains("."))
            script = script.split("\\.")[0];
        String next[];
        CSVReader reader = null;
        try {
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("nsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[1].toLowerCase().equals(script.toLowerCase()))
                    return next[0] + " [NSE: " + next[1] + "]";
            }
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("bsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                if (next[1].toLowerCase().equals(script.toLowerCase()))
                    return next[0] + " [BSE: " + next[1] + "]";
            }
        } catch (IOException e) {
        }
        return null;
    }

    public String zerodhaSymbolToScript(String tradingSymbol, String exchange) {
        String script;
        String company = scriptToCompany(tradingSymbol);
        if (company == null)
            return null;
        if (exchange.equals("NSE"))
            script = companyToScriptNseFirst(company) + ".ns";
        else if (exchange.equals("BSE"))
            script = companyToScriptBseFirst(company) + ".bo";
        else
            script = null;
        return script;
    }
}
