package com.utilitechnology.mystockplannerandroid;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.net.ssl.HandshakeCompletedListener;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by Bibaswann on 31-01-2016.
 */
public class SearchProvider extends ContentProvider {

    public static final String AUTHORITY = "com.utilitechnology.mystockplannerandroid.SearchProvider";

    public static final Uri SEARCH_URI = Uri.parse("content://" + AUTHORITY + "/search");

    public static final Uri DETAILS_URI = Uri.parse("content://" + AUTHORITY + "/details");

    private static final int SEARCH = 1;
    private static final int SUGGESTIONS = 2;
    private static final int DETAILS = 3;

    private static final UriMatcher mUriMatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {

        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // URI for "Go" button
        uriMatcher.addURI(AUTHORITY, "search", SEARCH);

        // URI for suggestions in Search Dialog
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SUGGESTIONS);

        // URI for Details
        uriMatcher.addURI(AUTHORITY, "details", DETAILS);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //Log.e("important",selection+" and "+selectionArgs[0]);
        Cursor c = null;
        List<String> list = getCompanies(selectionArgs[0]);
        MatrixCursor mCursor = null;

        switch (mUriMatcher.match(uri)) {
            case SEARCH:
                mCursor = new MatrixCursor(new String[]{"_id", SearchManager.SUGGEST_COLUMN_TEXT_1,SearchManager.SUGGEST_COLUMN_TEXT_2,SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});
//Todo always find the nse script where available
                for (int j = 0; j < list.size(); j++) {
                    String totalString=list.get(j);
                    String companyName=totalString.split(Pattern.quote("["))[0].trim();
                    String companyMeta="["+totalString.split(Pattern.quote("["))[1].trim();
                    mCursor.addRow(new String[]{Integer.toString(j), companyName, companyMeta,totalString});
                }
                c = mCursor;
                break;
            case SUGGESTIONS:
                mCursor = new MatrixCursor(new String[]{"_id", SearchManager.SUGGEST_COLUMN_TEXT_1,SearchManager.SUGGEST_COLUMN_TEXT_2,SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});

                    for (int i = 0; i < list.size(); i++) {
                        String totalString=list.get(i);
                        String companyName=totalString.split(Pattern.quote("["))[0].trim();
                        String companyMeta="["+totalString.split(Pattern.quote("["))[1].trim();
                        mCursor.addRow(new String[]{Integer.toString(i), companyName, companyMeta,totalString});
                    }
                c = mCursor;
                break;

            case DETAILS:
                mCursor = new MatrixCursor(new String[]{"_id", SearchManager.SUGGEST_COLUMN_TEXT_1,SearchManager.SUGGEST_COLUMN_TEXT_2,SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});
                for (int j = 0; j < list.size(); j++) {
                    String totalString=list.get(j);
                    String companyName=totalString.split(Pattern.quote("["))[0].trim();
                    String companyMeta="["+totalString.split(Pattern.quote("["))[1].trim();
                    mCursor.addRow(new String[]{Integer.toString(j), companyName, companyMeta,totalString});
                }
                c = mCursor;
                break;
        }
        return c;
    }

    private List<String> getCompanies(String suggestion) {
        String next[];
        List<String> company = new ArrayList<>();
        if (suggestion != null) {
            try {
                CSVReader reader = new CSVReader(new InputStreamReader(getContext().getAssets().open("nsecompanies.csv")));
                while ((next = reader.readNext()) != null) {
                    if (next[0].toLowerCase().contains(suggestion.toLowerCase()) || next[1].toLowerCase().contains(suggestion.toLowerCase()))
                        company.add(next[0] + " [NSE: " + next[1] + "]");
                }
                reader = new CSVReader(new InputStreamReader(getContext().getAssets().open("bsecompanies.csv")));
                while ((next = reader.readNext()) != null) {
                    if (next[0].toLowerCase().contains(suggestion.toLowerCase()) || next[1].toLowerCase().contains(suggestion.toLowerCase()))
                        company.add(next[0] + " [BSE: " + next[1] + "]");
                }
            } catch (IOException | NullPointerException e) {
                //Do nothing
            }
        }
        return company;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
