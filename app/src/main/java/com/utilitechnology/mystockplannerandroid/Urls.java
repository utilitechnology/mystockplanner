package com.utilitechnology.mystockplannerandroid;

/**
 * Created by Administrator on 24-12-2017.
 */

public class Urls {
    public static final String GOOGLE_JSON_PREFIX = "http://finance.google.com/finance/info?infotype=infoquoteall&q=";
    public static final String GOOGLE_WEB_PREFIX = "https://finance.google.com/finance?q=";
    public static final String MSN_INDEX_PREFIX = "https://www.msn.com/en-in/money/indexdetails/";
    public static final String BSE_INTRADAY = "https://chartapi.finance.yahoo.com/instrument/1.0/^bsesn/chartdata;type=quote;range=1d/csv";
    public static final String NSE_INTRADAY = "https://chartapi.finance.yahoo.com/instrument/1.0/^nsei/chartdata;type=quote;range=1d/csv";
    public static final String WORLD_URL="http://www.utilitechnology.com/api/mystockplanner/cached_data/world.xml";
    public static final String CURRENCY_URL="http://www.utilitechnology.com/api/mystockplanner/cached_data/currency.xml";
    public static final String INDEX_URL="http://www.utilitechnology.com/api/mystockplanner/cached_data/subindex.xml";
    public static final String MOVER_BSE_RESOURCE = "http://www.utilitechnology.com/api/mystockplanner/track_equity.php?scripts=BOM:532921,BOM:500820,BOM:532215,BOM:532977,BOM:532454,BOM:500103,BOM:500087,BOM:533278,BOM:500124,BOM:532155,BOM:500010,BOM:500180,BOM:500182,BOM:500696,BOM:532174,BOM:500209,BOM:500875,BOM:500510,BOM:500257,BOM:500520,BOM:532500,BOM:532555,BOM:500312,BOM:500325,BOM:500112,BOM:524715,BOM:500570,BOM:500470,BOM:532540,BOM:507685";
    public static final String MOVER_NSE_RESOURCE = "http://www.utilitechnology.com/api/mystockplanner/track_equity.php?scripts=NSE:ACC,NSE:ADANIPORTS,NSE:AMBUJACEM,NSE:ASIANPAINT,NSE:AUROPHARMA,NSE:AXISBANK,NSE:BAJAJ-AUTO,NSE:BANKBARODA,NSE:BHARTIARTL,NSE:INFRATEL,NSE:BOSCHLTD,NSE:BPCL,NSE:CIPLA,NSE:COALINDIA,NSE:DRREDDY,NSE:EICHERMOT,NSE:GAIL,NSE:HCLTECH,NSE:HDFC,NSE:HDFCBANK,NSE:HEROMOTOCO,NSE:HINDALCO,NSE:HINDUNILVR,NSE:ICICIBANK,NSE:IBULHSGFIN,NSE:IOC,NSE:INDUSINDBK,NSE:ITC,NSE:INFY,NSE:KOTAKBANK,NSE:LT,NSE:LUPIN,NSE:M%26M,NSE:MARUTI,NSE:NTPC,NSE:ONGC,NSE:POWERGRID,NSE:RELIANCE,NSE:SBIN,NSE:SUNPHARMA,NSE:TCS,NSE:TATAMOTORS,NSE:TATAPOWER,NSE:TATASTEEL,NSE:TECHM,NSE:ULTRACEMCO,NSE:VEDL,NSE:WIPRO,NSE:YESBANK,NSE:ZEEL";

}
