package com.utilitechnology.mystockplannerandroid.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.utilitechnology.mystockplannerandroid.BuildConfig;
import com.utilitechnology.mystockplannerandroid.handler.HandleDatabase;
import com.utilitechnology.mystockplannerandroid.object.PortfolioObject;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.object.WatchObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class AddEditActivity extends AppCompatActivity {

    android.support.v7.app.ActionBar actb;
    //mode can be Edit or Insert
    String mode, wholeString, where;
    SearchView stockName;
    AutoCompleteTextView stockToInsert;
    EditText bought, starget, quantity, btarget;
    String editBought, editSellTarget, editBuyTarget, editQuantity;
    LinearLayout botL, buyTargetL, sellTargetL, quantityL;
    FloatingActionButton done, del;
    HandleDatabase dbop;
    ScriptHelper sch;
    TradingAccountSettingsAPI tras;
    String userName = "na";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_insert_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbop = new HandleDatabase(this);
        sch = new ScriptHelper(this);
        tras = new TradingAccountSettingsAPI(this);

        userName = tras.readSetting("name");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mode = extras.getString("mode");
            wholeString = extras.getString("wholestring");
            where = extras.getString("where");
            editBought = extras.getString("bought");
            editSellTarget = extras.getString("selltarget");
            editBuyTarget = extras.getString("buytarget");
            editQuantity = extras.getString("quantity");
        }

        actb = getSupportActionBar();
        if (actb != null) {
            actb.setDisplayHomeAsUpEnabled(true);
            actb.setTitle(mode + where);
        }

        stockToInsert = (AutoCompleteTextView) findViewById(R.id.stockAutoComplete);
        bought = (EditText) findViewById(R.id.boughtAt);
        starget = (EditText) findViewById(R.id.target);
        quantity = (EditText) findViewById(R.id.quantity);
        btarget = (EditText) findViewById(R.id.buyTarget);

        botL = (LinearLayout) findViewById(R.id.botLayout);
        sellTargetL = (LinearLayout) findViewById(R.id.targetLayout);
        buyTargetL = (LinearLayout) findViewById(R.id.buyTargetLayout);
        quantityL = (LinearLayout) findViewById(R.id.quantityLayout);

        if (!wholeString.equals(""))
            stockToInsert.setText(wholeString);
        if (!editBought.equals("-1.0"))
            bought.setText(editBought);
        if (!editSellTarget.equals("-1.0"))
            starget.setText(editSellTarget);
        if (!editBuyTarget.equals("-1.0"))
            btarget.setText(editBuyTarget);
        if (!editQuantity.equals("-1"))
            quantity.setText(editQuantity);

        assert where != null;
        if (where.contains("Watchlist")) {
            botL.setVisibility(View.GONE);
            sellTargetL.setVisibility(View.GONE);
            quantityL.setVisibility(View.GONE);
            buyTargetL.setVisibility(View.VISIBLE);
        }

        stockName = (SearchView) findViewById(R.id.addInsertSearchView);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        stockName.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        handleIntent(getIntent());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getAllCompanies());
        stockToInsert.setAdapter(adapter);

        done = (FloatingActionButton) findViewById(R.id.fabDone);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addInsertSubmit(view);
            }
        });
        del = (FloatingActionButton) findViewById(R.id.fabDel);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInsertDelete(v);
            }
        });

        if (mode.equals("Edit")) {
            stockToInsert.setFocusable(false);
            del.setVisibility(View.VISIBLE);
        }
        //Ad section
        AdView mAdView = (AdView) findViewById(R.id.adView);
        if (userName.equals("na")) {
            AdRequest adRequest;
            if (BuildConfig.DEBUG)
                adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            else
                adRequest = new AdRequest.Builder().build();
            assert mAdView != null;
            mAdView.loadAd(adRequest);
        } else {
            if (mAdView != null)
                mAdView.setVisibility(View.GONE);
        }
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            String query = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
            Toast.makeText(this, "found " + query, Toast.LENGTH_SHORT).show();

        }
    }

    private void doSearch(String query) {
        //Todo do an actual search
        Snackbar.make(findViewById(android.R.id.content), "Sorry! " + query + " not found", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        Toast.makeText(this, "Sorry! " + query + " not found", Toast.LENGTH_LONG).show();
    }

    public void searchClick(View view) {
        stockName.setIconified(false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private String[] getAllCompanies() {
        String next[];
        List<String> company = new ArrayList<>();
        try {
            CSVReader reader = new CSVReader(new InputStreamReader(getAssets().open("nsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                company.add(next[0] + " [NSE: " + next[1] + "]");
            }
            reader = new CSVReader(new InputStreamReader(getAssets().open("bsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                company.add(next[0] + " [BSE: " + next[1] + "]");
            }
        } catch (IOException | NullPointerException e) {
            //Do nothing
        }
        return company.toArray(new String[company.size()]);
    }

    public void addInsertSubmit(View view) {
        String stockNameToInsert, scriptNameToInsert, exchangeNameToInsert, scriptNameToEdit;
        float iBoughtToInsert, sellTargetToInsert, buyTargetToInsert;
        float iBoughtToEdit, sellTargetToEdit, buyTargetToEdit;
        int quantityToInsert, quantityToEdit;
        if (stockToInsert == null || stockToInsert.getText().toString().isEmpty()) {
            Snackbar.make(view, "Please enter a stock name", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (where.contains("Portfolio")) {
            if (mode.equals("Insert")) {
                try {
                    HashMap<String, String> hashMap = sch.breakTotalString(stockToInsert.getText().toString());
                    stockNameToInsert = hashMap.get("name");
                    scriptNameToInsert = hashMap.get("script");
                    exchangeNameToInsert = hashMap.get("exchange");
                    iBoughtToInsert = Float.parseFloat(bought.getText().toString());
                    sellTargetToInsert = Float.parseFloat(starget.getText().toString());
                    quantityToInsert = Integer.parseInt(quantity.getText().toString());

                    //Check if exists
                    if (dbop.doesExistInPortfolio(scriptNameToInsert)) {
                        Snackbar.make(view, "Item exists in portfolio, Open portfolio and use Edit/Delete button to modify", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        }).show();
                    } else {
                        dbop.insertIntoPortfolio(new PortfolioObject(stockNameToInsert, scriptNameToInsert,
                                exchangeNameToInsert, iBoughtToInsert, quantityToInsert, sellTargetToInsert, true));
                        finish();
                    }

                } catch (NumberFormatException nfe) {
                    //Log.e("important","insert portfolio"+nfe.getMessage());
                    Snackbar.make(view, "If you want to leave a field, please write 0", Snackbar.LENGTH_LONG).show();
                } catch (ArrayIndexOutOfBoundsException nfe) {
                    Snackbar.make(view, "Please choose stock name from suggestions", Snackbar.LENGTH_LONG).show();
                }
            } else {
                try {
                    HashMap<String, String> hashMap = sch.breakTotalString(stockToInsert.getText().toString());
                    scriptNameToEdit = hashMap.get("script");
                    iBoughtToEdit = Float.parseFloat(bought.getText().toString());
                    sellTargetToEdit = Float.parseFloat(starget.getText().toString());
                    quantityToEdit = Integer.parseInt(quantity.getText().toString());
                    dbop.editPortfolio(scriptNameToEdit, iBoughtToEdit, quantityToEdit, sellTargetToEdit);
                    finish();
                } catch (NumberFormatException nfe) {
                    //Log.e("important","edit portfolio"+nfe.getMessage());
                    Snackbar.make(view, "If you want to leave a field, please write 0", Snackbar.LENGTH_LONG).show();
                }
            }
        } else {
            if (mode.equals("Insert")) {
                try {
                    HashMap<String, String> hashMap = sch.breakTotalString(stockToInsert.getText().toString());
                    stockNameToInsert = hashMap.get("name");
                    scriptNameToInsert = hashMap.get("script");
                    exchangeNameToInsert = hashMap.get("exchange");
                    buyTargetToInsert = Float.valueOf(btarget.getText().toString());
                    if (dbop.doesExistInWatchList(scriptNameToInsert)) {
                        Snackbar.make(view, "Item exists in watchlist, Open watchlist and use Edit/Delete button to modify", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        }).show();
                    } else {
                        dbop.insertIntoWatchlist(new WatchObject(exchangeNameToInsert,
                                stockNameToInsert, scriptNameToInsert, buyTargetToInsert));
                        finish();
                    }
                } catch (NumberFormatException nfe) {
                    //Log.e("important","insert watchlist"+nfe.getMessage());
                    Snackbar.make(view, "If you want to leave a field, please write 0", Snackbar.LENGTH_LONG).show();
                } catch (ArrayIndexOutOfBoundsException nfe) {
                    Snackbar.make(view, "Please choose stock name from suggestions", Snackbar.LENGTH_LONG).show();
                }
            } else {
                try {
                    HashMap<String, String> hashMap = sch.breakTotalString(stockToInsert.getText().toString());
                    scriptNameToEdit = hashMap.get("script");
                    buyTargetToEdit = Float.parseFloat(btarget.getText().toString());
                    dbop.editWatchList(scriptNameToEdit, buyTargetToEdit);
                    finish();
                } catch (NumberFormatException nfe) {
                    //Log.e("important","edit watchlist"+nfe.getMessage());
                    Snackbar.make(view, "If you want to leave a field, please write 0", Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    private void addInsertDelete(View v) {
        final String stockNameToDelete, scriptNameToDelete;
        HashMap<String, String> hashMap = sch.breakTotalString(stockToInsert.getText().toString());
        stockNameToDelete = hashMap.get("name");
        scriptNameToDelete = hashMap.get("script");

        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        dlgAlert.setMessage(stockNameToDelete + " will be deleted from" + where.toLowerCase());
        dlgAlert.setTitle("Are you sure?");
        dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (where.equals(" Portfolio"))
                    dbop.deleteFromPortfolio(scriptNameToDelete);
                else
                    dbop.deleteFromWatchList(scriptNameToDelete);
                finish();
            }
        });
        dlgAlert.setNegativeButton("Cancel", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }
}
