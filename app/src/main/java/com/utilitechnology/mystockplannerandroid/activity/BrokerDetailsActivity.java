package com.utilitechnology.mystockplannerandroid.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchBrokerDetails;

public class BrokerDetailsActivity extends AppCompatActivity {

    android.support.v7.app.ActionBar actb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broker_details);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        actb = getSupportActionBar();
        if (actb != null)
            actb.setDisplayHomeAsUpEnabled(true);

        final String bserial = getIntent().getStringExtra("serial");
        final String bname = getIntent().getStringExtra("name");
        final String burl = getIntent().getStringExtra("opnurl");
        final String bdp = getIntent().getStringExtra("dp");

        ImageView logo = (ImageView) findViewById(R.id.brkrLogo);
        if (logo != null)
            logo.setBackgroundResource(getResources().getIdentifier(bdp, "drawable", getPackageName()));

        TextView name = (TextView) findViewById(R.id.brkrName);
        TextView charge = (TextView) findViewById(R.id.opnCharge);
        TextView brokerage = (TextView) findViewById(R.id.brkrChrg);
        TextView margin = (TextView) findViewById(R.id.brkrMrgn);
        TextView website = (TextView) findViewById(R.id.brkrWeb);
        TextView phone = (TextView) findViewById(R.id.brkrPhone);
        TextView maintain = (TextView) findViewById(R.id.maintainCost);

        Button openAc= (Button) findViewById(R.id.dOpenAcc);
        Button login= (Button) findViewById(R.id.dLogin);
        Button call= (Button) findViewById(R.id.dCall);

        if (!bname.equals("Zerodha")) {
            login.setVisibility(View.GONE);
        }

        openAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(burl));
                startActivity(i);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BrokerDetailsActivity.this, ZerodhaLoginActivity.class);
                startActivity(i);
            }
        });

        String fetchUrl = getString(R.string.broker_details_url) + bserial;

        new FetchBrokerDetails(this,name, charge, brokerage, margin, website,phone,maintain,call, fetchUrl).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
