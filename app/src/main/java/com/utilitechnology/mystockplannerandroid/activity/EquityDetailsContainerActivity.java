package com.utilitechnology.mystockplannerandroid.activity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.utilitechnology.mystockplannerandroid.BuildConfig;
import com.utilitechnology.mystockplannerandroid.fragment.EquityOverview;
import com.utilitechnology.mystockplannerandroid.fragment.EquityStat;
import com.utilitechnology.mystockplannerandroid.fragment.EquitySummary;
import com.utilitechnology.mystockplannerandroid.handler.HandleDatabase;
import com.utilitechnology.mystockplannerandroid.handler.HandleTrade;
import com.utilitechnology.mystockplannerandroid.object.PortfolioObject;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.fragment.SurveyFragment;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.object.WatchObject;
import com.utilitechnology.mystockplannerandroid.utils.FragmentPagerItemAdapter;
import com.utilitechnology.mystockplannerandroid.utils.FragmentPagerItems;

import java.util.HashMap;
import java.util.Random;

public class EquityDetailsContainerActivity extends AppCompatActivity implements EquityOverview.OnFragmentInteractionListener, EquityStat.OnFragmentInteractionListener, EquitySummary.OnFragmentInteractionListener,SurveyFragment.OnFragmentInteractionListener {

    //FragmentTabHost mTabHost;
    SettingsAPI set;
    ScriptHelper sch;
    HandleDatabase hdb;
    String uncnvrtdscrtwe, uncnvrtdscrwoe;
    boolean inPort, inWatch;
    PortfolioObject currentItemInPort;
    WatchObject currentItemInWatch;
    TradingAccountSettingsAPI tras;
    String userName = "na";
    AdView mAdView;
    Button buy,sell,addTo;
    DialogFragment srvDlg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equity_details_container);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hdb = new HandleDatabase(this);
        sch = new ScriptHelper(this);
        tras = new TradingAccountSettingsAPI(this);

        userName = tras.readSetting("name");

        set = new SettingsAPI(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uncnvrtdscrtwe = extras.getString("script");
            uncnvrtdscrwoe = uncnvrtdscrtwe.split("\\.")[0];
            set.addUpdateSettings("currentStock", uncnvrtdscrtwe);
        }

//        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
//        if (mTabHost != null) {
//            mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
//            mTabHost.addTab(mTabHost.newTabSpec("overview").setIndicator("Overview"), EquityOverview.class, null);
//            mTabHost.addTab(mTabHost.newTabSpec("stats").setIndicator("Statistics"), EquityStat.class, null);
//            mTabHost.addTab(mTabHost.newTabSpec("profile").setIndicator("Summary"), EquitySummary.class, null);
//        }

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Overview", EquityOverview.class)
                .add("Statistics", EquityStat.class)
                .add("Summary", EquitySummary.class)
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.eviewpager);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.eviewpagertab);
        viewPagerTab.setViewPager(viewPager);

        mAdView = (AdView) findViewById(R.id.adView);

        if (userName.equals("na")) {
            AdRequest adRequest;
            if (BuildConfig.DEBUG)
                adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            else
                adRequest = new AdRequest.Builder().build();
            assert mAdView != null;
            mAdView.loadAd(adRequest);
        } else {
            if (mAdView != null)
                mAdView.setVisibility(View.GONE);
        }
        switch (set.readSetting("survey"))
        {
            case "na":
                set.addUpdateSettings("survey","prepared");
                break;
            case "prepared":
                set.addUpdateSettings("survey","initiated");
                break;
            case "initiated":
                srvDlg=new SurveyFragment();
                srvDlg.show(getSupportFragmentManager(),"Survey");
                set.addUpdateSettings("survey","showing");
                break;
            case "showing":
                Random r0=new Random();
                if(r0.nextInt(10)==3) {
                    srvDlg = new SurveyFragment();
                    srvDlg.show(getSupportFragmentManager(), "Survey");
                    set.addUpdateSettings("survey", "showing");
                }
                break;
            case "willrate":
                Random r1=new Random();
                if(r1.nextInt(5)==3) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("It seems like you like our app, will you please show your support by rating us five star on play store?");
                    builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Uri uri = Uri.parse("market://details?id=" + getPackageName());
                            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                            try {
                                startActivity(goToMarket);
                            } catch (ActivityNotFoundException e) {
                                Snackbar.make(findViewById(android.R.id.content), "This app is not downloaded from google play", Snackbar.LENGTH_LONG).show();
                            }
                            set.addUpdateSettings("survey", "complete");
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            set.addUpdateSettings("survey", "willrate");
                            dialog.dismiss();
                        }
                    });
                    builder.create().show();
                }
                default:
                    break;
        }

    }

    @Override
    protected void onResume() {
        //Log.e("important",uncnvrtdscrwoe);
        inPort = hdb.doesExistInPortfolio(uncnvrtdscrwoe);
        inWatch = hdb.doesExistInWatchList(uncnvrtdscrwoe);


        buy = (Button) findViewById(R.id.equityBuy);
        sell = (Button) findViewById(R.id.equitySell);
        addTo = (Button) findViewById(R.id.equityAddTo);

        if (inPort && inWatch) {
            //fabAddto.setVisibility(View.GONE);
            addTo.setEnabled(false);
        } else {
            //fabAddto.setVisibility(View.VISIBLE);
            addTo.setEnabled(true);
        }

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareBS("buy");
            }
        });
        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareBS("sell");
            }
        });

        addTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (!inPort && !inWatch) {
                    final String[] options = {"Portfolio","Watchlist"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(EquityDetailsContainerActivity.this);
                    builder.setTitle(R.string.addTo);
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (options[which].equals("Portfolio"))
                                addToP();
                            else
                                addToW();
                        }
                    });
                    builder.show();

                } else if (!inPort)
                    addToP();
                else if (!inWatch)
                   addToW();
            }
        });

        userName = tras.readSetting("name");
        if (!userName.equals("na")) {
            if (mAdView != null)
                mAdView.setVisibility(View.GONE);
        }

        super.onResume();
    }

    private void addToP() {
        Intent details = new Intent(EquityDetailsContainerActivity.this, AddEditActivity.class);
        details.putExtra("mode", "Insert");
        details.putExtra("wholestring", sch.unconvertedScriptToWholeString(uncnvrtdscrwoe));
        details.putExtra("where", " into Portfolio");
        details.putExtra("bought", "-1.0");
        details.putExtra("selltarget", "-1.0");
        details.putExtra("buytarget", "-1.0");
        details.putExtra("quantity", "-1");
        EquityDetailsContainerActivity.this.startActivity(details);
    }

    private void addToW() {
        Intent details = new Intent(EquityDetailsContainerActivity.this, AddEditActivity.class);
        details.putExtra("mode", "Insert");
        details.putExtra("wholestring", sch.unconvertedScriptToWholeString(uncnvrtdscrwoe));
        details.putExtra("where", " into Watchlist");
        details.putExtra("bought", "-1.0");
        details.putExtra("selltarget", "-1.0");
        details.putExtra("buytarget", "-1.0");
        details.putExtra("quantity", "-1");
        EquityDetailsContainerActivity.this.startActivity(details);
    }

    private void prepareBS(String type)
    {
        if (!tras.readSetting("access_token").equals("na")) {
            HashMap<String, String> modata = new HashMap<String, String>();
            modata.put("script", uncnvrtdscrtwe);

            final Dialog dialog = new Dialog(EquityDetailsContainerActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.fragment_buy_sell);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.show();
            new HandleTrade(dialog, dialog.getWindow().getDecorView(), type, null, modata);
        } else {
            //Todo: This part is for login, and users will be redirected to the default trading account opening page if
            //Todo: he hs no account, if the default https://zerodha.com/open-account?ref=ZMPRAT is not right url,
            //Todo: then I need to show 2 options for opening account and logging in, maybe with a dialog
            Intent i = new Intent(EquityDetailsContainerActivity.this, ZerodhaLoginActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
