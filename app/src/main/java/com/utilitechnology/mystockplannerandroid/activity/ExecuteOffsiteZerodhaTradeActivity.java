package com.utilitechnology.mystockplannerandroid.activity;

import android.content.DialogInterface;
import android.net.http.SslError;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.utilitechnology.mystockplannerandroid.R;

import cz.msebera.android.httpclient.util.EncodingUtils;

public class ExecuteOffsiteZerodhaTradeActivity extends AppCompatActivity {

    WebView zerodhaOrderPage;
    private static String API_KEY;
    private static final String REQUEST_URL = "https://kite.trade/connect/basket";
    private String orderJson;
    String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_execute_offsite_zerodha_trade);

        zerodhaOrderPage = (WebView) findViewById(R.id.zOrderWeb);
        API_KEY = getString(R.string.ZERODHA_API_KEY);
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            orderJson = extras.getString("data");

        zerodhaOrderPage.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Snackbar.make(ExecuteOffsiteZerodhaTradeActivity.this.getWindow().getDecorView(), "Trouble connecting to Zerodha server", Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ExecuteOffsiteZerodhaTradeActivity.this);
                builder.setMessage(R.string.order_redirection_warning);
                builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        ExecuteOffsiteZerodhaTradeActivity.this.finish();
                    }
                });
                builder.setCancelable(false);
                builder.create().show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //Do not use unidom here coz unidom is not https
                if (url.contains("https://www.utilitechnology.com/trading/zerodha/after_login_redirect.php")) {
                    String queryStr = url.split("\\?")[1];
                    for (String query : queryStr.split("&")) {
                        if (query.contains("status")) {
                            status = query.split("=")[1];
                            //Status can be completed or cancelled, maybe will require that in future
                            ExecuteOffsiteZerodhaTradeActivity.this.finish();
                        }
                    }
                }
                //mView.loadUrl(url);
                return false;//return true means do not load the url
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(!url.contains("sess_id") && !url.contains("api_key"))
                {
                    //This is error with order failure
                    status="failed";
                    ExecuteOffsiteZerodhaTradeActivity.this.finish();
                }
            }
        });

        zerodhaOrderPage.getSettings().setLoadWithOverviewMode(true);
        zerodhaOrderPage.getSettings().setUseWideViewPort(true);
        zerodhaOrderPage.getSettings().setJavaScriptEnabled(true);

        String postData = "api_key=" + API_KEY + "&data=" + orderJson;
        //Log.e("important", "will post " + postData);
        zerodhaOrderPage.postUrl(REQUEST_URL, EncodingUtils.getBytes(postData, "base64"));
//        zerodhaOrderPage.postUrl(REQUEST_URL, postData.getBytes());
//        try {zerodhaOrderPage.postUrl(REQUEST_URL, URLEncoder.encode(postData,"UTF-8").getBytes());
//        } catch (UnsupportedEncodingException ignored) {}
    }

    @Override
    protected void onDestroy() {
        if (zerodhaOrderPage != null)
            zerodhaOrderPage.destroy();
        super.onDestroy();
    }
}
