package com.utilitechnology.mystockplannerandroid.activity;

import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.utilitechnology.mystockplannerandroid.BuildConfig;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.fragment.BuySellFragment;
import com.utilitechnology.mystockplannerandroid.fragment.CommodityFragment;
import com.utilitechnology.mystockplannerandroid.fragment.CurrencyFragment;
import com.utilitechnology.mystockplannerandroid.fragment.HeatmapFragment;
import com.utilitechnology.mystockplannerandroid.fragment.IndexContainerFragment;
import com.utilitechnology.mystockplannerandroid.fragment.IndexFragment;
import com.utilitechnology.mystockplannerandroid.fragment.MoverFragment;
import com.utilitechnology.mystockplannerandroid.fragment.NewsFragment;
import com.utilitechnology.mystockplannerandroid.fragment.OrderHistoryFragment;
import com.utilitechnology.mystockplannerandroid.fragment.PortfolioContainerFragment;
import com.utilitechnology.mystockplannerandroid.fragment.PortfolioFragment;
import com.utilitechnology.mystockplannerandroid.fragment.PortfolioStatFragment;
import com.utilitechnology.mystockplannerandroid.fragment.SubindexFragment;
import com.utilitechnology.mystockplannerandroid.fragment.TradeContainerFragment;
import com.utilitechnology.mystockplannerandroid.fragment.TradingAccountFragment;
import com.utilitechnology.mystockplannerandroid.fragment.TradingConnectFragment;
import com.utilitechnology.mystockplannerandroid.fragment.TutorialFragment;
import com.utilitechnology.mystockplannerandroid.fragment.WatchFragment;
import com.utilitechnology.mystockplannerandroid.fragment.WorldFragment;

//import cn.hiroz.uninstallfeedback.FeedbackUtils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IndexContainerFragment.OnFragmentInteractionListener, IndexFragment.OnFragmentInteractionListener, MoverFragment.OnFragmentInteractionListener, HeatmapFragment.OnFragmentInteractionListener, PortfolioContainerFragment.OnFragmentInteractionListener, PortfolioFragment.OnFragmentInteractionListener, PortfolioStatFragment.OnFragmentInteractionListener, WatchFragment.OnFragmentInteractionListener, WorldFragment.OnFragmentInteractionListener, CurrencyFragment.OnFragmentInteractionListener, TradingConnectFragment.OnFragmentInteractionListener, TradeContainerFragment.OnFragmentInteractionListener, BuySellFragment.OnFragmentInteractionListener, OrderHistoryFragment.OnFragmentInteractionListener, TradingAccountFragment.OnFragmentInteractionListener, TutorialFragment.OnFragmentInteractionListener, SubindexFragment.OnFragmentInteractionListener, NewsFragment.OnFragmentInteractionListener, CommodityFragment.OnFragmentInteractionListener {

    Button raff;
    TextView welcomeMsg;
    DrawerLayout drawer;
    String userName = "na";
    AdView mAdView;
    TradingAccountSettingsAPI tras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();
//        drawer.openDrawer(GravityCompat.START);
//        drawer.closeDrawer(GravityCompat.START);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        IndexContainerFragment icf = new IndexContainerFragment();
        //icf.setRetainInstance(true);
        fragmentTransaction.add(R.id.main_container, icf, "Index jobs");
        fragmentTransaction.commit();

        handleIntent(getIntent());

        tras = new TradingAccountSettingsAPI(this);
        userName = tras.readSetting("name");

        //Ad section
        mAdView = (AdView) findViewById(R.id.adView);
    }

    @Override
    protected void onResume() {
        userName = tras.readSetting("name");
        if (!userName.equals("na"))
            hideAd();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            String query = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);

//            Intent detailsIntent = new Intent(getApplicationContext(), StockDetailsActivity.class);
//            detailsIntent.putExtra("script", new ScriptHelper(this).wholeStringToScriptWithExtension(query));
//            startActivity(detailsIntent);

            Intent detailsIntent = new Intent(getApplicationContext(), EquityDetailsContainerActivity.class);
            detailsIntent.putExtra("script", new ScriptHelper(this).wholeStringToScriptWithExtension(query));
            startActivity(detailsIntent);

            finish();
        }
    }

    private void doSearch(String query) {
        //Todo do an actual search
        try {
            Snackbar.make(findViewById(android.R.id.content), "Sorry! " + query + " not found", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        } catch (NullPointerException npe) {
            Toast.makeText(this, "Sorry! " + query + " not found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    //Unnecessary
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        raff = (Button) findViewById(R.id.removeAdBtn);
        welcomeMsg = (TextView) findViewById(R.id.welcomeTxt);

        if (userName.equals("na"))
            showAd();
        else
            hideAd();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation mView item clicks here.
        int id = item.getItemId();
//Use commitAllowingStateLoss to avoid exception
        if (id == R.id.nav_indices) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new IndexContainerFragment(), "Index jobs").commitAllowingStateLoss();
        } else if (id == R.id.nav_news) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new NewsFragment(), "Watch News").commitAllowingStateLoss();
        } else if (id == R.id.nav_trade) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new TradingConnectFragment(), "Connect to Trading Account").commitAllowingStateLoss();
        } else if (id == R.id.nav_portfolio) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new PortfolioContainerFragment(), "Manage Portfolio").commitAllowingStateLoss();
        } else if (id == R.id.nav_rate) {
            Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                Snackbar.make(findViewById(android.R.id.content), "This app is not downloaded from google play", Snackbar.LENGTH_LONG).show();
            }
        } else if (id == R.id.nav_watchlist) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new WatchFragment(), "Manage Watchlist").commitAllowingStateLoss();
        } else if (id == R.id.nav_world) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new WorldFragment(), "World markets").commitAllowingStateLoss();
        } else if (id == R.id.nav_currency) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new CurrencyFragment(), "Currencies").commitAllowingStateLoss();
        } else if (id == R.id.nav_commodity) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new CommodityFragment(), "Commodities").commitAllowingStateLoss();
        } else if (id == R.id.nav_tutorial) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new TutorialFragment(), "Tutorials").commitAllowingStateLoss();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        //super.onSaveInstanceState(outState, outPersistentState);
    }

    public void removeAdsForFree(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.remove_ad_message));
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new TradingConnectFragment(), "Connect to Trading Account").commitAllowingStateLoss();
            }
        });
        builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create().show();
        drawer.closeDrawer(GravityCompat.START);
    }

    public void hideAd() {
        if (mAdView != null)
            mAdView.setVisibility(View.GONE);
        if (welcomeMsg != null) {
            welcomeMsg.setText(userName);
            welcomeMsg.setVisibility(View.VISIBLE);
        }
        if (raff != null)
            raff.setVisibility(View.GONE);
    }

    public void showAd() {
        if (mAdView != null) {
            AdRequest adRequest;
            if (BuildConfig.DEBUG)
                adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            else
                adRequest = new AdRequest.Builder().build();
            assert mAdView != null;
            mAdView.loadAd(adRequest);
            mAdView.setVisibility(View.VISIBLE);
        }
        if (welcomeMsg != null)
            welcomeMsg.setVisibility(View.GONE);
        if (raff != null) {
            raff.setVisibility(View.VISIBLE);
        }
    }
}
