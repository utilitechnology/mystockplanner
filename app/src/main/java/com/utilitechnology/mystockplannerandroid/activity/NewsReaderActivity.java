package com.utilitechnology.mystockplannerandroid.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.utilitechnology.mystockplannerandroid.BuildConfig;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseNews;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NewsReaderActivity extends AppCompatActivity {

    AppBarLayout mainImgBar;
    TextView mainArtcl, headArtcl;
    String url, imgUrl, ser;
    ProgressBar loading;
    Button fullBtn;
    FloatingActionButton fab;
    TradingAccountSettingsAPI tras;
    String userName = "na";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_reader);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mainImgBar = (AppBarLayout) findViewById(R.id.app_bar);
        mainArtcl = (TextView) findViewById(R.id.artclBd);
        headArtcl = (TextView) findViewById(R.id.artclHd);
        loading = (ProgressBar) findViewById(R.id.newsLoadingProg);
        fullBtn = (Button) findViewById(R.id.fullNews);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        url = getIntent().getStringExtra("url");
        imgUrl = getIntent().getStringExtra("imgurl");
        ser = getIntent().getStringExtra("serial");

        new ShowStory().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        tras = new TradingAccountSettingsAPI(this);
        userName = tras.readSetting("name");

        AdView mAdView = (AdView) findViewById(R.id.adView);
        if (userName.equals("na")) {
            AdRequest adRequest;
            if (BuildConfig.DEBUG)
                adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            else
                adRequest = new AdRequest.Builder().build();
            assert mAdView != null;
            mAdView.loadAd(adRequest);
        } else {
            if (mAdView != null)
                mAdView.setVisibility(View.GONE);
        }

    }

    class ShowStory extends AsyncTask<Void, Void, Void> {
        String head, body;
        Bitmap myBitmap;

        @Override
        protected Void doInBackground(Void... voids) {
            setStory();
            if (imgUrl.contains("http"))
                setMainImg();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mainArtcl.setText(body);
            headArtcl.setText(head);
            //Log.e("important", "onPostExecute: url of image "+imgUrl);

            if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN) {
                if (!imgUrl.contains("http"))
                    mainImgBar.setBackground(getDrawable(R.drawable.news_placeholder));
                else
                    mainImgBar.setBackground(new BitmapDrawable(getResources(), myBitmap));
            } else {
                try {
                    if (!imgUrl.contains("http"))
                        mainImgBar.setBackgroundDrawable(getDrawable(R.drawable.news_placeholder));
                    else
                        mainImgBar.setBackgroundDrawable(new BitmapDrawable(getResources(), myBitmap));
                }
                catch (NoSuchMethodError ignored){}
            }

            loading.setVisibility(View.GONE);
            mainArtcl.setVisibility(View.VISIBLE);
            headArtcl.setVisibility(View.VISIBLE);
            fullBtn.setVisibility(View.VISIBLE);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, head + "-" + url);
                    intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download MyStockPlannerAndroid to stay updated about Stock markets of India");
                    startActivity(Intent.createChooser(intent, "Share this on"));
                }
            });

            fullBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });

            super.onPostExecute(aVoid);
        }

        public void setStory() {
            final ParseNews prn = new ParseNews(getString(R.string.news_url), ser);
            prn.fetchXML();
            while (prn.parsingInComplete) ;
            body = prn.getNewsItems().get(0).getBody().replace("&#xD", "\n");
            head = prn.getNewsItems().get(0).getTitle();
        }

        public void setMainImg() {
            try {
                URL url = new URL(imgUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                myBitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                //Log.e("important", Log.getStackTraceString(e));
            }
        }
    }
}
