package com.utilitechnology.mystockplannerandroid.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.CheckTradingToken;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.SaveIndexData;
import com.utilitechnology.mystockplannerandroid.Urls;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class SplashscreenActivity extends AppCompatActivity {
    SaveIndexData svi;
//    SaveMovers svm;
    SettingsAPI set;
    private TextView msg;
    private ProgressBar load;
    CheckTradingToken ctt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (Build.VERSION.SDK_INT < 16) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        } else {
//            View decorView = getWindow().getDecorView();
//            // Hide the status bar.
//            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//            decorView.setSystemUiVisibility(uiOptions);
//            // Remember that you should never show the action bar if the
//            // status bar is hidden, so hide that too if necessary.
////            ActionBar actionBar = getSupportActionBar();
////            if (actionBar != null)
////                actionBar.hide();
//        }

        setContentView(R.layout.activity_splashscreen);
        svi = new SaveIndexData(this);
//        svm = new SaveMovers(this);
        set = new SettingsAPI(this);
        ctt = new CheckTradingToken(this);
        if (set.readSetting("currentIndex").equals("na"))
            set.addUpdateSettings("currentIndex", "^bsesn");
        msg = (TextView) findViewById(R.id.onlyText);
        load = (ProgressBar) findViewById(R.id.splashLoad);

        new Thread(new Runnable() {
            public void run() {
                try {
                    if (hasActiveInternetConnection()) {
                        if (!ctt.isZerodhaValid()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    msg.setText(R.string.token_expired);
                                    msg.setVisibility(View.VISIBLE);
                                    load.setVisibility(View.GONE);
                                }
                            });
                        }
                        svi.downloadValues();
                        //svm.downloadMovers();
                        if (set.readSetting("currentIndex").equals("^bsesn"))
                            svi.downloadCharts(Urls.BSE_INTRADAY);
                        else
                            svi.downloadCharts(Urls.NSE_INTRADAY);
                        Intent intent = new Intent(SplashscreenActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                msg.setText(R.string.no_internet);
                                msg.setVisibility(View.VISIBLE);
                                load.setVisibility(View.GONE);
                            }
                        });
                    }
                } catch (final IndexOutOfBoundsException ioob) {
                    //Log.e("important", Log.getStackTraceString(ioob));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //msg.setText(Log.getStackTraceString(ioob));
                            msg.setText(R.string.other_error_splash);
                            msg.setVisibility(View.VISIBLE);
                            load.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }).start();
    }

//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        //Log.e("important","will start main activity now");
//        //Check internet
//        //Save points and charts
//        //Start mainActivity and finish this activity
//
//        super.onPostCreate(savedInstanceState);
//    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean hasActiveInternetConnection() {
        if (isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return false;
            }
        } else {
            return false;
        }
    }
}
