package com.utilitechnology.mystockplannerandroid.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jjoe64.graphview.GraphView;
import com.utilitechnology.mystockplannerandroid.BuildConfig;
import com.utilitechnology.mystockplannerandroid.handler.HandleYahooChartValue;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchStockDetails;

public class StockDetailsActivity extends AppCompatActivity {
    //Todo: abort this activity in case a stock is not found or the script is null or null.bo or something
    android.support.v7.app.ActionBar actb;
    String script;
    String companyName;
    TextView stockNameLbl;
    GraphView graph;
    ImageView cover, nocover;
    Button dayBtn, mnthBtn, hYrBtn, yrBtn, fYrBtn;
    TradingAccountSettingsAPI tras;
    String userName = "na";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actb = getSupportActionBar();
        if (actb != null)
            actb.setDisplayHomeAsUpEnabled(true);

        tras = new TradingAccountSettingsAPI(this);
        userName = tras.readSetting("name");

        stockNameLbl = (TextView) findViewById(R.id.stocknamelbl);
        graph = (GraphView) findViewById(R.id.stockgraph);
        cover = (ImageView) findViewById(R.id.stockgraphCover);
        nocover = (ImageView) findViewById(R.id.nostockgraphCover);

        dayBtn = (Button) findViewById(R.id.stockday);
        mnthBtn = (Button) findViewById(R.id.stockmonth);
        hYrBtn = (Button) findViewById(R.id.stockhalfyr);
        yrBtn = (Button) findViewById(R.id.stockyr);
        fYrBtn = (Button) findViewById(R.id.stock5yr);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            script = extras.getString("script");
            companyName = extras.getString("stock");
            //Toast.makeText(this, unconvertedScript, Toast.LENGTH_LONG).show();
        }

        stockNameLbl.setText(companyName);

        new FetchStockDetails(this.getWindow().getDecorView(), script).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new HandleYahooChartValue(graph, null, cover, nocover, script, "1d").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        dayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(dayBtn, StockDetailsActivity.this);
                new HandleYahooChartValue(graph, null, cover, nocover, script, "1d").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        mnthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(mnthBtn, StockDetailsActivity.this);
                new HandleYahooChartValue(graph, null, cover, nocover, script, "1m").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        hYrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(hYrBtn, StockDetailsActivity.this);
                new HandleYahooChartValue(graph, null, cover, nocover, script, "6m").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        yrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(yrBtn, StockDetailsActivity.this);
                new HandleYahooChartValue(graph, null, cover, nocover, script, "1y").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        fYrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(fYrBtn, StockDetailsActivity.this);
                new HandleYahooChartValue(graph, null, cover, nocover, script, "5y").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        //Ad section


        AdView mAdView = (AdView) findViewById(R.id.adView);
        if (userName.equals("na")) {
            AdRequest adRequest;
            if (BuildConfig.DEBUG)
                adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            else
                adRequest = new AdRequest.Builder().build();
            assert mAdView != null;
            mAdView.loadAd(adRequest);
        } else {
            if (mAdView != null)
                mAdView.setVisibility(View.GONE);
        }
    }

    private void makeEveryButtonDefault(Button btn, Context con) {
        dayBtn.setTextColor(ContextCompat.getColor(con, R.color.full_black));
        mnthBtn.setTextColor(ContextCompat.getColor(con, R.color.full_black));
        hYrBtn.setTextColor(ContextCompat.getColor(con, R.color.full_black));
        yrBtn.setTextColor(ContextCompat.getColor(con, R.color.full_black));
        fYrBtn.setTextColor(ContextCompat.getColor(con, R.color.full_black));
        btn.setTextColor(ContextCompat.getColor(con, R.color.colorAccent));
    }
}
