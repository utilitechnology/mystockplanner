package com.utilitechnology.mystockplannerandroid.activity;

import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class ZerodhaLoginActivity extends AppCompatActivity {

    private static String API_KEY;
    private static String API_SECRET;
    private static final String REQUEST_URL = "https://kite.trade/connect/login?api_key=";
    private static final String AUTH_URL = "https://api.kite.trade/session/token";
    private static String REQUEST_TOKEN = "";
    WebView zerodhaLoginPage;
    TradingAccountSettingsAPI tras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zerodha_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        zerodhaLoginPage = (WebView) findViewById(R.id.zLoginWeb);
        tras = new TradingAccountSettingsAPI(this);
        API_KEY = getString(R.string.ZERODHA_API_KEY);
        API_SECRET = getString(R.string.ZERODHA_API_SECRET);

        zerodhaLoginPage.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Snackbar.make(ZerodhaLoginActivity.this.getWindow().getDecorView(), "Trouble connecting to login page", Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ZerodhaLoginActivity.this);
                builder.setMessage(R.string.login_redirection_warning);
                builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                        ZerodhaLoginActivity.this.finish();
                    }
                });
                builder.setCancelable(false);
                builder.create().show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                Log.e("important", "url is now " + url);
                //Do not use unidom here coz unidom is not https
                if (url.contains("https://www.utilitechnology.com/trading/zerodha/after_login_redirect.php")) {
                    String queryStr = url.split("\\?")[1];
                    for (String query : queryStr.split("&")) {
                        if (query.contains("request_token")) {
                            REQUEST_TOKEN = query.split("=")[1];

                            HashMap<String, String> parahm = new HashMap<String, String>();
                            parahm.put("api_key", API_KEY);
                            parahm.put("request_token", REQUEST_TOKEN);
                            parahm.put("checksum", calculateSha256(API_KEY + REQUEST_TOKEN + API_SECRET));

                            performPostCall(AUTH_URL, parahm);

                            //Log.e("important","request token found "+REQUEST_TOKEN);
                        }
                    }
                }
                //mView.loadUrl(url);
                return false;//return true means do not load the url
            }
        });

        zerodhaLoginPage.getSettings().setLoadWithOverviewMode(true);
        zerodhaLoginPage.getSettings().setUseWideViewPort(true);
        zerodhaLoginPage.getSettings().setJavaScriptEnabled(true);

        zerodhaLoginPage.loadUrl(REQUEST_URL + API_KEY);
    }

    public void performPostCall(final String requestURL, final HashMap<String, String> postDataParams) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(requestURL);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStream os = conn.getOutputStream();
                    conn.connect();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                    writer.write(getPostData(postDataParams));

                    writer.flush();
                    writer.close();
                    os.close();
                    int responseCode = conn.getResponseCode();
                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        if ((line = br.readLine()) != null) {
                            //Log.e("important","lines are "+line);
                            ParseLoginDataJSON pldj = new ParseLoginDataJSON();
                            pldj.parseJSONAndStoreIt(line);
                            while (pldj.parsingInComplete) ;
                            if (pldj.getStatus().equals("success")) {
                                tras.addUpdateSettings("access_token", pldj.getAccessToken());
                                tras.addUpdateSettings("name", pldj.getUsrName().replaceAll(" +", " "));
                                tras.addUpdateSettings("id", pldj.getUsrId());
                                tras.addUpdateSettings("broker", "Zerodha");
                                ZerodhaLoginActivity.this.finish();
                            }
                        }
                    } else {
                        Snackbar.make(ZerodhaLoginActivity.this.getWindow().getDecorView(), "Trouble logging in", Snackbar.LENGTH_LONG).show();
                        ZerodhaLoginActivity.this.finish();
                    }
                } catch (Exception e) {
                    Snackbar.make(ZerodhaLoginActivity.this.getWindow().getDecorView(), "Trouble logging in", Snackbar.LENGTH_LONG).show();
                    ZerodhaLoginActivity.this.finish();
                    //Log.e("important",Log.getStackTraceString(e));
                }
            }
        }).start();
    }

    private String getPostData(HashMap<String, String> params) {
        StringBuilder result = new StringBuilder();
        try {
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return result.toString();
    }

    private String calculateSha256(String input) {
        MessageDigest digest;
        String hash = "";
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(input.getBytes());
            hash = bytesToHexString(digest.digest());
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hash;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    class ParseLoginDataJSON {
        String status, accessToken, usrName, usrId;
        public volatile boolean parsingInComplete = true;

        public String getStatus() {
            return status;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getUsrName() {
            return usrName;
        }

        public String getUsrId() {
            return usrId;
        }

        private void parseJSONAndStoreIt(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                status = jsonObject.optString("status");
                String dataString = jsonObject.optString("data");

                JSONObject jsonDataObject = new JSONObject(dataString);

                accessToken = jsonDataObject.optString("access_token");
                usrName = jsonDataObject.optString("user_name");
                usrId = jsonDataObject.optString("user_id");

                parsingInComplete = false;
            } catch (JSONException e) {
                //Log.e("important", "exception " + Log.getStackTraceString(e));
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (zerodhaLoginPage != null)
            zerodhaLoginPage.destroy();
        super.onDestroy();
    }
}
