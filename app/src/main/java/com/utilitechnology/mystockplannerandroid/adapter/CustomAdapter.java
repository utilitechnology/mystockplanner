package com.utilitechnology.mystockplannerandroid.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.AddEditActivity;
import com.utilitechnology.mystockplannerandroid.activity.EquityDetailsContainerActivity;
import com.utilitechnology.mystockplannerandroid.handler.HandleTrade;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.activity.ZerodhaLoginActivity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by Bibaswann on 28-01-2016.
 */
public class CustomAdapter extends BaseAdapter {

    String[] scripts, stockName, LTP, change, upDown, exchange, iBought, sellTarget, quantity, gainLoss, buyTarget, amountToReachBuy;
    Boolean[] modifiable;
    Context mContext;
    String type;//It will be portfolio or watchlist
    ScriptHelper sch;
    TradingAccountSettingsAPI tras;

    public CustomAdapter(Context context, String type, String[] scripts, String[] stockName, String[] LTP, String[] change, String[] upsNDowns, String[] exchange, String[] iBought, String[] sellTarget, String[] gainLoss, String[] quantity, String[] buyTarget, String[] amountToReachBuy, Boolean[] modifiable) {
        this.scripts = scripts;
        this.stockName = stockName;
        this.LTP = LTP;
        this.change = change;
        this.upDown = upsNDowns;
        this.exchange = exchange;
        this.type = type;
        this.iBought = iBought;
        this.sellTarget = sellTarget;
        this.gainLoss = gainLoss;
        this.buyTarget = buyTarget;
        this.amountToReachBuy = amountToReachBuy;
        this.quantity = quantity;
        this.modifiable = modifiable;

        mContext = context;
        sch = new ScriptHelper(mContext);
        tras = new TradingAccountSettingsAPI(mContext);
    }

    @Override
    public int getCount() {
        return stockName.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        //Todo: use recycleview viewholder pattern
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.custom_list_item, null);

        final LinearLayout portfolioExtraData = (LinearLayout) rowView.findViewById(R.id.extraport);
        final LinearLayout watchlistExtraData = (LinearLayout) rowView.findViewById(R.id.extrawatch);

        final TextView name = (TextView) rowView.findViewById(R.id.stocklbl);
        final TextView price = (TextView) rowView.findViewById(R.id.stocdetailskval);
        final TextView schange = (TextView) rowView.findViewById(R.id.stockchange);
        final TextView exchngPort = (TextView) rowView.findViewById(R.id.exchangeport);
        final TextView iBoughtPort = (TextView) rowView.findViewById(R.id.youbotport);
        final TextView iSellPort = (TextView) rowView.findViewById(R.id.yousellport);
        final TextView quantityPort = (TextView) rowView.findViewById(R.id.quantityport);
        final TextView gainLossPort = (TextView) rowView.findViewById(R.id.gainlossport);
        final TextView exchngWatch = (TextView) rowView.findViewById(R.id.exchangewatch);
        final TextView iBuyWatch = (TextView) rowView.findViewById(R.id.buytargetWatch);
        final TextView amToWatch = (TextView) rowView.findViewById(R.id.amountoreachwatch);
        final Button edel = (Button) rowView.findViewById(R.id.edelist);
        final Button bs = (Button) rowView.findViewById(R.id.buyselist);
        final ImageView arrow = (ImageView) rowView.findViewById(R.id.stockArrow);


        if (stockName != null && stockName.length > position)
            name.setText(stockName[position]);
        if (LTP != null && LTP.length > position)
            price.setText(LTP[position]);
        if (change != null && change.length > position)
            schange.setText(String.format("%s", change[position].replace("-", "").replace("+", "")));
        if (exchange != null && exchange.length > position)
            exchngPort.setText(exchange[position]);
        if (iBought != null && iBought.length > position)
            iBoughtPort.setText(iBought[position]);
        if (sellTarget != null && sellTarget.length > position)
            iSellPort.setText(sellTarget[position]);
        if (quantity != null && quantity.length > position)
            quantityPort.setText(quantity[position]);
        if (gainLoss != null && gainLoss.length > position) {
            if (gainLoss[position].contains("-"))
                gainLossPort.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
            else
                gainLossPort.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
            gainLossPort.setText(gainLoss[position]);
        }
        if (exchange != null && exchange.length > position)
            exchngWatch.setText(exchange[position]);
        if (buyTarget != null && buyTarget.length > position)
            iBuyWatch.setText(buyTarget[position]);
        if (amountToReachBuy != null && amountToReachBuy.length > position)
            amToWatch.setText(amountToReachBuy[position]);
        if (upDown[position].equals("up")) {
            price.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
            schange.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
            arrow.setImageResource(R.drawable.up);
        } else {
            price.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
            schange.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
            arrow.setImageResource(R.drawable.down);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scripts.length > position) {
                    try {
                        if (name.getText().toString().equals("No items here")) {
                        } else if (name.getText().toString().equals("Connection aborted")) {
                        } else if (scripts[position].equals("")) {
                        } else if (name.getText().toString().equals("Couldn't connect to service")) {
                        } else {
                                name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                                Intent details = new Intent(mContext, EquityDetailsContainerActivity.class);
                                details.putExtra("script", scripts[position]);
                                mContext.startActivity(details);
                        }
                    } catch (Exception ex) {
                        reportAProblem(scripts[position], Log.getStackTraceString(ex));
                        Snackbar.make(v, mContext.getString(R.string.adapter_crash_message), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        if (modifiable != null && modifiable.length > 0)
            if (!modifiable[position]) {
                edel.setEnabled(false);
                edel.setTextColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
            }

        bs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scripts.length > position) {
                    try {
                        if (name.getText().toString().equals("No items here")) {
                        } else if (name.getText().toString().equals("Connection aborted")) {
                        } else if (scripts[position].equals("")) {
                        } else if (name.getText().toString().equals("Couldn't connect to service")) {
                        } else {
                            if (!tras.readSetting("access_token").equals("na")) {
                                HashMap<String, String> modata = new HashMap<String, String>();
                                modata.put("script", scripts[position]);

                                final Dialog dialog = new Dialog(mContext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.fragment_buy_sell);
                                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                dialog.show();
                                HandleTrade hndltr = new HandleTrade(dialog, dialog.getWindow().getDecorView(), "buysell", null, modata);
                            } else {
                                //Todo: it is for login, and users will be redirected to the default trading account opening page if he hs no account, if the default https://zerodha.com/open-account?ref=ZMPRAT is not right url (which comes when users tries to open account from login page of zerodha), I need to show 2 options for opening account and logging in, maybe with a dialog
                                Intent i = new Intent(mContext, ZerodhaLoginActivity.class);
                                mContext.startActivity(i);
                            }
                        }
                    } catch (Exception aiobx) {
                        reportAProblem(scripts[position], Log.getStackTraceString(aiobx));
                        Snackbar.make(v, mContext.getString(R.string.buysell_crash_message), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        edel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scripts.length > position) {
                    try {
                        if (name.getText().toString().equals("No items here")) {
                        } else if (name.getText().toString().equals("Connection aborted")) {
                        } else if (scripts[position].equals("")) {
                        } else if (name.getText().toString().equals("Couldn't connect to service")) {
                        } else {
                            String unconvertedScriptWithoutExtension = scripts[position].split("\\.")[0];
                            //Toast.makeText(mContext, uncnvrtdscrwoe, Toast.LENGTH_LONG).show();
                            if (type.equals("portfolio")) {
                                Intent details = new Intent(mContext, AddEditActivity.class);
                                details.putExtra("mode", "Edit");
                                details.putExtra("wholestring", sch.unconvertedScriptToWholeString(unconvertedScriptWithoutExtension));
                                details.putExtra("where", " Portfolio");
                                details.putExtra("bought", iBought[position]);
                                details.putExtra("selltarget", sellTarget[position]);
                                details.putExtra("buytarget", "-1.0");
                                details.putExtra("quantity", quantity[position]);
                                mContext.startActivity(details);
                            } else {
                                Intent details = new Intent(mContext, AddEditActivity.class);
                                details.putExtra("mode", "Edit");
                                details.putExtra("wholestring", sch.unconvertedScriptToWholeString(unconvertedScriptWithoutExtension));
                                details.putExtra("where", " Watchlist");
                                details.putExtra("bought", "-1.0");
                                details.putExtra("selltarget", "-1.0");
                                details.putExtra("buytarget", buyTarget[position]);
                                details.putExtra("quantity", "-1");
                                mContext.startActivity(details);
                            }
                        }
                    } catch (Exception ex) {
                        reportAProblem(scripts[position], Log.getStackTraceString(ex));
                        Snackbar.make(v, mContext.getString(R.string.general_crash_message), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        if (type.equals("portfolio")) {
            if (name.getText().toString().equals("No items here") || name.getText().toString().equals("Connection aborted")) {
                portfolioExtraData.setVisibility(View.GONE);
                watchlistExtraData.setVisibility(View.GONE);
                edel.setVisibility(View.GONE);
                bs.setVisibility(View.GONE);
                arrow.setVisibility(View.GONE);
            } else {
                portfolioExtraData.setVisibility(View.VISIBLE);
                watchlistExtraData.setVisibility(View.GONE);
                edel.setVisibility(View.VISIBLE);
                bs.setVisibility(View.VISIBLE);
                arrow.setVisibility(View.VISIBLE);
            }
        } else {
            if (name.getText().toString().equals("No items here") || name.getText().toString().equals("Connection aborted")) {
                portfolioExtraData.setVisibility(View.GONE);
                watchlistExtraData.setVisibility(View.GONE);
                edel.setVisibility(View.GONE);
                bs.setVisibility(View.GONE);
                arrow.setVisibility(View.GONE);
            } else {
                watchlistExtraData.setVisibility(View.VISIBLE);
                portfolioExtraData.setVisibility(View.GONE);
                edel.setVisibility(View.VISIBLE);
                bs.setVisibility(View.VISIBLE);
                arrow.setVisibility(View.VISIBLE);
            }
        }
        return rowView;
    }

    private void reportAProblem(final String script, final String msg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String complainUrl = mContext.getString(R.string.unidom) + "feedback/mystockplanner_problem_report.php?script=" + URLEncoder.encode(msg + "  for script:" + script, "UTF-8");
                    URL url = new URL(complainUrl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder result = new StringBuilder();
                    for (String line; (line = reader.readLine()) != null; ) {
                        result.append(line);
                    }
                    stream.close();
                } catch (Exception e) {
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
