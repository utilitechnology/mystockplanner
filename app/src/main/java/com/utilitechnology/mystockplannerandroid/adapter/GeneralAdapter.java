package com.utilitechnology.mystockplannerandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.EquityDetailsContainerActivity;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.activity.StockDetailsActivity;

/**
 * Created by Bibaswann on 13-02-2016.
 */
public class GeneralAdapter extends BaseAdapter {

    String[] scripts, stockName, Ltp, change, upDown;
    String dataType;//Type can be movers, world market or anything that doesn't have an expandable details section
    Context mContext;

    public GeneralAdapter(Context context, String type, String[] scripts, String[] stockName, String[] Ltp, String[] change, String[] upsNDowns) {
        dataType = type;
        mContext = context;
        this.scripts = scripts;
        this.stockName = stockName;
        this.Ltp = Ltp;
        this.change = change;
        this.upDown = upsNDowns;
    }

    @Override
    public int getCount() {
        return stockName.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        //Todo: use recycleview viewholder pattern
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.general_list_item, null);

        final TextView name = (TextView) rowView.findViewById(R.id.genLbl);
        final TextView price = (TextView) rowView.findViewById(R.id.genVal);
        final TextView schange = (TextView) rowView.findViewById(R.id.genChange);
        final ImageView arrow = (ImageView) rowView.findViewById(R.id.genArrow);

        //+ and - are to be set here when not scraped from xml
        try {
            name.setText(stockName[position]);
            price.setText(Ltp[position]);
            if (!change[position].contains("NaN")) {
                schange.setText(String.format("%s", change[position].replace("-", "").replace("+", "")));
                if (upDown[position].equals("up")) {
                    price.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
                    schange.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
                    arrow.setImageResource(R.drawable.up);
                } else {
                    price.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
                    schange.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
                    arrow.setImageResource(R.drawable.down);
                }
            }
            else
            {
                schange.setVisibility(View.INVISIBLE);
                arrow.setVisibility(View.INVISIBLE);
            }

            if (name.getText().toString().equals("No items here") || name.getText().toString().equals("Connection aborted"))
                arrow.setVisibility(View.GONE);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dataType.equals("movers")) {
                        if (!scripts[position].split("\\.")[0].equals("null")) {
                            if (name.getText().toString().equals("No items here")) {
                            } else if (name.getText().toString().equals("Connection aborted")) {
                            } else {
                                name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));

                                Intent details = new Intent(mContext, EquityDetailsContainerActivity.class);
                                details.putExtra("script", scripts[position]);
                                mContext.startActivity(details);
                            }
                        }
                    } else if (dataType.equals("world")) {
                        if (name.getText().toString().equals("No items here")) {
                        } else if (name.getText().toString().equals("Connection aborted")) {
                        } else {
                            name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));

                            Intent details = new Intent(mContext, StockDetailsActivity.class);
                            details.putExtra("script", scripts[position]);
                            details.putExtra("stock", stockName[position]);
                            mContext.startActivity(details);
                        }
                    } else if (dataType.equals("sub")) {
                        if (name.getText().toString().equals("No items here")) {
                        } else if (name.getText().toString().equals("Connection aborted")) {
                        } else {
                            name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));

                            Intent details = new Intent(mContext, StockDetailsActivity.class);
                            details.putExtra("script", scripts[position]);
                            details.putExtra("stock", stockName[position]);
                            mContext.startActivity(details);
                        }
                    } else if (dataType.equals("currency")) {
                        //We don't have a details currency page yet
                    } else if (dataType.equals("commodity")) {
                        //We don't have a details commodity page yet
                    }
                }
            });
        } catch (IndexOutOfBoundsException ignored) {
        }

        return rowView;
    }
}
