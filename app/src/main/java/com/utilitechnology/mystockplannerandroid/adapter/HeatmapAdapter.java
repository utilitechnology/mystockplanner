package com.utilitechnology.mystockplannerandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.EquityDetailsContainerActivity;
import com.utilitechnology.mystockplannerandroid.R;

/**
 * Created by Bibaswann on 11-05-2016.
 */
public class HeatmapAdapter extends BaseAdapter {

    String[] scripts, stockName, Ltp, change, upDown;
    Context mContext;

    HeatmapAdapter(Context context, String[] scripts, String[] stockName, String[] Ltp, String[] change, String[] upsNDowns) {
        mContext = context;
        this.scripts = scripts;
        this.stockName = stockName;
        this.Ltp = Ltp;
        this.change = change;
        this.upDown = upsNDowns;
    }

    @Override
    public int getCount() {
        return stockName.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final HeatmapItemView htmiv;
        if (convertView == null) {
            //Todo: pass root relativelayout in order to keep margin
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.heatmap_item, null);
            //convertView = View.inflate(mContext, R.layout.heatmap_item, null);//Alternate, never tested though
            htmiv=new HeatmapItemView();

             htmiv.name= (TextView) convertView.findViewById(R.id.htLbl);
             htmiv.price= (TextView) convertView.findViewById(R.id.htVal);
             htmiv.schange= (TextView) convertView.findViewById(R.id.htChange);
             htmiv.holder= (LinearLayout) convertView.findViewById(R.id.htHolder);
            convertView.setTag(htmiv);
        } else {
            htmiv = (HeatmapItemView) convertView.getTag();
        }
        //+ and - are to be set here when not scraped from xml
        htmiv.name.setText(stockName[position]);
        htmiv.price.setText(Ltp[position]);
        htmiv.schange.setText(String.format("%s", change[position]));
        //double absChange=Double.parseDouble(change[position].replace("+","").replace("-",""));
        if (upDown[position].equals("up")) {
            float absoluteChange = Float.parseFloat(change[position].replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace("%", "").split(" ")[1]);
            if (absoluteChange == 0)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
            else if (absoluteChange > 0 && absoluteChange < 1)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_0));
            else if (absoluteChange > 1 && absoluteChange < 2)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_1));
            else if (absoluteChange > 2 && absoluteChange < 3)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_2));
            else if (absoluteChange > 3 && absoluteChange < 4)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_3));
            else if (absoluteChange > 4 && absoluteChange < 5)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_4));
            else if (absoluteChange > 5)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_5));
        } else {
            float absoluteChange = Float.parseFloat(change[position].replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace("%", "").split(" ")[1]);
            if (absoluteChange == 0)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
            else if (absoluteChange > 0 && absoluteChange < 1)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_0));
            else if (absoluteChange > 1 && absoluteChange < 2)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_1));
            else if (absoluteChange > 2 && absoluteChange < 3)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_2));
            else if (absoluteChange > 3 && absoluteChange < 4)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_3));
            else if (absoluteChange > 4 && absoluteChange < 5)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_4));
            else if (absoluteChange > 5)
                htmiv.holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_5));
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!scripts[position].split("\\.")[0].equals("null")) {
                    if (htmiv.name.getText().toString().equals("No items here")) {
                    } else if (htmiv.name.getText().toString().equals("Connection aborted")) {
                    } else {
                        htmiv.name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                        Intent details = new Intent(mContext, EquityDetailsContainerActivity.class);
                        details.putExtra("script", scripts[position]);
                        mContext.startActivity(details);
                    }
                }
            }
        });

        return convertView;
    }
    static class HeatmapItemView
    {
        TextView name;
        TextView price;
        TextView schange;
        LinearLayout holder;
    }
}
