package com.utilitechnology.mystockplannerandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.utilitechnology.mystockplannerandroid.object.NewsObject;
import com.utilitechnology.mystockplannerandroid.activity.NewsReaderActivity;
import com.utilitechnology.mystockplannerandroid.R;

import java.util.List;

/**
 * Created by Bibaswann on 17-11-2016.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    List<NewsObject> newsItems;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, meta;
        ImageView thumb;
        LinearLayout total;

        public MyViewHolder(View view) {
            super(view);
            total = (LinearLayout) view.findViewById(R.id.totalNews);
            title = (TextView) view.findViewById(R.id.headLine);
            meta = (TextView) view.findViewById(R.id.metaNews);
            thumb = (ImageView) view.findViewById(R.id.thumg);
        }
    }

    public NewsAdapter(Context context, List<NewsObject> newsObjects) {
        mContext = context;
        newsItems = newsObjects;
        //Log.e("important", "got "+newsItems.size()+"items");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //Log.e("important", "setting view");

        holder.title.setText(newsItems.get(position).getTitle());
        holder.meta.setText(newsItems.get(position).getMedia());

        if (newsItems.get(position).getThumb().equals(""))
            holder.thumb.setImageResource(R.drawable.news_placeholder);
        else
            Picasso.with(mContext).load(newsItems.get(position).getThumb()).placeholder(R.drawable.news_placeholder).error(R.drawable.news_placeholder).into(holder.thumb);

        holder.total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent details = new Intent(mContext, NewsReaderActivity.class);
                details.putExtra("url", newsItems.get(position).getLink());
                details.putExtra("imgurl", newsItems.get(position).getThumb());
                details.putExtra("serial", newsItems.get(position).getSerial());
                mContext.startActivity(details);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsItems.size();
    }
}
