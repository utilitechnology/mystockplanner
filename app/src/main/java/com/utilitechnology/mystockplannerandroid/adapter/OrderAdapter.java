package com.utilitechnology.mystockplannerandroid.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.EquityDetailsContainerActivity;
import com.utilitechnology.mystockplannerandroid.handler.HandleTrade;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;

import java.util.HashMap;

/**
 * Created by Bibaswann on 08-06-2016.
 */
public class OrderAdapter extends BaseAdapter {

    String[] orderId, scripts, symbol, stockName, LTP, change, upDown, transaction, product, status, exchange, iBought, type, quantity, gainLoss, limit, trigger, validity, disclosed;
    Context mContext;
    ScriptHelper sch;
    TradingAccountSettingsAPI tras;

    public OrderAdapter(Context context, String[] orderId, String[] scripts, String[] symbol, String[] stockName, String[] LTP, String[] change, String[] upDown, String[] transaction, String[] product, String[] status, String[] exchange, String[] iBought, String[] type, String[] quantity, String[] gainLoss, String[] limit, String[] trigger, String[] validity, String[] disclosed) {
        mContext = context;

        this.orderId = orderId;
        this.scripts = scripts;
        this.symbol = symbol;
        this.stockName = stockName;
        this.LTP = LTP;
        this.change = change;
        this.upDown = upDown;
        this.transaction = transaction;
        this.product = product;
        this.status = status;
        this.exchange = exchange;
        this.iBought = iBought;
        this.type = type;
        this.quantity = quantity;
        this.gainLoss = gainLoss;
        this.limit = limit;
        this.trigger = trigger;
        this.validity = validity;
        this.disclosed = disclosed;

        sch = new ScriptHelper(mContext);
        tras = new TradingAccountSettingsAPI(mContext);
    }

    @Override
    public int getCount() {
        return stockName.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        //Todo: use recycleview viewholder pattern
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.order_list_item, null);

        LinearLayout orderExtra = (LinearLayout) rowView.findViewById(R.id.extraOrder);
        LinearLayout divider= (LinearLayout) rowView.findViewById(R.id.orderVerticalLine);

        final TextView sname = (TextView) rowView.findViewById(R.id.orderlbl);
        final TextView sprice = (TextView) rowView.findViewById(R.id.orderStockPrice);
        final TextView schange = (TextView) rowView.findViewById(R.id.orderStockPriceChange);
        final TextView stransaction = (TextView) rowView.findViewById(R.id.orderTransactionType);
        final TextView sstatus = (TextView) rowView.findViewById(R.id.orderStatus);
        final TextView sbotprice = (TextView) rowView.findViewById(R.id.orderPrice);
        final TextView stype = (TextView) rowView.findViewById(R.id.orderType);
        final TextView squantity = (TextView) rowView.findViewById(R.id.orderQuantity);
        final TextView sgainloss = (TextView) rowView.findViewById(R.id.orderPl);
        final TextView slim = (TextView) rowView.findViewById(R.id.orderLimitPrice);
        final TextView strig = (TextView) rowView.findViewById(R.id.orderTriggerPrice);
        final TextView sprod = (TextView) rowView.findViewById(R.id.orderProduct);

        final Button exit = (Button) rowView.findViewById(R.id.orderExit);
        final Button model = (Button) rowView.findViewById(R.id.orderModify);

        final ImageView arrow= (ImageView) rowView.findViewById(R.id.orderArrow);

        LinearLayout avgPrice = (LinearLayout) rowView.findViewById(R.id.orderPriceSect);//any complete order
        LinearLayout pl = (LinearLayout) rowView.findViewById(R.id.orderPLSect);//any complete order
        LinearLayout limPrice = (LinearLayout) rowView.findViewById(R.id.orderLimitPriceSect);//incomplete limit order
        LinearLayout trigPrice = (LinearLayout) rowView.findViewById(R.id.orderTriggerPriceSect);//any stoploss order

        try {
            sname.setText(stockName[position]+" ["+exchange[position]+"]");
            sprice.setText(LTP[position]);
            schange.setText(change[position].replace("-","").replace("+",""));
            sstatus.setText(status[position]);
            sbotprice.setText(iBought[position]);
            stype.setText(type[position]);
            squantity.setText(quantity[position]);
            sgainloss.setText(gainLoss[position]);
            slim.setText(limit[position]);
            strig.setText(trigger[position]);
            stransaction.setText(transaction[position]);
            sprod.setText(product[position]);

            if (upDown[position].equals("up")) {
                sprice.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
                schange.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
                arrow.setImageResource(R.drawable.up);
            } else {
                sprice.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
                schange.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
                arrow.setImageResource(R.drawable.down);
            }

            if (gainLoss[position].contains("-"))
                sgainloss.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
            else
                sgainloss.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));

            if (stockName[position].equals("No items here") || stockName[position].equals("Connection aborted")) {
                orderExtra.setVisibility(View.GONE);
                model.setVisibility(View.GONE);
                exit.setVisibility(View.GONE);
                divider.setVisibility(View.GONE);
                arrow.setVisibility(View.GONE);
                sname.setText(stockName[position]);//So that [] not shown
            } else {
                orderExtra.setVisibility(View.VISIBLE);
                model.setVisibility(View.VISIBLE);
                exit.setVisibility(View.VISIBLE);
                divider.setVisibility(View.VISIBLE);
                arrow.setVisibility(View.VISIBLE);
            }

            if (!status[position].equals("OPEN")) {
                model.setEnabled(false);
                model.setTextColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
            }
            if (type[position].equals("MARKET") || type[position].equals("LIMIT"))
                trigPrice.setVisibility(View.GONE);
            if (!type[position].equals("LIMIT"))
                limPrice.setVisibility(View.GONE);
            if (status[position].equals("COMPLETE"))
                limPrice.setVisibility(View.GONE);
            else {
                pl.setVisibility(View.GONE);
                avgPrice.setVisibility(View.GONE);
            }
//            if (transaction[position].equals("BUY"))
//                exit.setText("Sell");
//            else
//                exit.setText("Buy");

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sname.getText().toString().equals("No items here")) {
                    } else if (sname.getText().toString().equals("Connection aborted")) {
                    } else {
                        sname.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                        Intent details = new Intent(mContext, EquityDetailsContainerActivity.class);
                        details.putExtra("script", scripts[position]);
                        mContext.startActivity(details);
                    }
                }
            });

            //Create the dialog

            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.fragment_buy_sell);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> modata = new HashMap<String, String>();
                    modata.put("script", scripts[position]);
                    dialog.show();
                    HandleTrade hndltr = new HandleTrade(dialog,dialog.getWindow().getDecorView(), "buysell", null, modata);
                }
            });
            model.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://kite.zerodha.com/"));
                    mContext.startActivity(i);

/////////////////////////////////////////The Below lines will cost me 25k bucks///////////////////////////////////////////

//                    HashMap<String, String> modata = new HashMap<String, String>();
//                    modata.put("tradingsymbol", symbol[position]);
//                    modata.put("exchange", exchange[position]);
//                    modata.put("script", scripts[position]);
//                    modata.put("transaction_type", transaction[position]);
//                    modata.put("order_type", type[position]);
//                    modata.put("product", product[position]);//mis or cnc
//                    modata.put("quantity", quantity[position]);
//                    modata.put("validity", validity[position]);
//                    modata.put("disclosed_quantity", disclosed[position]);
//                    if (type[position].equals("SL"))
//                        modata.put("trigger_price", trigger[position]);
//                    if (type[position].equals("LIMIT"))
//                        modata.put("price", limit[position]);
//
//                    dialog.show();
//                    HandleTrade hndltr = new HandleTrade(dialog,dialog.getWindow().getDecorView(), "modify", orderId[position], modata);
                }
            });
        } catch (IndexOutOfBoundsException iobx) {
            //Log.e("important",Log.getStackTraceString(iobx));
            //Snackbar.make(rowView,"Problem fetching data, please try again",Snackbar.LENGTH_LONG).show();
        }

        return rowView;
    }
}
