package com.utilitechnology.mystockplannerandroid.caching;

import android.content.Context;
import android.content.SharedPreferences;

import com.utilitechnology.mystockplannerandroid.R;

import java.io.FileOutputStream;

/**
 * Created by Bibaswann on 06-03-2016.
 */
public class IndexCachingAPI {
    Context mContext;
    private SharedPreferences sharedSettings;

    public IndexCachingAPI(Context context) {
        mContext = context;
        sharedSettings = mContext.getSharedPreferences(mContext.getString(R.string.cached_index_data_file), Context.MODE_PRIVATE);
    }

    public void addNewValue(String key, String value) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getValue(String key) {
        return sharedSettings.getString(key, "na");
    }

    public void saveChart(String fileName, String chartData) {
        FileOutputStream outputStream;
        try {
            outputStream = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(chartData.getBytes());
            outputStream.close();
        } catch (Exception e) {
        }
    }

    public void deleteOldCharts(String fileName) {
        mContext.deleteFile(fileName);
    }
}
