package com.utilitechnology.mystockplannerandroid.caching;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Bibaswann on 10-04-2016.
 */
public class MoverCachingAPI {
    Context mContext;
    private List<String> CompaniesToReturn = new ArrayList<>();
    private List<String> ScriptsToReturn = new ArrayList<>();
    private List<String> LTPsToReturn = new ArrayList<>();
    private List<String> ChangesToReturn = new ArrayList<>();
    private List<String> UpsDownsToReturn = new ArrayList<>();

    FileOutputStream fos;
    XmlSerializer serializer;

    private static final String BSE_FILENAME = "com.utilitechnology.mystockplannerandroid.bsemoverdata";
    private static final String NSE_FILENAME = "com.utilitechnology.mystockplannerandroid.nsemoverdata";

    public MoverCachingAPI(Context context) {
        mContext = context;
        serializer = Xml.newSerializer();
    }

    public List<String> getCompaniesToReturn() {
        return CompaniesToReturn;
    }

    public List<String> getScriptsToReturn() {
        return ScriptsToReturn;
    }

    public List<String> getLTPsToReturn() {
        return LTPsToReturn;
    }

    public List<String> getChangesToReturn() {
        return ChangesToReturn;
    }

    public List<String> getUpsDownsToReturn() {
        return UpsDownsToReturn;
    }

    public void saveBSEData(List<String> BSEGainerCompanies, List<String> BSEGainerScripts, List<String> BSEGainerLTPs, List<String> BSEGainerChanges, List<String> BSEGainerUpsDowns,
                            List<String> BSELoserCompanies, List<String> BSELoserScripts, List<String> BSELoserLTPs, List<String> BSELoserChanges, List<String> BSELoserUpsDowns) {
        try {
            //Log.e("important","wring bse companies");
            fos = mContext.openFileOutput(BSE_FILENAME, Context.MODE_PRIVATE);
            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument(null, true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            serializer.startTag(null, "companies");
            for (String oneCompany : BSEGainerCompanies) {
                serializer.startTag(null, "bsegainer");
                int index = BSEGainerCompanies.indexOf(oneCompany);
                serializer.startTag(null, "name");
                serializer.text(oneCompany);
                serializer.endTag(null, "name");

                serializer.startTag(null, "script");
                serializer.text(BSEGainerScripts.get(index));
                serializer.endTag(null, "script");

                serializer.startTag(null, "ltp");
                serializer.text(BSEGainerLTPs.get(index));
                serializer.endTag(null, "ltp");

                serializer.startTag(null, "change");
                serializer.text(BSEGainerChanges.get(index));
                serializer.endTag(null, "change");

                serializer.startTag(null, "updown");
                serializer.text(BSEGainerUpsDowns.get(index));
                serializer.endTag(null, "updown");

                serializer.endTag(null, "bsegainer");
            }
            for (String oneCompany : BSELoserCompanies) {
                serializer.startTag(null, "bseloser");
                int index = BSELoserCompanies.indexOf(oneCompany);
                serializer.startTag(null, "name");
                serializer.text(oneCompany);
                serializer.endTag(null, "name");

                serializer.startTag(null, "script");
                serializer.text(BSELoserScripts.get(index));
                serializer.endTag(null, "script");

                serializer.startTag(null, "ltp");
                serializer.text(BSELoserLTPs.get(index));
                serializer.endTag(null, "ltp");

                serializer.startTag(null, "change");
                serializer.text(BSELoserChanges.get(index));
                serializer.endTag(null, "change");

                serializer.startTag(null, "updown");
                serializer.text(BSELoserUpsDowns.get(index));
                serializer.endTag(null, "updown");

                serializer.endTag(null, "bseloser");
            }
            serializer.endDocument();
            serializer.flush();
            fos.close();
        } catch (IOException ioex) {
            //Nothing I can do here
        }
    }

    public void saveNSEData(List<String> NSEGainerCompanies, List<String> NSEGainerScripts, List<String> NSEGainerLTPs, List<String> NSEGainerChanges, List<String> NSEGainerUpsDowns,
                            List<String> NSELoserCompanies, List<String> NSELoserScripts, List<String> NSELoserLTPs, List<String> NSELoserChanges, List<String> NSELoserUpsDowns) {
        try {
            //Log.e("important","wring nse companies");
            fos = mContext.openFileOutput(NSE_FILENAME, Context.MODE_PRIVATE);
            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument(null, true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            serializer.startTag(null, "companies");
            for (String oneCompany : NSEGainerCompanies) {
                serializer.startTag(null, "nsegainer");
                int index = NSEGainerCompanies.indexOf(oneCompany);
                serializer.startTag(null, "name");
                serializer.text(oneCompany);
                serializer.endTag(null, "name");

                serializer.startTag(null, "script");
                serializer.text(NSEGainerScripts.get(index));
                serializer.endTag(null, "script");

                serializer.startTag(null, "ltp");
                serializer.text(NSEGainerLTPs.get(index));
                serializer.endTag(null, "ltp");

                serializer.startTag(null, "change");
                serializer.text(NSEGainerChanges.get(index));
                serializer.endTag(null, "change");

                serializer.startTag(null, "updown");
                serializer.text(NSEGainerUpsDowns.get(index));
                serializer.endTag(null, "updown");

                serializer.endTag(null, "nsegainer");
            }
            for (String oneCompany : NSELoserCompanies) {
                serializer.startTag(null, "nseloser");
                int index = NSELoserCompanies.indexOf(oneCompany);
                serializer.startTag(null, "name");
                serializer.text(oneCompany);
                serializer.endTag(null, "name");

                serializer.startTag(null, "script");
                serializer.text(NSELoserScripts.get(index));
                serializer.endTag(null, "script");

                serializer.startTag(null, "ltp");
                serializer.text(NSELoserLTPs.get(index));
                serializer.endTag(null, "ltp");

                serializer.startTag(null, "change");
                serializer.text(NSELoserChanges.get(index));
                serializer.endTag(null, "change");

                serializer.startTag(null, "updown");
                serializer.text(NSELoserUpsDowns.get(index));
                serializer.endTag(null, "updown");

                serializer.endTag(null, "nseloser");
            }
            serializer.endDocument();
            serializer.flush();
            fos.close();
        } catch (IOException ioex) {
            //Log.e("important",Log.getStackTraceString(ioex));
        }
    }

    //Allowed types are bsegainer,bseloser,nsegainer,nseloser
    public boolean readMovers(String type) {
        CompaniesToReturn.clear();
        ScriptsToReturn.clear();
        LTPsToReturn.clear();
        ChangesToReturn.clear();
        UpsDownsToReturn.clear();

        FileInputStream fis = null;
        InputStreamReader isr = null;
        try {
            if (type.equals("bsegainer") || type.equals("bseloser"))
                fis = mContext.openFileInput(BSE_FILENAME);
            else
                fis = mContext.openFileInput(NSE_FILENAME);
            isr = new InputStreamReader(fis);
            char[] inputBuffer = new char[fis.available()];
            isr.read(inputBuffer);
            String data = new String(inputBuffer);
            isr.close();
            fis.close();

            InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

            DocumentBuilderFactory dbf;
            DocumentBuilder db;
            NodeList items = null;
            Document dom;

            dbf = DocumentBuilderFactory.newInstance();
            db = dbf.newDocumentBuilder();
            dom = db.parse(is);
            // normalize the document
            dom.getDocumentElement().normalize();

            items = dom.getElementsByTagName(type);

            for (int i = 0; i < items.getLength(); i++) {
                Node item = items.item(i);
                Element eElement = (Element) item;
                //Log.e("important","adding "+eElement.getElementsByTagName("name").item(0).getTextContent()+" when type is "+type);
                CompaniesToReturn.add(eElement.getElementsByTagName("name").item(0).getTextContent());
                ScriptsToReturn.add(eElement.getElementsByTagName("script").item(0).getTextContent());
                LTPsToReturn.add(eElement.getElementsByTagName("ltp").item(0).getTextContent());
                ChangesToReturn.add(eElement.getElementsByTagName("change").item(0).getTextContent());
                UpsDownsToReturn.add(eElement.getElementsByTagName("updown").item(0).getTextContent());
            }
            //Log.e("important", "returning true");
            return true;
        } catch (IOException | ParserConfigurationException | SAXException ioe) {
            //same
        }
        //Log.e("important", "returning false");
        return false;
    }
}
