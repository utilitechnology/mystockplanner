package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.parser.ParseBrokers;

/**
 * Created by Bibaswann on 23-10-2016.
 */

public class FetchBrokerDetails extends AsyncTask<Void, Void, Void> {
    private TextView bName, bCharge, bBrokerage, bMargin, bWebsite, bPhone,bMaintain;
    private String name, charge, brokerage, website, margin, phone, maintain;
    private String fetchUrl;
    private Button bCall;
    Context mContext;

    public FetchBrokerDetails(Context context, TextView name, TextView charge, TextView brokerage, TextView margin, TextView website, TextView phone, TextView maintain, Button call, String url) {
        mContext=context;
        bName = name;
        bCharge = charge;
        bBrokerage = brokerage;
        bWebsite = website;
        bPhone=phone;
        fetchUrl = url;
        bMaintain=maintain;
        bMargin = margin;
        bCall=call;
    }

    @Override
    protected Void doInBackground(Void... params) {
        ParseBrokers pb = new ParseBrokers(fetchUrl);
        pb.fetchXML();
        while (pb.parsingInComplete) ;
        name = pb.getBname().get(0);
        charge = pb.getBcharge().get(0);
        brokerage = pb.getBbrokerage().get(0);
        website = pb.getBweb().get(0);
        phone = pb.getBphone().get(0);
        margin=pb.getBmargin().get(0);
        maintain=pb.getBmaintain().get(0);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        bName.setText(name);
        bCharge.setText(charge.replace("<br/>", "\r\n"));
        bBrokerage.setText(brokerage.replace("<br/>", "\r\n"));
        bMargin.setText(margin.replace("<br/>", "\r\n"));
        bWebsite.setText(website);
        bPhone.setText(phone);
        bMaintain.setText(maintain);
        bCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                mContext.startActivity(intent);
            }
        });
        super.onPostExecute(aVoid);
    }
}
