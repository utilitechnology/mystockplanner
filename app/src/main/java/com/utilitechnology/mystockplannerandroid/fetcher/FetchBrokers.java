package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.BrokerDetailsActivity;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.activity.ZerodhaLoginActivity;
import com.utilitechnology.mystockplannerandroid.parser.ParseBrokers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 23-10-2016.
 */

public class FetchBrokers extends AsyncTask<Void, Void, Void> {

    Context mContext;
    View mView;
    LinearLayout brohof;
    LinearLayout brohod;
    TextView topHead,bottomHead,bonus;
    ProgressDialog progress;
    ParseBrokers prb;
    private List<String> names,dps, brokerages, serials, urls,fulls;
    FetchBrokers asyncCountdown;

    public FetchBrokers(Context context, View v) {
        mContext = context;
        mView = v;
        names = new ArrayList<>();
        dps=new ArrayList<>();
        brokerages = new ArrayList<>();
        serials = new ArrayList<>();
        urls = new ArrayList<>();
        fulls=new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        brohod = (LinearLayout) mView.findViewById(R.id.brokerHolderDiscount);
        brohof = (LinearLayout) mView.findViewById(R.id.brokerHolderFull);

        topHead= (TextView) mView.findViewById(R.id.topDescription);
        bottomHead= (TextView) mView.findViewById(R.id.bottomDescription);
        bonus= (TextView) mView.findViewById(R.id.bonus);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
        progress.show();
        super.onPreExecute();

        asyncCountdown = this;
        new CountDownTimer(10000, 10000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }
            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }
                    Snackbar.make(mView,"This version is obsolete. Please update your app",Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
                            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                            try {
                                mContext.startActivity(goToMarket);
                            } catch (ActivityNotFoundException e) {
                            }
                        }
                    }).setActionTextColor(Color.GREEN).show();
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        prb = new ParseBrokers(mContext.getString(R.string.broker_list_url));
        prb.fetchXML();
        while (prb.parsingInComplete) ;
        serials = prb.getBser();
        names = prb.getBname();
        dps=prb.getBdp();
        brokerages = prb.getBbrokerage();
        urls = prb.getBurl();
        fulls=prb.getBfull();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {
        } finally {
            progress = null;
        }
        brohof.removeAllViews();
        brohod.removeAllViews();

        for (final String serial : serials) {
            final int index = serials.indexOf(serial);
            if(fulls.get(index).equals("0")) {
                LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.broker_item, brohod, false);
                ImageView img = (ImageView) ll.findViewById(R.id.bdp);
                img.setBackgroundResource(mContext.getResources().getIdentifier(dps.get(index), "drawable", mContext.getPackageName()));

                TextView bro = (TextView) ll.findViewById(R.id.brokerage);
                bro.setText(brokerages.get(index).replace("<br/>", "\r\n"));

                Button logIn = (Button) ll.findViewById(R.id.loginConnectBtn);
                Button openAc = (Button) ll.findViewById(R.id.openAccBtn);

                if (!names.get(index).equals("Zerodha")) {
                    logIn.setEnabled(false);
                    logIn.setTextColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
                }

                openAc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(urls.get(index)));
                        mContext.startActivity(i);
                    }
                });

                logIn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, ZerodhaLoginActivity.class);
                        mContext.startActivity(i);
                    }
                });

                ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, BrokerDetailsActivity.class);
                        i.putExtra("serial", serial);
                        i.putExtra("name", names.get(index));
                        i.putExtra("opnurl", urls.get(index));
                        i.putExtra("dp", dps.get(index));
                        mContext.startActivity(i);
                    }
                });
                brohod.addView(ll);
            }
            else
            {
                LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.broker_item, brohof, false);
                ImageView img = (ImageView) ll.findViewById(R.id.bdp);
                img.setBackgroundResource(mContext.getResources().getIdentifier(dps.get(index), "drawable", mContext.getPackageName()));

                TextView bro = (TextView) ll.findViewById(R.id.brokerage);
                bro.setText(brokerages.get(index).replace("<br/>", "\r\n"));

                Button logIn = (Button) ll.findViewById(R.id.loginConnectBtn);
                Button openAc = (Button) ll.findViewById(R.id.openAccBtn);

                if (!names.get(index).equals("Zerodha")) {
                    logIn.setEnabled(false);
                    logIn.setTextColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
                }

                openAc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(urls.get(index)));
                        mContext.startActivity(i);
                    }
                });

                logIn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, ZerodhaLoginActivity.class);
                        mContext.startActivity(i);
                    }
                });

                ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, BrokerDetailsActivity.class);
                        i.putExtra("serial", serial);
                        i.putExtra("name", names.get(index));
                        i.putExtra("opnurl", urls.get(index));
                        i.putExtra("dp", dps.get(index));
                        mContext.startActivity(i);
                    }
                });
                brohof.addView(ll);
            }
        }

        topHead.setVisibility(View.VISIBLE);
        bottomHead.setVisibility(View.VISIBLE);
        bonus.setVisibility(View.VISIBLE);

        super.onPostExecute(aVoid);
    }
}
