package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.adapter.GeneralAdapter;
import com.utilitechnology.mystockplannerandroid.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 16-12-2016.
 */

public class FetchCommodity extends AsyncTask<Void, Void, Void> {
    Context mContext;
    ListView commoList;

    private final String COMMODITY_RESOURCE_WEB = "http://mcxdata.in/";

    ProgressDialog progress;
    FetchCommodity asyncCountdown;

    private List<String> indices = new ArrayList<>();
    private List<String> scripts = new ArrayList<>();
    private List<String> ltps = new ArrayList<>();
    private List<String> changes = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    public FetchCommodity(View v) {
        mContext = v.getContext();
        commoList = (ListView) v.findViewById(R.id.commodityList);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        super.onPreExecute();
        asyncCountdown = this;
        new CountDownTimer(20000, 20000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    indices.clear();
                    scripts.clear();
                    ltps.clear();
                    changes.clear();
                    upsNDowns.clear();

                    indices.add("Connection aborted");
                    scripts.add("");
                    ltps.add("");
                    changes.add("");
                    upsNDowns.add("up");

                    commoList.setAdapter(new GeneralAdapter(mContext, "commodity", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
                }
            }
        }.start();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        indices.add("Gold");
        indices.add("Gold M");
        indices.add("Silver");
        indices.add("Silver M");
        indices.add("Copper");
        indices.add("Copper M");
        indices.add("Aluminium");
        indices.add("Alumini");
        indices.add("Crude Oil");
        indices.add("Crude Oil M");
        indices.add("Lead");
        indices.add("Leadmini");
        indices.add("Zinc");
        indices.add("Zincmini");
        indices.add("Nickel");
        indices.add("Nickel M");
        indices.add("Natural Gas");
        indices.add("Mentha Oil");

        scripts.add("GOLD");
        scripts.add("GOLDM");
        scripts.add("SILVER");
        scripts.add("SILVERM");
        scripts.add("COPPER");
        scripts.add("COPPERM");
        scripts.add("ALUMINIUM");
        scripts.add("ALUMINI");
        scripts.add("CRUDEOIL");
        scripts.add("CRUDEOILM");
        scripts.add("LEAD");
        scripts.add("LEADMINI");
        scripts.add("ZINC");
        scripts.add("ZINCMINI");
        scripts.add("NICKEL");
        scripts.add("NICKELM");
        scripts.add("NATURALGAS");
        scripts.add("MENTHAOIL");

        getMCXValues();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }
        commoList.setAdapter(new GeneralAdapter(mContext, "commodity", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
        super.onPostExecute(aVoid);
    }

    private void getMCXValues() {
        try {
            Document doc = Jsoup.parse(new URL(COMMODITY_RESOURCE_WEB), 10000);
            for (String scr : scripts) {
                Elements topicList = doc.select("tr[id=" + scr + "]").select("td");
                ltps.add(topicList.get(1).text().trim());
                changes.add(topicList.get(2).text().trim() + " (" + make2Decimal(topicList.get(3).text().trim()) + "%)");
                if (topicList.get(2).text().contains("-"))
                    upsNDowns.add("down");
                else
                    upsNDowns.add("up");
            }
        } catch (IOException e) {
            //Log.e("important","exception "+e.getMessage());
        }
    }

    private String make2Decimal(String number) {
        if (number.equals(""))
            return "0";
        if (!number.contains(".")) {
            return number + ".00";
        } else {
            String integer = number.split("\\.")[0];
            String decimal = number.split("\\.")[1];
            if (decimal.length() == 1)
                return integer + "." + decimal + "0";
            if (decimal.length() == 2)
                return integer + "." + decimal;
            else {
                decimal = decimal.substring(0, 2);
                return integer + "." + decimal;
            }
        }
    }
}
