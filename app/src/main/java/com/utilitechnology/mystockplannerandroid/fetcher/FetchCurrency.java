package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.adapter.GeneralAdapter;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.parser.ParseCustomXML;
import com.utilitechnology.mystockplannerandroid.parser.ParseWebCurrency;
import com.utilitechnology.mystockplannerandroid.parser.ParseWebYahoo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 15-04-2016.
 */
public class FetchCurrency extends AsyncTask<Void,Void,Void> {

    View mView;
    Context mContext;
    ListView currencyList;

//    String CURRENCY_RESOURCE_XML="https://finance.yahoo.com/webservice/v1/symbols/gbpinr=x,usdinr=x,inrjpy=x,eurinr=x,chfinr=x,cadinr=x,audinr=x,zarinr=x,cnyinr=x,hkdinr=x,inrrub=x,brlinr=x,inridr=x,inrkrw=x,myrinr=x,sgdinr=x,kwdinr=x,mxninr=x,sarinr=x/quote?format=xml&view=detail";

    ProgressDialog progress;
    FetchCurrency asyncCountdown;

    private List<String> indices = new ArrayList<>();
    private List<String> scripts = new ArrayList<>();
    private List<String> ltps = new ArrayList<>();
    private List<String> changes = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

//    ParseXML prx;
//    ParseWebCurrency prw;
    ParseCustomXML pcx;

    public FetchCurrency(View view)
    {
        mView=view;
        mContext=mView.getContext();
        currencyList= (ListView) mView.findViewById(R.id.currencyList);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        super.onPreExecute();
        asyncCountdown = this;
        new CountDownTimer(60000, 60000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }
            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    indices.clear();
                    scripts.clear();
                    ltps.clear();
                    changes.clear();
                    upsNDowns.clear();

                    indices.add("Connection aborted");
                    scripts.add("");
                    ltps.add("");
                    changes.add("");
                    upsNDowns.add("up");

                    currencyList.setAdapter(new GeneralAdapter(mContext, "currency", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Important: Do not change this order
        indices.add("US Dollar (USD/INR)");
        indices.add("UK Pound (GBP/INR)");
        indices.add("Japanese Yen (INR/JPY)");
        indices.add("Euro (EUR/INR)");
        indices.add("Switzerland Franc (CHF/INR)");
        indices.add("Canada Dollar (CAD/INR)");
        indices.add("Australia Dollar (AUD/INR)");
        indices.add("South Africa Rand (ZAR/INR)");
        indices.add("China Yuan Renminbi (CNY/INR)");
        indices.add("Hong Kong Dollar (HKD/INR)");
        indices.add("Russia Ruble (INR/RUB)");
        indices.add("Brazil Real (BRL/INR)");
        indices.add("Indonesia Rupiah (INR/IDR)");
        indices.add("South Korea Won (INR/KRW)");
        indices.add("Malaysia Ringgit (MYR/INR)");
        indices.add("Singapore Dollar (SGD/INR)");
        indices.add("Kuwait Dinar (KWD/INR)");
        indices.add("Mexico Peso (MXN/INR)");
        indices.add("Saudi Arabia Riyal (SAR/INR)");

        scripts.add("usdinr");
        scripts.add("gbpinr");
        scripts.add("inrjpy");
        scripts.add("eurinr");
        scripts.add("chfinr");
        scripts.add("cadinr");
        scripts.add("audinr");
        scripts.add("zarinr");
        scripts.add("cnyinr");
        scripts.add("hkdinr");
        scripts.add("inrrub");
        scripts.add("brlinr");
        scripts.add("inridr");
        scripts.add("inrkrw");
        scripts.add("myrinr");
        scripts.add("sgdinr");
        scripts.add("kwdinr");
        scripts.add("mxninr");
        scripts.add("sarinr");

//        parseCurrencyValues();
//        getCurrencyValues();
        parseCustomCurrencyValues();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }

        currencyList.setAdapter(new GeneralAdapter(mContext, "currency", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));

        super.onPostExecute(aVoid);
    }


//    private void getCurrencyValues()
//    {
//        prw = new ParseWebCurrency(scripts);
//        prw.fetchWeb();
//        ltps = prw.getPrice();
//        changes = prw.getChangeString();
//        upsNDowns = prw.getUpsNDowns();
//    }

    private void parseCustomCurrencyValues()
    {
        pcx = new ParseCustomXML("currency", null);
        pcx.fetchXML();
        while (pcx.parsingInComplete) ;
        ltps = pcx.getPrice();
        changes = pcx.getChangeString();
        upsNDowns = pcx.getUpsNDowns();
    }

//    private void parseCurrencyValues() {
//        prx = new ParseXML(CURRENCY_RESOURCE_XML);
//        prx.fetchXML();
//        while (prx.parsingInComplete) ;
//        ltps = prx.getPrice();
//        changes = prx.getChangeString();
//        upsNDowns = prx.getUpsNDowns();
//
//    }
}
