package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.handler.HandleEquity;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;

import java.util.Locale;

/**
 * Created by Bibaswann on 12-04-2016.
 */
public class FetchEquityOverview extends AsyncTask<Void, Void, Void> {

    View view;
    String unconvertedScriptWithExtension;
    private String stockNow, stockChangeWithPercent, stockLastUpdate, stockUp;
    private TextView stockNameLbl;
    private TextView stockPriceLbl, stockChangeLbl;
    private String open, PrevClose, volume, dayHigh, dayLow, yrHigh, yrLow, pe;
    private TextView openLbl, prevLbl, dhighLbl, dlowLbl, yhighLbl, ylowLbl, volLbl, peLbl;
    private TextView lastUpdate;
    private ImageView arrow;
    HandleEquity heq;
    ScriptHelper sch;
    Context mContext;
    String companyName;

    public FetchEquityOverview(View v, String script) {
        view = v;
        mContext = view.getContext();
        unconvertedScriptWithExtension = script;
        sch = new ScriptHelper(mContext);

        stockNameLbl = (TextView) view.findViewById(R.id.stocknamelbl);
        stockPriceLbl = (TextView) view.findViewById(R.id.stockPriceLbl);
        stockChangeLbl = (TextView) view.findViewById(R.id.stockchangeLbl);
        openLbl = (TextView) view.findViewById(R.id.openval);
        prevLbl = (TextView) view.findViewById(R.id.prval);
        dhighLbl = (TextView) view.findViewById(R.id.dhval);
        dlowLbl = (TextView) view.findViewById(R.id.dlval);
        yhighLbl = (TextView) view.findViewById(R.id.yhval);
        ylowLbl = (TextView) view.findViewById(R.id.ylval);
        volLbl = (TextView) view.findViewById(R.id.vlvl);
        peLbl = (TextView) view.findViewById(R.id.pevl);
        lastUpdate = (TextView) view.findViewById(R.id.sLastUpdate);
        arrow= (ImageView) view.findViewById(R.id.equityArrow);
    }

    @Override
    protected Void doInBackground(Void... params) {
        heq = new HandleEquity(mContext, unconvertedScriptWithExtension);
        stockNow = convertFloatToIndianString(heq.getLTP());
        stockChangeWithPercent = heq.getChange();
        stockUp = heq.getStockUp();
        open = heq.getOpen();
        PrevClose = heq.getPrevClose();
        volume = heq.getVolume();
        dayHigh = heq.getDayHigh();
        dayLow = heq.getDayLow();
        yrHigh = heq.getYrHigh();
        yrLow = heq.getYrLow();
        pe = heq.getPeRatio();
        stockLastUpdate = heq.getLastUpdate();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        companyName = sch.scriptToCompany(unconvertedScriptWithExtension);
        if (unconvertedScriptWithExtension.contains(".ns"))
            stockNameLbl.setText(String.format("%s [NSE]", companyName));
        else
            stockNameLbl.setText(String.format("%s [BSE]", companyName));
        stockPriceLbl.setText(stockNow);
        stockChangeLbl.setText(String.format("%s", stockChangeWithPercent.replace("+","").replace("-","")));

        if(stockUp!=null) {
            if (stockUp.equals("up")) {
                stockPriceLbl.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
                stockChangeLbl.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
                arrow.setImageResource(R.drawable.up);
            } else {
                stockPriceLbl.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
                stockChangeLbl.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
                arrow.setImageResource(R.drawable.down);
            }
        }

        openLbl.setText(open);
        prevLbl.setText(PrevClose);
        dhighLbl.setText(dayHigh);
        dlowLbl.setText(dayLow);
        yhighLbl.setText(yrHigh);
        ylowLbl.setText(yrLow);
        volLbl.setText(volume);
        peLbl.setText(pe);
        lastUpdate.setText(stockLastUpdate);

        super.onPostExecute(aVoid);
    }

    private String convertFloatToIndianString(float input) {
        return String.format(new Locale("en", "IN"), "%.2f", input);
    }
}
