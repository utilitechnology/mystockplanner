package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.handler.HandleEquityStat;
import com.utilitechnology.mystockplannerandroid.R;

/**
 * Created by Bibaswann on 14-04-2016.
 */
public class FetchEquityStat extends AsyncTask<Void, Void, Void> {

    View mView;
    Context mContext;
    String unconvertedScriptWithExtension;
    HandleEquityStat hes;

    TextView marketCapTxt, enterpriceValTxt, trailingPeTxt, forwardPeTxt, pegRatioTxt, priceSalesTxt, priceBookTxt, profitMarginTxt, operatingMarginTxt, revenueTxt, revenueShareTxt, quarterlyRevenueGrowthTxt, grossProfitTxt, quarterlyEarningGrowthTxt, totalCashTxt, totalDebtTxt, yearChangeTxt, averageVolTxt, shareOutstandingTxt, floatValTxt, annualDividendTxt;

    String marketCap, enterpriceVal, trailingPe, forwardPe, pegRatio, priceSales, priceBook, profitMargin, operatingMargin, revenue, revenueShare, quarterlyRevenueGrowth, grossProfit, quarterlyEarningGrowth, totalCash, totalDebt, yearChange, averageVol, shareOutstanding, floatVal, annualDividend;

    public FetchEquityStat(View v, String script) {
        mView = v;
        mContext = mView.getContext();
        unconvertedScriptWithExtension = script;

        marketCapTxt = (TextView) mView.findViewById(R.id.mcapval);
        enterpriceValTxt = (TextView) mView.findViewById(R.id.entprval);
        trailingPeTxt = (TextView) mView.findViewById(R.id.tpeval);
        forwardPeTxt = (TextView) mView.findViewById(R.id.fwpeval);
        pegRatioTxt = (TextView) mView.findViewById(R.id.pegval);
        priceSalesTxt = (TextView) mView.findViewById(R.id.psval);
        priceBookTxt = (TextView) mView.findViewById(R.id.pbvl);
        profitMarginTxt = (TextView) mView.findViewById(R.id.prmvl);
        operatingMarginTxt = (TextView) mView.findViewById(R.id.opmvl);
        revenueTxt = (TextView) mView.findViewById(R.id.rvvl);
        revenueShareTxt = (TextView) mView.findViewById(R.id.rpsvl);
        quarterlyRevenueGrowthTxt = (TextView) mView.findViewById(R.id.qrgvl);
        grossProfitTxt = (TextView) mView.findViewById(R.id.gpvl);
        quarterlyEarningGrowthTxt = (TextView) mView.findViewById(R.id.qegvl);
        totalCashTxt = (TextView) mView.findViewById(R.id.tcvl);
        totalDebtTxt = (TextView) mView.findViewById(R.id.tdvl);
        yearChangeTxt = (TextView) mView.findViewById(R.id.ycvl);
        averageVolTxt = (TextView) mView.findViewById(R.id.avvl);
        shareOutstandingTxt = (TextView) mView.findViewById(R.id.sovl);
        floatValTxt = (TextView) mView.findViewById(R.id.flvl);
        annualDividendTxt = (TextView) mView.findViewById(R.id.dyvl);
    }

    @Override
    protected Void doInBackground(Void... params) {
        hes = new HandleEquityStat(mContext, unconvertedScriptWithExtension);

        marketCap = hes.getMarketCap();
        enterpriceVal = hes.getEnterpriceVal();
        trailingPe = hes.getTrailingPe();
        forwardPe = hes.getForwardPe();
        pegRatio = hes.getPegRatio();
        priceSales = hes.getPriceSales();
        priceBook = hes.getPriceBook();
        profitMargin = hes.getProfitMargin();
        operatingMargin = hes.getOperatingMargin();
        revenue = hes.getRevenue();
        revenueShare = hes.getRevenueShare();
        quarterlyRevenueGrowth = hes.getQuarterlyRevenueGrowth();
        grossProfit = hes.getGrossProfit();
        quarterlyEarningGrowth = hes.getQuarterlyEarningGrowth();
        totalCash = hes.getTotalCash();
        totalDebt = hes.getTotalDebt();
        yearChange = hes.getYearChange();
        averageVol = hes.getAverageVol();
        shareOutstanding = hes.getShareOutstanding();
        floatVal = hes.getFloatVal();
        annualDividend = hes.getAnnualDividend();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        marketCapTxt.setText(marketCap);
        enterpriceValTxt.setText(enterpriceVal);
        trailingPeTxt.setText(trailingPe);
        forwardPeTxt.setText(forwardPe);
        pegRatioTxt.setText(pegRatio);
        priceSalesTxt.setText(priceSales);
        priceBookTxt.setText(priceBook);
        profitMarginTxt.setText(profitMargin);
        operatingMarginTxt.setText(operatingMargin);
        revenueTxt.setText(revenue);
        revenueShareTxt.setText(revenueShare);
        quarterlyRevenueGrowthTxt.setText(quarterlyRevenueGrowth);
        grossProfitTxt.setText(grossProfit);
        quarterlyEarningGrowthTxt.setText(quarterlyEarningGrowth);
        totalCashTxt.setText(totalCash);
        totalDebtTxt.setText(totalDebt);
        yearChangeTxt.setText(yearChange);
        averageVolTxt.setText(averageVol);
        shareOutstandingTxt.setText(shareOutstanding);
        floatValTxt.setText(floatVal);
        annualDividendTxt.setText(annualDividend);

        super.onPostExecute(aVoid);
    }
}
