package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.handler.HandleEquitySummary;
import com.utilitechnology.mystockplannerandroid.R;

/**
 * Created by Bibaswann on 14-04-2016.
 */
public class FetchEquitySummary extends AsyncTask<Void, Void, Void> {

    View mView;
    Context mContext;
    String unconvertedScriptWithExtension;
    TextView summaryTxt;
    HandleEquitySummary hem;
    String summaryBody;

    public FetchEquitySummary(View view, String script) {
        mView = view;
        mContext = mView.getContext();
        unconvertedScriptWithExtension = script;

        summaryTxt = (TextView) mView.findViewById(R.id.summerytxt);
    }

    @Override
    protected Void doInBackground(Void... params) {
        hem = new HandleEquitySummary(mContext, unconvertedScriptWithExtension);
        summaryBody = hem.getSummaryBody();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        summaryTxt.setText(summaryBody);
        super.onPostExecute(aVoid);
    }
}
