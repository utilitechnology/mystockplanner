package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.EquityDetailsContainerActivity;
import com.utilitechnology.mystockplannerandroid.caching.MoverCachingAPI;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Bibaswann on 12-05-2016.
 */
public class FetchHeatmapFromFile extends AsyncTask<Void, Void, Void> {
    Context mContext;
    GridView map;
    GridLayout mapLayout;
    TextView heatHead;
    String heatHeadText;
    MoverCachingAPI mca;
    SettingsAPI set;

    private List<String> companiesG = new ArrayList<>();
    private List<String> scriptsG = new ArrayList<>();
    private List<String> ltpsG = new ArrayList<>();
    private List<String> changesG = new ArrayList<>();
    private List<String> upsNDownsG = new ArrayList<>();

    private List<String> companiesL = new ArrayList<>();
    private List<String> scriptsL = new ArrayList<>();
    private List<String> ltpsL = new ArrayList<>();
    private List<String> changesL = new ArrayList<>();
    private List<String> upsNDownsL = new ArrayList<>();

    private List<String> companiesH;
    private List<String> scriptsH;
    private List<String> ltpsH;
    private List<String> changesH;
    private List<String> upsNDownsH;

    public FetchHeatmapFromFile(View v) {
        mContext = v.getContext();
        map = (GridView) v.findViewById(R.id.heatGrid);
        mapLayout = (GridLayout) v.findViewById(R.id.heatGridLayout);
        heatHead = (TextView) v.findViewById(R.id.htHead);
        set = new SettingsAPI(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            //This is how you solve accidental array change problem
            if (set.readSetting("currentIndex").equals("^nsei")) {
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("nsegainer")) {
                    companiesG = mca.getCompaniesToReturn();
                    scriptsG = mca.getScriptsToReturn();
                    ltpsG = mca.getLTPsToReturn();
                    changesG = mca.getChangesToReturn();
                    upsNDownsG = mca.getUpsDownsToReturn();
                }
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("nseloser")) {
                    companiesL = mca.getCompaniesToReturn();
                    scriptsL = mca.getScriptsToReturn();
                    ltpsL = mca.getLTPsToReturn();
                    changesL = mca.getChangesToReturn();
                    upsNDownsL = mca.getUpsDownsToReturn();
                }
                heatHeadText = mContext.getString(R.string.nse_heatmap_head);
                fillHeatList();
            } else {
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("bsegainer")) {
                    companiesG = mca.getCompaniesToReturn();
                    scriptsG = mca.getScriptsToReturn();
                    ltpsG = mca.getLTPsToReturn();
                    changesG = mca.getChangesToReturn();
                    upsNDownsG = mca.getUpsDownsToReturn();
                    //Log.e("important","gainer size is "+companiesG.size());
                }
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("bseloser")) {
                    companiesL = mca.getCompaniesToReturn();
                    scriptsL = mca.getScriptsToReturn();
                    ltpsL = mca.getLTPsToReturn();
                    changesL = mca.getChangesToReturn();
                    upsNDownsL = mca.getUpsDownsToReturn();
                    //Log.e("important","gainer size is "+companiesG.size()+" and loser size is "+companiesL.size());
                }
                heatHeadText = mContext.getString(R.string.bse_heatmap_head);
                fillHeatList();
            }
        } catch (Exception ex) {
            //Log.e("important", Log.getStackTraceString(ex));
            companiesL.clear();
            scriptsL.clear();
            ltpsL.clear();
            changesL.clear();
            upsNDownsL.clear();
            companiesL.add("No items here");
            scriptsL.add("");
            ltpsL.add("");
            changesL.add("");
            upsNDownsL.add("up");

            companiesG.clear();
            scriptsG.clear();
            ltpsG.clear();
            changesG.clear();
            upsNDownsG.clear();
            companiesG.add("No items here");
            scriptsG.add("");
            ltpsG.add("");
            changesG.add("");
            upsNDownsG.add("up");

            companiesH = new ArrayList<>();
            scriptsH = new ArrayList<>();
            ltpsH = new ArrayList<>();
            changesH = new ArrayList<>();
            upsNDownsH = new ArrayList<>();
            companiesH.add("No items here");
            scriptsH.add("");
            ltpsH.add("");
            changesH.add("+0.00 +0.00");
            upsNDownsH.add("up");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        heatHead.setText(heatHeadText);

        mapLayout.removeAllViews();
        for (final String scr : scriptsH) {
            final int heatIndex = scriptsH.indexOf(scr);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mapLayout.getWidth() / 5, mapLayout.getWidth() / 5);
            GridLayout.LayoutParams params = new GridLayout.LayoutParams(new LinearLayout.LayoutParams(mapLayout.getWidth() / 5, mapLayout.getWidth() / 5));
            LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.heatmap_item, mapLayout, false);
            ll.setLayoutParams(params);

            final TextView name = (TextView) ll.findViewById(R.id.htLbl);
            final TextView price = (TextView) ll.findViewById(R.id.htVal);
            final TextView change = (TextView) ll.findViewById(R.id.htChange);

            name.setText(companiesH.get(heatIndex));
            price.setText(ltpsH.get(heatIndex));
            change.setText(changesH.get(heatIndex));

            float absoluteChange;
            if (upsNDownsH.get(heatIndex).equals("up")) {
                try {
                    absoluteChange = Float.parseFloat(changesH.get(heatIndex).replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace("%", "").split(" ")[1]);
                } catch (IndexOutOfBoundsException ignored) {
                    absoluteChange = 0;
                }
                if (absoluteChange == 0)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
                else if (absoluteChange >= 0 && absoluteChange <= 1)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_0));
                else if (absoluteChange >= 1 && absoluteChange <= 2)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_1));
                else if (absoluteChange >= 2 && absoluteChange <= 3)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_2));
                else if (absoluteChange >= 3 && absoluteChange <= 4)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_3));
                else if (absoluteChange >= 4 && absoluteChange <= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_4));
                else if (absoluteChange >= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_5));
            } else {
                try {
                    absoluteChange = Float.parseFloat(changesH.get(heatIndex).replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace("%", "").split(" ")[1]);
                } catch (IndexOutOfBoundsException ignored) {
                    absoluteChange = 0;
                }
                if (absoluteChange == 0)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
                else if (absoluteChange >= 0 && absoluteChange <= 1)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_0));
                else if (absoluteChange >= 1 && absoluteChange <= 2)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_1));
                else if (absoluteChange >= 2 && absoluteChange <= 3)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_2));
                else if (absoluteChange >= 3 && absoluteChange <= 4)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_3));
                else if (absoluteChange >= 4 && absoluteChange <= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_4));
                else if (absoluteChange >= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_5));
            }

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!scr.split("\\.")[0].equals("null")) {
                        if (companiesH.get(heatIndex).equals("No items here")) {
                        } else if (companiesH.get(heatIndex).equals("Connection aborted")) {
                        } else {
                            name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                            Intent details = new Intent(mContext, EquityDetailsContainerActivity.class);
                            details.putExtra("script", scr);
                            mContext.startActivity(details);
                        }
                    }
                }
            });
            mapLayout.addView(ll);
        }

        //Map is for adapter, which for some obscure reason, is not working (Gridview scroll problem)
        //If needed to activate, change the changesH in exception part
        //map.setAdapter(new HeatmapAdapter(mContext, scriptsH.toArray(new String[scriptsH.size()]), companiesH.toArray(new String[companiesH.size()]), ltpsH.toArray(new String[ltpsH.size()]), changesH.toArray(new String[changesH.size()]), upsNDownsH.toArray(new String[upsNDownsH.size()])));

        super.onPostExecute(aVoid);
    }

    private void fillHeatList() {
        companiesH = new ArrayList<>(companiesG);
        scriptsH = new ArrayList<>(scriptsG);
        ltpsH = new ArrayList<>(ltpsG);
        changesH = new ArrayList<>(changesG);
        upsNDownsH = new ArrayList<>(upsNDownsG);

        Collections.reverse(companiesL);
        Collections.reverse(scriptsL);
        Collections.reverse(ltpsL);
        Collections.reverse(changesL);
        Collections.reverse(upsNDownsL);

        companiesH.addAll(companiesL);
        scriptsH.addAll(scriptsL);
        ltpsH.addAll(ltpsL);
        changesH.addAll(changesL);
        upsNDownsH.addAll(upsNDownsL);
    }
}
