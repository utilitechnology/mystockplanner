package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.activity.EquityDetailsContainerActivity;
import com.utilitechnology.mystockplannerandroid.caching.MoverCachingAPI;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseCustomXML;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 11-05-2016.
 */
public class FetchHeatmapOnline extends AsyncTask<Void, Void, Void> {
    Context mContext;
    GridView map;
    GridLayout mapLayout;
    TextView heatHead;
    String heatHeadText;
    SettingsAPI set;
    ScriptHelper sch;
    ParseCustomXML pcx;
    MoverCachingAPI mca;
    List<String> companies, scripts, ltps, changes;
    List<Float> realChangePercentList;
    List<Float> realChangePercentListFixed;

    String[] b30 = {"adaniports.bo", "asianpaint.bo", "axisbank.bo", "bajaj-auto.bo", "bhartiartl.bo", "bhel.bo", "cipla.bo", "coalindia.bo", "drreddy.bo", "gail.bo", "hdfc.bo", "hdfcbank.bo", "heromotoco.bo", "hindunilvr.bo", "icicibank.bo", "infy.bo", "itc.bo", "lt.bo", "lupin.bo", "m&m.bo", "maruti.bo", "ntpc.bo", "ongc.bo", "reliance.bo", "sbin.bo", "sunpharma.bo", "tatamotors.bo", "tatasteel.bo", "tcs.bo", "wipro.bo"};

    String[] n50 = {"ACC.NS","ADANIPORTS.NS","AMBUJACEM.NS","ASIANPAINT.NS","AUROPHARMA.NS","AXISBANK.NS","BAJAJ-AUTO.NS","BANKBARODA.NS","BHARTIARTL.NS","INFRATEL.NS","BOSCHLTD.NS","BPCL.NS","CIPLA.NS","COALINDIA.NS","DRREDDY.NS","EICHERMOT.NS","GAIL.NS","HCLTECH.NS","HDFC.NS","HDFCBANK.NS","HEROMOTOCO.NS","HINDALCO.NS","HINDUNILVR.NS","ICICIBANK.NS","IBULHSGFIN.NS","IOC.NS","INDUSINDBK.NS","ITC.NS","INFY.NS","KOTAKBANK.NS","LT.NS","LUPIN.NS","M%26M.NS","MARUTI.NS","NTPC.NS","ONGC.NS","POWERGRID.NS","RELIANCE.NS","SBIN.NS","SUNPHARMA.NS","TCS.NS","TATAMOTORS.NS","TATAPOWER.NS","TATASTEEL.NS","TECHM.NS","ULTRACEMCO.NS","VEDL.NS","WIPRO.NS","YESBANK.NS","ZEEL.NS"};

//    private final String YAHOO_BSE_RESOURCE = "http://finance.yahoo.com/webservice/v1/symbols/adaniports.bo,asianpaint.bo,axisbank.bo,bajaj-auto.bo,bhartiartl.bo,bhel.bo,cipla.bo,coalindia.bo,drreddy.bo,gail.bo,hdfc.bo,hdfcbank.bo,heromotoco.bo,hindunilvr.bo,icicibank.bo,infy.bo,itc.bo,lt.bo,lupin.bo,m&m.bo,maruti.bo,ntpc.bo,ongc.bo,reliance.bo,sbin.bo,sunpharma.bo,tatamotors.bo,tatasteel.bo,tcs.bo,wipro.bo/quote?format=xml&mView=detail";
//
//    private final String YAHOO_NSE_RESOURCE = "http://finance.yahoo.com/webservice/v1/symbols/acc.ns,adaniports.ns,ambujacem.ns,asianpaint.ns,axisbank.ns,bajaj-auto.ns,bankbaroda.ns,bhel.ns,bpcl.ns,bhartiartl.ns,boschltd.ns,cairn.ns,cipla.ns,coalindia.ns,drreddy.ns,gail.ns,grasim.ns,hcltech.ns,hdfcbank.ns,heromotoco.ns,hindalco.ns,hindunilvr.ns,hdfc.ns,itc.ns,icicibank.ns,idea.ns,indusindbk.ns,infy.ns,kotakbank.ns,lt.ns,lupin.ns,m&m.ns,maruti.ns,ntpc.ns,ongc.ns,powergrid.ns,pnb.ns,reliance.ns,sbin.ns,sunpharma.ns,tcs.ns,tatamotors.ns,tatapower.ns,tatasteel.ns,techm.ns,ultracemco.ns,vedl.ns,wipro.ns,yesbank.ns,zeel.ns/quote?format=xml&mView=detail";

    ProgressDialog progress;

    private List<String> companiesG = new ArrayList<>();
    private List<String> scriptsG = new ArrayList<>();
    private List<String> ltpsG = new ArrayList<>();
    private List<String> changesG = new ArrayList<>();
    private List<String> upsNDownsG = new ArrayList<>();

    private List<String> companiesL = new ArrayList<>();
    private List<String> scriptsL = new ArrayList<>();
    private List<String> ltpsL = new ArrayList<>();
    private List<String> changesL = new ArrayList<>();
    private List<String> upsNDownsL = new ArrayList<>();

    private List<String> companiesH;
    private List<String> scriptsH;
    private List<String> ltpsH;
    private List<String> changesH;
    private List<String> upsNDownsH;

    FetchHeatmapOnline asyncCountdown;

    public FetchHeatmapOnline(View v) {
        mContext = v.getContext();

        map = (GridView) v.findViewById(R.id.heatGrid);
        heatHead = (TextView) v.findViewById(R.id.htHead);
        mapLayout = (GridLayout) v.findViewById(R.id.heatGridLayout);

        companies = new ArrayList<>();
        scripts = new ArrayList<>();
        ltps = new ArrayList<>();
        changes = new ArrayList<>();
        realChangePercentList = new ArrayList<>();
        realChangePercentListFixed = new ArrayList<>();

        set = new SettingsAPI(mContext);
        sch = new ScriptHelper(mContext);
        mca = new MoverCachingAPI(mContext);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        super.onPreExecute();

        asyncCountdown = this;
        new CountDownTimer(60000, 60000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    companiesG.clear();
                    scriptsG.clear();
                    ltpsG.clear();
                    changesG.clear();
                    upsNDownsG.clear();
                    companiesL.clear();
                    scriptsL.clear();
                    ltpsL.clear();
                    changesL.clear();
                    upsNDownsL.clear();

                    companiesG.add("Connection aborted");
                    scriptsG.add("");
                    ltpsG.add("");
                    changesG.add("");
                    upsNDownsG.add("");

                    companiesL.add("Connection aborted");
                    scriptsL.add("");
                    ltpsL.add("");
                    changesL.add("");
                    upsNDownsL.add("up");

                    companiesH = new ArrayList<>();
                    scriptsH = new ArrayList<>();
                    ltpsH = new ArrayList<>();
                    changesH = new ArrayList<>();
                    upsNDownsH = new ArrayList<>();

                    companiesH.add("No items here");
                    scriptsH.add("");
                    ltpsH.add("");
                    changesH.add("+0.00 +0.00");
                    upsNDownsH.add("up");
                    companiesH.add("No items here");
                    scriptsH.add("");
                    ltpsH.add("");
                    changesH.add("+0.00 +0.00");
                    upsNDownsH.add("down");

                    heatHead.setText(heatHeadText);
                    mapLayout.removeAllViews();

//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mapLayout.getWidth() / 5, mapLayout.getWidth() / 5);
                    GridLayout.LayoutParams params = new GridLayout.LayoutParams(new LinearLayout.LayoutParams(mapLayout.getWidth() / 5, mapLayout.getWidth() / 5));
                    LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.heatmap_item, mapLayout, false);
                    ll.setLayoutParams(params);

                    final TextView name = (TextView) ll.findViewById(R.id.htLbl);
                    final TextView price = (TextView) ll.findViewById(R.id.htVal);
                    final TextView change = (TextView) ll.findViewById(R.id.htChange);

                    name.setText(companiesH.get(0));
                    price.setText(ltpsH.get(0));
                    change.setText(changesH.get(0));

                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));

                    mapLayout.addView(ll);

                    //map.setAdapter(new HeatmapAdapter(mContext, scriptsH.toArray(new String[scriptsH.size()]), companiesH.toArray(new String[companiesH.size()]), ltpsH.toArray(new String[ltpsH.size()]), changesH.toArray(new String[changesH.size()]), upsNDownsH.toArray(new String[upsNDownsH.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            if (set.readSetting("currentIndex").equals("^nsei")) {
                filList("nse");
                getGainerList();
                getLoserList();
                mca.saveNSEData(companiesG, scriptsG, ltpsG, changesG, upsNDownsG, companiesL, scriptsL, ltpsL, changesL, upsNDownsL);
                set.addUpdateSettings("nseMoverTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
                heatHeadText = mContext.getString(R.string.nse_heatmap_head);
                fillHeatList();
            } else {
                filList("bse");
                getGainerList();
                getLoserList();
                mca.saveBSEData(companiesG, scriptsG, ltpsG, changesG, upsNDownsG, companiesL, scriptsL, ltpsL, changesL, upsNDownsL);
                set.addUpdateSettings("bseMoverTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
                heatHeadText = mContext.getString(R.string.bse_heatmap_head);
                fillHeatList();
            }
        } catch (Exception ex) {
//            Log.e("important", Log.getStackTraceString(ex));
            companiesL.clear();
            scriptsL.clear();
            ltpsL.clear();
            changesL.clear();
            upsNDownsL.clear();
            companiesL.add("No items here");
            scriptsL.add("");
            ltpsL.add("");
            changesL.add("");
            upsNDownsL.add("up");

            companiesG.clear();
            scriptsG.clear();
            ltpsG.clear();
            changesG.clear();
            upsNDownsG.clear();
            companiesG.add("No items here");
            scriptsG.add("");
            ltpsG.add("");
            changesG.add("");
            upsNDownsG.add("up");

            companiesH = new ArrayList<>();
            scriptsH = new ArrayList<>();
            ltpsH = new ArrayList<>();
            changesH = new ArrayList<>();
            upsNDownsH = new ArrayList<>();
            companiesH.add("No items here");
            scriptsH.add("");
            ltpsH.add("");
            changesH.add("+0.00 +0.00");
            upsNDownsH.add("up");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }

        heatHead.setText(heatHeadText);

        mapLayout.removeAllViews();
        for (final String scr : scriptsH) {
            final int heatIndex = scriptsH.indexOf(scr);
            //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mapLayout.getWidth() / 5, mapLayout.getWidth() / 5);
            GridLayout.LayoutParams params = new GridLayout.LayoutParams(new LinearLayout.LayoutParams(mapLayout.getWidth() / 5, mapLayout.getWidth() / 5));
            LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.heatmap_item, mapLayout, false);
            ll.setLayoutParams(params);

            final TextView name = (TextView) ll.findViewById(R.id.htLbl);
            final TextView price = (TextView) ll.findViewById(R.id.htVal);
            final TextView change = (TextView) ll.findViewById(R.id.htChange);

            name.setText(companiesH.get(heatIndex));
            price.setText(ltpsH.get(heatIndex));
            change.setText(changesH.get(heatIndex));

            if (upsNDownsH.get(heatIndex).equals("up")) {
                float absoluteChange = Float.parseFloat(changesH.get(heatIndex).replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace("%", "").split(" ")[1]);
                if (absoluteChange == 0)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
                else if (absoluteChange >= 0 && absoluteChange <= 1)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_0));
                else if (absoluteChange >= 1 && absoluteChange <= 2)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_1));
                else if (absoluteChange >= 2 && absoluteChange <= 3)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_2));
                else if (absoluteChange >= 3 && absoluteChange <= 4)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_3));
                else if (absoluteChange >= 4 && absoluteChange <= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_4));
                else if (absoluteChange >= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_up_5));
            } else {
                float absoluteChange = Float.parseFloat(changesH.get(heatIndex).replace("(", "").replace(")", "").replace("+", "").replace("-", "").replace("%", "").split(" ")[1]);
                if (absoluteChange == 0)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_nochange));
                else if (absoluteChange >= 0 && absoluteChange <= 1)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_0));
                else if (absoluteChange >= 1 && absoluteChange <= 2)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_1));
                else if (absoluteChange >= 2 && absoluteChange <= 3)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_2));
                else if (absoluteChange >= 3 && absoluteChange <= 4)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_3));
                else if (absoluteChange >= 4 && absoluteChange <= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_4));
                else if (absoluteChange >= 5)
                    ll.setBackgroundColor(ContextCompat.getColor(mContext, R.color.heatmap_down_5));
            }

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!scr.split("\\.")[0].equals("null")) {
                        if (companiesH.get(heatIndex).equals("No items here")) {
                        } else if (companiesH.get(heatIndex).equals("Connection aborted")) {
                        } else {
                            name.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                            Intent details = new Intent(mContext, EquityDetailsContainerActivity.class);
                            details.putExtra("script", scr);
                            mContext.startActivity(details);
                        }
                    }
                }
            });

            mapLayout.addView(ll);
        }


        //map.setAdapter(new HeatmapAdapter(mContext, scriptsH.toArray(new String[scriptsH.size()]), companiesH.toArray(new String[companiesH.size()]), ltpsH.toArray(new String[ltpsH.size()]), changesH.toArray(new String[changesH.size()]), upsNDownsH.toArray(new String[upsNDownsH.size()])));

        super.onPostExecute(aVoid);
    }

    private void fillHeatList() {
        companiesH = new ArrayList<>(companiesG);
        scriptsH = new ArrayList<>(scriptsG);
        ltpsH = new ArrayList<>(ltpsG);
        changesH = new ArrayList<>(changesG);
        upsNDownsH = new ArrayList<>(upsNDownsG);

        Collections.reverse(companiesL);
        Collections.reverse(scriptsL);
        Collections.reverse(ltpsL);
        Collections.reverse(changesL);
        Collections.reverse(upsNDownsL);

        companiesH.addAll(companiesL);
        scriptsH.addAll(scriptsL);
        ltpsH.addAll(ltpsL);
        changesH.addAll(changesL);
        upsNDownsH.addAll(upsNDownsL);
    }

    private void fillGainerLoserList() {
        companiesG.clear();
        scriptsG.clear();
        ltpsG.clear();
        changesG.clear();
        upsNDownsG.clear();

        companiesL.clear();
        scriptsL.clear();
        ltpsL.clear();
        changesL.clear();
        upsNDownsL.clear();

        List<String> unsortedCompaniesG = new ArrayList<>();
        List<String> unsortedScriptsG = new ArrayList<>();
        List<String> unsortedLtpsG = new ArrayList<>();
        List<String> unsortedChangesG = new ArrayList<>();
        List<Float> unsortedRealChangePercentListG = new ArrayList<>();
        List<Float> unsortedRealChangePercentListFixedG = new ArrayList<>();

        List<String> unsortedCompaniesL = new ArrayList<>();
        List<String> unsortedScriptsL = new ArrayList<>();
        List<String> unsortedLtpsL = new ArrayList<>();
        List<String> unsortedChangesL = new ArrayList<>();
        List<Float> unsortedRealChangePercentListL = new ArrayList<>();
        List<Float> unsortedRealChangePercentListFixedL = new ArrayList<>();

        int index;
        for (String oneScript : scripts) {
            index = scripts.indexOf(oneScript);
            if (realChangePercentList.get(index) > 0) {
                unsortedCompaniesG.add(companies.get(index));
                unsortedScriptsG.add(scripts.get(index));
                unsortedLtpsG.add(ltps.get(index));
                unsortedChangesG.add(changes.get(index));
                unsortedRealChangePercentListG.add(realChangePercentList.get(index));
                unsortedRealChangePercentListFixedG.add(realChangePercentList.get(index));
            } else {
                unsortedCompaniesL.add(companies.get(index));
                unsortedScriptsL.add(scripts.get(index));
                unsortedLtpsL.add(ltps.get(index));
                unsortedChangesL.add(changes.get(index));
                unsortedRealChangePercentListL.add(realChangePercentList.get(index));
                unsortedRealChangePercentListFixedL.add(realChangePercentList.get(index));
            }
        }

        Comparator com = Collections.reverseOrder();
        Collections.sort(unsortedRealChangePercentListG, com);

        int sortIndex;
        for (Float real : unsortedRealChangePercentListG) {
            sortIndex = unsortedRealChangePercentListFixedG.indexOf(real);

            companiesG.add(unsortedCompaniesG.get(sortIndex));
            scriptsG.add(unsortedScriptsG.get(sortIndex));
            ltpsG.add(unsortedLtpsG.get(sortIndex));
            changesG.add(unsortedChangesG.get(sortIndex));
            upsNDownsG.add("up");
        }

        Collections.sort(unsortedRealChangePercentListL);

        for (Float real : unsortedRealChangePercentListL) {
            sortIndex = unsortedRealChangePercentListFixedL.indexOf(real);

            companiesL.add(unsortedCompaniesL.get(sortIndex));
            scriptsL.add(unsortedScriptsL.get(sortIndex));
            ltpsL.add(unsortedLtpsL.get(sortIndex));
            changesL.add(unsortedChangesL.get(sortIndex));
            upsNDownsL.add("down");
        }
    }

    private void getGainerList() {
        int numOfPositiveItems = 0;
        for (Float real : realChangePercentList) {
            if (real >= 0)
                numOfPositiveItems++;
        }
        while (numOfPositiveItems > 0) {
            int indexToRemove = -1;
            Float highest = 0.0f;
            for (Float real : realChangePercentList) {
                if (real >= highest) {
                    highest = real;
                    indexToRemove = realChangePercentList.indexOf(real);
                    //Log.e("important","index of change "+highest+ " is "+index);
                }
            }
            if (indexToRemove != -1) {
                //Log.e("important","gainer found: "+companies.get(index)+" changes "+changes.get(index)+" index "+index);
                companiesG.add(companies.get(indexToRemove));
                scriptsG.add(scripts.get(indexToRemove));
                ltpsG.add(ltps.get(indexToRemove));
                changesG.add(changes.get(indexToRemove));
                upsNDownsG.add("up");

                realChangePercentList.remove(indexToRemove);
                companies.remove(indexToRemove);
                scripts.remove(indexToRemove);
                ltps.remove(indexToRemove);
                changes.remove(indexToRemove);

                numOfPositiveItems--;
            }
        }
    }

    private void getLoserList() {
        int numOfNegativeItems = 0;
        for (Float real : realChangePercentList) {
            if (real < 0)
                numOfNegativeItems++;
        }
        while (numOfNegativeItems > 0) {
            int indexToRemove = -1;
            Float lowest = 0.0f;
            for (Float real : realChangePercentList) {
                if (real <= lowest) {
                    lowest = real;
                    indexToRemove = realChangePercentList.indexOf(real);
                }
            }
            if (indexToRemove != -1) {
                //Log.e("important","loser found: "+companies.get(index));
                companiesL.add(companies.get(indexToRemove));
                scriptsL.add(scripts.get(indexToRemove));
                ltpsL.add(ltps.get(indexToRemove));
                changesL.add(changes.get(indexToRemove));
                upsNDownsL.add("down");

                realChangePercentList.remove(indexToRemove);
                companies.remove(indexToRemove);
                scripts.remove(indexToRemove);
                ltps.remove(indexToRemove);
                changes.remove(indexToRemove);

                numOfNegativeItems--;
            }
        }
    }

    private void filList(String index) {
        scripts.clear();
        companies.clear();
        ltps.clear();
        changes.clear();
        realChangePercentList.clear();
        realChangePercentListFixed.clear();
        if (index.equals("nse")) {
            for (String scr : n50) {
                scripts.add(scr);
                companies.add(sch.scriptToCompany(scr));
            }
            pcx = new ParseCustomXML("mover-nse",null);
        } else {
            for (String scr : b30) {
                scripts.add(sch.unconvertScript(scr));
                companies.add(sch.scriptToCompany(scr));
            }
            pcx = new ParseCustomXML("mover-bse",null);
        }

        pcx.fetchXML();
        while (pcx.parsingInComplete) ;
        for (String oneChange : pcx.getRealChangePercent()) {
            realChangePercentList.add(stringToIndianFloat(oneChange));
            realChangePercentListFixed.add(stringToIndianFloat(oneChange));
        }
        ltps = pcx.getPrice();
        changes = pcx.getChangeString();

//        for (String comp:companies) {
//            int i=0;
//            Log.e("important","company "+comp+" change_percent "+realChangePercentList.get(i));
//            i++;
//        }
    }

    private float stringToIndianFloat(String text) {
        NumberFormat format = NumberFormat.getInstance(new Locale("en", "IN"));
        float formattedFloat = 0.00f;
        try {
            Number number = format.parse(text);
            formattedFloat = number.floatValue();
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        //Log.e("important","returning "+formattedFloat);
        return formattedFloat;
    }
}
