package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.adapter.GeneralAdapter;
import com.utilitechnology.mystockplannerandroid.caching.MoverCachingAPI;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 12-04-2016.
 */
public class FetchMoversFromFile extends AsyncTask<Void, Void, Void> {

    Context mContext;
    ListView gainerList, loserList;
    TextView gainerHead, loserHead;
    String gainerHeadText, loserHeadText;
    MoverCachingAPI mca;
    SettingsAPI set;

    private List<String> companiesG = new ArrayList<>();
    private List<String> scriptsG = new ArrayList<>();
    private List<String> ltpsG = new ArrayList<>();
    private List<String> changesG = new ArrayList<>();
    private List<String> upsNDownsG = new ArrayList<>();

    private List<String> companiesL = new ArrayList<>();
    private List<String> scriptsL = new ArrayList<>();
    private List<String> ltpsL = new ArrayList<>();
    private List<String> changesL = new ArrayList<>();
    private List<String> upsNDownsL = new ArrayList<>();

    public FetchMoversFromFile(View v) {
        mContext = v.getContext();
        gainerList = (ListView) v.findViewById(R.id.gainerList);
        loserList = (ListView) v.findViewById(R.id.loserList);
        gainerHead = (TextView) v.findViewById(R.id.head1);
        loserHead = (TextView) v.findViewById(R.id.head2);
        set = new SettingsAPI(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            //This is how you solve accidental array change problem
            if (set.readSetting("currentIndex").equals("^nsei")) {
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("nsegainer")) {
                    companiesG = mca.getCompaniesToReturn();
                    scriptsG = mca.getScriptsToReturn();
                    ltpsG = mca.getLTPsToReturn();
                    changesG = mca.getChangesToReturn();
                    upsNDownsG = mca.getUpsDownsToReturn();
                }
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("nseloser")) {
                    companiesL = mca.getCompaniesToReturn();
                    scriptsL = mca.getScriptsToReturn();
                    ltpsL = mca.getLTPsToReturn();
                    changesL = mca.getChangesToReturn();
                    upsNDownsL = mca.getUpsDownsToReturn();
                }
                gainerHeadText = mContext.getString(R.string.nse_gainer_head);
                loserHeadText = mContext.getString(R.string.nse_loser_head);
            } else {
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("bsegainer")) {
                    companiesG = mca.getCompaniesToReturn();
                    scriptsG = mca.getScriptsToReturn();
                    ltpsG = mca.getLTPsToReturn();
                    changesG = mca.getChangesToReturn();
                    upsNDownsG = mca.getUpsDownsToReturn();
                    //Log.e("important","gainer size is "+companiesG.size());
                }
                mca = new MoverCachingAPI(mContext);
                if (mca.readMovers("bseloser")) {
                    companiesL = mca.getCompaniesToReturn();
                    scriptsL = mca.getScriptsToReturn();
                    ltpsL = mca.getLTPsToReturn();
                    changesL = mca.getChangesToReturn();
                    upsNDownsL = mca.getUpsDownsToReturn();
                    //Log.e("important","gainer size is "+companiesG.size()+" and loser size is "+companiesL.size());
                }
                gainerHeadText = mContext.getString(R.string.bse_gainer_head);
                loserHeadText = mContext.getString(R.string.bse_loser_head);
            }
        } catch (Exception ex) {
            //Log.e("important", Log.getStackTraceString(ex));
            companiesL.clear();
            scriptsL.clear();
            ltpsL.clear();
            changesL.clear();
            upsNDownsL.clear();
            companiesL.add("No items here");
            scriptsL.add("");
            ltpsL.add("");
            changesL.add("");
            upsNDownsL.add("up");

            companiesG.clear();
            scriptsG.clear();
            ltpsG.clear();
            changesG.clear();
            upsNDownsG.clear();
            companiesG.add("No items here");
            scriptsG.add("");
            ltpsG.add("");
            changesG.add("");
            upsNDownsG.add("up");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        gainerHead.setText(gainerHeadText);
        loserHead.setText(loserHeadText);

        gainerList.setAdapter(new GeneralAdapter(mContext, "movers", scriptsG.toArray(new String[scriptsG.size()]), companiesG.toArray(new String[companiesG.size()]), ltpsG.toArray(new String[ltpsG.size()]), changesG.toArray(new String[changesG.size()]), upsNDownsG.toArray(new String[upsNDownsG.size()])));

        loserList.setAdapter(new GeneralAdapter(mContext, "movers", scriptsL.toArray(new String[scriptsL.size()]), companiesL.toArray(new String[companiesL.size()]), ltpsL.toArray(new String[ltpsL.size()]), changesL.toArray(new String[changesL.size()]), upsNDownsL.toArray(new String[upsNDownsL.size()])));

        super.onPostExecute(aVoid);
    }
}
