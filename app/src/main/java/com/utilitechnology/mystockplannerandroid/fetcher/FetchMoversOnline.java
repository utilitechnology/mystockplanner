package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.adapter.GeneralAdapter;
import com.utilitechnology.mystockplannerandroid.caching.MoverCachingAPI;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseCustomXML;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 21-04-2016.
 */
public class FetchMoversOnline extends AsyncTask<Void, Void, Void> {

    Context mContext;
    ListView gainerList, loserList;
    TextView gainerHead, loserHead;
    String gainerHeadText, loserHeadText;
    SettingsAPI set;
    ScriptHelper sch;
    ParseCustomXML pcx;
    MoverCachingAPI mca;
    List<String> companies, scripts, ltps, changes;
    List<Float> realChangePercentList;
    List<Float> realChangePercentListFixed;

    String[] b30 = {"adaniports.bo", "asianpaint.bo", "axisbank.bo", "bajaj-auto.bo", "bhartiartl.bo", "bhel.bo", "cipla.bo", "coalindia.bo", "drreddy.bo", "gail.bo", "hdfc.bo", "hdfcbank.bo", "heromotoco.bo", "hindunilvr.bo", "icicibank.bo", "infy.bo", "itc.bo", "lt.bo", "lupin.bo", "m&m.bo", "maruti.bo", "ntpc.bo", "ongc.bo", "reliance.bo", "sbin.bo", "sunpharma.bo", "tatamotors.bo", "tatasteel.bo", "tcs.bo", "wipro.bo"};

    String[] n50 = {"ACC.NS","ADANIPORTS.NS","AMBUJACEM.NS","ASIANPAINT.NS","AUROPHARMA.NS","AXISBANK.NS","BAJAJ-AUTO.NS","BANKBARODA.NS","BHARTIARTL.NS","INFRATEL.NS","BOSCHLTD.NS","BPCL.NS","CIPLA.NS","COALINDIA.NS","DRREDDY.NS","EICHERMOT.NS","GAIL.NS","HCLTECH.NS","HDFC.NS","HDFCBANK.NS","HEROMOTOCO.NS","HINDALCO.NS","HINDUNILVR.NS","ICICIBANK.NS","IBULHSGFIN.NS","IOC.NS","INDUSINDBK.NS","ITC.NS","INFY.NS","KOTAKBANK.NS","LT.NS","LUPIN.NS","M%26M.NS","MARUTI.NS","NTPC.NS","ONGC.NS","POWERGRID.NS","RELIANCE.NS","SBIN.NS","SUNPHARMA.NS","TCS.NS","TATAMOTORS.NS","TATAPOWER.NS","TATASTEEL.NS","TECHM.NS","ULTRACEMCO.NS","VEDL.NS","WIPRO.NS","YESBANK.NS","ZEEL.NS"};

//    private final String YAHOO_BSE_RESOURCE = "http://finance.yahoo.com/webservice/v1/symbols/adaniports.bo,asianpaint.bo,axisbank.bo,bajaj-auto.bo,bhartiartl.bo,bhel.bo,cipla.bo,coalindia.bo,drreddy.bo,gail.bo,hdfc.bo,hdfcbank.bo,heromotoco.bo,hindunilvr.bo,icicibank.bo,infy.bo,itc.bo,lt.bo,lupin.bo,m&m.bo,maruti.bo,ntpc.bo,ongc.bo,reliance.bo,sbin.bo,sunpharma.bo,tatamotors.bo,tatasteel.bo,tcs.bo,wipro.bo/quote?format=xml&mView=detail";
//
//    private final String YAHOO_NSE_RESOURCE = "http://finance.yahoo.com/webservice/v1/symbols/acc.ns,adaniports.ns,ambujacem.ns,asianpaint.ns,axisbank.ns,bajaj-auto.ns,bankbaroda.ns,bhel.ns,bpcl.ns,bhartiartl.ns,boschltd.ns,cairn.ns,cipla.ns,coalindia.ns,drreddy.ns,gail.ns,grasim.ns,hcltech.ns,hdfcbank.ns,heromotoco.ns,hindalco.ns,hindunilvr.ns,hdfc.ns,itc.ns,icicibank.ns,idea.ns,indusindbk.ns,infy.ns,kotakbank.ns,lt.ns,lupin.ns,m&m.ns,maruti.ns,ntpc.ns,ongc.ns,powergrid.ns,pnb.ns,reliance.ns,sbin.ns,sunpharma.ns,tcs.ns,tatamotors.ns,tatapower.ns,tatasteel.ns,techm.ns,ultracemco.ns,vedl.ns,wipro.ns,yesbank.ns,zeel.ns/quote?format=xml&mView=detail";

    ProgressDialog progress;

    private List<String> companiesG = new ArrayList<>();
    private List<String> scriptsG = new ArrayList<>();
    private List<String> ltpsG = new ArrayList<>();
    private List<String> changesG = new ArrayList<>();
    private List<String> upsNDownsG = new ArrayList<>();

    private List<String> companiesL = new ArrayList<>();
    private List<String> scriptsL = new ArrayList<>();
    private List<String> ltpsL = new ArrayList<>();
    private List<String> changesL = new ArrayList<>();
    private List<String> upsNDownsL = new ArrayList<>();

    FetchMoversOnline asyncCountdown;

    public FetchMoversOnline(View v) {
        mContext = v.getContext();
        gainerList = (ListView) v.findViewById(R.id.gainerList);
        loserList = (ListView) v.findViewById(R.id.loserList);
        gainerHead = (TextView) v.findViewById(R.id.head1);
        loserHead = (TextView) v.findViewById(R.id.head2);

        companies = new ArrayList<>();
        scripts = new ArrayList<>();
        ltps = new ArrayList<>();
        changes = new ArrayList<>();
        realChangePercentList = new ArrayList<>();
        realChangePercentListFixed = new ArrayList<>();

        set = new SettingsAPI(mContext);
        sch = new ScriptHelper(mContext);
        mca=new MoverCachingAPI(mContext);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        super.onPreExecute();

        asyncCountdown = this;
        new CountDownTimer(60000, 60000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    companiesG.clear();
                    scriptsG.clear();
                    ltpsG.clear();
                    changesG.clear();
                    upsNDownsG.clear();
                    companiesL.clear();
                    scriptsL.clear();
                    ltpsL.clear();
                    changesL.clear();
                    upsNDownsL.clear();

                    companiesG.add("Connection aborted");
                    scriptsG.add("");
                    ltpsG.add("");
                    changesG.add("");
                    upsNDownsG.add("");

                    companiesL.add("Connection aborted");
                    scriptsL.add("");
                    ltpsL.add("");
                    changesL.add("");
                    upsNDownsL.add("up");

                    gainerHead.setText(gainerHeadText);
                    loserHead.setText(loserHeadText);

                    gainerList.setAdapter(new GeneralAdapter(mContext, "movers", scriptsG.toArray(new String[scriptsG.size()]), companiesG.toArray(new String[companiesG.size()]), ltpsG.toArray(new String[ltpsG.size()]), changesG.toArray(new String[changesG.size()]), upsNDownsG.toArray(new String[upsNDownsG.size()])));

                    loserList.setAdapter(new GeneralAdapter(mContext, "movers", scriptsL.toArray(new String[scriptsL.size()]), companiesL.toArray(new String[companiesL.size()]), ltpsL.toArray(new String[ltpsL.size()]), changesL.toArray(new String[changesL.size()]), upsNDownsL.toArray(new String[upsNDownsL.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            if (set.readSetting("currentIndex").equals("^nsei")) {
                filList("nse");
                getGainerList();
                getLoserList();
                //fillGainerLoserList();
                mca.saveNSEData(companiesG, scriptsG, ltpsG, changesG, upsNDownsG, companiesL, scriptsL, ltpsL, changesL, upsNDownsL);
                set.addUpdateSettings("nseMoverTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
                gainerHeadText = mContext.getString(R.string.nse_gainer_head);
                loserHeadText = mContext.getString(R.string.nse_loser_head);
            } else {
                filList("bse");
                getGainerList();
                getLoserList();
                //fillGainerLoserList();
                mca.saveBSEData(companiesG, scriptsG, ltpsG, changesG, upsNDownsG, companiesL, scriptsL, ltpsL, changesL, upsNDownsL);
                set.addUpdateSettings("bseMoverTimeStamp", String.valueOf(System.currentTimeMillis() / 1000));
                gainerHeadText = mContext.getString(R.string.bse_gainer_head);
                loserHeadText = mContext.getString(R.string.bse_loser_head);
            }
        } catch (Exception ex) {
//            Log.e("important",Log.getStackTraceString(ex));
            companiesL.clear();
            scriptsL.clear();
            ltpsL.clear();
            changesL.clear();
            upsNDownsL.clear();
            companiesL.add("No items here");
            scriptsL.add("");
            ltpsL.add("");
            changesL.add("");
            upsNDownsL.add("up");

            companiesG.clear();
            scriptsG.clear();
            ltpsG.clear();
            changesG.clear();
            upsNDownsG.clear();
            companiesG.add("No items here");
            scriptsG.add("");
            ltpsG.add("");
            changesG.add("");
            upsNDownsG.add("up");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }

        gainerHead.setText(gainerHeadText);
        loserHead.setText(loserHeadText);

        gainerList.setAdapter(new GeneralAdapter(mContext, "movers", scriptsG.toArray(new String[scriptsG.size()]), companiesG.toArray(new String[companiesG.size()]), ltpsG.toArray(new String[ltpsG.size()]), changesG.toArray(new String[changesG.size()]), upsNDownsG.toArray(new String[upsNDownsG.size()])));

        loserList.setAdapter(new GeneralAdapter(mContext, "movers", scriptsL.toArray(new String[scriptsL.size()]), companiesL.toArray(new String[companiesL.size()]), ltpsL.toArray(new String[ltpsL.size()]), changesL.toArray(new String[changesL.size()]), upsNDownsL.toArray(new String[upsNDownsL.size()])));

        super.onPostExecute(aVoid);
    }

    private void fillGainerLoserList() {
        companiesG.clear();
        scriptsG.clear();
        ltpsG.clear();
        changesG.clear();
        upsNDownsG.clear();

        companiesL.clear();
        scriptsL.clear();
        ltpsL.clear();
        changesL.clear();
        upsNDownsL.clear();

        List<String> unsortedCompaniesG = new ArrayList<>();
        List<String> unsortedScriptsG = new ArrayList<>();
        List<String> unsortedLtpsG = new ArrayList<>();
        List<String> unsortedChangesG = new ArrayList<>();
        List<Float> unsortedRealChangePercentListG = new ArrayList<>();
        List<Float> unsortedRealChangePercentListFixedG = new ArrayList<>();

        List<String> unsortedCompaniesL = new ArrayList<>();
        List<String> unsortedScriptsL = new ArrayList<>();
        List<String> unsortedLtpsL = new ArrayList<>();
        List<String> unsortedChangesL = new ArrayList<>();
        List<Float> unsortedRealChangePercentListL = new ArrayList<>();
        List<Float> unsortedRealChangePercentListFixedL = new ArrayList<>();

        int index;
        for (String oneScript : scripts) {
            index = scripts.indexOf(oneScript);
            if (realChangePercentList.get(index) > 0) {
                unsortedCompaniesG.add(companies.get(index));
                unsortedScriptsG.add(scripts.get(index));
                unsortedLtpsG.add(ltps.get(index));
                unsortedChangesG.add(changes.get(index));
                unsortedRealChangePercentListG.add(realChangePercentList.get(index));
                unsortedRealChangePercentListFixedG.add(realChangePercentList.get(index));
            } else {
                unsortedCompaniesL.add(companies.get(index));
                unsortedScriptsL.add(scripts.get(index));
                unsortedLtpsL.add(ltps.get(index));
                unsortedChangesL.add(changes.get(index));
                unsortedRealChangePercentListL.add(realChangePercentList.get(index));
                unsortedRealChangePercentListFixedL.add(realChangePercentList.get(index));
            }
        }

        Comparator com = Collections.reverseOrder();
        Collections.sort(unsortedRealChangePercentListG, com);

        int sortIndex;
        for (Float real : unsortedRealChangePercentListG) {
            sortIndex = unsortedRealChangePercentListFixedG.indexOf(real);

            companiesG.add(unsortedCompaniesG.get(sortIndex));
            scriptsG.add(unsortedScriptsG.get(sortIndex));
            ltpsG.add(unsortedLtpsG.get(sortIndex));
            changesG.add(unsortedChangesG.get(sortIndex));
            upsNDownsG.add("up");
        }

        Collections.sort(unsortedRealChangePercentListL);

        for (Float real : unsortedRealChangePercentListL) {
            sortIndex = unsortedRealChangePercentListFixedL.indexOf(real);

            companiesL.add(unsortedCompaniesL.get(sortIndex));
            scriptsL.add(unsortedScriptsL.get(sortIndex));
            ltpsL.add(unsortedLtpsL.get(sortIndex));
            changesL.add(unsortedChangesL.get(sortIndex));
            upsNDownsL.add("down");
        }
    }

    private void getGainerList() {
        int numOfPositiveItems = 0;
        for (Float real : realChangePercentList) {
            if (real >= 0)
                numOfPositiveItems++;
        }
        while (numOfPositiveItems > 0) {
            int indexToRemove = -1;
            Float highest = 0.0f;
            for (Float real : realChangePercentList) {
                if (real >= highest) {
                    highest = real;
                    indexToRemove = realChangePercentList.indexOf(real);
                    //Log.e("important","index of change "+highest+ " is "+index);
                }
            }
            if (indexToRemove != -1) {
                //Log.e("important","gainer found: "+companies.get(index)+" changes "+changes.get(index)+" index "+index);
                companiesG.add(companies.get(indexToRemove));
                scriptsG.add(scripts.get(indexToRemove));
                ltpsG.add(ltps.get(indexToRemove));
                changesG.add(changes.get(indexToRemove));
                upsNDownsG.add("up");

                realChangePercentList.remove(indexToRemove);
                companies.remove(indexToRemove);
                scripts.remove(indexToRemove);
                ltps.remove(indexToRemove);
                changes.remove(indexToRemove);

                numOfPositiveItems--;
            }
        }
    }

    private void getLoserList() {
        int numOfNegativeItems = 0;
        for (Float real : realChangePercentList) {
            if (real < 0)
                numOfNegativeItems++;
        }
        while (numOfNegativeItems > 0) {
            int indexToRemove = -1;
            Float lowest = 0.0f;
            for (Float real : realChangePercentList) {
                if (real <= lowest) {
                    lowest = real;
                    indexToRemove = realChangePercentList.indexOf(real);
                }
            }
            if (indexToRemove != -1) {
                //Log.e("important","loser found: "+companies.get(index));
                companiesL.add(companies.get(indexToRemove));
                scriptsL.add(scripts.get(indexToRemove));
                ltpsL.add(ltps.get(indexToRemove));
                changesL.add(changes.get(indexToRemove));
                upsNDownsL.add("down");

                realChangePercentList.remove(indexToRemove);
                companies.remove(indexToRemove);
                scripts.remove(indexToRemove);
                ltps.remove(indexToRemove);
                changes.remove(indexToRemove);

                numOfNegativeItems--;
            }
        }
    }

    private void filList(String index) {
        scripts.clear();
        companies.clear();
        ltps.clear();
        changes.clear();
        realChangePercentList.clear();
        realChangePercentListFixed.clear();
        if (index.equals("nse")) {
            for (String scr : n50) {
                scripts.add(scr);
                companies.add(sch.scriptToCompany(scr));
            }
            pcx = new ParseCustomXML("mover-nse",null);
        } else {
            for (String scr : b30) {
                scripts.add(sch.unconvertScript(scr));
                companies.add(sch.scriptToCompany(scr));
            }
            pcx = new ParseCustomXML("mover-bse",null);
        }
        pcx.fetchXML();
        while (pcx.parsingInComplete) ;
        for (String oneChange : pcx.getRealChangePercent()) {
            realChangePercentList.add(stringToIndianFloat(oneChange));
            realChangePercentListFixed.add(stringToIndianFloat(oneChange));
        }
        ltps = pcx.getPrice();
        changes = pcx.getChangeString();

//        for (String comp:companies) {
//            int i=0;
//            Log.e("important","company "+comp+" change_percent "+realChangePercentList.get(i));
//            i++;
//        }
    }

    private float stringToIndianFloat(String text) {
        NumberFormat format = NumberFormat.getInstance(new Locale("en", "IN"));
        float formattedFloat = 0.00f;
        try {
            Number number = format.parse(text);
            formattedFloat = number.floatValue();
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        //Log.e("important","returning "+formattedFloat);
        return formattedFloat;
    }
}
