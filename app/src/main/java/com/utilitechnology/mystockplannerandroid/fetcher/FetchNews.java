package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.utilitechnology.mystockplannerandroid.adapter.NewsAdapter;
import com.utilitechnology.mystockplannerandroid.object.NewsObject;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.parser.ParseNews;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 17-11-2016.
 */

public class FetchNews extends AsyncTask<Void, Void, Void> {
    List<NewsObject> newsItems = new ArrayList<>();
    Context mContext;
    private RecyclerView newsHolder;
    private NewsAdapter rAdapter;

    ProgressDialog progress;
    FetchNews asyncCountdown;

    public FetchNews(View view) {
        mContext = view.getContext();

        newsHolder = (RecyclerView) view.findViewById(R.id.liveRecycler);
        rAdapter = new NewsAdapter(mContext,newsItems);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        newsHolder.setLayoutManager(mLayoutManager);
        newsHolder.setItemAnimator(new DefaultItemAnimator());
        //newsHolder.setAdapter(rAdapter);

        progress = new ProgressDialog(mContext);
        //progress.setTitle(mContext.getString(R.string.waiting_head));
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.show();
        asyncCountdown = this;
        new CountDownTimer(20000, 20000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {
                    } finally {
                        progress = null;
                    }
                    newsItems.add(new NewsObject("0", "Connection failure, Sorry!", "", "", "", "", "", ""));
                    newsHolder.setAdapter(rAdapter);
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            ParseNews prsnws = new ParseNews(mContext.getString(R.string.news_url), null);
            prsnws.fetchXML();
            while (prsnws.parsingInComplete) ;
            newsItems.addAll(prsnws.getNewsItems());
            //Log.e("important", "doInBackground: got "+newsItems.size());
        } catch (Exception ex) {
            //Log.e("important", Log.getStackTraceString(ex));
            newsItems.clear();
            newsItems.add(new NewsObject("0", "Connection failure, Sorry!", "", "", "", "", "", ""));
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }
        newsHolder.setAdapter(rAdapter);
        super.onPostExecute(aVoid);
    }
}
