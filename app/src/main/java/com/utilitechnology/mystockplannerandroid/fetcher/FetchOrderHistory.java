package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.handler.HandleZerodhaOrderHistory;
import com.utilitechnology.mystockplannerandroid.adapter.OrderAdapter;
import com.utilitechnology.mystockplannerandroid.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 07-06-2016.
 */
public class FetchOrderHistory extends AsyncTask<Void, Void, Void> {

    Context mContext;
    ProgressDialog progress;
    FetchOrderHistory asyncCountdown;


    List<String> company=new ArrayList<>();
    List<String> ltp=new ArrayList<>();
    List<String> change=new ArrayList<>();
    List<String> upDown=new ArrayList<>();

    List<String> script=new ArrayList<>();
    List<String> symbol=new ArrayList<>();
    List<String> orderId=new ArrayList<>();
    List<String> orderStatus=new ArrayList<>();
    List<String> exchange=new ArrayList<>();
    List<String> orderType=new ArrayList<>();
    List<String> TransactionType=new ArrayList<>();
    List<String> product=new ArrayList<>();
    List<String> limitPrice=new ArrayList<>();
    List<String> quantity=new ArrayList<>();
    List<String> triggerPrice=new ArrayList<>();
    List<String> averagePrice=new ArrayList<>();
    List<String> validity=new ArrayList<>();
    List<String> disclosed=new ArrayList<>();
    List<String> profitLoss=new ArrayList<>();

    ListView orderList;

    public FetchOrderHistory(View view) {
        mContext = view.getContext();
        orderList= (ListView) view.findViewById(R.id.orderHistoryList);

        progress = new ProgressDialog(mContext);
        //progress.setTitle(mContext.getString(R.string.waiting_head));
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        asyncCountdown = this;
        new CountDownTimer(20000, 20000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    company.add("Connection aborted");
                    ltp.add("");
                    change.add("");
                    upDown.add("none");
                    script.add("");
                    symbol.add("");
                    TransactionType.add("");
                    product.add("");
                    orderStatus.add("");
                    exchange.add("");
                    averagePrice.add("");
                    orderType.add("");
                    quantity.add("");
                    profitLoss.add("");
                    limitPrice.add("");
                    triggerPrice.add("");
                    validity.add("");
                    disclosed.add("");

                    orderList.setAdapter(new OrderAdapter(mContext,orderId.toArray(new String[orderId.size()]),script.toArray(new String[script.size()]),symbol.toArray(new String[symbol.size()]),company.toArray(new String[company.size()]),ltp.toArray(new String[ltp.size()]),change.toArray(new String[change.size()]),upDown.toArray(new String[upDown.size()]),TransactionType.toArray(new String[TransactionType.size()]),product.toArray(new String[product.size()]),orderStatus.toArray(new String[orderStatus.size()]),exchange.toArray(new String[exchange.size()]),averagePrice.toArray(new String[averagePrice.size()]),orderType.toArray(new String[orderType.size()]),quantity.toArray(new String[quantity.size()]),profitLoss.toArray(new String[profitLoss.size()]),limitPrice.toArray(new String[limitPrice.size()]),triggerPrice.toArray(new String[triggerPrice.size()]),validity.toArray(new String[validity.size()]),disclosed.toArray(new String[disclosed.size()])));
                }
            }
        }.start();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        HandleZerodhaOrderHistory hoh = new HandleZerodhaOrderHistory(mContext);
        company = hoh.getCompany();
        ltp = hoh.getLtp();
        change = hoh.getChange();
        upDown = hoh.getUpDown();
        script = hoh.getScript();
        symbol = hoh.getSymbol();
        orderId = hoh.getOrderId();
        orderStatus = hoh.getOrderStatus();
        exchange = hoh.getExchange();
        orderType = hoh.getOrderType();
        TransactionType = hoh.getTransactionType();
        product = hoh.getProduct();
        limitPrice = hoh.getLimitPrice();
        quantity = hoh.getQuantity();
        triggerPrice = hoh.getTriggerPrice();
        averagePrice = hoh.getAveragePrice();
        validity = hoh.getValidity();
        disclosed = hoh.getDisclosed();
        profitLoss = hoh.getProfitLoss();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }
        orderList.setAdapter(new OrderAdapter(mContext,orderId.toArray(new String[orderId.size()]),script.toArray(new String[script.size()]),symbol.toArray(new String[symbol.size()]),company.toArray(new String[company.size()]),ltp.toArray(new String[ltp.size()]),change.toArray(new String[change.size()]),upDown.toArray(new String[upDown.size()]),TransactionType.toArray(new String[TransactionType.size()]),product.toArray(new String[product.size()]),orderStatus.toArray(new String[orderStatus.size()]),exchange.toArray(new String[exchange.size()]),averagePrice.toArray(new String[averagePrice.size()]),orderType.toArray(new String[orderType.size()]),quantity.toArray(new String[quantity.size()]),profitLoss.toArray(new String[profitLoss.size()]),limitPrice.toArray(new String[limitPrice.size()]),triggerPrice.toArray(new String[triggerPrice.size()]),validity.toArray(new String[validity.size()]),disclosed.toArray(new String[disclosed.size()])));
        super.onPostExecute(aVoid);
    }
}
