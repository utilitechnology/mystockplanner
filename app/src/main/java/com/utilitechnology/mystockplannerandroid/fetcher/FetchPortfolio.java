package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.adapter.CustomAdapter;
import com.utilitechnology.mystockplannerandroid.handler.HandleDatabase;
import com.utilitechnology.mystockplannerandroid.handler.HandleMultipleEquities;
import com.utilitechnology.mystockplannerandroid.object.PortfolioObject;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseZerodhaPortfolio;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 05-02-2016.
 */
public class FetchPortfolio extends AsyncTask<Void, Void, Void> {

    List<PortfolioObject> portListItems = new ArrayList<>();
    List<String> scripts, stockName, Ltp, change, upDown, exchange, iBought, sellTarget, gainLoss, quantity;
    List<Float> reaLtp;
    List<Boolean> modifiable;
    HandleDatabase dbop;
    Context mContext;
    ListView folioListHolder;

    String scriptNameWithExtension;
    //HandleEquity heq;
    HandleMultipleEquities hmeq;

    ProgressDialog progress;
    FetchPortfolio asyncCountdown;
    TradingAccountSettingsAPI tras;

    public FetchPortfolio(View view) {
        mContext = view.getContext();
        dbop = new HandleDatabase(mContext);
        scripts = new ArrayList<>();
        stockName = new ArrayList<>();
        Ltp = new ArrayList<>();
        reaLtp = new ArrayList<>();
        change = new ArrayList<>();
        upDown = new ArrayList<>();
        exchange = new ArrayList<>();
        iBought = new ArrayList<>();
        sellTarget = new ArrayList<>();
        quantity = new ArrayList<>();
        gainLoss = new ArrayList<>();
        modifiable = new ArrayList<>();

        folioListHolder = (ListView) view.findViewById(R.id.portfolioList);

        progress = new ProgressDialog(mContext);
        //progress.setTitle(mContext.getString(R.string.waiting_head));
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.show();
        asyncCountdown = this;
        new CountDownTimer(50000, 50000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    stockName.add("Connection aborted");
                    Ltp.add("");
                    change.add("");
                    scripts.add("");
                    upDown.add("none");

                    folioListHolder.setAdapter(new CustomAdapter(mContext, "portfolio", scripts.toArray(new String[scripts.size()]), stockName.toArray(new String[stockName.size()]), Ltp.toArray(new String[Ltp.size()]), change.toArray(new String[change.size()]), upDown.toArray(new String[upDown.size()]), exchange.toArray(new String[exchange.size()]), iBought.toArray(new String[iBought.size()]), sellTarget.toArray(new String[sellTarget.size()]), gainLoss.toArray(new String[gainLoss.size()]), quantity.toArray(new String[quantity.size()]), null, null, modifiable.toArray(new Boolean[modifiable.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        portListItems = dbop.getAllPortfolio();
        //Get zerodha portfolio if account attached
        tras = new TradingAccountSettingsAPI(mContext);
        if (!tras.readSetting("access_token").equals("na")) {
            ParseZerodhaPortfolio pzp = new ParseZerodhaPortfolio("https://api.kite.trade/portfolio/holdings/?api_key=" + mContext.getString(R.string.ZERODHA_API_KEY) + "&access_token=" + tras.readSetting("access_token"), mContext);
            pzp.fetchJSON();
            while (pzp.parsingInComplete) ;
            portListItems.addAll(pzp.getPortfolioObjectList());
        }
        //Todo: check internet here
        try {
            if (portListItems.size() > 0) {
                for (PortfolioObject poo : portListItems) {
                    if (poo.getExchangeName().equals("BSE"))
                        scriptNameWithExtension = poo.getScriptName().toLowerCase() + ".bo";
                    else
                        scriptNameWithExtension = poo.getScriptName().toLowerCase() + ".ns";
                    scripts.add(scriptNameWithExtension);
                    //Log.e("important", scriptNameWithExtension);
                }
                hmeq = new HandleMultipleEquities(mContext, scripts);
                Ltp = hmeq.getStringLTPs();
                reaLtp = hmeq.getLTPs();
                change = hmeq.getChangeStrings();
                upDown = hmeq.getUpDown();
                for (PortfolioObject temP : portListItems) {
                    int index = portListItems.indexOf(temP);
                    stockName.add(temP.getStockName());
                    exchange.add(temP.getExchangeName());
                    iBought.add(convertFloatToString(temP.getiBoughtPrice()));
                    quantity.add(String.valueOf(temP.getQuantityAmt()));
                    sellTarget.add(convertFloatToString(temP.getSellTargetPrice()));
                    gainLoss.add(convertFloatToString((reaLtp.get(index) - temP.getiBoughtPrice()) * temP.getQuantityAmt()));
                    modifiable.add(temP.getModifiable());
                }
            } else {
                stockName.add("No items here");
                Ltp.add("");
                change.add("");
                scripts.add("");
                upDown.add("none");
            }
        } catch (Exception ex) {
            //Log.e("important",Log.getStackTraceString(ex));
            //Log.e("important",ex.getMessage());
            stockName.clear();
            Ltp.clear();
            change.clear();
            scripts.clear();
            exchange.clear();
            iBought.clear();
            sellTarget.clear();
            quantity.clear();
            gainLoss.clear();
            upDown.clear();
            modifiable.clear();

            stockName.add("Couldn't connect to service");
            Ltp.add("");
            change.add("");
            scripts.add("");
            upDown.add("none");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }
        folioListHolder.setAdapter(new CustomAdapter(mContext, "portfolio", scripts.toArray(new String[scripts.size()]), stockName.toArray(new String[stockName.size()]), Ltp.toArray(new String[Ltp.size()]), change.toArray(new String[change.size()]), upDown.toArray(new String[upDown.size()]), exchange.toArray(new String[exchange.size()]), iBought.toArray(new String[iBought.size()]), sellTarget.toArray(new String[sellTarget.size()]), gainLoss.toArray(new String[gainLoss.size()]), quantity.toArray(new String[quantity.size()]), null, null, modifiable.toArray(new Boolean[modifiable.size()])));
        super.onPostExecute(aVoid);
    }

    private String convertFloatToString(float input) {
        return String.format(new Locale("en", "IN"), "%.2f", input);
    }
}
