package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.utilitechnology.mystockplannerandroid.handler.HandleDatabase;
import com.utilitechnology.mystockplannerandroid.handler.HandleMultipleEquities;
import com.utilitechnology.mystockplannerandroid.object.PortfolioObject;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseZerodhaPortfolio;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 13-05-2016.
 */
public class FetchPortfolioStat extends AsyncTask<Void, Void, Void> {

    private List<PortfolioObject> portListItems = new ArrayList<>();
    List<String> scripts;
    private List<Float> reaLtp;
    private HandleDatabase dbop;
    Context mContext;

    private String scriptNameWithExtension;
    private HandleMultipleEquities hmeq;
    TradingAccountSettingsAPI tras;
    FetchPortfolioStat asyncCountdown;

    private TextView totalInvested, currentVal, totalGainLoss;
    private Float invested, current, gainloss;
    private String investedStr, currentStr, gainlossStr;
    private int numberOfStocksInProfit, numberOfStocksInLoss, numberOfStocksNoChange;
    private float amountInProfit, amountInLoss, amountInNoChange;
    //private String numberOfStocksInProfitStr, numberOfStocksInLossStr, numberOfStocksNoChangeStr, amountInProfitStr, amountInLossStr, amountInNoChangeStr;

    private PieChart amtPC, stockPC;

    public FetchPortfolioStat(View view) {
        mContext = view.getContext();
        dbop = new HandleDatabase(mContext);
        scripts = new ArrayList<>();
        reaLtp = new ArrayList<>();

        invested = current = gainloss = amountInProfit = amountInLoss = amountInNoChange = 0.0f;
        numberOfStocksInProfit = numberOfStocksInLoss = numberOfStocksNoChange = 0;
        investedStr = currentStr = gainlossStr = mContext.getString(R.string.global_not_found);

        amtPC = (PieChart) view.findViewById(R.id.amtPieChart);
        stockPC = (PieChart) view.findViewById(R.id.stkPieChart);

        totalInvested = (TextView) view.findViewById(R.id.amtInvestedVal);
        currentVal = (TextView) view.findViewById(R.id.amtCurrentVal);
        totalGainLoss = (TextView) view.findViewById(R.id.amtUnrealizedGainVal);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        asyncCountdown = this;
        new CountDownTimer(20000, 20000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);

                    totalInvested.setText(investedStr);
                    currentVal.setText(currentStr);
                    totalGainLoss.setText(gainlossStr);
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        portListItems = dbop.getAllPortfolio();
        tras = new TradingAccountSettingsAPI(mContext);
        if (!tras.readSetting("access_token").equals("na")) {
            ParseZerodhaPortfolio pzp = new ParseZerodhaPortfolio("https://api.kite.trade/portfolio/holdings/?api_key=" + mContext.getString(R.string.ZERODHA_API_KEY) + "&access_token=" + tras.readSetting("access_token"), mContext);
            pzp.fetchJSON();
            while (pzp.parsingInComplete) ;
            portListItems.addAll(pzp.getPortfolioObjectList());
        }
        //Todo: check internet here
        try {
            if (portListItems.size() > 0) {
                for (PortfolioObject poo : portListItems) {
                    if (poo.getExchangeName().equals("BSE"))
                        scriptNameWithExtension = poo.getScriptName().toLowerCase() + ".bo";
                    else
                        scriptNameWithExtension = poo.getScriptName().toLowerCase() + ".ns";
                    scripts.add(scriptNameWithExtension);

                }
                hmeq = new HandleMultipleEquities(mContext, scripts);
                reaLtp = hmeq.getLTPs();
                for (PortfolioObject temP : portListItems) {
                    int index = portListItems.indexOf(temP);
                    invested += (temP.getiBoughtPrice() * temP.getQuantityAmt());
                    current += (reaLtp.get(index) * temP.getQuantityAmt());
                    if (reaLtp.get(index) > temP.getiBoughtPrice()) {
                        numberOfStocksInProfit++;
                        amountInProfit += (temP.getiBoughtPrice() * temP.getQuantityAmt());
                    } else if (reaLtp.get(index) == temP.getiBoughtPrice()) {
                        numberOfStocksNoChange++;
                        amountInNoChange += (temP.getiBoughtPrice() * temP.getQuantityAmt());
                    } else {
                        numberOfStocksInLoss++;
                        amountInLoss += (temP.getiBoughtPrice() * temP.getQuantityAmt());
                    }
                }
                gainloss = current - invested;
            }
        } catch (Exception ex) {
            //Log.e("important",Log.getStackTraceString(ex));
            scripts.clear();
        }
        investedStr = convertFloatToString(invested);
        currentStr = convertFloatToString(current);
        gainlossStr = convertFloatToString(gainloss);

//        numberOfStocksInProfitStr = String.valueOf(numberOfStocksInProfit);
//        numberOfStocksInLossStr = String.valueOf(numberOfStocksInLoss);
//        numberOfStocksNoChangeStr = String.valueOf(numberOfStocksNoChange);
//
//        amountInProfitStr = convertFloatToString(amountInProfit);
//        amountInLossStr = convertFloatToString(amountInLoss);
//        amountInNoChangeStr = convertFloatToString(amountInNoChange);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        totalInvested.setText(investedStr);
        currentVal.setText(currentStr);
        totalGainLoss.setText(gainlossStr);
        if (gainlossStr.contains("-"))
            totalGainLoss.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
        else
            totalGainLoss.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));

//      Section 1: Pie chart

        ArrayList<Entry> amtEntries = new ArrayList<>();
        amtEntries.add(new Entry(amountInProfit, 0));
        if (amountInNoChange > 0.00f)
            amtEntries.add(new Entry(amountInNoChange, 1));
        amtEntries.add(new Entry(amountInLoss, 2));

        PieDataSet amtDataset = new PieDataSet(amtEntries, "by invested amount");
        //dataset.setColors(ColorTemplate.COLORFUL_COLORS);

        if (amountInNoChange > 0.00f)
            amtDataset.setColors(new int[]{ContextCompat.getColor(mContext, R.color.color_index_up), ContextCompat.getColor(mContext, R.color.color_index_nochange), ContextCompat.getColor(mContext, R.color.color_index_down)});
        else
            amtDataset.setColors(new int[]{ContextCompat.getColor(mContext, R.color.color_index_up), ContextCompat.getColor(mContext, R.color.color_index_down)});

        amtDataset.setValueTextColor(ContextCompat.getColor(mContext, R.color.full_black));

        ArrayList<String> amtLbl = new ArrayList<String>();
        amtLbl.add("Gain");
        if (amountInNoChange > 0.00f)
            amtLbl.add("Unchanged");
        amtLbl.add("Loss");

        PieData amtData = new PieData(amtLbl, amtDataset);
        amtPC.setData(amtData);
        amtPC.animateY(1000);
        amtPC.setHoleColorTransparent(true);
        amtPC.setRotationEnabled(false);
        //amtPC.animateY(1000, Easing.EasingOption.EaseInCirc);
        //amtPC.animateX(1000, Easing.EasingOption.EaseInCirc);
        amtPC.setDescription("Touch to select segment");

        ArrayList<Entry> numEntries = new ArrayList<>();
        numEntries.add(new Entry(numberOfStocksInProfit, 0));
        if (numberOfStocksNoChange > 0)
            numEntries.add(new Entry(numberOfStocksNoChange, 1));
        numEntries.add(new Entry(numberOfStocksInLoss, 2));

        PieDataSet numDataset = new PieDataSet(numEntries, "by number of stocks");

        if (numberOfStocksNoChange > 0)
            numDataset.setColors(new int[]{ContextCompat.getColor(mContext, R.color.color_index_up), ContextCompat.getColor(mContext, R.color.color_index_nochange), ContextCompat.getColor(mContext, R.color.color_index_down)});
        else
            numDataset.setColors(new int[]{ContextCompat.getColor(mContext, R.color.color_index_up), ContextCompat.getColor(mContext, R.color.color_index_down)});

        numDataset.setValueTextColor(ContextCompat.getColor(mContext, R.color.full_black));

        ArrayList<String> numLbl = new ArrayList<String>();
        numLbl.add("Gain");
        if (numberOfStocksNoChange > 0)
            numLbl.add("Unchanged");
        numLbl.add("Loss");

        PieData numData = new PieData(numLbl, numDataset);
        stockPC.setData(numData);
        stockPC.animateY(1000);
        stockPC.setHoleColorTransparent(true);
        stockPC.setRotationEnabled(false);
        stockPC.setDescription("Touch to select segment");

//        Section 2: Linear chart, set the visibility in xml before using it

//        LinearLayout.LayoutParams paramsAmtGain = new LinearLayout.LayoutParams(0, 100, amountInProfit);
//        gainMoneyL.setLayoutParams(paramsAmtGain);
//        LinearLayout.LayoutParams paramsAmtNoChange = new LinearLayout.LayoutParams(0, 100, amountInNoChange);
//        noChangeMoneyL.setLayoutParams(paramsAmtNoChange);
//        LinearLayout.LayoutParams paramsAmtLoss = new LinearLayout.LayoutParams(0, 100, amountInLoss);
//        lossMoneyL.setLayoutParams(paramsAmtLoss);
//
//        LinearLayout.LayoutParams paramsNumGain = new LinearLayout.LayoutParams(0, 100, numberOfStocksInProfit);
//        gainNumL.setLayoutParams(paramsNumGain);
//        LinearLayout.LayoutParams paramsNumNoChange = new LinearLayout.LayoutParams(0, 100, numberOfStocksNoChange);
//        noChangeNumL.setLayoutParams(paramsNumNoChange);
//        LinearLayout.LayoutParams paramsNumLoss = new LinearLayout.LayoutParams(0, 100, numberOfStocksInLoss);
//        lossNumL.setLayoutParams(paramsNumLoss);
//
//        gainMoneyT.setText("Gain: " + amountInProfitStr);
//        lossMoneyT.setText("Loss: " + amountInLossStr);
//        noChangeMoneyT.setText("Unchanged: " + amountInNoChangeStr);
//
//        gainNumT.setText("Gain: " + numberOfStocksInProfitStr);
//        lossNumT.setText("Loss: " + numberOfStocksInLossStr);
//        noChangeNumT.setText("Unchanged: " + numberOfStocksNoChangeStr);
//
//        amtH.setVisibility(View.VISIBLE);
//        numH.setVisibility(View.VISIBLE);

        super.onPostExecute(aVoid);
    }

    private String convertFloatToString(float input) {
        return String.format(new Locale("en", "IN"), "%.2f", input);
    }
}
