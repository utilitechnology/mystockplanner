package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.Urls;
import com.utilitechnology.mystockplannerandroid.parser.ParseJSON;
import com.utilitechnology.mystockplannerandroid.parser.ParseWebGoogle;

/**
 * Created by Bibaswann on 15-04-2016.
 */
public class FetchStockDetails extends AsyncTask<Void, Void, Void> {

    Context mContext;
    TextView price, change;
    private String LTP, changeString, dayHigh, dayLow, yrHigh, yrLow, stockUp;
    private ImageView arrow;
    private TextView dhighLbl, dlowLbl, yhighLbl, ylowLbl;
    private TextView lastUpdate;
    private String lup;
    //    private ParseXML prx;
//    private ParseWebYahoo prw;
    ParseWebGoogle prwg;
    private String stockScript;
    ScriptHelper scriptHelper;

    public FetchStockDetails(View view, String script) {
        stockScript = script;
        mContext = view.getContext();

        price = (TextView) view.findViewById(R.id.stockPriceLbl);
        change = (TextView) view.findViewById(R.id.stockchangeLbl);

        dhighLbl = (TextView) view.findViewById(R.id.dhval);
        dlowLbl = (TextView) view.findViewById(R.id.dlval);
        yhighLbl = (TextView) view.findViewById(R.id.yhval);
        ylowLbl = (TextView) view.findViewById(R.id.ylval);
        lastUpdate = (TextView) view.findViewById(R.id.sLastUpdate);

        arrow = (ImageView) view.findViewById(R.id.stockArrow);
        scriptHelper = new ScriptHelper(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        getStockValue();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

        price.setText(LTP);
        if (changeString != null)
            change.setText(changeString.replace("+", "").replace("-", ""));

        if (stockUp != null && stockUp.equals("up")) {
            price.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
            change.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
            arrow.setImageResource(R.drawable.up);
        } else {
            price.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
            change.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
            arrow.setImageResource(R.drawable.down);
        }

        dhighLbl.setText(dayHigh);
        dlowLbl.setText(dayLow);
        yhighLbl.setText(yrHigh);
        ylowLbl.setText(yrLow);

        lastUpdate.setText(lup);
        super.onPostExecute(aVoid);
    }

    private void getStockValue() {
        Log.e("important", "trying to parse "+Urls.GOOGLE_WEB_PREFIX + scriptHelper.getGoogleScript(stockScript));
        prwg = new ParseWebGoogle(Urls.GOOGLE_WEB_PREFIX + scriptHelper.getGoogleScript(stockScript));
        prwg.fetchWeb();
        if (prwg.getPrice().size() > 0)
            LTP = prwg.getPrice().get(0);
        if (prwg.getChangeString().size() > 0)
            changeString = prwg.getChangeString().get(0);
        if (prwg.getDayHigh().size() > 0)
            dayHigh = prwg.getDayHigh().get(0);
        if (prwg.getDayLow().size() > 0)
            dayLow = prwg.getDayLow().get(0);
        if (prwg.getYearHigh().size() > 0)
            yrHigh = prwg.getYearHigh().get(0);
        if (prwg.getYearLow().size() > 0)
            yrLow = prwg.getYearLow().get(0);
        if (prwg.getUpsNDowns().size() > 0)
            stockUp = prwg.getUpsNDowns().get(0);
        lup = prwg.getUpdate();

//        List<String> tempScr = new ArrayList<>();
//        tempScr.add(stockScript);
//        prw = new ParseWebYahoo(tempScr);
//        prw.fetchWeb();
//        if (prw.getPrice().size() > 0)
//            LTP = prw.getPrice().get(0);
//        if (prw.getChangeString().size() > 0)
//            changeString = prw.getChangeString().get(0);
//        if (prw.getDayHigh().size() > 0)
//            dayHigh = prw.getDayHigh().get(0);
//        if (prw.getDayLow().size() > 0)
//            dayLow = prw.getDayLow().get(0);
//        if (prw.getYearHigh().size() > 0)
//            yrHigh = prw.getYearHigh().get(0);
//        if (prw.getYearLow().size() > 0)
//            yrLow = prw.getYearLow().get(0);
//        if (prw.getUpsNDowns().size() > 0)
//            stockUp = prw.getUpsNDowns().get(0);
//        lup = prw.getUpdate();

//        prx = new ParseXML(RESOURCE_URL_XML_P1 + stockScript + RESOURCE_URL_XML_P2);
//        prx.fetchXML();
//        while (prx.parsingInComplete) ;
//        if (prx.getPrice().size() > 0)
//            LTP = prx.getPrice().get(0);
//        if (prx.getChangeString().size() > 0)
//            changeString = prx.getChangeString().get(0);
//        if (prx.getDayHigh().size() > 0)
//            dayHigh = prx.getDayHigh().get(0);
//        if (prx.getDayLow().size() > 0)
//            dayLow = prx.getDayLow().get(0);
//        if (prx.getYearHigh().size() > 0)
//            yrHigh = prx.getYearHigh().get(0);
//        if (prx.getYearLow().size() > 0)
//            yrLow = prx.getYearLow().get(0);
//        if(prx.getUpsNDowns().size()>0)
//            stockUp=prx.getUpsNDowns().get(0);
//        lup = prx.getUpdate();
    }
}
