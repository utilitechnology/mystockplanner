package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.adapter.GeneralAdapter;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.parser.ParseCustomXML;
import com.utilitechnology.mystockplannerandroid.parser.ParseWebYahoo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 25-10-2016.
 */

public class FetchSubindex extends AsyncTask<Void, Void, Void> {
    Context mContext;
    ListView subList;

//    private final String SUB_RESOURCE_XML = "https://finance.yahoo.com/webservice/v1/symbols/BSE-100.BO,BSE-500.BO,ALLCAP.BO,LRGCAP.BO,BSE-MIDCAP.BO,BSE-SMLCAP.BO,BSE-IT.BO,BSE-OILGAS.BO,BSE-METAL.BO,BSE-POWER.BO,BSE-AUTO.BO,BSE-BANK.BO,INFRASTRUCTURE.BO,TELCOM.BO,%5ECNX100,%5ECRSLDX,%5ENSEBANK,%5ECNXMETAL,%5ECNXPHARMA,%5ECNXAUTO,%5ECNXIT,%5ECNXINFRA/quote?format=xml&view=detail";

    ProgressDialog progress;
    FetchSubindex asyncCountdown;

    private List<String> indices = new ArrayList<>();
    private List<String> scripts = new ArrayList<>();
    private List<String> ltps = new ArrayList<>();
    private List<String> changes = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    //    ParseXML prx;
//    ParseWebYahoo prw;
    ParseCustomXML pcx;

    public FetchSubindex(View v) {
        mContext = v.getContext();
        subList = (ListView) v.findViewById(R.id.subList);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        super.onPreExecute();
        asyncCountdown = this;
        new CountDownTimer(60000, 60000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    indices.clear();
                    scripts.clear();
                    ltps.clear();
                    changes.clear();
                    upsNDowns.clear();

                    indices.add("Connection aborted");
                    scripts.add("");
                    ltps.add("");
                    changes.add("");
                    upsNDowns.add("up");

                    subList.setAdapter(new GeneralAdapter(mContext, "sub", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Important: Do not change this order
        //Method 1: Get from XML
        indices.add("S&P BSE 100");
        indices.add("S&P BSE 500");
        indices.add("S&P BSE Allcap");
        indices.add("S&P BSE Largecap");
        indices.add("S&P BSE Midcap");
        indices.add("S&P BSE Smallcap");
        indices.add("S&P BSE IT");
        indices.add("S&P BSE Oil & Gas");
        indices.add("S&P BSE Metal");
        indices.add("S&P BSE Power");
        indices.add("S&P BSE Auto");
        indices.add("S&P BSE Bank");
        indices.add("S&P BSE Infrastructure");
        indices.add("S&P BSE Telecom");
        indices.add("Nifty 100");
        indices.add("Nifty 500");
        indices.add("Nifty Bank");
        indices.add("Nifty Metal");
        indices.add("Nifty Pharma");
        indices.add("Nifty Auto");
        indices.add("Nifty IT");
        indices.add("Nifty Infra");

        //Todo: only yahoo scripts are passed here, StockDetailsActivity and HandleChartValues dynamically generating google scripts. Send google scripts from here from the beginning
        scripts.add("BSE-100.BO");
        scripts.add("BSE-500.BO");
        scripts.add("ALLCAP.BO");
        scripts.add("LRGCAP.BO");
        scripts.add("BSE-MIDCAP.BO");
        scripts.add("BSE-SMLCAP.BO");
        scripts.add("BSE-IT.BO");
        scripts.add("BSE-OILGAS.BO");
        scripts.add("BSE-METAL.BO");
        scripts.add("BSE-POWER.BO");
        scripts.add("BSE-AUTO.BO");
        scripts.add("BSE-BANK.BO");
        scripts.add("INFRASTRUCTURE.BO");
        scripts.add("TELCOM.BO");
        scripts.add("^CNX100");
        scripts.add("^CRSLDX");
        scripts.add("^NSEBANK");
        scripts.add("^CNXMETAL");
        scripts.add("^CNXPHARMA");
        scripts.add("^CNXAUTO");
        scripts.add("^CNXIT");
        scripts.add("^CNXINFRA");

//        parseSubValues();
//        getSubValues();
        parseCustomIndexValues();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }

        subList.setAdapter(new GeneralAdapter(mContext, "sub", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
        super.onPostExecute(aVoid);
    }

//    private void getSubValues()
//    {
//        prw = new ParseWebYahoo(scripts);
//        prw.fetchWeb();
//        ltps = prw.getPrice();
//        changes = prw.getChangeString();
//        upsNDowns = prw.getUpsNDowns();
//    }

    private void parseCustomIndexValues() {
        pcx = new ParseCustomXML("index",null);
        pcx.fetchXML();
        while (pcx.parsingInComplete) ;
        ltps = pcx.getPrice();
        changes = pcx.getChangeString();
        upsNDowns = pcx.getUpsNDowns();
    }

//    private void parseSubValues() {
//        prx = new ParseXML(SUB_RESOURCE_XML);
//        prx.fetchXML();
//        while (prx.parsingInComplete) ;
//        ltps = prx.getPrice();
//        changes = prx.getChangeString();
//        upsNDowns = prx.getUpsNDowns();
//
//    }
}
