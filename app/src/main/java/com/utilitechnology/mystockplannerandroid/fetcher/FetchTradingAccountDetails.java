package com.utilitechnology.mystockplannerandroid.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseZerodhaAccountDetails;

/**
 * Created by Bibaswann on 18-06-2016.
 */
public class FetchTradingAccountDetails extends AsyncTask<Void,Void,Void> {

    TextView userName,userId,totalC,availableC,utilizedC;
    String name,id,totC,avlC,utlC;
    TradingAccountSettingsAPI tras;
    Context mContext;

    public FetchTradingAccountDetails(View view)
    {
        mContext =view.getContext();

        userName= (TextView) view.findViewById(R.id.userName);
        userId= (TextView) view.findViewById(R.id.userId);
        totalC= (TextView) view.findViewById(R.id.totalCash);
        availableC= (TextView) view.findViewById(R.id.availableCash);
        utilizedC= (TextView) view.findViewById(R.id.utilizedCash);

        tras=new TradingAccountSettingsAPI(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        name=tras.readSetting("name");
        id=tras.readSetting("id");
        ParseZerodhaAccountDetails pztad=new ParseZerodhaAccountDetails("https://api.kite.trade/user/margins/equity?api_key="+mContext.getString(R.string.ZERODHA_API_KEY)+"&access_token="+tras.readSetting("access_token"));
        pztad.fetchJSON();
        while (pztad.parsingInComplete);
        totC=pztad.getTotC();
        avlC=pztad.getAvlC();
        utlC=pztad.getUtlC();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        userName.setText(name);
        userId.setText(id);
        totalC.setText(totC);
        availableC.setText(avlC);
        utilizedC.setText(utlC);
        if(!utlC.equals("0")) {
            if (utlC.contains("-"))
                utilizedC.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_up));
            else
                utilizedC.setTextColor(ContextCompat.getColor(mContext, R.color.color_index_down));
        }
        super.onPostExecute(aVoid);
    }
}
