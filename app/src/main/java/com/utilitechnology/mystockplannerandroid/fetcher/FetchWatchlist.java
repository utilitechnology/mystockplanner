package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.adapter.CustomAdapter;
import com.utilitechnology.mystockplannerandroid.handler.HandleDatabase;
import com.utilitechnology.mystockplannerandroid.handler.HandleMultipleEquities;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.object.WatchObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 08-02-2016.
 */
public class FetchWatchlist extends AsyncTask<Void, Void, Void> {

    List<WatchObject> watchListItems = new ArrayList<>();
    List<String> scripts, stockName, Ltp, change, upDown, exchange, buyTarget, amtToReachBuy;
    List<Float> reaLtp;
    List<Boolean> modifiable;
    HandleDatabase dbop;
    Context mContext;
    ListView watchListHolder;

    String scriptNameWithExtension;
    //HandleEquity heq;
    HandleMultipleEquities hmeq;

    ProgressDialog progress;
    FetchWatchlist asyncCountdown;

    public FetchWatchlist(View view) {
        mContext = view.getContext();
        dbop = new HandleDatabase(mContext);

        scripts = new ArrayList<>();
        stockName = new ArrayList<>();
        Ltp = new ArrayList<>();
        reaLtp = new ArrayList<>();
        change = new ArrayList<>();
        upDown = new ArrayList<>();
        exchange = new ArrayList<>();
        buyTarget = new ArrayList<>();
        amtToReachBuy = new ArrayList<>();
        modifiable=new ArrayList<>();

        watchListHolder = (ListView) view.findViewById(R.id.watchList);

        progress = new ProgressDialog(mContext);
        //progress.setTitle(mContext.getString(R.string.waiting_head));
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.show();
        asyncCountdown = this;
        new CountDownTimer(50000, 50000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    stockName.add("Connection aborted");
                    Ltp.add("");
                    change.add("");
                    scripts.add("");
                    upDown.add("none");
                    watchListHolder.setAdapter(new CustomAdapter(mContext, "watchlist", scripts.toArray(new String[scripts.size()]), stockName.toArray(new String[stockName.size()]), Ltp.toArray(new String[Ltp.size()]), change.toArray(new String[change.size()]), upDown.toArray(new String[upDown.size()]), exchange.toArray(new String[exchange.size()]), null, null, null, null, buyTarget.toArray(new String[buyTarget.size()]), amtToReachBuy.toArray(new String[amtToReachBuy.size()]),modifiable.toArray(new Boolean[modifiable.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        watchListItems = dbop.getAllWatchList();
        try {
            if (watchListItems.size() > 0) {
                for (WatchObject wao : watchListItems) {
                    if (wao.getExchangeName().equals("BSE"))
                        scriptNameWithExtension = wao.getScriptName().toLowerCase() + ".bo";
                    else
                        scriptNameWithExtension = wao.getScriptName().toLowerCase() + ".ns";
                    scripts.add(scriptNameWithExtension);
                }
                hmeq = new HandleMultipleEquities(mContext, scripts);
                Ltp = hmeq.getStringLTPs();
                reaLtp = hmeq.getLTPs();
                change = hmeq.getChangeStrings();
                upDown = hmeq.getUpDown();
                for (WatchObject tempW : watchListItems) {
                    int index = watchListItems.indexOf(tempW);
                    stockName.add(tempW.getStockName());
                    exchange.add(tempW.getExchangeName());
                    buyTarget.add(convertFloatToString(tempW.getBuyTargetPrice()));

                    /*heq = new HandleEquity(mContext, scriptNameWithExtension);
                    Ltp.add(convertFloatToString(heq.getLTP()));
                    change.add(heq.getChange());
                    if (heq.getStockUp())
                        upDown.add("up");
                    else
                        upDown.add("down");*/
                    float amt = reaLtp.get(index) - tempW.getBuyTargetPrice();
                    if (amt <= 0)
                        amtToReachBuy.add("Reached");
                    else
                        amtToReachBuy.add(convertFloatToString(amt));
                    modifiable.add(true);
                }
            } else {
                stockName.add("No items here");
                Ltp.add("");
                change.add("");
                scripts.add("");
                upDown.add("none");
            }
        } catch (Exception ex) {
            //Log.e("important", Log.getStackTraceString(ex));
            //Log.e("important",ex.getMessage());
            stockName.clear();
            Ltp.clear();
            change.clear();
            scripts.clear();
            exchange.clear();
            buyTarget.clear();
            amtToReachBuy.clear();
            upDown.clear();
            modifiable.clear();

            stockName.add("Couldn't connect to service");
            Ltp.add("");
            change.add("");
            scripts.add("");
            upDown.add("none");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }
        watchListHolder.setAdapter(new CustomAdapter(mContext, "watchlist", scripts.toArray(new String[scripts.size()]), stockName.toArray(new String[stockName.size()]), Ltp.toArray(new String[Ltp.size()]), change.toArray(new String[change.size()]), upDown.toArray(new String[upDown.size()]), exchange.toArray(new String[exchange.size()]), null, null, null, null, buyTarget.toArray(new String[buyTarget.size()]), amtToReachBuy.toArray(new String[amtToReachBuy.size()]),modifiable.toArray(new Boolean[modifiable.size()])));
        super.onPostExecute(aVoid);
    }

    private String convertFloatToString(float input) {
        return String.format(new Locale("en", "IN"), "%.2f", input);
    }
}
