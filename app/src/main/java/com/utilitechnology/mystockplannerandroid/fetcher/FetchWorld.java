package com.utilitechnology.mystockplannerandroid.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ListView;

import com.utilitechnology.mystockplannerandroid.adapter.GeneralAdapter;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.parser.ParseCustomXML;
import com.utilitechnology.mystockplannerandroid.parser.ParseWebYahoo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 13-02-2016.
 */
public class FetchWorld extends AsyncTask<Void, Void, Void> {

    Context mContext;
    ListView worldList;

//    private final String WORLD_RESOURCE_XML = "https://finance.yahoo.com/webservice/v1/symbols/%5Egspc,%5Edji,%5Eixic,%5Egsptse,%5Ebvsp,%5Emxx,%5Egdaxi,%5Eftse,%5Efchi,%5Ejkse,%5En225,%5Eaxjo,000001.ss,%5Ehsi,%5Eks11,%5Ebsesn,%5Ensei/quote?format=xml&view=detail";

    ProgressDialog progress;
    FetchWorld asyncCountdown;

    private List<String> indices = new ArrayList<>();
    private List<String> scripts = new ArrayList<>();
    private List<String> ltps = new ArrayList<>();
    private List<String> changes = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    //    ParseXML prx;
//    ParseWebYahoo prw;
    ParseCustomXML pcx;

    public FetchWorld(View v) {
        mContext = v.getContext();
        worldList = (ListView) v.findViewById(R.id.worldList);

        progress = new ProgressDialog(mContext);
        progress.setMessage(mContext.getString(R.string.waiting_head));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        super.onPreExecute();
        asyncCountdown = this;
        new CountDownTimer(60000, 60000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);
                    try {
                        progress.dismiss();
                    } catch (IllegalArgumentException ile) {

                    } finally {
                        progress = null;
                    }

                    indices.clear();
                    scripts.clear();
                    ltps.clear();
                    changes.clear();
                    upsNDowns.clear();

                    indices.add("Connection aborted");
                    scripts.add("");
                    ltps.add("");
                    changes.add("");
                    upsNDowns.add("up");

                    worldList.setAdapter(new GeneralAdapter(mContext, "world", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Important: Do not change this order
        indices.add("S&P 500 (USA)");
        indices.add("Dow Jones (USA)");
        indices.add("NASDAQ Composite (USA)");
        indices.add("S&P/TSX (Canada)");
        indices.add("Bovespa (Brazil)");
        indices.add("IPC (Mexico)");
        indices.add("Dax (Germany)");
        indices.add("FTSE 100 (UK)");
        indices.add("CAC 40 (France)");
        indices.add("Jakarta Composite (Indonesia)");
        indices.add("Nikkei 225 (Japan)");
        indices.add("S&P/ASX 200 (Australia)");
        indices.add("Shanghai Composite (China)");
        indices.add("Hang Seng (Hong Kong)");
        indices.add("KOSPI (South Korea)");
        indices.add("BSE Sensex (India)");
        indices.add("Nifty 50 (India)");

        //Todo: only yahoo scripts are passed here, StockDetailsActivity and HandleChartValues dynamically generating google scripts. Send google scripts from here from the beginning
        scripts.add("^gspc");
        scripts.add("^dji");
        scripts.add("^ixic");
        scripts.add("^gsptse");
        scripts.add("^bvsp");
        scripts.add("^mxx");
        scripts.add("^gdaxi");
        scripts.add("^ftse");
        scripts.add("^fchi");
        scripts.add("^jkse");
        scripts.add("^n225");
        scripts.add("^axjo");
        scripts.add("000001.ss");
        scripts.add("^hsi");
        scripts.add("^ks11");
        scripts.add("^bsesn");
        scripts.add("^nsei");

        //Method 1: Get from XML
//        parseGlobalValues();

        //Method 2: Scrap from websites
//        getGlobalValues();
        parseCustomWorldValues();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            progress.dismiss();
        } catch (IllegalArgumentException ile) {

        } finally {
            progress = null;
        }

        worldList.setAdapter(new GeneralAdapter(mContext, "world", scripts.toArray(new String[scripts.size()]), indices.toArray(new String[indices.size()]), ltps.toArray(new String[ltps.size()]), changes.toArray(new String[changes.size()]), upsNDowns.toArray(new String[upsNDowns.size()])));
        super.onPostExecute(aVoid);
    }

//    private void getGlobalValues() {
//        prw = new ParseWebYahoo(scripts);
//        prw.fetchWeb();
//        ltps = prw.getPrice();
//        changes = prw.getChangeString();
//        upsNDowns = prw.getUpsNDowns();
//    }

    private void parseCustomWorldValues() {
        pcx = new ParseCustomXML("world",null);
        pcx.fetchXML();
        while (pcx.parsingInComplete) ;
        ltps = pcx.getPrice();
        changes = pcx.getChangeString();
        upsNDowns = pcx.getUpsNDowns();
    }

//    private void parseGlobalValues() {
//        prx = new ParseXML(WORLD_RESOURCE_XML);
//        prx.fetchXML();
//        while (prx.parsingInComplete) ;
//        ltps = prx.getPrice();
//        changes = prx.getChangeString();
//        upsNDowns = prx.getUpsNDowns();
//
//    }
}
