package com.utilitechnology.mystockplannerandroid.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.jjoe64.graphview.GraphView;
import com.utilitechnology.mystockplannerandroid.handler.HandleYahooChartValue;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchEquityOverview;

public class EquityOverview extends Fragment {

    GraphView graph, againGraph;
    ImageView cover, nocover;
    String unconvertedScriptWithExtension;

    SettingsAPI set;
    Button dayBtn, mnthBtn, hYrBtn, yrBtn, fYrBtn;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public EquityOverview() {
        // Required empty public constructor
    }

    public static EquityOverview newInstance(String param1, String param2) {
        EquityOverview fragment = new EquityOverview();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_equity_overview, container, false);
        set = new SettingsAPI(view.getContext());
        unconvertedScriptWithExtension = set.readSetting("currentStock");
        graph = (GraphView) view.findViewById(R.id.equitygraph);
        againGraph = (GraphView) view.findViewById(R.id.volgraph);
        cover = (ImageView) view.findViewById(R.id.equitygraphCover);
        nocover = (ImageView) view.findViewById(R.id.noequitygraphCover);

        new FetchEquityOverview(view, unconvertedScriptWithExtension).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new HandleYahooChartValue(graph, againGraph, cover, nocover, unconvertedScriptWithExtension, "1d").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        dayBtn = (Button) view.findViewById(R.id.stockday);
        mnthBtn = (Button) view.findViewById(R.id.stockmonth);
        hYrBtn = (Button) view.findViewById(R.id.stockhalfyr);
        yrBtn = (Button) view.findViewById(R.id.stockyr);
        fYrBtn = (Button) view.findViewById(R.id.stock5yr);

        dayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(dayBtn, view);
                new HandleYahooChartValue(graph, againGraph, cover, nocover, unconvertedScriptWithExtension, "1d").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        mnthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(mnthBtn, view);
                new HandleYahooChartValue(graph, againGraph, cover, nocover, unconvertedScriptWithExtension, "1m").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        hYrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(hYrBtn, view);
                new HandleYahooChartValue(graph, againGraph, cover, nocover, unconvertedScriptWithExtension, "6m").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        yrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(yrBtn, view);
                new HandleYahooChartValue(graph, againGraph, cover, nocover, unconvertedScriptWithExtension, "1y").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        fYrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEveryButtonDefault(fYrBtn, view);
                new HandleYahooChartValue(graph, againGraph, cover, nocover, unconvertedScriptWithExtension, "5y").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void makeEveryButtonDefault(Button btn, View view) {
        dayBtn.setTextColor(ContextCompat.getColor(view.getContext(), R.color.full_black));
        mnthBtn.setTextColor(ContextCompat.getColor(view.getContext(), R.color.full_black));
        hYrBtn.setTextColor(ContextCompat.getColor(view.getContext(), R.color.full_black));
        yrBtn.setTextColor(ContextCompat.getColor(view.getContext(), R.color.full_black));
        fYrBtn.setTextColor(ContextCompat.getColor(view.getContext(), R.color.full_black));
        btn.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
    }
}
