package com.utilitechnology.mystockplannerandroid.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.SaveIndexData;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchHeatmapFromFile;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchHeatmapOnline;

public class HeatmapFragment extends Fragment {
    SaveIndexData svi;
    SettingsAPI set;
    View view;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public HeatmapFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static HeatmapFragment newInstance(String param1, String param2) {
        HeatmapFragment fragment = new HeatmapFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_heatmap, container, false);
        view = v;
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            svi = new SaveIndexData(view.getContext());
            set = new SettingsAPI(view.getContext());
            //new FetchMovers(mView).execute();
            //Check if saved value is more than 5 minutes old, if so, reload

            if (set.readSetting("currentIndex").equals("^bsesn")) {
                //Log.e("important",set.readSetting("bseMoverTimeStamp"));
                if (set.readSetting("bseMoverTimeStamp").equals("na"))
                    new FetchHeatmapOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else if (((System.currentTimeMillis() / 1000) - (Long.valueOf(set.readSetting("bseMoverTimeStamp")))) > (5 * 60))
                    new FetchHeatmapOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else
                    new FetchHeatmapFromFile(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                //Log.e("important",set.readSetting("nseMoverTimeStamp"));
                if (set.readSetting("nseMoverTimeStamp").equals("na"))
                    new FetchHeatmapOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else if (((System.currentTimeMillis() / 1000) - (Long.valueOf(set.readSetting("nseMoverTimeStamp")))) > (5 * 60))
                    new FetchHeatmapOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else
                    new FetchHeatmapFromFile(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
