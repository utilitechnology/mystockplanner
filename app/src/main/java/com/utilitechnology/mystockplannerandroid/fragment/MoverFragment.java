package com.utilitechnology.mystockplannerandroid.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.SaveIndexData;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchMoversFromFile;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchMoversOnline;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MoverFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MoverFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoverFragment extends Fragment {
    SaveIndexData svi;
    SettingsAPI set;
    View view;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MoverFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MoverFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MoverFragment newInstance(String param1, String param2) {
        MoverFragment fragment = new MoverFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mover, container, false);
        view = v;
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            svi = new SaveIndexData(view.getContext());
            set = new SettingsAPI(view.getContext());
            //new FetchMovers(mView).execute();
            //Check if saved value is more than 5 minutes old, if so, reload

            if (set.readSetting("currentIndex").equals("^bsesn")) {
//                Log.e("important",set.readSetting("bseMoverTimeStamp"));
                if (set.readSetting("bseMoverTimeStamp").equals("na"))
                    new FetchMoversOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else if (((System.currentTimeMillis() / 1000) - (Long.valueOf(set.readSetting("bseMoverTimeStamp")))) > (5 * 60))
                    new FetchMoversOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else
                    new FetchMoversFromFile(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
//                Log.e("important",set.readSetting("nseMoverTimeStamp"));
                if (set.readSetting("nseMoverTimeStamp").equals("na"))
                    new FetchMoversOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else if (((System.currentTimeMillis() / 1000) - (Long.valueOf(set.readSetting("nseMoverTimeStamp")))) > (5 * 60))
                    new FetchMoversOnline(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else
                    new FetchMoversFromFile(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
