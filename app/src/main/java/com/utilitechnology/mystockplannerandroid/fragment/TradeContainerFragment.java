package com.utilitechnology.mystockplannerandroid.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.utils.FragmentPagerItemAdapter;
import com.utilitechnology.mystockplannerandroid.utils.FragmentPagerItems;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TradeContainerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TradeContainerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TradeContainerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private FragmentTabHost mTabHost;

    private OnFragmentInteractionListener mListener;

    public TradeContainerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TradeContainerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TradeContainerFragment newInstance(String param1, String param2) {
        TradeContainerFragment fragment = new TradeContainerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_trade_container, container, false);

//        mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
//        mTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);
//
//        mTabHost.addTab(mTabHost.newTabSpec("buysell").setIndicator("Buy/Sell"), BuySellFragment.class, null);
//        mTabHost.addTab(mTabHost.newTabSpec("orderhistory").setIndicator("Orders"), OrderHistoryFragment.class, null);
//        mTabHost.addTab(mTabHost.newTabSpec("account").setIndicator("Account"), TradingAccountFragment.class, null);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getContext())
                .add("Buy/Sell", BuySellFragment.class)
                .add("Orders", OrderHistoryFragment.class)
                .add("Account", TradingAccountFragment.class)
                .create());

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.tviewpager);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) rootView.findViewById(R.id.tviewpagertab);
        viewPagerTab.setViewPager(viewPager);

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
