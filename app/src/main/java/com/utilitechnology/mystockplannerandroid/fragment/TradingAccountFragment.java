package com.utilitechnology.mystockplannerandroid.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.fetcher.FetchTradingAccountDetails;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class TradingAccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View view;

    private OnFragmentInteractionListener mListener;

    private static String API_KEY;
    TradingAccountSettingsAPI tras;

    public TradingAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TradingAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TradingAccountFragment newInstance(String param1, String param2) {
        TradingAccountFragment fragment = new TradingAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_trading_account, container, false);
        view = v;
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            tras = new TradingAccountSettingsAPI(view.getContext());
            API_KEY = view.getContext().getString(R.string.ZERODHA_API_KEY);
            Button logoutBtn = (Button) view.findViewById(R.id.logoutBtn);
            logoutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logOut();
                }
            });

            new FetchTradingAccountDetails(view).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void logOut() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final String ACCESS_TOKEN = tras.readSetting("access_token");
                URL url = null;
                try {
                    url = new URL("https://api.kite.trade/session/token?api_key=" + API_KEY + "&access_token=" + ACCESS_TOKEN);
                } catch (MalformedURLException exception) {
                    exception.printStackTrace();
                }
                HttpsURLConnection conn = null;
                try {
                    conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestMethod("DELETE");
                    conn.connect();
                    if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        //Log.e("important", "success");
                        tras.deleteAllSettings();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new TradingConnectFragment(), "Connect to Trading Account").commitAllowingStateLoss();
                    }
                } catch (IOException ignored) {
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }
        }).start();
    }
}
