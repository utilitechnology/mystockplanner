package com.utilitechnology.mystockplannerandroid.handler;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.parser.ParseJSON;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by bibaswann on 27/09/18.
 * DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


public class HandleAlphaChartValue extends AsyncTask<Void, Void, Void> {
    GraphView graph1, graph2 = null;
    ImageView cover, noCover;
    ArrayList<DataPoint> chartData = new ArrayList<>();
    ArrayList<DataPoint> volData = new ArrayList<>();
    ArrayList<String> timeTrack = new ArrayList<>();
    ArrayList<DataPoint> previousData = new ArrayList<>();
    double yMax = 0, yMin = Double.MAX_VALUE;
    double xMax = 0, xMin = 0;
    double yMaxVol = 0, yMinVol = Double.MAX_VALUE;
    String script, dur;
    ScriptHelper sch;
    private final String PATH_OF_CSV;

    HandleAlphaChartValue(GraphView graphView, GraphView volGraphView, ImageView graphCover, ImageView nographCover, String script, String duration) {
        graph1 = graphView;
        graph2 = volGraphView;
        cover = graphCover;
        noCover = nographCover;

        graph1.setVisibility(View.GONE);
        cover.setVisibility(View.VISIBLE);
        noCover.setVisibility(View.GONE);
        if (graph2 != null)
            graph2.setVisibility(View.INVISIBLE);

        sch = new ScriptHelper(graphView.getContext());
        this.script = script;


        dur = duration;
        // TODO: 27/09/18 The timezone is US, check how to convert
        if (dur.equals("1d")) {
            PATH_OF_CSV = "https://finance.google.com/finance/getprices?q=" + chartScriptQ + "&x=" + chartScriptX + "&i=60&p=1d&f=d,o,h,l,c,v";
        } else if (dur.equals("1m")) {
            PATH_OF_CSV = "https://finance.google.com/finance/getprices?q=" + chartScriptQ + "&x=" + chartScriptX + "&i=86400&p=30d&f=d,o,h,l,c,v";
        } else if (dur.equals("6m")) {
            PATH_OF_CSV = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+script+"&apikey=3P8JGBKZJIKLAY6P&datatype=csv";
        } else if (dur.equals("1y")) {
            PATH_OF_CSV = "https://finance.google.com/finance/getprices?q=" + chartScriptQ + "&x=" + chartScriptX + "&i=86400&p=365d&f=d,o,h,l,c,v";
        } else if (dur.equals("5y")) {
            PATH_OF_CSV = "https://finance.google.com/finance/getprices?q=" + chartScriptQ + "&x=" + chartScriptX + "&i=86400&p=1825d&f=d,o,h,l,c,v";
        } else {
            PATH_OF_CSV = "https://finance.google.com/finance/getprices?q=" + chartScriptQ + "&x=" + chartScriptX + "&i=60&p=1d&f=d,o,h,l,c,v";
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        fetchData(PATH_OF_CSV);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        if (graph2 != null)
//            graph1.setTitle("Price chart");
        graph1.removeAllSeries();
        if (chartData.size() > 0) {
            LineGraphSeries<DataPoint> series = new LineGraphSeries<>(chartData.toArray(new DataPoint[chartData.size()]));
            series.setThickness(2);
            series.setDrawBackground(true);
            series.setBackgroundColor(ContextCompat.getColor(graph1.getContext(), R.color.graphFill));
            series.setColor(ContextCompat.getColor(graph1.getContext(), R.color.graphLine));

            LineGraphSeries<DataPoint> prevSeries = new LineGraphSeries<>(previousData.toArray(new DataPoint[previousData.size()]));
            prevSeries.setThickness(2);
            prevSeries.setColor(ContextCompat.getColor(graph1.getContext(), R.color.previousGraphLine));

            graph1.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
            graph1.getGridLabelRenderer().setGridColor(Color.LTGRAY);
            NumberFormat ynf = NumberFormat.getInstance();
            ynf.setMaximumFractionDigits(2);
            //nf.setMinimumIntegerDigits(2);
            NumberFormat xnf = NumberFormat.getInstance();
            //xnf.setMaximumFractionDigits(4);//Unnecessary, I think
            //nf.setMinimumIntegerDigits(2);
            graph1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(xnf, ynf));
            graph1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                @Override
                public String formatLabel(double value, boolean isValueX) {
                    if (isValueX) {
                        // show normal x values
                        if (dur.equals("1d")) {
                            // *1000 is to convert seconds to milliseconds
                            if (value <= xMax) {
                                Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", new Locale("en", "IN"));
                                return sdf.format(time);
                            } else
                                return "";
                        } else if (dur.equals("1m") || dur.equals("6m")) {
                            if (value <= xMax) {
                                Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM", new Locale("en", "IN"));
                                return sdf.format(time);
                            } else
                                return "";
                        } else {
                            if (value <= xMax) {
                                Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy", new Locale("en", "IN"));
                                return sdf.format(time);
                            } else
                                return "";
                        }
                    } else {
                        // show normal y value
                        return super.formatLabel(value, isValueX);
                    }
                }
            });
            //graph.getGridLabelRenderer().setHighlightZeroLines(true);
            graph1.getViewport().setYAxisBoundsManual(true);
            graph1.getViewport().setXAxisBoundsManual(true);
            graph1.getViewport().setMinY(yMin);
            graph1.getViewport().setMaxY(yMax);
            graph1.getViewport().setMaxX(xMax);
            graph1.getViewport().setMinX(xMin);
            //Log.e("important","max x is "+xMax);

            //Use series if no animation required
            graph1.addSeries(series);
            graph1.addSeries(prevSeries);
            graph1.setVisibility(View.VISIBLE);

            //Use DrawChart if animation required
            //Todo: This is not working aparently because I cannot change series from doInBackground, and a runnable thread is freezing the UI
            //new DrawChart(chartData,graph1).execute();

            //Show volume bar graph if present
            if (graph2 != null) {
                graph2.removeAllSeries();
                //graph2.setTitle("Volume chart");
                if (volData.size() > 0) {
                    BarGraphSeries<DataPoint> volSeries = new BarGraphSeries<DataPoint>(volData.toArray(new DataPoint[volData.size()]));
                    volSeries.setColor(ContextCompat.getColor(graph1.getContext(), R.color.graphLine));
                    volSeries.setSpacing(10);
                    graph2.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
                    graph2.getGridLabelRenderer().setGridColor(Color.LTGRAY);
                    NumberFormat ynf2 = NumberFormat.getInstance();
                    ynf.setMaximumFractionDigits(2);
                    NumberFormat xnf2 = NumberFormat.getInstance();
                    graph2.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(xnf2, ynf2));
                    graph2.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                        @Override
                        public String formatLabel(double value, boolean isValueX) {
                            if (isValueX) {
                                // show normal x values
                                if (dur.equals("1d")) {
                                    // *1000 is to convert seconds to milliseconds
                                    if (value <= xMax) {
                                        Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", new Locale("en", "IN"));
                                        return sdf.format(time);
                                    } else
                                        return "";
                                } else if (dur.equals("1m") || dur.equals("6m")) {
                                    if (value <= xMax) {
                                        Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM", new Locale("en", "IN"));
                                        return sdf.format(time);
                                    } else
                                        return "";
                                } else {
                                    if (value <= xMax) {
                                        Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy", new Locale("en", "IN"));
                                        return sdf.format(time);
                                    } else
                                        return "";
                                }
                            } else {
                                // show normal y value
                                return super.formatLabel(value, isValueX);
                            }
                        }
                    });
                    graph2.getViewport().setYAxisBoundsManual(true);
                    graph2.getViewport().setXAxisBoundsManual(true);
                    graph2.getViewport().setMinY(yMinVol);
                    graph2.getViewport().setMaxY(yMaxVol);
                    graph2.getViewport().setMaxX(xMax);
                    graph2.getViewport().setMinX(xMin);
                    graph2.addSeries(volSeries);

                    graph2.setVisibility(View.VISIBLE);
                }
            }

            cover.setVisibility(View.GONE);
            noCover.setVisibility(View.GONE);
        } else {
            cover.setVisibility(View.GONE);
            noCover.setVisibility(View.VISIBLE);
        }
    }

    private void fetchData(String uri) {
        boolean startNow = false;
        boolean continueCount = false;
//        Log.e("important", "fetchData: from " + uri);
        if (uri == null)
            return;
        try {
            InputStream stream;
            BufferedReader br;

//            Uri.Builder builder = new Uri.Builder();
//            builder.scheme("https")
//                    .authority("www.myawesomesite.com")
//                    .appendPath("turtles")
//                    .appendPath("types")
//                    .appendQueryParameter("type", "1")
//                    .appendQueryParameter("sort", "relevance")
//                    .fragment("section-name");
//            String url = builder.build().toString();

            URL url = new URL(uri);
            stream = url.openStream();
            br = new BufferedReader(new InputStreamReader(stream));
            int counter = 0;
            int arrayTrack = 0;
            int skipVal;
            if (dur.equals("1d"))
                skipVal = 2;
            else
                skipVal = 1;
            double valueOfY, volOfY, closeValueOfY = 0, preValOfY = 0;
            //I have taken care of the situation where previous close is not present
            if (dur.equals("1d")) {
                preValOfY = findPrevCloseTheHardWay();
                if (preValOfY > yMax)
                    yMax = preValOfY;
                //When previous value is not present, the value will always be 0 and <yMin
                if (preValOfY < yMin)
                    if (preValOfY > 0)
                        yMin = preValOfY;
            }
            String line;
            while ((line = br.readLine()) != null) {
//                Log.e("important", "found " + line);
                if (startNow) {
                    //For price graph
                    valueOfY = Double.valueOf(line.split(",")[1]);
                    if (valueOfY > yMax)
                        yMax = valueOfY;
                    if (valueOfY < yMin)
                        yMin = valueOfY;
                    chartData.add(new DataPoint(0, valueOfY));
                    if (preValOfY > 0)
                        previousData.add(new DataPoint(0, preValOfY));

                    //For volume graph
                    volOfY = Double.valueOf(line.split(",")[line.split(",").length - 1]);
                    if (volOfY > yMaxVol)
                        yMaxVol = volOfY;
                    if (volOfY < yMinVol)
                        yMinVol = volOfY;
                    volData.add(new DataPoint(0, volOfY));

                    timeTrack.add(line.split(",")[0].substring(1, line.split(",")[0].length()));
                    arrayTrack++;
                    continueCount = true;
                    startNow = false;
                } else if (continueCount) {
                    if (counter % skipVal == 0) {
                        //For price graph
                        valueOfY = Double.valueOf(line.split(",")[1]);
                        if (valueOfY > yMax)
                            yMax = valueOfY;
                        if (valueOfY < yMin)
                            yMin = valueOfY;
                        chartData.add(new DataPoint(arrayTrack, valueOfY));
                        if (preValOfY > 0)
                            previousData.add(new DataPoint(arrayTrack, preValOfY));

                        //For volume graph
                        volOfY = Double.valueOf(line.split(",")[line.split(",").length - 1]);
                        if (volOfY > yMaxVol)
                            yMaxVol = volOfY;
                        if (volOfY < yMinVol)
                            yMinVol = volOfY;
                        volData.add(new DataPoint(arrayTrack, volOfY));
                        if (dur.equals("1d")) {
                            String ts = String.valueOf(Long.parseLong(timeTrack.get(0)) + Long.parseLong(line.split(",")[0]) * 60);
                            timeTrack.add(ts);
                        } else {
                            String ts = String.valueOf(Long.parseLong(timeTrack.get(0)) + Long.parseLong(line.split(",")[0]) * 86400);
                            timeTrack.add(ts);
                        }
                        arrayTrack++;
                    }
                }
                counter++;
                if (line.startsWith("TIMEZONE_OFFSET"))
                    startNow = true;
            }

            xMax = arrayTrack - 1;
            //Todo: make it range dependent instead of fixed 10
            yMax += (yMax - yMin) / 10;
            yMin -= (yMax - yMin) / 10;
            stream.close();
        } catch (Exception e) {
//            Log.e("important", "The exception here is: " + Log.getStackTraceString(e));
        }
    }

    private Double findPrevClose() {
        ParseJSON prj;
        prj = new ParseJSON("http://finance.google.com/finance/info?infotype=infoquoteall&q=" + sch.getGoogleScript(unconvertedScriptWithExtension));
        prj.fetchJSON();
        while (prj.parsingInComplete) ;
        if (prj.getP_closes().size() > 0)
            return Double.valueOf(prj.getP_closes().get(0));
        else
            return 0.0d;
    }

    private Double findPrevCloseTheHardWay() {
        try {
            Document doc = Jsoup.parse(new URL("https://www.msn.com/en-in/money/stockdetails/" + sch.makeMSNString(unconvertedScriptWithExtension)), 10000);
            Elements topicListHelper = doc.select("ul[class=today-trading-container]").select("li").select("span").select("p");
            return Double.valueOf(topicListHelper.get(3).text().replace(",", "").trim());
        } catch (NumberFormatException | IOException | ArrayIndexOutOfBoundsException x) {
            return 0.0d;
        }
    }
}