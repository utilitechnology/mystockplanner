package com.utilitechnology.mystockplannerandroid.handler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.utilitechnology.mystockplannerandroid.object.PortfolioObject;
import com.utilitechnology.mystockplannerandroid.object.WatchObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 28-01-2016.
 */
public class HandleDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "userStockManager";
    private static final String TABLE_PORTFOLIO = "holdinglist";
    private static final String TABLE_WATCHLIST = "watchlist";

    public HandleDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createPortfolioSql = "CREATE TABLE IF NOT EXISTS " + TABLE_PORTFOLIO +
                "(serial INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "exchange VARCHAR(3), stockname VARCHAR(100),"
                + "scriptname VARCHAR(20), ibought REAL,"
                + "quantity INTEGER, selltarget REAL)";
        db.execSQL(createPortfolioSql);
        String createWatchListSql = "CREATE TABLE IF NOT EXISTS " + TABLE_WATCHLIST +
                "(serial INTEGER PRIMARY KEY AUTOINCREMENT, exchange VARCHAR(3),"
                + "stockname VARCHAR(100), scriptname VARCHAR(20), buytarget REAL)";
        db.execSQL(createWatchListSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
            If you need to change the database structure, like adding date
            1. Rename old tables to holdinglist_temp or something
            2. Create new tables
            3. Copy old values to new table and add N/A to new column
            4. Drop the old tables
        */

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PORTFOLIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WATCHLIST);

        // Create tables again
        onCreate(db);
    }

    public void insertIntoPortfolio(PortfolioObject portfolio) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put("serial", "NULL");
        values.put("exchange", portfolio.getExchangeName());
        values.put("stockname", portfolio.getStockName());
        values.put("scriptname", portfolio.getScriptName());
        values.put("ibought", portfolio.getiBoughtPrice());
        values.put("quantity", portfolio.getQuantityAmt());
        values.put("selltarget", portfolio.getSellTargetPrice());

        // Inserting Row
        db.insert(TABLE_PORTFOLIO, null, values);
        db.close();
        //Log.e("important", "inserted into portfolio " + portfolio.getScriptName());
    }

    public void insertIntoWatchlist(WatchObject watch) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put("serial", "NULL");
        values.put("exchange", watch.getExchangeName());
        values.put("stockname", watch.getStockName());
        values.put("scriptname", watch.getScriptName());
        values.put("buytarget", watch.getBuyTargetPrice());

        db.insert(TABLE_WATCHLIST, null, values);
        db.close();
        //Log.e("important","inserted into watchlist "+watch.getScriptName());
    }

    public List<PortfolioObject> getAllPortfolio() {
        List<PortfolioObject> portfolioObjectList = new ArrayList<PortfolioObject>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PORTFOLIO;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PortfolioObject temPort = new PortfolioObject();
                temPort.setExchangeName(cursor.getString(1));
                temPort.setStockName(cursor.getString(2));
                temPort.setScriptName(cursor.getString(3));
                temPort.setiBoughtPrice(Float.parseFloat(cursor.getString(4)));
                temPort.setQuantityAmt(Integer.parseInt(cursor.getString(5)));
                temPort.setSellTargetPrice(Float.parseFloat(cursor.getString(6)));
                temPort.setModifiable(true);
                // Adding contact to list
                portfolioObjectList.add(temPort);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return contact list
        return portfolioObjectList;
    }

    public PortfolioObject getOnePortfolio(String script) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_PORTFOLIO + " WHERE scriptname='" + script + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            PortfolioObject contact = new PortfolioObject(cursor.getString(1), cursor.getString(2), cursor.getString(3), Float.parseFloat(cursor.getString(4)), Integer.parseInt(cursor.getString(5)), Float.parseFloat(cursor.getString(6)),true);

            cursor.close();
            db.close();
            // return contact
            return contact;
        } else {
            db.close();
            return null;
        }
    }

    public List<WatchObject> getAllWatchList() {
        List<WatchObject> watchObjectList = new ArrayList<WatchObject>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WATCHLIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                WatchObject tempWatch = new WatchObject();
                tempWatch.setExchangeName(cursor.getString(1));
                tempWatch.setStockName(cursor.getString(2));
                tempWatch.setScriptName(cursor.getString(3));
                tempWatch.setBuyTargetPrice(Float.parseFloat(cursor.getString(4)));
                // Adding contact to list
                watchObjectList.add(tempWatch);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return contact list
        return watchObjectList;
    }

    public WatchObject getOneWatchList(String script) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_WATCHLIST + " WHERE scriptname='" + script + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            WatchObject watchObject = new WatchObject(cursor.getString(1), cursor.getString(2), cursor.getString(3), Float.parseFloat(cursor.getString(4)));

            cursor.close();
            db.close();

            return watchObject;
        } else {
            db.close();
            return null;
        }
    }

    public void editPortfolio(String script, float buyPrice, int quantity, float selltarget) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ibought", buyPrice);
        values.put("quantity", quantity);
        values.put("selltarget", selltarget);

        // updating row
        db.update(TABLE_PORTFOLIO, values, "scriptname = ?", new String[]{script});
        db.close();
    }

    public void editWatchList(String script, float buytarget) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("buytarget", buytarget);

        // updating row
        db.update(TABLE_WATCHLIST, values, "scriptname = ?", new String[]{script});
        db.close();
    }

    public boolean doesExistInPortfolio(String script) {
        String countQuery = "SELECT  * FROM " + TABLE_PORTFOLIO + " WHERE scriptname='" + script + "' COLLATE NOCASE";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.getCount() > 0) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }

    public boolean doesExistInWatchList(String script) {
        String countQuery = "SELECT  * FROM " + TABLE_WATCHLIST + " WHERE scriptname='" + script.toUpperCase() + "' COLLATE NOCASE";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.getCount() > 0) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }

    public void deleteFromPortfolio(String script) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PORTFOLIO, "scriptname = ?", new String[]{script});
        db.close();
    }

    public void deleteFromWatchList(String script) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WATCHLIST, "scriptname = ?", new String[]{script});
        db.close();
    }
}
