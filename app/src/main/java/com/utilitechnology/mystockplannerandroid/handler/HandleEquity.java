package com.utilitechnology.mystockplannerandroid.handler;

import android.content.Context;
import android.util.Log;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.parser.ParseJSON;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Bibaswann on 06-02-2016.
 */
public class HandleEquity {
    float LTP = 0.0f;
    private String open, prevClose, dayHigh, dayLow, yrHigh, yrLow, lastUpdate, volume, peRatio;
    String change = "", changePercent = "";
    String unconvertedScriptWithExtension, convertedscriptNameWithExtension;
    String stockUp;
    private String RESOURCE_URL_WEB = "https://www.msn.com/en-in/money/stockdetails/";
//    private String RESOURCE_URL_XML_P1 = "http://finance.yahoo.com/webservice/v1/symbols/";
//    private String RESOURCE_URL_XML_P2 = "/quote?format=xml&mView=detail";
//    private String RESOURCE_URL_GOOGLE = "http://finance.google.com/finance/info?infotype=infoquoteall&q=";
    ScriptHelper sch;
    Context mContext;
    Document doc = null;
    //ParseXML prx;
//    ParseJSON prj;

    public HandleEquity(Context context, String scriptName) {//It is unconverted scriptname with extension
        mContext = context;
        sch = new ScriptHelper(mContext);
        unconvertedScriptWithExtension = scriptName;
        convertedscriptNameWithExtension = sch.convertScript(scriptName);
        //Download the webpage and set everything

        //Method 1: Parse from XML (open & previous close is not found)

//        prx = new ParseXML(RESOURCE_URL_XML_P1 + convertedscriptNameWithExtension + RESOURCE_URL_XML_P2);
//        prx.fetchXML();
//        while (prx.parsingInComplete) ;
//        if (prx.getPrice().size() > 0)
//            LTP = stringToIndianFloat(prx.getPrice().get(0));
//        if (prx.getChange().size() > 0)
//            change = prx.getChange().get(0);
//        if (prx.getChangePercent().size() > 0)
//            changePercent = prx.getChangePercent().get(0);
//        lastUpdate = prx.getUpdate();
//        if (prx.getUpsNDowns().size() > 0)
//            stockUp = prx.getUpsNDowns().get(0);
//        if (prx.getDayHigh().size() > 0)
//            dayHigh = prx.getDayHigh().get(0);
//        if (prx.getDayLow().size() > 0)
//            dayLow = prx.getDayLow().get(0);
//        if (prx.getYearHigh().size() > 0)
//            yrHigh = prx.getYearHigh().get(0);
//        if (prx.getYearLow().size() > 0)
//            yrLow = prx.getYearLow().get(0);
//        if (prx.getVolume().size() > 0)
//            volume = prx.getVolume().get(0);

//        String googleScript;
//        if (unconvertedScriptWithExtension.split("\\.")[1].equals("ns"))
//            googleScript = "nse:" + unconvertedScriptWithExtension.split("\\.")[0].replace("&", "%26");
//        else
//            googleScript = "bom:" + unconvertedScriptWithExtension.split("\\.")[0].replace("&", "%26");
//        prj = new ParseJSON(RESOURCE_URL_GOOGLE + googleScript);
//        prj.fetchJSON();
//        while (prj.parsingInComplete) ;
//        if (prj.getPrice().size() > 0)
//            LTP = stringToIndianFloat(prj.getPrice().get(0));
//        if (prj.getChange().size() > 0)
//            change = prj.getChange().get(0);
//        if (prj.getChangePercent().size() > 0)
//            changePercent = prj.getChangePercent().get(0);
//        lastUpdate = prj.getUpdate();
//        if (prj.getUpsNDowns().size() > 0)
//            stockUp = prj.getUpsNDowns().get(0);
//        if (prj.getDayHigh().size() > 0)
//            dayHigh = prj.getDayHigh().get(0);
//        if (prj.getDayLow().size() > 0)
//            dayLow = prj.getDayLow().get(0);
//        if (prj.getYearHigh().size() > 0)
//            yrHigh = prj.getYearHigh().get(0);
//        if (prj.getYearLow().size() > 0)
//            yrLow = prj.getYearLow().get(0);
//        if (prj.getVolume().size() > 0)
//            volume = prj.getVolume().get(0);
//        if (prj.getP_closes().size() > 0)
//            prevClose = prj.getP_closes().get(0);
//        if (prj.getOpens().size() > 0)
//            open = prj.getOpens().get(0);
//        if (prj.getPe_ratios().size() > 0)
//            peRatio = prj.getPe_ratios().get(0);

        //Method 2: Scrape from website
        //Replace & here also
        try {
//            Log.e("important", RESOURCE_URL_WEB + sch.makeMSNString(unconvertedScriptWithExtension));
            doc = Jsoup.parse(new URL(RESOURCE_URL_WEB + sch.makeMSNString(unconvertedScriptWithExtension)), 10000);

            Elements topicList = doc.select("span[class=current-price]");
            for (org.jsoup.nodes.Element topic : topicList)
                LTP = stringToIndianFloat(topic.text().trim());
            Elements topicListChange = doc.select("div[data-role=change]");
                change = topicListChange.get(0).text().trim();
            Elements topicListChangePercent = doc.select("div[data-role=percentchange]");
                changePercent = topicListChangePercent.get(0).text().trim();
            Elements topicListLastUpdate = doc.select("span[class=update-datetime]");
            for (org.jsoup.nodes.Element topic : topicListLastUpdate)
                lastUpdate = "Last update: " + topic.text().trim() + " (Quotes delayed)";
            if (change.contains("+"))
                stockUp = "up";
            else
                stockUp = "down";

            Elements topicListHelper = doc.select("ul[class=today-trading-container]").select("li").select("span").select("p");
            try {
                prevClose = topicListHelper.get(3).text().trim();
                open = topicListHelper.get(1).text().trim();
                volume = topicListHelper.get(5).text().trim();
                dayHigh = topicListHelper.get(7).text().split(Pattern.quote("-"))[1].trim();
                dayLow = topicListHelper.get(7).text().split(Pattern.quote("-"))[0].trim();
                yrHigh = topicListHelper.get(9).text().split(Pattern.quote("-"))[1].trim();
                yrLow = topicListHelper.get(9).text().split(Pattern.quote("-"))[0].trim();
                peRatio=topicListHelper.get(19).text().trim();
            } catch (IndexOutOfBoundsException ioob) {
                prevClose = mContext.getString(R.string.global_not_found);
                open = mContext.getString(R.string.global_not_found);
                volume = mContext.getString(R.string.global_not_found);
                dayHigh = mContext.getString(R.string.global_not_found);
                dayLow = mContext.getString(R.string.global_not_found);
                yrHigh = mContext.getString(R.string.global_not_found);
                yrLow = mContext.getString(R.string.global_not_found);
                peRatio = mContext.getString(R.string.global_not_found);
            }
        } catch (IOException e) {
            //Log.e("important",e.getMessage());
        }
    }

    public float getLTP() {
        return LTP;
    }

    public String getChange() {
        return change + " (" + changePercent + ")";//Remove the parantheses if, god forbid, need to scrape screen again
    }

    public String getOpen() {
        return open;
    }

    public String getPrevClose() {
        return prevClose;
    }

    public String getDayHigh() {
        return dayHigh;
    }

    public String getDayLow() {
        return dayLow;
    }

    public String getYrHigh() {
        return yrHigh;
    }

    public String getYrLow() {
        return yrLow;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public String getStockUp() {
        return stockUp;
    }

    public String getVolume() {
        return volume;
    }

    public String getPeRatio() {
        return peRatio;
    }

    private float stringToIndianFloat(String text) {
        text=text.replace(",","");
        NumberFormat format = NumberFormat.getInstance(new Locale("en", "IN"));
        float formattedFloat = 0.00f;
        try {
            Number number = format.parse(text);
            formattedFloat = number.floatValue();
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        //Log.e("important","returning "+formattedFloat);
        return formattedFloat;
    }
}
