package com.utilitechnology.mystockplannerandroid.handler;

import android.content.Context;
import android.util.Log;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * Created by Bibaswann on 14-04-2016.
 */
public class HandleEquityStat {

    Context mContext;
    ScriptHelper sch;
    private String RESOURCE_URL_WEB = "https://in.finance.yahoo.com/quote/";
    String unconvertedScriptWithExtension;
    Document doc;
    String marketCap, enterpriceVal, trailingPe, forwardPe, pegRatio, priceSales, priceBook, profitMargin, operatingMargin, revenue, revenueShare, quarterlyRevenueGrowth, grossProfit, quarterlyEarningGrowth, totalCash, totalDebt, yearChange, averageVol, shareOutstanding, floatVal, annualDividend;

    public HandleEquityStat(Context context, String script) {
        mContext = context;
        sch = new ScriptHelper(mContext);
        unconvertedScriptWithExtension = script;
// TODO: 10-05-2017 many na's coming, remove them
        try {
            String tempScr=sch.convertScript(unconvertedScriptWithExtension).replace("&", "%26");
            doc = Jsoup.parse(new URL(RESOURCE_URL_WEB+tempScr+"/key-statistics?p="+tempScr), 10000);
            Elements topicListHelper = doc.select("span[class=_2bean]");
//            for (Element t:topicListHelper) {
//                Log.e("important", t.text());
//            }
            try {
                marketCap = topicListHelper.get(22).text().trim();
                enterpriceVal = topicListHelper.get(23).text().trim();
                trailingPe = topicListHelper.get(24).text().trim();
                forwardPe = topicListHelper.get(25).text().trim();
                pegRatio = topicListHelper.get(26).text().trim();
                priceSales = topicListHelper.get(27).text().trim();
                priceBook = topicListHelper.get(28).text().trim();

                profitMargin = topicListHelper.get(33).text().trim();
                operatingMargin = topicListHelper.get(34).text().trim();

                revenue = topicListHelper.get(37).text().trim();
                revenueShare = topicListHelper.get(38).text().trim();
                quarterlyRevenueGrowth = topicListHelper.get(39).text().trim();
                grossProfit = topicListHelper.get(40).text().trim();

                quarterlyEarningGrowth = topicListHelper.get(44).text().trim();
                totalCash = topicListHelper.get(45).text().trim();

                totalDebt = topicListHelper.get(47).text().trim();

                yearChange = topicListHelper.get(54).text().trim();

                averageVol = topicListHelper.get(60).text().trim();

                shareOutstanding = topicListHelper.get(62).text().trim();
                floatVal = topicListHelper.get(63).text().trim();

                annualDividend = topicListHelper.get(72).text().trim() + " (" + topicListHelper.get(73).text().trim()+")";
            } catch (IndexOutOfBoundsException iobx) {
                marketCap = mContext.getString(R.string.global_not_found);
                enterpriceVal = mContext.getString(R.string.global_not_found);
                trailingPe = mContext.getString(R.string.global_not_found);
                forwardPe = mContext.getString(R.string.global_not_found);
                pegRatio = mContext.getString(R.string.global_not_found);
                priceSales = mContext.getString(R.string.global_not_found);
                priceBook = mContext.getString(R.string.global_not_found);
                profitMargin = mContext.getString(R.string.global_not_found);
                operatingMargin = mContext.getString(R.string.global_not_found);
                revenue = mContext.getString(R.string.global_not_found);
                revenueShare = mContext.getString(R.string.global_not_found);
                quarterlyRevenueGrowth = mContext.getString(R.string.global_not_found);
                quarterlyEarningGrowth = mContext.getString(R.string.global_not_found);
                grossProfit = mContext.getString(R.string.global_not_found);
                totalCash = mContext.getString(R.string.global_not_found);
                totalDebt = mContext.getString(R.string.global_not_found);
                yearChange = mContext.getString(R.string.global_not_found);
                averageVol = mContext.getString(R.string.global_not_found);
                shareOutstanding = mContext.getString(R.string.global_not_found);
                floatVal = mContext.getString(R.string.global_not_found);
                annualDividend = mContext.getString(R.string.global_not_found);
            }
        } catch (IOException ignored) {
        }
    }

    public String getMarketCap() {
        return marketCap;
    }

    public String getEnterpriceVal() {
        return enterpriceVal;
    }

    public String getTrailingPe() {
        return trailingPe;
    }

    public String getForwardPe() {
        return forwardPe;
    }

    public String getPegRatio() {
        return pegRatio;
    }

    public String getPriceSales() {
        return priceSales;
    }

    public String getPriceBook() {
        return priceBook;
    }

    public String getProfitMargin() {
        return profitMargin;
    }

    public String getOperatingMargin() {
        return operatingMargin;
    }

    public String getRevenue() {
        return revenue;
    }

    public String getRevenueShare() {
        return revenueShare;
    }

    public String getQuarterlyRevenueGrowth() {
        return quarterlyRevenueGrowth;
    }

    public String getGrossProfit() {
        return grossProfit;
    }

    public String getQuarterlyEarningGrowth() {
        return quarterlyEarningGrowth;
    }

    public String getTotalCash() {
        return totalCash;
    }

    public String getTotalDebt() {
        return totalDebt;
    }

    public String getYearChange() {
        return yearChange;
    }

    public String getAverageVol() {
        return averageVol;
    }

    public String getShareOutstanding() {
        return shareOutstanding;
    }

    public String getFloatVal() {
        return floatVal;
    }

    public String getAnnualDividend() {
        return annualDividend;
    }
}
