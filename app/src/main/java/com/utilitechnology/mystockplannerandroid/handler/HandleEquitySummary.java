package com.utilitechnology.mystockplannerandroid.handler;

import android.content.Context;
import android.util.Log;

import com.utilitechnology.mystockplannerandroid.ScriptHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Bibaswann on 14-04-2016.
 */
public class HandleEquitySummary {

    Context mContext;
    ScriptHelper sch;
    private String RESOURCE_URL_WEB = "https://www.google.com/finance?q=";
    String unconvertedScriptWithExtension;
    Document doc;
    String summaryBody;

    public HandleEquitySummary(Context context, String script) {
        mContext = context;
        unconvertedScriptWithExtension = script;
        sch = new ScriptHelper(mContext);

        try {
            String googleScript;
            if (unconvertedScriptWithExtension.split("\\.")[1].equals("ns"))
                googleScript = "nse:" + unconvertedScriptWithExtension.split("\\.")[0].replace("&", "%26");
            else
                googleScript = "bom:" + unconvertedScriptWithExtension.split("\\.")[0].replace("&", "%26");

            doc = Jsoup.parse(new URL(RESOURCE_URL_WEB + googleScript), 10000);
            Elements topicListHelper = doc.select("div[class=companySummary]");
            if (topicListHelper.size() > 0)
                summaryBody = topicListHelper.get(0).text().trim();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getSummaryBody() {
        return summaryBody;
    }
}
