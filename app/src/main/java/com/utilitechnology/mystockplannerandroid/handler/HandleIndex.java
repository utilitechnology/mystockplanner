package com.utilitechnology.mystockplannerandroid.handler;

import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.SaveIndexData;
import com.utilitechnology.mystockplannerandroid.caching.SettingsAPI;

/**
 * Created by Bibaswann on 26-01-2016.
 */
public class HandleIndex extends AsyncTask<Void, Void, Void> {
    private String BSENow, NSENow, BSEchange, NSEchange, BSEChangePercent, NSEChangePercent, BSELastUpdate, NSELastUpdate;
    private String openVNS, PrevCloseNS, dayHighNS, dayLowNS, yrHighNS, yrLowNS;
    private String openVBS, PrevCloseBS, dayHighBS, dayLowBS, yrHighBS, yrLowBS;
    private boolean bseUp, nseUp;
    View rootView;
    private final String BSE_RESOURCE = "https://in.finance.yahoo.com/q?s=^bsesn";
    private final String NSE_RESOURCE = "https://in.finance.yahoo.com/q?s=^nsei";
    private TextView BSEPriceLbl, NSEPriceLbl, BSEChangeLbl, NSEChangeLbl;
    private ImageView BSEArrow,NSEArrow;
    private TextView open, prev, dhigh, dlow, yhigh, ylow;
    private TextView lastUpdate;
    SettingsAPI set;
    SaveIndexData svi;

    public HandleIndex(View v) {
        rootView = v;
        BSEPriceLbl = (TextView) rootView.findViewById(R.id.stocdetailskval);
        BSEChangeLbl = (TextView) rootView.findViewById(R.id.sensexchange);
        NSEPriceLbl = (TextView) rootView.findViewById(R.id.niftyval);
        NSEChangeLbl = (TextView) rootView.findViewById(R.id.niftychange);

        BSEArrow= (ImageView) rootView.findViewById(R.id.genArrowSensex);
        NSEArrow= (ImageView) rootView.findViewById(R.id.genArrowNifty);

        open = (TextView) rootView.findViewById(R.id.openval);
        prev = (TextView) rootView.findViewById(R.id.prval);
        dhigh = (TextView) rootView.findViewById(R.id.dhval);
        dlow = (TextView) rootView.findViewById(R.id.dlval);
        yhigh = (TextView) rootView.findViewById(R.id.yhval);
        ylow = (TextView) rootView.findViewById(R.id.ylval);

        lastUpdate = (TextView) rootView.findViewById(R.id.lastUpdate);

        set = new SettingsAPI(rootView.getContext());
        svi = new SaveIndexData(rootView.getContext());
    }

    @Override
    protected Void doInBackground(Void... params) {
        getBSEValues();
        getNSEValues();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        BSEPriceLbl.setText(BSENow);
        if (bseUp) {
            BSEChangeLbl.setText(String.format("%s %s", BSEchange.replace("+",""), BSEChangePercent.replace("+","")));
            BSEPriceLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_up));
            BSEChangeLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_up));
            BSEArrow.setImageResource(R.drawable.up);
        } else {
            BSEChangeLbl.setText(String.format("%s %s", BSEchange.replace("-",""), BSEChangePercent.replace("-","")));
            BSEPriceLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_down));
            BSEChangeLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_down));
            BSEArrow.setImageResource(R.drawable.down);
        }

        NSEPriceLbl.setText(NSENow);
        if (nseUp) {
            NSEChangeLbl.setText(String.format("%s %s", NSEchange.replace("+",""), NSEChangePercent.replace("+","")));
            NSEPriceLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_up));
            NSEChangeLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_up));
            NSEArrow.setImageResource(R.drawable.up);
        } else {
            NSEChangeLbl.setText(String.format("%s %s", NSEchange.replace("-",""), NSEChangePercent.replace("-","")));
            NSEPriceLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_down));
            NSEChangeLbl.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.color_index_down));
            NSEArrow.setImageResource(R.drawable.down);
        }

        if (set.readSetting("currentIndex").equals("^nsei")) {
            open.setText(openVNS);
            prev.setText(PrevCloseNS);
            dhigh.setText(dayHighNS);
            dlow.setText(dayLowNS);
            yhigh.setText(yrHighNS);
            ylow.setText(yrLowNS);
            //Todo: show actual value when google finance shows correct
            lastUpdate.setText(NSELastUpdate);
        } else {
            open.setText(openVBS);
            prev.setText(PrevCloseBS);
            dhigh.setText(dayHighBS);
            dlow.setText(dayLowBS);
            yhigh.setText(yrHighBS);
            ylow.setText(yrLowBS);
            lastUpdate.setText(BSELastUpdate);
        }
    }

    private void getBSEValues() {
        //First check if offline value is not present or more than 5 minutes old, if so, or if not present, save and show, save using SaveIndexData.java
        if (set.readSetting("indexTimeStamp").equals("na"))
            svi.downloadValues();
        else if (((System.currentTimeMillis() / 1000) - (Long.valueOf(set.readSetting("indexTimeStamp")))) > (5 * 60))
            svi.downloadValues();
        BSENow = svi.getBSENow();
        BSEchange = svi.getBSEchange();
        BSEChangePercent = svi.getBSEChangePercent();
        BSELastUpdate = svi.getBSELastUpdate();
        if (svi.getBseUp().equals("up"))
            bseUp = true;
        else
            bseUp = false;
        PrevCloseBS = svi.getPrevCloseBS();
        openVBS = svi.getOpenVBS();
        dayHighBS = svi.getDayHighBS();
        dayLowBS = svi.getDayLowBS();
        yrHighBS = svi.getYrHighBS();
        yrLowBS = svi.getYrLowBS();
    }

    private void getNSEValues() {
        if (set.readSetting("indexTimeStamp").equals("na"))
            svi.downloadValues();
        else if (((System.currentTimeMillis() / 1000) - (Long.valueOf(set.readSetting("indexTimeStamp")))) > (5 * 60))
            svi.downloadValues();
        NSENow = svi.getNSENow();
        NSEchange = svi.getNSEchange();
        NSEChangePercent = svi.getNSEChangePercent();
        NSELastUpdate = svi.getNSELastUpdate();
        if (svi.getNseUp().equals("up"))
            nseUp = true;
        else
            nseUp = false;
        PrevCloseNS = svi.getPrevCloseNS();
        openVNS = svi.getOpenVNS();
        dayHighNS = svi.getDayHighNS();
        dayLowNS = svi.getDayLowNS();
        yrHighNS = svi.getYrHighNS();
        yrLowNS = svi.getYrLowNS();
        //getTempNiftyFiftyFiftyTwoWeekValue();
    }
}
