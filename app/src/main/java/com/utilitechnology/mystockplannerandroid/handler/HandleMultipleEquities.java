package com.utilitechnology.mystockplannerandroid.handler;

import android.content.Context;

import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.parser.ParseCustomXML;
import com.utilitechnology.mystockplannerandroid.parser.ParseJSON;

import org.jsoup.nodes.Document;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 09-03-2016.
 */
public class HandleMultipleEquities {
    List<String> LTPs = new ArrayList<>();
    List<String> changeStrings = new ArrayList<>();
    String change, changePercent;
    //String yahooQueryString = "";
    String googleQueryString = "";
    List<String> upDown = new ArrayList<>();
//    private String RESOURCE_URL_WEB = "https://in.finance.yahoo.com/quotes/";
//    private String RESOURCE_URL_XML_P1 = "http://finance.yahoo.com/webservice/v1/symbols/";
//    private String RESOURCE_URL_XML_P2 = "/quote?format=xml&mView=detail";
//    private String RESOURCE_URL_GOOGLE = "http://finance.google.com/finance/info?infotype=infoquoteall&q=";
    private String RESOURCE_URL_CUSTOM = "http://www.utilitechnology.com/api/mystockplanner/track_equity.php?scripts=";
    ScriptHelper sch;
    Context mContext;
    Document doc = null;
//    ParseXML prx;
    ParseCustomXML prj;

    public HandleMultipleEquities(Context context, List<String> scriptNamesWithExtension) {//It is unconverted scriptname with extension
        mContext = context;
        sch = new ScriptHelper(mContext);

//        String yahooQueryString ="";
//        for (String query : scriptNamesWithExtension) {
//            String cswe = sch.convertScript(query);
//            yahooQueryString += cswe + ",";
//        }
//        yahooQueryString = yahooQueryString.substring(0, yahooQueryString.length() - 1);
//
//        prx = new ParseXML(RESOURCE_URL_XML_P1 + yahooQueryString + RESOURCE_URL_XML_P2);
//        prx.fetchXML();
//        while (prx.parsingInComplete) ;
//        LTPs = prx.getPrice();
//        changeStrings = prx.getChangeString();
//        upDown = prx.getUpsNDowns();

        //Method 1: parse from google finance
        for (String query : scriptNamesWithExtension) {
            String googleString;
            if (query.split("\\.")[1].equals("ns"))
                googleString = "nse:" + query.split("\\.")[0].replace("&", "%26");
            else
                googleString = "bom:" + query.split("\\.")[0].replace("&", "%26");
            googleQueryString += googleString + ",";
        }

        googleQueryString = googleQueryString.substring(0, googleQueryString.length() - 1);

        prj = new ParseCustomXML("custom",RESOURCE_URL_CUSTOM + googleQueryString);
        prj.fetchXML();
        while (prj.parsingInComplete) ;
        LTPs = prj.getPrice();
        changeStrings = prj.getChangeString();
        upDown = prj.getUpsNDowns();

//            //Method 2: Scrap from webpage
//        String yahooQueryString ="";
//        List<String> convertedScriptsWithExtension = new ArrayList<>();
//        for (String query : scriptNamesWithExtension) {
//            String cswe = sch.convertScript(query);
//            convertedScriptsWithExtension.add(cswe);
//            yahooQueryString += cswe + ",";
//        }
//        yahooQueryString = yahooQueryString.substring(0, yahooQueryString.length() - 1);
//
//        try {
//            doc = Jsoup.parse(new URL(RESOURCE_URL_WEB + yahooQueryString), 10000);
//            for (String script : convertedScriptsWithExtension) {
//                Elements topicListPrice = doc.select("span[id=yfs_l84_" + script + "]");
//                for (org.jsoup.nodes.Element topic : topicListPrice)
//                    LTPs.add(topic.text().trim());
//                Elements topicListChange = doc.select("span[id=yfs_c63_" + script + "]");
//                for (org.jsoup.nodes.Element topic : topicListChange) {
//                    if (topic.text().contains("-"))
//                        upDown.add("down");
//                    else
//                        upDown.add("up");
//                    change = topic.text().trim().replace("+", "").replace("-", "");
//                }
//                Elements topicListChangePercent = doc.select("span[id=yfs_pp0_" + script + "]");
//                for (org.jsoup.nodes.Element topic : topicListChangePercent)
//                    changePercent = topic.text().trim().replace("+", "").replace("-", "");
//                changeStrings.add(change + " (" + changePercent + ")");
//            }
//        } catch (IOException e) {
//            //Log.e("important",e.getMessage());
//        }
    }

    public List<Float> getLTPs() {
        List<Float> floatLTPs = new ArrayList<>();
        for (String cmp : LTPs) {
            floatLTPs.add(stringToIndianFloat(cmp));
        }
        return floatLTPs;
    }

    public List<String> getStringLTPs() {
        return LTPs;
    }

    public List<String> getChangeStrings() {
        return changeStrings;
    }

    public List<String> getUpDown() {
        return upDown;
    }

    private float stringToIndianFloat(String text) {
        NumberFormat format = NumberFormat.getInstance(new Locale("en", "IN"));
        float formattedFloat = 0.00f;
        try {
            Number number = format.parse(text);
            formattedFloat = number.floatValue();
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        //Log.e("important","returning "+formattedFloat);
        return formattedFloat;
    }
}
