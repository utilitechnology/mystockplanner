package com.utilitechnology.mystockplannerandroid.handler;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.activity.ExecuteOffsiteZerodhaTradeActivity;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.prade.Alarm;
import com.utilitechnology.mystockplannerandroid.prade.BackSquareOff;
import com.utilitechnology.mystockplannerandroid.prade.BackTrade;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by Bibaswann on 18-06-2016.
 */
public class HandleTrade {

    View v;
    Context mContext;
    ScriptHelper sch;
    TradingAccountSettingsAPI tras;
    FloatingActionButton tradone, tradel;
    RadioGroup bOrS, mktOrLimOrSl, cncOrMis;
    RadioButton mktLimSl;
    RadioButton rbuy, rsell, rmkt, rlimit, rsl, rslm, rcnc, rmis;
    LinearLayout limPriceSect, triggerSect;
    AutoCompleteTextView stockToInsert;
    EditText limitPrice, quantity, disclosed, trigger;
    Spinner validity;
    View view;
    TextView info;
    //ExecuteZerodhaTrade exzt;

    public HandleTrade(@Nullable final Dialog parentDlg, final View v, final String type, @Nullable final String ordrId, @Nullable final HashMap modata)//type can be buysell or modify or delete. OrdrId is only used in modification and deletion
    {
        view = v;
        mContext = view.getContext();
        sch = new ScriptHelper(mContext);
        tras = new TradingAccountSettingsAPI(mContext);
        //exzt = new ExecuteZerodhaTrade();

        info = (TextView) view.findViewById(R.id.infoTxt);
        limPriceSect = (LinearLayout) view.findViewById(R.id.buyLimPriceLayout);
        triggerSect = (LinearLayout) view.findViewById(R.id.buyTriggerLayout);

        tradone = (FloatingActionButton) view.findViewById(R.id.tradeDone);
        tradel = (FloatingActionButton) view.findViewById(R.id.tradeDel);

        stockToInsert = (AutoCompleteTextView) view.findViewById(R.id.buyStockAutoComplete);
        if (modata == null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, getAllCompanies());
            stockToInsert.setAdapter(adapter);
        } else {
            String script = modata.get("script").toString();
            String wholeName = sch.unconvertedScriptToWholeString(script);
            stockToInsert.setText(wholeName);
            stockToInsert.setFocusable(false);
        }

        limitPrice = (EditText) view.findViewById(R.id.buyLimPrice);
        quantity = (EditText) view.findViewById(R.id.buyQuantity);
        disclosed = (EditText) view.findViewById(R.id.buyDisclosed);
        trigger = (EditText) view.findViewById(R.id.buyTrigger);
        validity = (Spinner) view.findViewById(R.id.buyValidityDropdown);

        rbuy = (RadioButton) view.findViewById(R.id.radioButtonBuy);
        rsell = (RadioButton) view.findViewById(R.id.radioButtonSell);
        rmkt = (RadioButton) view.findViewById(R.id.radioButtonMkt);
        rlimit = (RadioButton) view.findViewById(R.id.radioButtonLmt);
        rsl = (RadioButton) view.findViewById(R.id.radioButtonSl);
        rslm = (RadioButton) view.findViewById(R.id.radioButtonSlM);
        rcnc = (RadioButton) view.findViewById(R.id.radioButtonDelivery);
        rmis = (RadioButton) view.findViewById(R.id.radioButtonIntraday);

        bOrS = (RadioGroup) view.findViewById(R.id.bOrS);
        cncOrMis = (RadioGroup) view.findViewById(R.id.cncOrMis);
        mktOrLimOrSl = (RadioGroup) view.findViewById(R.id.mktOrLimOrSl);
        mktOrLimOrSl.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mktLimSl = (RadioButton) view.findViewById(checkedId);
                switch (mktLimSl.getText().toString()) {
                    case "Market":
                        limPriceSect.setVisibility(View.GONE);
                        triggerSect.setVisibility(View.GONE);
                        break;
                    case "Limit":
                        limPriceSect.setVisibility(View.VISIBLE);
                        triggerSect.setVisibility(View.GONE);
                        break;
                    case "SL-L":
                        limPriceSect.setVisibility(View.VISIBLE);
                        triggerSect.setVisibility(View.VISIBLE);
                        break;
                    case "SL-M":
                        limPriceSect.setVisibility(View.GONE);
                        triggerSect.setVisibility(View.VISIBLE);
                        break;
                    default:
                        limPriceSect.setVisibility(View.GONE);
                        triggerSect.setVisibility(View.GONE);
                        break;
                }
            }
        });

        if (type.equals("buy") || type.equals("sell") || type.equals("buysell")) {
            if (type.equals("buy") || type.equals("sell")) {
                if (type.equals("buy"))
                    rbuy.setChecked(true);
                else
                    rsell.setChecked(true);
                rbuy.setEnabled(false);
                rsell.setEnabled(false);
            }

            final Alarm alarm = new Alarm();
            tradone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (stockToInsert.getText().toString().equals("S")) {
                        alarm.SetTradeAlarm(mContext);
                        Toast.makeText(mContext, "Set", Toast.LENGTH_SHORT).show();
                    } else if (stockToInsert.getText().toString().equals("T")) {
                        new BackTrade(mContext).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        Toast.makeText(mContext, "Starting now", Toast.LENGTH_SHORT).show();
                    } else if (stockToInsert.getText().toString().equals("E")) {
                        new BackSquareOff(mContext).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        Toast.makeText(mContext, "Stopping now", Toast.LENGTH_SHORT).show();
                    } else {
                        if (stockToInsert.getText().toString().equals("")) {
                            if (parentDlg != null) {
                                info.setText("Please provide stock name");
                                info.setVisibility(View.VISIBLE);
                            } else
                                Snackbar.make(view, "Please provide stock name", Snackbar.LENGTH_LONG).show();
                            return;
                        } else if (quantity.getText().toString().equals("")) {
                            if (parentDlg != null) {
                                info.setText("Please provide quantity");
                                info.setVisibility(View.VISIBLE);
                            } else
                                Snackbar.make(view, "Please provide quantity", Snackbar.LENGTH_LONG).show();
                            return;
                        } else if (disclosed.getText().toString().equals("")) {
                            if (parentDlg != null) {
                                info.setText("Disclosed quantity must be 0 or more");
                                info.setVisibility(View.VISIBLE);
                            } else
                                Snackbar.make(view, "Disclosed quantity must be 0 or more", Snackbar.LENGTH_LONG).show();
                            return;
                        }

                        HashMap<String, String> hashMap = sch.breakTotalString(stockToInsert.getText().toString());
                        String scriptToBuy = sch.convertScript(hashMap.get("script")).toUpperCase();
                        String exchangeToBuy = hashMap.get("exchange").toUpperCase();
                        String transactionType = ((RadioButton) view.findViewById(bOrS.getCheckedRadioButtonId())).getText().toString().toUpperCase();
                        String orderType;
                        if (((RadioButton) view.findViewById(mktOrLimOrSl.getCheckedRadioButtonId())).getText().toString().equals("SL-L"))
                            orderType = "SL";
                        else
                            orderType = ((RadioButton) view.findViewById(mktOrLimOrSl.getCheckedRadioButtonId())).getText().toString().toUpperCase();
                        String product;
                        if (((RadioButton) view.findViewById(cncOrMis.getCheckedRadioButtonId())).getText().toString().equals("Delivery"))
                            product = "CNC";
                        else
                            product = "MIS";
                        HashMap<String, String> parahm = new HashMap<String, String>();
//                        parahm.put("api_key", mContext.getString(R.string.ZERODHA_API_KEY));
//                        parahm.put("access_token", tras.readSetting("access_token"));
                        parahm.put("tradingsymbol", scriptToBuy);
                        parahm.put("exchange", exchangeToBuy);
                        parahm.put("transaction_type", transactionType);
                        parahm.put("order_type", orderType);
                        parahm.put("product", product);//mis or cnc
                        parahm.put("quantity", quantity.getText().toString());
                        if (disclosed.getText().toString().equals("0"))
                            parahm.put("disclosed_quantity", quantity.getText().toString());
                        else
                            parahm.put("disclosed_quantity", disclosed.getText().toString());
                        parahm.put("validity", validity.getSelectedItem().toString());
                        if (!orderType.equals("MARKET")) {
                            if (orderType.equals("LIMIT")) {
                                if (limitPrice.getText().toString().equals("")) {
                                    if (parentDlg != null) {
                                        info.setText("Please provide limit price");
                                        info.setVisibility(View.VISIBLE);
                                    } else
                                        Snackbar.make(view, "Please provide limit price", Snackbar.LENGTH_LONG).show();
                                    return;
                                } else
                                    parahm.put("price", limitPrice.getText().toString());//for limit
                            } else if (orderType.equals("SL")) {
                                String limForSl, trigForSl;
                                if (limitPrice.getText().toString().equals("")) {
                                    if (parentDlg != null) {
                                        info.setText("Please provide limit price");
                                        info.setVisibility(View.VISIBLE);
                                    } else
                                        Snackbar.make(view, "Please provide limit price", Snackbar.LENGTH_LONG).show();
                                    return;
                                } else
                                    limForSl = limitPrice.getText().toString();
                                if (trigger.getText().toString().equals("")) {
                                    if (parentDlg != null) {
                                        info.setText("Please provide trigger price");
                                        info.setVisibility(View.VISIBLE);
                                    } else
                                        Snackbar.make(view, "Please provide trigger price", Snackbar.LENGTH_LONG).show();
                                    return;
                                } else
                                    trigForSl = trigger.getText().toString();

                                if (transactionType.equals("BUY")) {
                                    if (Float.parseFloat(trigForSl) < Float.parseFloat(limForSl)) {
                                        parahm.put("price", limForSl);//for limit & sl
                                        parahm.put("trigger_price", trigForSl);//for sl
                                    } else {
                                        if (parentDlg != null) {
                                            info.setText("Trigger price cannot be greater than or equal to limit");
                                            info.setVisibility(View.VISIBLE);
                                        } else
                                            Snackbar.make(view, "Trigger price cannot be greater than or equal to limit", Snackbar.LENGTH_LONG).show();
                                        return;
                                    }
                                } else {
                                    if (Float.parseFloat(trigForSl) > Float.parseFloat(limForSl)) {
                                        parahm.put("price", limForSl);//for limit & sl
                                        parahm.put("trigger_price", trigForSl);//for sl
                                    } else {
                                        if (parentDlg != null) {
                                            info.setText("Trigger price cannot be less than or equal to limit");
                                            info.setVisibility(View.VISIBLE);
                                        } else
                                            Snackbar.make(view, "Trigger price cannot be less than or equal to limit", Snackbar.LENGTH_LONG).show();
                                        return;
                                    }
                                }
                            } else if (orderType.equals("SL-M")) {
                                String trigForSlM;
                                if (trigger.getText().toString().equals("")) {
                                    if (parentDlg != null) {
                                        info.setText("Please provide trigger price");
                                        info.setVisibility(View.VISIBLE);
                                    } else
                                        Snackbar.make(view, "Please provide trigger price", Snackbar.LENGTH_LONG).show();
                                    return;
                                } else
                                    trigForSlM = trigger.getText().toString();
                                parahm.put("trigger_price", trigForSlM);//for slm
                            }
                        }
                        //Log.e("important","final hashmap "+parahm.toString());

                        //Placing order using zerodha
                        if (parentDlg != null) {
                            info.setText("Redirecting for confirmation");
                            info.setVisibility(View.VISIBLE);
                            parentDlg.dismiss();
                        } else {
                            Snackbar.make(view, "Redirecting for confirmation", Snackbar.LENGTH_LONG).show();
                            makeFormDefault();
                        }

                        Intent orderIntent = new Intent(mContext, ExecuteOffsiteZerodhaTradeActivity.class);
                        Integer intQty = Integer.parseInt(parahm.get("quantity"));
//                        parahm.remove("api_key");
//                        parahm.remove("access_token");
                        parahm.remove("quantity");
                        String offsiteReadyDataWOQ = new JSONObject(parahm).toString();
                        String offsiteReadyDataWQ = "[" + offsiteReadyDataWOQ.substring(0, offsiteReadyDataWOQ.length() - 1) + ",\"quantity\":" + intQty + "}]";
                        orderIntent.putExtra("data", offsiteReadyDataWQ);
                        mContext.startActivity(orderIntent);

////////////////////////////////////////The Below lines will cost me 25k bucks//////////////////////////////////////////
/////////// TODO: 05-03-2017 important, after the first order, the session becomes invalid, test that //////////////////
//                        exzt.placeBuySellOrder(parahm, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
//                        while (exzt.orderProcessingIncomplete) ;
//                        if (exzt.orderProcessingSuccess) {
//                            if (parentDlg != null) {
//                                info.setText("Order placed successfully");
//                                info.setVisibility(View.VISIBLE);
//                                parentDlg.dismiss();
//                            } else {
//                                Snackbar.make(view, "Order placed successfully", Snackbar.LENGTH_LONG).show();
//                                makeFormDefault();
//                            }
//                        } else {
//                            if (parentDlg != null) {
//                                info.setText("Order rejected by server");
//                                info.setVisibility(View.VISIBLE);
//                                parentDlg.dismiss();
//                            } else {
//                                Snackbar.make(view, "Order rejected by server", Snackbar.LENGTH_LONG).show();
//                                makeFormDefault();
//                            }
//                        }
                    }
                }
            });
        } else {
            //// TODO: 02-03-2017 important: I don't know whether modify and delete works, test before use
            //Set the texts and make them unchangable
//            if (modata != null) {
//                if (modata.get("transaction_type").toString().equals("BUY"))
//                    rbuy.setChecked(true);
//                else
//                    rsell.setChecked(true);
//                rbuy.setEnabled(false);
//                rsell.setEnabled(false);
//
//                if (modata.get("order_type").toString().equals("MARKET"))
//                    rmkt.setChecked(true);
//                else if (modata.get("order_type").toString().equals("LIMIT"))
//                    rlimit.setChecked(true);
//                else
//                    rsl.setChecked(true);
//                rmkt.setEnabled(false);
//                rlimit.setEnabled(false);
//                rsl.setEnabled(false);
//
//                if (modata.get("product").toString().equals("MIS"))
//                    rmis.setChecked(true);
//                else
//                    rcnc.setChecked(true);
//                rmis.setEnabled(false);
//                rcnc.setEnabled(false);
//
//                quantity.setText(modata.get("quantity").toString());
//
//                if (modata.get("order_type").toString().equals("LIMIT"))
//                    limitPrice.setText(modata.get("price").toString());
//                else if (modata.get("order_type").toString().equals("SL"))
//                    trigger.setText(modata.get("trigger_price").toString());
//
//                disclosed.setText(modata.get("disclosed_quantity").toString());
//
//                if (modata.get("validity").toString().equals("DAY"))
//                    validity.setSelection(0);
//                else
//                    validity.setSelection(1);
//
//                tradone.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (quantity.getText().toString().equals("")) {
//                            if (parentDlg != null) {
//                                info.setText("Please provide quantity");
//                                info.setVisibility(View.VISIBLE);
//                            } else
//                                Snackbar.make(view, "Please provide quantity", Snackbar.LENGTH_LONG).show();
//                            return;
//                        } else if (disclosed.getText().toString().equals("")) {
//                            if (parentDlg != null) {
//                                info.setText("Disclosed quantity must be 0 or more");
//                                info.setVisibility(View.VISIBLE);
//                            } else
//                                Snackbar.make(view, "Disclosed quantity must be 0 or more", Snackbar.LENGTH_LONG).show();
//                            return;
//                        }
//
//                        HashMap<String, String> parahm = new HashMap<String, String>();
////                        parahm.put("api_key", mContext.getString(R.string.ZERODHA_API_KEY));
////                        parahm.put("access_token", tras.readSetting("access_token"));
//                        parahm.put("tradingsymbol", modata.get("tradingsymbol").toString());
//                        parahm.put("exchange", modata.get("exchange").toString());
//                        parahm.put("transaction_type", modata.get("transaction_type").toString());
//                        parahm.put("order_type", modata.get("order_type").toString());
//                        parahm.put("product", modata.get("product").toString());//mis or cnc
//                        parahm.put("quantity", quantity.getText().toString());
//                        if (disclosed.getText().toString().equals("0"))
//                            parahm.put("disclosed_quantity", quantity.getText().toString());
//                        else
//                            parahm.put("disclosed_quantity", disclosed.getText().toString());
//                        parahm.put("validity", validity.getSelectedItem().toString());
//                        if (!modata.get("order_type").toString().equals("MARKET")) {
//                            if (modata.get("order_type").toString().equals("LIMIT")) {
//                                if (limitPrice.getText().toString().equals("")) {
//                                    if (parentDlg != null) {
//                                        info.setText("Please provide limit price");
//                                        info.setVisibility(View.VISIBLE);
//                                    } else
//                                        Snackbar.make(view, "Please provide limit price", Snackbar.LENGTH_LONG).show();
//                                    return;
//                                } else
//                                    parahm.put("price", limitPrice.getText().toString());//for limit
//                            } else if (modata.get("order_type").toString().equals("SL")) {
//                                String limForSl, trigForSl;
//                                if (limitPrice.getText().toString().equals("")) {
//                                    if (parentDlg != null) {
//                                        info.setText("Please provide limit price");
//                                        info.setVisibility(View.VISIBLE);
//                                    } else
//                                        Snackbar.make(view, "Please provide limit price", Snackbar.LENGTH_LONG).show();
//                                    return;
//                                } else
//                                    limForSl = limitPrice.getText().toString();
//                                if (trigger.getText().toString().equals("")) {
//                                    if (parentDlg != null) {
//                                        info.setText("Please provide trigger price");
//                                        info.setVisibility(View.VISIBLE);
//                                    } else
//                                        Snackbar.make(view, "Please provide trigger price", Snackbar.LENGTH_LONG).show();
//                                    return;
//                                } else
//                                    trigForSl = trigger.getText().toString();
//
//                                if (modata.get("transaction_type").toString().equals("BUY")) {
//                                    if (Float.parseFloat(trigForSl) < Float.parseFloat(limForSl)) {
//                                        parahm.put("price", limForSl);//for limit & sl
//                                        parahm.put("trigger_price", trigForSl);//for sl
//                                    } else {
//                                        if (parentDlg != null) {
//                                            info.setText("Trigger price cannot be greater than or equal to limit");
//                                            info.setVisibility(View.VISIBLE);
//                                        } else
//                                            Snackbar.make(view, "Trigger price cannot be greater than or equal to limit", Snackbar.LENGTH_LONG).show();
//                                        return;
//                                    }
//                                } else {
//                                    if (Float.parseFloat(trigForSl) > Float.parseFloat(limForSl)) {
//                                        parahm.put("price", limForSl);//for limit & sl
//                                        parahm.put("trigger_price", trigForSl);//for sl
//                                    } else {
//                                        if (parentDlg != null) {
//                                            info.setText("Trigger price cannot be less than or equal to limit");
//                                            info.setVisibility(View.VISIBLE);
//                                        } else
//                                            Snackbar.make(view, "Trigger price cannot be less than or equal to limit", Snackbar.LENGTH_LONG).show();
//                                        return;
//                                    }
//                                }
//                            }
//                        }
//
//
//                        //modifying order using zerodha
//                        exzt.placeModifyOrder(parahm, ordrId, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
//                        while (exzt.orderProcessingIncomplete) ;
//                        if (exzt.orderProcessingSuccess) {
//                            if (parentDlg != null) {
//                                info.setText("Order modified successfully");
//                                info.setVisibility(View.VISIBLE);
//                                parentDlg.dismiss();
//                            } else {
//                                Snackbar.make(view, "Order modified successfully", Snackbar.LENGTH_LONG).show();
//                                makeFormDefault();
//                            }
//                        } else {
//                            if (parentDlg != null) {
//                                info.setText("Order rejected by server");
//                                info.setVisibility(View.VISIBLE);
//                                parentDlg.dismiss();
//                            } else {
//                                Snackbar.make(view, "Order rejected by server", Snackbar.LENGTH_LONG).show();
//                                makeFormDefault();
//                            }
//                        }
//                    }
//                });
//                tradel.setVisibility(View.VISIBLE);
//                tradel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);
//                        dlgAlert.setMessage("Order will be deleted permanently");
//                        dlgAlert.setTitle("Are you sure?");
//                        dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                exzt.placeDeleteOrder(ordrId, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
//                                while (exzt.orderProcessingIncomplete) ;
//                                if (exzt.orderProcessingSuccess) {
//                                    if (parentDlg != null) {
//                                        info.setText("Order deleted successfully");
//                                        info.setVisibility(View.VISIBLE);
//                                        parentDlg.dismiss();
//                                    } else {
//                                        Snackbar.make(view, "Order deleted successfully", Snackbar.LENGTH_LONG).show();
//                                        makeFormDefault();
//                                    }
//                                } else {
//                                    if (parentDlg != null) {
//                                        info.setText("Order rejected by server");
//                                        info.setVisibility(View.VISIBLE);
//                                        parentDlg.dismiss();
//                                    } else {
//                                        Snackbar.make(view, "Order rejected by server", Snackbar.LENGTH_LONG).show();
//                                        makeFormDefault();
//                                    }
//                                }
//                            }
//                        });
//                        dlgAlert.setNegativeButton("Cancel", null);
//                        dlgAlert.setCancelable(true);
//                        dlgAlert.create().show();
//                    }
//                });
//            }
        }
    }

    private String[] getAllCompanies() {
        String next[];
        List<String> company = new ArrayList<>();
        try {
            CSVReader reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("nsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                company.add(next[0] + " [NSE: " + next[1] + "]");
            }
            reader = new CSVReader(new InputStreamReader(mContext.getAssets().open("bsecompanies.csv")));
            while ((next = reader.readNext()) != null) {
                company.add(next[0] + " [BSE: " + next[1] + "]");
            }
        } catch (IOException | NullPointerException e) {
            //Do nothing
        }
        return company.toArray(new String[company.size()]);
    }

    private void makeFormDefault() {
        stockToInsert.setText("");
        limitPrice.setText("");
        validity.setSelection(0);
        quantity.setText("");
        disclosed.setText("0");
        trigger.setText("");
    }
}
