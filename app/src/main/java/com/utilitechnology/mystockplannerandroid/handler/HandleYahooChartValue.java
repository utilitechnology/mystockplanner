package com.utilitechnology.mystockplannerandroid.handler;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.parser.ParseJSON;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Bibaswann on 25-01-2016.
 */
public class HandleYahooChartValue extends AsyncTask<Void, Void, Void> {
    GraphView graph1, graph2 = null;
    ImageView cover, noCover;
    ArrayList<DataPoint> chartData = new ArrayList<>();
    ArrayList<DataPoint> volData = new ArrayList<>();
    ArrayList<String> timeTrack = new ArrayList<>();
    ArrayList<DataPoint> previousData = new ArrayList<>();
    double yMax = 0, yMin = Double.MAX_VALUE;
    double xMax = 0, xMin = 0;
    double yMaxVol = 0, yMinVol = Double.MAX_VALUE;
    int startFrom;
    String chartScript, unconvertedScriptWithExtension, dur;
    ScriptHelper sch;
    private final String PATH_OF_CSV;

    public HandleYahooChartValue(GraphView graphView, GraphView volGraphView, ImageView graphCover, ImageView nographCover, String script, String duration) {
        graph1 = graphView;
        graph2 = volGraphView;
        cover = graphCover;
        noCover = nographCover;

        graph1.setVisibility(View.GONE);
        cover.setVisibility(View.VISIBLE);
        noCover.setVisibility(View.GONE);
        if (graph2 != null)
            graph2.setVisibility(View.INVISIBLE);

        sch = new ScriptHelper(graphView.getContext());
        unconvertedScriptWithExtension = script;
        if (script.contains("^"))
            chartScript = script;
        else if (script.equals("000001.ss"))
            chartScript = script;
        else if (script.equals("rts.rs"))
            chartScript = script;
        else {
            //I didn't mention all broad market indices because, well, it's much easier
            try {
                chartScript = sch.convertScript(script);
            }
            catch (NullPointerException npe)
            {
                chartScript = script;
            }
        }

        dur = duration;
        if (dur.equals("1d")) {
            PATH_OF_CSV = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=1d/csv";
            startFrom = 17;
        } else if (dur.equals("1m")) {
            PATH_OF_CSV = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=1m/csv";
            startFrom = 18;
        } else if (dur.equals("6m")) {
            PATH_OF_CSV = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=6m/csv";
            startFrom = 18;
        } else if (dur.equals("1y")) {
            PATH_OF_CSV = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=1y/csv";
            startFrom = 18;
        } else if (dur.equals("5y")) {
            PATH_OF_CSV = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=5y/csv";
            startFrom = 18;
        } else {
            PATH_OF_CSV = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=my/csv";
            startFrom = 17;
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        fetchData(PATH_OF_CSV);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        if (graph2 != null)
//            graph1.setTitle("Price chart");
        graph1.removeAllSeries();
        if (chartData.size() > 0) {
            LineGraphSeries<DataPoint> series = new LineGraphSeries<>(chartData.toArray(new DataPoint[chartData.size()]));
            series.setThickness(2);
            series.setDrawBackground(true);
            series.setBackgroundColor(ContextCompat.getColor(graph1.getContext(), R.color.graphFill));
            series.setColor(ContextCompat.getColor(graph1.getContext(), R.color.graphLine));

            LineGraphSeries<DataPoint> prevSeries = new LineGraphSeries<>(previousData.toArray(new DataPoint[previousData.size()]));
            prevSeries.setThickness(2);
            prevSeries.setColor(ContextCompat.getColor(graph1.getContext(), R.color.previousGraphLine));

            graph1.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
            graph1.getGridLabelRenderer().setGridColor(Color.LTGRAY);
            NumberFormat ynf = NumberFormat.getInstance();
            ynf.setMaximumFractionDigits(2);
            //nf.setMinimumIntegerDigits(2);
            NumberFormat xnf = NumberFormat.getInstance();
            //xnf.setMaximumFractionDigits(4);//Unnecessary, I think
            //nf.setMinimumIntegerDigits(2);
            graph1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(xnf, ynf));
            graph1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                @Override
                public String formatLabel(double value, boolean isValueX) {
                    if (isValueX) {
                        // show normal x values
                        if (dur.equals("1d")) {
                            // *1000 is to convert seconds to milliseconds
                            if (value <= xMax) {
                                Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", new Locale("en", "IN"));
                                return sdf.format(time);
                            } else
                                return "";
                        } else if (dur.equals("1m") || dur.equals("6m")) {
                            if (value <= xMax)
                                return timeTrack.get((int) value).substring(6, 8) + "-" + timeTrack.get((int) value).substring(4, 6);
                            else
                                return "";
                        } else {
                            if (value <= xMax)
                                return timeTrack.get((int) value).substring(4, 6) + "-" + timeTrack.get((int) value).substring(0, 4);
                            else
                                return "";
                        }
                    } else {
                        // show normal y value
                        return super.formatLabel(value, isValueX);
                    }
                }
            });
            //graph.getGridLabelRenderer().setHighlightZeroLines(true);
            graph1.getViewport().setYAxisBoundsManual(true);
            graph1.getViewport().setXAxisBoundsManual(true);
            graph1.getViewport().setMinY(yMin);
            graph1.getViewport().setMaxY(yMax);
            graph1.getViewport().setMaxX(xMax);
            graph1.getViewport().setMinX(xMin);
            //Log.e("important","max x is "+xMax);

            //Use series if no animation required
            graph1.addSeries(series);
            graph1.addSeries(prevSeries);
            graph1.setVisibility(View.VISIBLE);

            //Use DrawChart if animation required
            //Todo: This is not working aparently because I cannot change series from doInBackground, and a runnable thread is freezing the UI
            //new DrawChart(chartData,graph1).execute();

            //Show volume bar graph if present
            if (graph2 != null) {
                graph2.removeAllSeries();
                //graph2.setTitle("Volume chart");
                if (volData.size() > 0) {
                    BarGraphSeries<DataPoint> volSeries = new BarGraphSeries<DataPoint>(volData.toArray(new DataPoint[volData.size()]));
                    volSeries.setColor(ContextCompat.getColor(graph1.getContext(), R.color.graphLine));
                    volSeries.setSpacing(10);
                    graph2.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
                    graph2.getGridLabelRenderer().setGridColor(Color.LTGRAY);
                    NumberFormat ynf2 = NumberFormat.getInstance();
                    ynf.setMaximumFractionDigits(2);
                    NumberFormat xnf2 = NumberFormat.getInstance();
                    graph2.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(xnf2, ynf2));
                    graph2.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                        @Override
                        public String formatLabel(double value, boolean isValueX) {
                            if (isValueX) {
                                // show normal x values
                                if (dur.equals("1d")) {
                                    // *1000 is to convert seconds to milliseconds
                                    if (value <= xMax) {
                                        Date time = new Date(Long.valueOf(timeTrack.get((int) value)) * 1000L);
                                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", new Locale("en", "IN"));
                                        return sdf.format(time);
                                    } else
                                        return "";
                                } else if (dur.equals("1m") || dur.equals("6m")) {
                                    if (value <= xMax)
                                        return timeTrack.get((int) value).substring(6, 8) + "-" + timeTrack.get((int) value).substring(4, 6);
                                    else
                                        return "";
                                } else {
                                    if (value <= xMax)
                                        return timeTrack.get((int) value).substring(4, 6) + "-" + timeTrack.get((int) value).substring(0, 4);
                                    else
                                        return "";
                                }
                            } else {
                                // show normal y value
                                return super.formatLabel(value, isValueX);
                            }
                        }
                    });
                    graph2.getViewport().setYAxisBoundsManual(true);
                    graph2.getViewport().setXAxisBoundsManual(true);
                    graph2.getViewport().setMinY(yMinVol);
                    graph2.getViewport().setMaxY(yMaxVol);
                    graph2.getViewport().setMaxX(xMax);
                    graph2.getViewport().setMinX(xMin);
                    graph2.addSeries(volSeries);

                    graph2.setVisibility(View.VISIBLE);
                }
            }

            cover.setVisibility(View.GONE);
            noCover.setVisibility(View.GONE);
        } else {
            new HandleGoogleChartValue(graph1,graph2,cover,noCover,unconvertedScriptWithExtension,dur).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void fetchData(String uri) {
//        Log.e("important", "fetchData: from " +uri);
        if (uri == null)
            return;
        try {
            InputStream stream;
            BufferedReader br;
            String localChartFileName = uri.replace("/", "").replace(";", "").replace("=", "").replace("^", "").replace("https:chartapi.finance.yahoo.cominstrument1.0", "") + ".csv";
            //Log.e("important", "trying to find " + localChartFileName);
            //File file = new File(graph.getContext().getFilesDir().getAbsolutePath()+"/"+localChartFileName);
            File file = graph1.getContext().getFileStreamPath(localChartFileName);
            if (file.exists()) {
                stream = new FileInputStream(file);
                br = new BufferedReader(new InputStreamReader(stream));
//                Log.e("important", "chart shown from local file");
            } else {
                URL url = new URL(uri);
                stream = url.openStream();
                br = new BufferedReader(new InputStreamReader(stream));
//                Log.e("important", "chart shown from internet");
            }

            int counter = 0;
            int arrayTrack = 0;
            int skipVal;
            if (dur.equals("1m"))
                skipVal = 1;
            else
                skipVal = 2;
            double valueOfY, volOfY, closeValueOfY = 0, preValOfY = 0;
            //I have taken care of the situation where previous close is not present
            if (dur.equals("1d")) {
                preValOfY = findPrevClose();
                if (preValOfY > yMax)
                    yMax = preValOfY;
                //When previous value is not present, the value will always be 0 and <yMin
                if (preValOfY < yMin)
                    if (preValOfY > 0)
                        yMin = preValOfY;
            }
            String line;
            while ((line = br.readLine()) != null) {
//                Log.e("important","found "+line);
//                The Only reason I haven't used previous close from yahoo is that it returns wrong values frequently
//                if (line.contains("previous_close:")) {
//                    preValOfY = Double.valueOf(line.split(":")[1]);
//                    if (preValOfY > yMax)
//                        yMax = preValOfY;
//                    //When previous value is not present, the value will always be 0 and <yMin
//                    if (preValOfY < yMin)
//                        if (preValOfY > 0)
//                            yMin = preValOfY;
//                }
//                The only reason I haven't added a close value is the close value does not correspond to a time
//                if (line.contains("close:")) {
//                    closeValueOfY = Double.valueOf(line.split(":")[1].split(",")[0]);
//                    if (closeValueOfY > yMax)
//                        yMax = closeValueOfY + 10;
//                    if (closeValueOfY < yMin)
//                        yMin = closeValueOfY - 10;
//                }
                if (counter == startFrom) {
                    //For price graph
                    valueOfY = Double.valueOf(line.split(",")[1]);
                    if (valueOfY > yMax)
                        yMax = valueOfY;
                    if (valueOfY < yMin)
                        yMin = valueOfY;
                    chartData.add(new DataPoint(0, valueOfY));
                    if (preValOfY > 0)
                        previousData.add(new DataPoint(0, preValOfY));

                    //For volume graph
                    volOfY = Double.valueOf(line.split(",")[line.split(",").length - 1]);
                    if (volOfY > yMaxVol)
                        yMaxVol = volOfY;
                    if (volOfY < yMinVol)
                        yMinVol = volOfY;
                    volData.add(new DataPoint(0, volOfY));

                    timeTrack.add(line.split(",")[0]);
                    arrayTrack++;
                } else if (counter > startFrom) {
                    if (counter % skipVal == 0) {
                        //For price graph
                        valueOfY = Double.valueOf(line.split(",")[1]);
                        if (valueOfY > yMax)
                            yMax = valueOfY;
                        if (valueOfY < yMin)
                            yMin = valueOfY;
                        chartData.add(new DataPoint(arrayTrack, valueOfY));
                        if (preValOfY > 0)
                            previousData.add(new DataPoint(arrayTrack, preValOfY));

                        //For volume graph
                        volOfY = Double.valueOf(line.split(",")[line.split(",").length - 1]);
                        if (volOfY > yMaxVol)
                            yMaxVol = volOfY;
                        if (volOfY < yMinVol)
                            yMinVol = volOfY;
                        volData.add(new DataPoint(arrayTrack, volOfY));

                        timeTrack.add(line.split(",")[0]);
                        arrayTrack++;
                    }
                }
                counter++;
            }
//            if (closeValueOfY != 0) {
//                chartData.add(new DataPoint(arrayTrack, closeValueOfY));
//                if (preValOfY > 0)
//                    previousData.add(new DataPoint(arrayTrack, preValOfY));
//                //Log.e("important", "Added close value x=" + arrayTrack + ", y=" + closeValueOfY);
//                timeTrack.add("");
//            }

            xMax = arrayTrack - 1;
            //Todo: make it range dependent instead of fixed 10
            yMax += (yMax - yMin) / 10;
            yMin -= (yMax - yMin) / 10;
            stream.close();
        } catch (Exception e) {
//            Log.e("important", "The exception here is: "+Log.getStackTraceString(e));
        }
//        if (chartData.size() <= 0) {
//            if (!newTicker)
//                findTicker();
//        }
    }

//    private void findTicker() {
//        String PATH_OF_CSV_NEW = null;
//        try {
//            Document bsdoc = Jsoup.parse(new URL("https://in.finance.yahoo.com/q?s=" + chartScript.replace("&", "%26")), 10000);
//            Elements bstopicList = bsdoc.select("div[id=quote-header-info]").select("h1");
//            for (org.jsoup.nodes.Element topic : bstopicList)
//                chartScript = topic.text().split("\\(")[1].replace(")", "").trim();
//
//            if (dur.equals("1d")) {
//                PATH_OF_CSV_NEW = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=1d/csv";
//            } else if (dur.equals("1m")) {
//                PATH_OF_CSV_NEW = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=1m/csv";
//            } else if (dur.equals("6m")) {
//                PATH_OF_CSV_NEW = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=6m/csv";
//            } else if (dur.equals("1y")) {
//                PATH_OF_CSV_NEW = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=1y/csv";
//            } else if (dur.equals("5y")) {
//                PATH_OF_CSV_NEW = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=5y/csv";
//            } else {
//                PATH_OF_CSV_NEW = "https://chartapi.finance.yahoo.com/instrument/1.0/" + chartScript + "/chartdata;type=quote;range=my/csv";
//            }
//
//        } catch (IndexOutOfBoundsException | IOException ignored) {
//        }
//
//        //Log.e("important","new script is "+chartScript+" and new path "+PATH_OF_CSV_NEW);
//        fetchData(PATH_OF_CSV_NEW, true);
//    }

    private Double findPrevClose() {
        ParseJSON prj;
        prj = new ParseJSON("http://finance.google.com/finance/info?infotype=infoquoteall&q=" + sch.getGoogleScript(unconvertedScriptWithExtension));
        prj.fetchJSON();
        while (prj.parsingInComplete) ;
        if (prj.getP_closes().size() > 0)
            return Double.valueOf(prj.getP_closes().get(0));
        else
            return 0.0d;
    }
}
