package com.utilitechnology.mystockplannerandroid.handler;

import android.content.Context;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseZerodhaOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 13-06-2016.
 */
public class HandleZerodhaOrderHistory {

    Context mContext;

    List<String> company = new ArrayList<>();
    List<String> ltp = new ArrayList<>();
    List<Float> reaLtp = new ArrayList<>();
    List<String> change = new ArrayList<>();
    List<String> upDown = new ArrayList<>();

    List<String> script = new ArrayList<>();
    List<String> symbol = new ArrayList<>();
    List<String> orderId = new ArrayList<>();
    List<String> orderStatus = new ArrayList<>();
    List<String> exchange = new ArrayList<>();
    List<String> orderType = new ArrayList<>();
    List<String> TransactionType = new ArrayList<>();
    List<String> product = new ArrayList<>();
    List<String> limitPrice = new ArrayList<>();
    List<String> quantity = new ArrayList<>();
    List<String> triggerPrice = new ArrayList<>();
    List<String> averagePrice = new ArrayList<>();
    List<String> validity = new ArrayList<>();
    List<String> disclosed = new ArrayList<>();
    List<String> profitLoss = new ArrayList<>();

    TradingAccountSettingsAPI tras;
    ScriptHelper sch;
    HandleMultipleEquities hmeq;

    public HandleZerodhaOrderHistory(Context context) {
        mContext = context;
        tras = new TradingAccountSettingsAPI(mContext);
        sch = new ScriptHelper(mContext);

        ParseZerodhaOrder pzo = new ParseZerodhaOrder("https://api.kite.trade/orders?api_key=" + mContext.getString(R.string.ZERODHA_API_KEY) + "&access_token=" + tras.readSetting("access_token"), mContext);
        pzo.fetchJSON();
        while (pzo.parsingInComplete) ;
        script = pzo.getScript();
        //Log.e("important","found "+script.size()+" items");
        if (script.size() > 0) {
            symbol = pzo.getZerodhaSymbol();
            orderId = pzo.getOrderId();
            orderStatus = pzo.getOrderStatus();
            exchange = pzo.getExchange();
            orderType = pzo.getOrderType();
            TransactionType = pzo.getTransactionType();
            product = pzo.getProduct();
            limitPrice = pzo.getLimitPrice();
            quantity = pzo.getQuantity();
            triggerPrice = pzo.getTriggerPrice();
            averagePrice = pzo.getAveragePrice();
            validity = pzo.getValidity();
            disclosed = pzo.getDisclosed();

            hmeq = new HandleMultipleEquities(mContext, script);
            ltp = hmeq.getStringLTPs();
            reaLtp = hmeq.getLTPs();
            change = hmeq.getChangeStrings();
            upDown = hmeq.getUpDown();

            int i = 0;
            for (String oneScript : script) {
                company.add(sch.scriptToCompany(oneScript));
                // TODO: 06-03-2017 check problem with BO
                try {
                    //for buy orders if ltp is higher, it's profit, for sell orders if ltp is lower
                    if (TransactionType.get(i).equals("BUY"))
                        profitLoss.add(convertFloatToString((reaLtp.get(i) - Float.parseFloat(averagePrice.get(i))) * Integer.parseInt(quantity.get(i))));
                    else
                        profitLoss.add(convertFloatToString((Float.parseFloat(averagePrice.get(i)) - reaLtp.get(i)) * Integer.parseInt(quantity.get(i))));
                    i++;
                } catch (IndexOutOfBoundsException ioobe) {
                    profitLoss.add("NA");
//                    Log.e("important", Log.getStackTraceString(ioobe));
                }
            }
        } else {
            company.add("No items here");
            ltp.add("");
            change.add("");
            upDown.add("none");
            script.add("");
            symbol.add("");
            orderStatus.add("");
            exchange.add("");
            averagePrice.add("");
            orderType.add("");
            orderId.add("");
            quantity.add("");
            profitLoss.add("");
            limitPrice.add("");
            triggerPrice.add("");
            TransactionType.add("");
            product.add("");
            validity.add("");
            disclosed.add("");
        }
    }

    public List<String> getScript() {
        return script;
    }

    public List<String> getOrderStatus() {
        return orderStatus;
    }

    public List<String> getTransactionType() {
        return TransactionType;
    }

    public List<String> getChange() {
        return change;
    }

    public List<String> getCompany() {
        return company;
    }

    public List<String> getExchange() {
        return exchange;
    }

    public List<String> getLtp() {
        return ltp;
    }

    public List<String> getOrderId() {
        return orderId;
    }

    public List<String> getOrderType() {
        return orderType;
    }

    public List<String> getAveragePrice() {
        return averagePrice;
    }

    public List<String> getProduct() {
        return product;
    }

    public List<String> getLimitPrice() {
        return limitPrice;
    }

    public List<String> getQuantity() {
        return quantity;
    }

    public List<String> getTriggerPrice() {
        return triggerPrice;
    }

    public List<String> getUpDown() {
        return upDown;
    }

    public List<String> getProfitLoss() {
        return profitLoss;
    }

    public List<String> getValidity() {
        return validity;
    }

    public List<String> getDisclosed() {
        return disclosed;
    }

    public List<String> getSymbol() {
        return symbol;
    }

    private String convertFloatToString(float input) {
        return String.format(new Locale("en", "IN"), "%.2f", input);
    }
}
