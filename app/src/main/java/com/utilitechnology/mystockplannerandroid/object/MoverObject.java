package com.utilitechnology.mystockplannerandroid.object;

/**
 * Created by Bibaswann on 28-01-2016.
 */
public class MoverObject {
    String stockName, scriptName, changeAmt,ltp, highVal, lowVal;

    MoverObject(String stock, String script, String cmp, String high, String low, String change) {
        stockName = stock;
        scriptName = script;
        ltp = cmp;
        changeAmt = change;
        highVal = high;
        lowVal = low;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getChangeAmt() {
        return changeAmt;
    }

    public void setChangeAmt(String changeAmt) {
        this.changeAmt = changeAmt;
    }

    public String getLtp() {
        return ltp;
    }

    public void setLtp(String ltp) {
        this.ltp = ltp;
    }

    public String getHighVal() {
        return highVal;
    }

    public void setHighVal(String highVal) {
        this.highVal = highVal;
    }

    public String getLowVal() {
        return lowVal;
    }

    public void setLowVal(String lowVal) {
        this.lowVal = lowVal;
    }
}
