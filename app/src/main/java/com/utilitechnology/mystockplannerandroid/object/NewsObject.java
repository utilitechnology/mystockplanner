package com.utilitechnology.mystockplannerandroid.object;

/**
 * Created by Bibaswann on 17-11-2016.
 */

public class NewsObject {
    private String serial,title, link, date, media, body, thumb,important;

    NewsObject()
    {}
    public NewsObject(String serial, String title, String link, String date, String media, String body, String thumb, String important)
    {
        this.serial=serial;
        this.title=title;
        this.link=link;
        this.date=date;
        this.media=media;
        this.body=body;
        this.thumb=thumb;
        this.important=important;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImportant() {
        return important;
    }

    public void setImportant(String important) {
        this.important = important;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
