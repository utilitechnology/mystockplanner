package com.utilitechnology.mystockplannerandroid.object;

/**
 * Created by Bibaswann on 28-01-2016.
 */
public class PortfolioObject {
    String stockName, scriptName, exchangeName, changeAmt;
    float iBoughtPrice, ltp, sellTargetPrice, gainLossAmt;
    int quantityAmt;
    boolean modifiable;

    public PortfolioObject() {

    }

    public PortfolioObject(String stock, String script, String exchange, float ibought, int quantity, float selltarget, boolean editable) {
        stockName = stock;
        scriptName = script;
        exchangeName = exchange;
        iBoughtPrice = ibought;
        quantityAmt = quantity;
        sellTargetPrice = selltarget;
        modifiable=editable;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stock) {
        stockName = stock;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String script) {
        scriptName = script;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchange) {
        exchangeName = exchange;
    }

    public float getiBoughtPrice() {
        return iBoughtPrice;
    }

    public void setiBoughtPrice(float ibought) {
        iBoughtPrice = ibought;
    }

    public int getQuantityAmt() {
        return quantityAmt;
    }

    public void setQuantityAmt(int quantity) {
        quantityAmt = quantity;
    }

    public float getLtp() {
        return ltp;
    }

    public void setLtp(float price) {
        ltp = price;
    }

    public float getSellTargetPrice() {
        return sellTargetPrice;
    }

    public void setSellTargetPrice(float selltarget) {
        sellTargetPrice = selltarget;
    }

    public float getGainLossAmt() {
        return gainLossAmt;
    }

    public void setGainLossAmt(float gainLoss) {
        gainLossAmt = gainLoss;
    }

    public String getChangeAmt() {
        return changeAmt;
    }

    public void setChangeAmt(String changeAmt) {
        this.changeAmt = changeAmt;
    }

    public void setModifiable(boolean modifiable) {
        this.modifiable = modifiable;
    }
    public boolean getModifiable()
    {
        return modifiable;
    }
}
