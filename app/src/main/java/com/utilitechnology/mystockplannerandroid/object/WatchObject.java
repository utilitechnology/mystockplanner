package com.utilitechnology.mystockplannerandroid.object;

/**
 * Created by Bibaswann on 28-01-2016.
 */
public class WatchObject {
    String stockName, scriptName, exchangeName, changeAmt, upDown;
    float bsePrice, nsePrice, buyTargetPrice;

    public WatchObject() {

    }

    public WatchObject(String exchange, String stock, String script, float buyTarget) {
        stockName = stock;
        scriptName = script;
        exchangeName = exchange;
        buyTargetPrice = buyTarget;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public float getBsePrice() {
        return bsePrice;
    }

    public void setBsePrice(float bsePrice) {
        this.bsePrice = bsePrice;
    }

    public float getNsePrice() {
        return nsePrice;
    }

    public void setNsePrice(float nsePrice) {
        this.nsePrice = nsePrice;
    }

    public float getBuyTargetPrice() {
        return buyTargetPrice;
    }

    public void setBuyTargetPrice(float buyTargetPrice) {
        this.buyTargetPrice = buyTargetPrice;
    }

    public String getChangeAmt() {
        return changeAmt;
    }

    public void setChangeAmt(String changeAmt) {
        this.changeAmt = changeAmt;
    }

    public String getUpDown() {
        return upDown;
    }

    public void setUpDown(String upDown) {
        this.upDown = upDown;
    }
}
