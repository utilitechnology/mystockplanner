package com.utilitechnology.mystockplannerandroid.parser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 23-10-2016.
 */

public class ParseBrokers {
    private List<String> bser = new ArrayList<>();
    private List<String> bname = new ArrayList<>();
    private List<String> bbrokerage = new ArrayList<>();
    private List<String> burl = new ArrayList<>();
    private List<String> bfull = new ArrayList<>();

    private List<String> bdp = new ArrayList<>();
    private List<String> bcharge = new ArrayList<>();
    private List<String> bmaintain = new ArrayList<>();
    private List<String> bmargin = new ArrayList<>();
    private List<String> bpromo = new ArrayList<>();
    private List<String> bweb = new ArrayList<>();
    private List<String> bphone = new ArrayList<>();

    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParseBrokers(String url) {
        urlString = url;
    }

    public List<String> getBbrokerage() {
        return bbrokerage;
    }

    public List<String> getBname() {
        return bname;
    }

    public List<String> getBser() {
        return bser;
    }

    public List<String> getBurl() {
        return burl;
    }

    public List<String> getBdp() {
        return bdp;
    }

    public List<String> getBcharge() {
        return bcharge;
    }

    public List<String> getBmaintain() {
        return bmaintain;
    }

    public List<String> getBmargin() {
        return bmargin;
    }

    public List<String> getBpromo() {
        return bpromo;
    }

    public List<String> getBweb() {
        return bweb;
    }

    public List<String> getBfull() {
        return bfull;
    }

    public List<String> getBphone() {
        return bphone;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name.equals("serial")) {
                            bser.add(text);
                        } else if (name.equals("name")) {
                            bname.add(text);
                        } else if (name.equals("brokerage")) {
                            bbrokerage.add(text);
                        } else if (name.equals("dp")) {
                            bdp.add(text);
                        } else if (name.equals("openingcharge")) {
                            bcharge.add(text);
                        } else if (name.equals("maintainance")) {
                            bmaintain.add(text);
                        } else if (name.equals("margin")) {
                            bmargin.add(text);
                        } else if (name.equals("promo")) {
                            bpromo.add(text);
                        } else if (name.equals("website")) {
                            bweb.add(text);
                        } else if (name.equals("url")) {
                            burl.add(text);
                        }
                        else if (name.equals("full")) {
                            bfull.add(text);
                        }
                        else if (name.equals("phone")) {
                            bphone.add(text);
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                    //Log.e("important",Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
