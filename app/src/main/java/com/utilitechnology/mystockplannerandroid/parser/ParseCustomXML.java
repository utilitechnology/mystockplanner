package com.utilitechnology.mystockplannerandroid.parser;

import android.util.Log;

import com.utilitechnology.mystockplannerandroid.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.utilitechnology.mystockplannerandroid.Urls.CURRENCY_URL;
import static com.utilitechnology.mystockplannerandroid.Urls.INDEX_URL;
import static com.utilitechnology.mystockplannerandroid.Urls.MOVER_BSE_RESOURCE;
import static com.utilitechnology.mystockplannerandroid.Urls.MOVER_NSE_RESOURCE;
import static com.utilitechnology.mystockplannerandroid.Urls.WORLD_URL;

/**
 * Created by Bibaswann on 16-05-2017.
 */

public class ParseCustomXML {
    private List<String> price = new ArrayList<>();
    private List<String> changeString = new ArrayList<>();
    private List<String> realChangePercent = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;
    private String urlString = null;

    public ParseCustomXML(String type, String url)
    {
        if(type.equals("world"))
            urlString = WORLD_URL;
        else if(type.equals("currency"))
            urlString=CURRENCY_URL;
        else if(type.equals("index"))
            urlString=INDEX_URL;
        else if(type.equals("mover-bse"))
            urlString=MOVER_BSE_RESOURCE;
        else if(type.equals("mover-nse"))
            urlString=MOVER_NSE_RESOURCE;
        else
            urlString=url;
//        Log.e("important", urlString);
    }

    public List<String> getChangeString() {
        return changeString;
    }

    public List<String> getPrice() {
        return price;
    }

    public List<String> getUpsNDowns() {
        return upsNDowns;
    }

    public List<String> getRealChangePercent() {
        return realChangePercent;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        if(text.equals(""))
                            text="NA";
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null) {
                            if (name.equalsIgnoreCase("ltp")) {
                                price.add(text.trim());
                            } else if (name.equalsIgnoreCase("change")) {
                                changeString.add(text.trim());
                            } else if (name.equalsIgnoreCase("real_change")) {
                                realChangePercent.add(text.trim());
                            } else if (name.equalsIgnoreCase("ud")) {
                               upsNDowns.add(text.trim());
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
//            Log.e("important", Log.getStackTraceString(e));
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
//                    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; MotoE2(4G-LTE) Build/MPI24.65-39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.81 Mobile Safari/537.36");
                    conn.setReadTimeout(100000);
                    conn.setConnectTimeout(150000);
                    conn.setRequestMethod("GET");
                    conn.setInstanceFollowRedirects(false);

                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                    //Todo: this line below is experimental, it may cause iobx, use with caution
                    parsingInComplete =false;
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
