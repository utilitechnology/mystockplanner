package com.utilitechnology.mystockplannerandroid.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 10-04-2016.
 */
public class ParseJSON {
    private String urlString = null;

    private List<String> price = new ArrayList<>();
    private List<String> change = new ArrayList<>();
    private List<String> changePercent = new ArrayList<>();
    private List<String> realChangePercent = new ArrayList<>();
    private List<String> dayHigh = new ArrayList<>();
    private List<String> dayLow = new ArrayList<>();
    private List<String> yearHigh = new ArrayList<>();
    private List<String> yearLow = new ArrayList<>();
    private List<String> volume = new ArrayList<>();
    private List<String> changeString = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();
    private List<String> opens = new ArrayList<>();
    private List<String> p_closes = new ArrayList<>();
    private List<String> pe_ratios = new ArrayList<>();
    private String update = null;

    public volatile boolean parsingInComplete = true;

    public ParseJSON(String url) {
        //Log.e("important","will parse "+url);
        urlString = url;
    }

    public List<String> getPrice() {
        return price;
    }

    public List<String> getChange() {
        return change;
    }

    public List<String> getChangePercent() {
        return changePercent;
    }

    public List<String> getRealChangePercent() {
        return realChangePercent;
    }

    public List<String> getDayHigh() {
        return dayHigh;
    }

    public List<String> getDayLow() {
        return dayLow;
    }

    public List<String> getYearHigh() {
        return yearHigh;
    }

    public List<String> getYearLow() {
        return yearLow;
    }

    public List<String> getVolume() {
        return volume;
    }

    public List<String> getChangeString() {
        return changeString;
    }

    public List<String> getUpsNDowns() {
        return upsNDowns;
    }

    public String getUpdate() {
        return update;
    }

    public List<String> getOpens() {
        return opens;
    }

    public List<String> getP_closes() {
        return p_closes;
    }

    public List<String> getPe_ratios() {
        return pe_ratios;
    }

    private void parseJSONAndStoreIt(String jsonString) {
        try {
//            Either use a root element and encapsulate the whole string, including the root in curly
//            braces use a root object like the one in commented part OR just use the array like the one used here

//            JSONObject jsonRootObject = new JSONObject(jsonString);
//            JSONArray jsonArray = jsonRootObject.optJSONArray("root");

            JSONArray jsonArray = new JSONArray(jsonString);
            String tempUpDown, tempChange, tempChangePercent;
            //Log.e("important", "found " + jsonArray.length() + " items");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                //Log.e("important", "found open " + jsonObject.optString("op"));
                price.add(make2DecimalAndRemoveComma(jsonObject.optString("l_fix")));//l_cur contains word "Rs."

                if (jsonObject.optString("c").contains("-")) {
                    upsNDowns.add("down");
                    tempUpDown = "-";
                } else {
                    upsNDowns.add("up");
                    tempUpDown = "+";
                }
                if (tempUpDown.equals("+"))
                    tempChange = tempUpDown + make2DecimalAndRemoveComma(jsonObject.optString("c_fix"));
                else
                    tempChange = make2DecimalAndRemoveComma(jsonObject.optString("c_fix"));

                change.add(tempChange);
                realChangePercent.add(make2DecimalAndRemoveComma(jsonObject.optString("cp_fix")));

                if (tempUpDown.equals("+"))
                    tempChangePercent = tempUpDown + make2DecimalAndRemoveComma(jsonObject.optString("cp_fix")) + "%";
                else
                    tempChangePercent = make2DecimalAndRemoveComma(jsonObject.optString("cp_fix")) + "%";

                changePercent.add(tempChangePercent);
                changeString.add(tempChange + " (" + tempChangePercent + ")");

                dayHigh.add(make2DecimalAndRemoveComma(jsonObject.optString("hi")));
                dayLow.add(make2DecimalAndRemoveComma(jsonObject.optString("lo")));
                yearHigh.add(make2DecimalAndRemoveComma(jsonObject.optString("hi52")));
                yearLow.add(make2DecimalAndRemoveComma(jsonObject.optString("lo52")));
                volume.add(make2DecimalAndRemoveComma(jsonObject.optString("vo")));
                update = "Last traded: " + jsonObject.optString("lt").replace("GMT+5:30", "") + "(Realtime quote)";

                opens.add(make2DecimalAndRemoveComma(jsonObject.optString("op")));
                p_closes.add(make2DecimalAndRemoveComma(jsonObject.optString("pcls_fix")));
                pe_ratios.add(make2DecimalAndRemoveComma(jsonObject.optString("pe")));
            }
            parsingInComplete = false;
        } catch (JSONException e) {
            //Log.e("important", "exception " + Log.getStackTraceString(e));
        }
    }

    public void fetchJSON() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");

                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    //Log.e("important", "found string " +sb.toString());
                    String modifiedString = "";
                    modifiedString += sb.toString().replace("//", "");
                    parseJSONAndStoreIt(modifiedString);
                    stream.close();
                } catch (Exception e) {
                    parsingInComplete=false;
                    //Log.e("important","exception in reading "+e.getMessage());
                }
            }
        }).start();
    }

    //Todo: Not really needed here, check for a few days and then remove
    private String make2DecimalAndRemoveComma(String number) {
        if(number.equals(""))
            return "0";
        number = number.replace(",", "");
        if (!number.contains(".")) {
            return number + ".00";
        } else {
            String integer = number.split("\\.")[0];
            String decimal = number.split("\\.")[1];
            if (decimal.length() == 1)
                return integer + "." + decimal + "0";
            if (decimal.length() == 2)
                return integer + "." + decimal;
            else {
                decimal = decimal.substring(0, 2);
                return integer + "." + decimal;
            }
        }
    }
}
