package com.utilitechnology.mystockplannerandroid.parser;

import android.support.annotation.Nullable;

import com.utilitechnology.mystockplannerandroid.object.NewsObject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 18-11-2016.
 */

public class ParseNews {
    private List<NewsObject> newsItems = new ArrayList<>();

    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    @Nullable
    String serial;

    public ParseNews(String url, @Nullable String newserial) {
        urlString = url;
        serial = newserial;
    }

    public List<NewsObject> getNewsItems() {
        return newsItems;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            List<String> tempSerial = new ArrayList<>();
            List<String> tempTitle = new ArrayList<>();
            List<String> tempLink = new ArrayList<>();
            List<String> tempDate = new ArrayList<>();
            List<String> tempMedia = new ArrayList<>();
            List<String> tempBody = new ArrayList<>();
            List<String> tempThumb = new ArrayList<>();
            List<String> tempImportant = new ArrayList<>();
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name.equals("title")) {
                            tempTitle.add(text);
                        } else if (name.equals("serial")) {
                            tempSerial.add(text);
                        } else if (name.equals("link")) {
                            tempLink.add(text);
                        } else if (name.equals("date")) {
                            tempDate.add(text);
                        } else if (name.equals("media")) {
                            tempMedia.add(text);
                        } else if (name.equals("body")) {
                            tempBody.add(text);
                        } else if (name.equals("thumb")) {
                            tempThumb.add(text);
                        } else if (name.equals("important")) {
                            tempImportant.add(text);
                        }
                        break;
                }
                event = myParser.next();
            }
            for (String newser:tempSerial) {
                int newsIndex=tempSerial.indexOf(newser);
                if (serial != null) {
                    if (!serial.equals(newser)) {
                        continue;
                    }
                }
                NewsObject tempNews = new NewsObject(newser, tempTitle.get(newsIndex), tempLink.get(newsIndex), tempDate.get(newsIndex), tempMedia.get(newsIndex), tempBody.get(newsIndex), tempThumb.get(newsIndex), tempImportant.get(newsIndex));
                newsItems.add(tempNews);
            }
            parsingInComplete = false;
        } catch (Exception e) {
            //Log.e("important",Log.getStackTraceString(e));
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                    //Log.e("important",Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
