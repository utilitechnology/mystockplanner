package com.utilitechnology.mystockplannerandroid.parser;

import com.utilitechnology.mystockplannerandroid.Urls;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 10-05-2017.
 */

public class ParseWebCurrency {
    private List<String> price = new ArrayList<>();
    private List<String> changeString = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    private String url;

    public ParseWebCurrency(String url) {
        this.url=url;
    }

    public List<String> getPrice() {
        return price;
    }

    public List<String> getChangeString() {
        return changeString;
    }

    public List<String> getUpsNDowns() {
        return upsNDowns;
    }


    public void fetchWeb() {
            try {
                int spanCounter = 0;
                Document doc = Jsoup.parse(new URL(this.url), 10000);

                Elements t1 = doc.select("span[class=bld]");
                if(t1.size()>0)
                price.add(t1.get(0).text().split(" ")[0].trim());

                Elements t2 = doc.select("span[class=nwp]").select("span");
                if(t2.size()>0) {
                    String changeP1=make2Decimal(t2.get(0).text().split("\\(")[0]);
                    String changeP2=make2Decimal(t2.get(0).text().split("\\(")[1].replace(")",""));
                    changeString.add(changeP1+" ("+changeP2+")");

                    if (!t2.get(0).text().trim().contains("-")) {
                        upsNDowns.add("up");
                    } else {
                        upsNDowns.add("down");
                    }
                }

//                for (Element topic : topicList) {
////                    Log.e("important", topic.text() + " for " + scr);
//                    if (spanCounter == 0)
//                        price.add(topic.text().trim());
//                    if (spanCounter == 1) {
//                        changeString.add(topic.text().trim());
//                        if (!topic.text().trim().contains("-")) {
//                            upsNDowns.add("up");
//                        } else {
//                            upsNDowns.add("down");
//                        }
//                    }
//
//                    if (spanCounter == 14)
//                        spanCounter = 0;
//                    else
//                        spanCounter++;
//                }

            } catch (IOException e) {
//                Log.e("important", "exception " + e.getMessage());
            }
    }

    //Todo: approximation here
    private String make2Decimal(String number) {
        if(number.equals(""))
            return "0";
        if (!number.contains(".")) {
            return number + ".00";
        } else {
            String integer = number.split("\\.")[0];
            String decimal = number.split("\\.")[1];
            if (decimal.length() == 1)
                return integer + "." + decimal + "0";
            if (decimal.length() == 2)
                return integer + "." + decimal;
            else {
                decimal = decimal.substring(0, 2);
                return integer + "." + decimal;
            }
        }
    }
}
