package com.utilitechnology.mystockplannerandroid.parser;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 10-05-2017.
 */

public class ParseWebYahoo {

    private List<String> price = new ArrayList<>();
    private List<String> dayHigh = new ArrayList<>();
    private List<String> dayLow = new ArrayList<>();
    private List<String> yearHigh = new ArrayList<>();
    private List<String> yearLow = new ArrayList<>();
    private List<String> volume = new ArrayList<>();
    private List<String> changeString = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    private List<String> scripts;
    private String update = null;
    private final String RESOURCE_WEB = "https://in.finance.yahoo.com/q?s=";

    public ParseWebYahoo(List<String> query) {
        scripts = query;
    }

    public List<String> getDayHigh() {
        return dayHigh;
    }

    public List<String> getDayLow() {
        return dayLow;
    }

    public List<String> getYearHigh() {
        return yearHigh;
    }

    public List<String> getYearLow() {
        return yearLow;
    }

    public List<String> getPrice() {
        return price;
    }

    public List<String> getVolume() {
        return volume;
    }

    public List<String> getChangeString() {
        return changeString;
    }

    public List<String> getUpsNDowns() {
        return upsNDowns;
    }

    public String getUpdate() {
        if (update != null) {
            return "Last update: " + update + " (Realtime quote)";
        } else
            return "Last update: N/A (Realtime quote)";
    }

    public void fetchWeb() {
        for (String scr : scripts) {
            try {
                int spanCounter = 0;
                Document doc = Jsoup.parse(new URL(RESOURCE_WEB + scr), 10000);
                Elements topicList = doc.select("span");
                for (Element topic : topicList) {
//                    Log.e("important", topic.text() + " for " + scr);
                    if (spanCounter == 0)
                        price.add(make2Decimal(topic.text().trim().replace(",", "")));
                    if (spanCounter == 1) {
                        changeString.add(make2Decimal(topic.text().trim().replace(",", "")));
                        if (!topic.text().trim().contains("-")) {
                            upsNDowns.add("up");
                        } else {
                            upsNDowns.add("down");
                        }
                    }
                    if (spanCounter == 2)
                        update = topic.text().trim();
                    if (spanCounter == 8)
                        volume.add(topic.text().trim());
                    if (spanCounter == 10) {
                        String[] s = topic.text().split("-");
                        if (s.length > 0)
                            dayLow.add(s[0].trim());
                        if (s.length > 1)
                            dayHigh.add(s[1].trim());
                    }
                    if (spanCounter == 12) {
                        String[] s = topic.text().split("-");
                        if (s.length > 0)
                            yearLow.add(s[0].trim());
                        if (s.length > 1)
                            yearHigh.add(s[1].trim());
                    }
                    if (spanCounter == 14)
                        spanCounter = 0;
                    else
                        spanCounter++;
                }

            } catch (IOException e) {
//                Log.e("important", "exception " + e.getMessage());
            }
        }
    }

    private String make2Decimal(String number) {
        if (number.equals(""))
            return "0";
        if (!number.contains(".")) {
            return number + ".00";
        } else {
            String internalPart = null;
            String returnVal = null;
            if (number.contains("(")) {
                internalPart = number.split("\\(")[1].trim();
                number = number.split("\\(")[0].trim();
            }
            String integer = number.split("\\.")[0];
            String decimal = number.split("\\.")[1];
            if (decimal.length() == 1)
                returnVal = integer + "." + decimal + "0";
            if (decimal.length() == 2)
                returnVal = integer + "." + decimal;
            else {
                decimal = decimal.substring(0, 2);
                returnVal = integer + "." + decimal;
            }
            if (internalPart != null)
                return returnVal + " (" + internalPart;
            else
                return returnVal;
        }
    }
}
