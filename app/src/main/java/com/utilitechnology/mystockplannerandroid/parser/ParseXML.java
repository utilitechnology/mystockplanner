package com.utilitechnology.mystockplannerandroid.parser;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 24-03-2016.
 */
public class ParseXML {
    private List<String> price = new ArrayList<>();
    private List<String> change = new ArrayList<>();
    private List<String> changePercent = new ArrayList<>();
    private List<String> realChangePercent = new ArrayList<>();
    private List<String> dayHigh = new ArrayList<>();
    private List<String> dayLow = new ArrayList<>();
    private List<String> yearHigh = new ArrayList<>();
    private List<String> yearLow = new ArrayList<>();
    private List<String> volume = new ArrayList<>();
    private List<String> changeString = new ArrayList<>();
    private List<String> upsNDowns = new ArrayList<>();

    private String urlString = null;
    private String update = null;
    String attr = "";
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    ParseXML(String url) {
        urlString = url;
    }

    public List<String> getChange() {
        return change;
    }

    //Real means not rounded up to 2 decimal
    public List<String> getRealChangePercent() {
        return realChangePercent;
    }

    public List<String> getChangePercent() {
        return changePercent;
    }

    public List<String> getDayHigh() {
        return dayHigh;
    }

    public List<String> getDayLow() {
        return dayLow;
    }

    public List<String> getYearHigh() {
        return yearHigh;
    }

    public List<String> getYearLow() {
        return yearLow;
    }

    public List<String> getPrice() {
        return price;
    }

    public List<String> getVolume() {
        return volume;
    }

    public List<String> getChangeString() {
        return changeString;
    }

    public List<String> getUpsNDowns() {
        return upsNDowns;
    }

    public String getUpdate() {
        if (update != null) {
            Date time = new Date(Long.valueOf(update) * 1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM HH:mm", new Locale("en", "IN"));
            return "Last update: " + sdf.format(time) + " (Quotes delayed)";
        } else
            return "Last update: N/A (Quotes delayed)";
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        String tempChange = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                if (name != null)
                    if (name.equals("field"))
                        if (myParser.getAttributeValue(null, "name") != null)
                            attr = myParser.getAttributeValue(null, "name");
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null && attr != null)
                            if (name.equals("field")) {
                                if (attr.equalsIgnoreCase("price")) {
                                    //Log.e("important",text);
                                    price.add(make2Decimal(text));
                                } else if (attr.equalsIgnoreCase("change")) {
                                    if (!text.contains("-")) {
                                        text = "+" + text;
                                        upsNDowns.add("up");
                                    } else
                                        upsNDowns.add("down");
                                    change.add(make2Decimal(text));
                                    tempChange = make2Decimal(text);
                                } else if (attr.equalsIgnoreCase("chg_percent")) {
                                    realChangePercent.add(text);
                                    text = make2Decimal(text);
                                    if (!text.contains("-"))
                                        text = "+" + text + "%";
                                    else
                                        text = text + "%";
                                    changePercent.add(text);
                                    changeString.add(tempChange + " (" + text + ")");
                                } else if (attr.equalsIgnoreCase("day_high")) {
                                    dayHigh.add(make2Decimal(text));
                                } else if (attr.equalsIgnoreCase("day_low")) {
                                    dayLow.add(make2Decimal(text));
                                } else if (attr.equalsIgnoreCase("year_high")) {
                                    yearHigh.add(make2Decimal(text));
                                } else if (attr.equalsIgnoreCase("year_low")) {
                                    yearLow.add(make2Decimal(text));
                                } else if (attr.equalsIgnoreCase("volume")) {
                                    volume.add(text);
                                } else if (attr.equalsIgnoreCase("ts")) {
                                    update = text;
                                }
                            }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
//                    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; MotoE2(4G-LTE) Build/MPI24.65-39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.81 Mobile Safari/537.36");
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");
                    conn.setInstanceFollowRedirects(false);

                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                    //Todo: this line below is experimental, it may cause iobx, use with caution
                    parsingInComplete =false;
                    //Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
//Todo: approximation here
    private String make2Decimal(String number) {
        if(number.equals(""))
            return "0";
        if (!number.contains(".")) {
            return number + ".00";
        } else {
            String integer = number.split("\\.")[0];
            String decimal = number.split("\\.")[1];
            if (decimal.length() == 1)
                return integer + "." + decimal + "0";
            if (decimal.length() == 2)
                return integer + "." + decimal;
            else {
                decimal = decimal.substring(0, 2);
                return integer + "." + decimal;
            }
        }
    }
}
