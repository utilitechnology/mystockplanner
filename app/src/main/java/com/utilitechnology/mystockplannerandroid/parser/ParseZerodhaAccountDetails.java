package com.utilitechnology.mystockplannerandroid.parser;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Bibaswann on 19-06-2016.
 */
public class ParseZerodhaAccountDetails {

    String urlString;
    public volatile boolean parsingInComplete = true;
    String totC, avlC, utlC;

    public ParseZerodhaAccountDetails(String url) {
        urlString = url;
    }

    public String getTotC() {
        return totC;
    }

    public String getAvlC() {
        return avlC;
    }

    public String getUtlC() {
        return utlC;
    }

    private void parseJSONAndStoreIt(String jsonString) {
        try {

            JSONObject jsonRootObject = new JSONObject(jsonString);
            String status = jsonRootObject.optString("status");
            if (status.equals("success")) {
                String data = jsonRootObject.optString("data");
                JSONObject jsonTotalData = new JSONObject(data);
                totC = jsonTotalData.optString("net");
                JSONObject avlData = new JSONObject(jsonTotalData.optString("available"));
                avlC = avlData.optString("cash");
                JSONObject utlData = new JSONObject(jsonTotalData.optString("utilised"));
                utlC = utlData.optString("debits");

                //Log.e("important","message from server "+jsonObject.optString("status_message"));

            }
            parsingInComplete = false;
        } catch (JSONException e) {
            //Log.e("important", "exception " + Log.getStackTraceString(e));
        }
    }

    public void fetchJSON() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");

                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    //Log.e("important", "found string " +sb.toString());
                    String modifiedString = "";
                    modifiedString += sb.toString().replace("//", "");
                    parseJSONAndStoreIt(modifiedString);
                    stream.close();
                } catch (Exception e) {
                    //Log.e("important","exception in reading "+Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
