package com.utilitechnology.mystockplannerandroid.parser;

import android.content.Context;
import android.util.Log;

import com.utilitechnology.mystockplannerandroid.ScriptHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Bibaswann on 12-06-2016.
 */
public class ParseZerodhaOrder {

    private String urlString = null;
    public volatile boolean parsingInComplete = true;
    ScriptHelper sch;
    Context mContext;

    List<String> script = new ArrayList<>();
    List<String> zerodhaSymbol = new ArrayList<>();
    List<String> orderId = new ArrayList<>();
    List<String> orderStatus = new ArrayList<>();//complete, open or rejected
    List<String> exchange = new ArrayList<>();
    List<String> orderType = new ArrayList<>();//market, limit or stoploss
    List<String> TransactionType = new ArrayList<>();//buy or sell
    List<String> product = new ArrayList<>();//mis or cnc (intrady or delivery), not required now
    List<String> limitPrice = new ArrayList<>();
    List<String> quantity = new ArrayList<>();
    List<String> triggerPrice = new ArrayList<>();
    List<String> averagePrice = new ArrayList<>();
    List<String> validity = new ArrayList<>();
    List<String> disclosed = new ArrayList<>();


    public ParseZerodhaOrder(String url, Context context) {
        urlString = url;
        mContext = context;
        sch = new ScriptHelper(mContext);
    }

    public List<String> getAveragePrice() {
        return averagePrice;
    }

    public List<String> getExchange() {
        return exchange;
    }

    public List<String> getLimitPrice() {
        return limitPrice;
    }

    public List<String> getOrderId() {
        return orderId;
    }

    public List<String> getOrderStatus() {
        return orderStatus;
    }

    public List<String> getOrderType() {
        return orderType;
    }

    public List<String> getProduct() {
        return product;
    }

    public List<String> getQuantity() {
        return quantity;
    }

    public List<String> getScript() {
        return script;
    }

    public List<String> getTransactionType() {
        return TransactionType;
    }

    public List<String> getTriggerPrice() {
        return triggerPrice;
    }

    public List<String> getValidity() {
        return validity;
    }

    public List<String> getDisclosed() {
        return disclosed;
    }

    public List<String> getZerodhaSymbol() {
        return zerodhaSymbol;
    }

    private void parseJSONAndStoreIt(String jsonString) {
        try {

            JSONObject jsonRootObject = new JSONObject(jsonString);
            String status = jsonRootObject.optString("status");
            if (status.equals("success")) {
                String data = jsonRootObject.optString("data");
                JSONArray jsonArray = new JSONArray(data);
                String tempSymbol, tempExchange, tempScript;
                //Log.e("important", "found " + jsonArray.length() + " items");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    tempExchange = jsonObject.optString("exchange");
                    tempSymbol = jsonObject.optString("tradingsymbol");
                    tempScript = sch.zerodhaSymbolToScript(tempSymbol, tempExchange);
                    if(tempScript==null)
                        continue;

                    zerodhaSymbol.add(tempSymbol);
                    script.add(tempScript);
                    exchange.add(tempExchange);

                    orderId.add(jsonObject.optString("order_id"));
                    orderStatus.add(jsonObject.optString("status"));

                    orderType.add(jsonObject.optString("order_type"));
                    TransactionType.add(jsonObject.optString("transaction_type"));
                    if (jsonObject.optString("product").equals("MIS"))
                        product.add("INTRADAY");
                    else if (jsonObject.optString("product").equals("CNC"))
                        product.add("DELIVERY");
                    else
                        product.add(jsonObject.optString("product"));
                    limitPrice.add(jsonObject.optString("price"));
                    quantity.add(jsonObject.optString("quantity"));
                    triggerPrice.add(jsonObject.optString("trigger_price"));
                    averagePrice.add(jsonObject.optString("average_price"));
                    validity.add(jsonObject.optString("validity"));
                    disclosed.add(jsonObject.optString("disclosed_quantity"));
                    //Log.e("important","message from server "+jsonObject.optString("status_message"));
                }
            }
            parsingInComplete = false;
        } catch (JSONException e) {
            //Log.e("important", "exception " + Log.getStackTraceString(e));
        }
    }

    public void fetchJSON() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");

                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    //Log.e("important", "found string " +sb.toString());
                    String modifiedString = "";
                    modifiedString += sb.toString().replace("//", "");
                    parseJSONAndStoreIt(modifiedString);
                    stream.close();
                } catch (Exception e) {
                    //Log.e("important","exception in reading "+Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
