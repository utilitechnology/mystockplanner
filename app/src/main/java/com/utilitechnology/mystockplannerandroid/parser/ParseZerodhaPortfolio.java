package com.utilitechnology.mystockplannerandroid.parser;

import android.content.Context;

import com.utilitechnology.mystockplannerandroid.object.PortfolioObject;
import com.utilitechnology.mystockplannerandroid.ScriptHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Bibaswann on 19-06-2016.
 */
public class ParseZerodhaPortfolio {

    String urlString;
    Context mContext;
    ScriptHelper sch;
    public volatile boolean parsingInComplete = true;

   List<PortfolioObject> portfolioObjectList=new ArrayList<>();

    public ParseZerodhaPortfolio(String url, Context context) {
        urlString = url;
        mContext = context;
        sch = new ScriptHelper(mContext);
    }

    public List<PortfolioObject> getPortfolioObjectList() {
        return portfolioObjectList;
    }

    private void parseJSONAndStoreIt(String jsonString) {
        try {

            JSONObject jsonRootObject = new JSONObject(jsonString);
            String status = jsonRootObject.optString("status");
            if (status.equals("success")) {
                String data = jsonRootObject.optString("data");
                JSONArray jsonArray = new JSONArray(data);
                String tempSymbol, tempExchange, tempScript;
                //Log.e("important", "found " + jsonArray.length() + " items");
                for (int i = 0; i < jsonArray.length(); i++) {
                    PortfolioObject temPort=new PortfolioObject();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    tempExchange = jsonObject.optString("exchange");
                    tempSymbol = jsonObject.optString("tradingsymbol");
                    tempScript = sch.zerodhaSymbolToScript(tempSymbol, tempExchange);

                    if(tempScript==null)
                        continue;

                    temPort.setExchangeName(tempExchange);
                    temPort.setStockName(sch.scriptToCompany(tempSymbol));
                    temPort.setScriptName(tempScript);
                    temPort.setiBoughtPrice(Float.parseFloat(jsonObject.optString("average_price")));
                    temPort.setQuantityAmt(Integer.parseInt(jsonObject.optString("quantity"))+Integer.parseInt(jsonObject.optString("t1_quantity")));
                    temPort.setSellTargetPrice(0.0f);
                    temPort.setModifiable(false);
                    // Adding contact to list
                    portfolioObjectList.add(temPort);
                    //Log.e("important","message from server "+jsonObject.optString("status_message"));
                }
            }
            parsingInComplete = false;
        } catch (JSONException e) {
            //Log.e("important", "exception " + Log.getStackTraceString(e));
        }
    }

    public void fetchJSON() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4");
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");

                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    //Log.e("important", "found string " +sb.toString());
                    String modifiedString = "";
                    modifiedString += sb.toString().replace("//", "");
                    parseJSONAndStoreIt(modifiedString);
                    stream.close();
                } catch (Exception e) {
                    //Log.e("important","exception in reading "+Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
