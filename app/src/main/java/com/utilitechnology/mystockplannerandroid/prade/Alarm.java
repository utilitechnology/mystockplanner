package com.utilitechnology.mystockplannerandroid.prade;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.util.Calendar;

/**
 * Created by Bibaswann on 22-02-2017.
 */

public class Alarm extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (hour == 12)
            new BackTrade(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            new BackSquareOff(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void SetTradeAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 12);
        cal.set(Calendar.MINUTE, 40);
        am.set(AlarmManager.RTC, cal.getTimeInMillis(), pi);
    }

//    public void setTradeAlarmAgain(Context context) {
//        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//        if (hour < 13) {
//            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//
//            Intent i = new Intent(context, Alarm.class);
//            PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
//
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.MINUTE, 30);
//            am.set(AlarmManager.RTC, cal.getTimeInMillis(), pi);
//        }
//    }

    public void SetSquareOffAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 14);
        am.set(AlarmManager.RTC, cal.getTimeInMillis(), pi);
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
