package com.utilitechnology.mystockplannerandroid.prade;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;

import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Bibaswann on 19-05-2017.
 */

public class BackSquareOff extends AsyncTask<Void, Void, Void> {

    private static final String ORDER_URL = "https://api.kite.trade/orders";
    private static final String DELETE_URL = "https://api.kite.trade/orders/bo/";
    //    private static final String POSITION_URL = "https://api.kite.trade/portfolio/positions/";
    Context mContext;
    TradingAccountSettingsAPI tras;
    String apiKey, accessToken;
    private List<String> openOrders;

    private volatile boolean orderProcessingIncomplete = true;
    private volatile boolean orderProcessingSuccess;

    public BackSquareOff(Context context) {
        mContext = context;
        tras = new TradingAccountSettingsAPI(mContext);
        apiKey = mContext.getString(R.string.ZERODHA_API_KEY);
        accessToken = tras.readSetting("access_token");
        openOrders = new ArrayList<>();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (hasActiveInternetConnection()) {
            ParseOrderHistoryDataJSON pohdj = new ParseOrderHistoryDataJSON();
            pohdj.fetchOrders();
            while (pohdj.parsingInComplete) ;

            for (String id : openOrders) {
                performDeleteOperation(id.split(":")[1], id.split(":")[0]);
                while (orderProcessingIncomplete) ;
                if (orderProcessingSuccess) {
//                    Log.e("important", "yey! itz success, too bad i will not see this msz");
                }
                orderProcessingIncomplete = true;
            }
            notify("Process finished");
        } else
            notify("No internet, act quickly");
        return null;
    }

    private void notify(String msg) {
        int mNotificationId = 2;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_timeline_black_24dp)
                .setContentTitle("MSP Background")
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentText(msg);

        NotificationManager mNotifyMgr = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean hasActiveInternetConnection() {
        if (isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1000);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    private class ParseOrderHistoryDataJSON {
        String status;
        public volatile boolean parsingInComplete = true;

        public String getStatus() {
            return status;
        }

        private void parseJSONAndStoreIt(String jsonString) {
            try {
                JSONObject jsonRootObject = new JSONObject(jsonString);
                status = jsonRootObject.optString("status");
                if (status.equals("success")) {
                    String data = jsonRootObject.optString("data");
                    JSONArray jsonArray = new JSONArray(data);
                    //Log.e("important", "found " + jsonArray.length() + " items");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.optString("product").equals("BO")) {
                            //parent_order_id != null removes all entry orders, successful or rejected
                            //status != COMPLETE removes all executed second orders
                            //status != CANCELLED removes all cancelled second orders
                            if (!jsonObject.optString("status").equals("COMPLETE") && !jsonObject.optString("status").equals("CANCELLED") && !jsonObject.optString("status").equals("REJECTED")) {
                                openOrders.add(jsonObject.optString("parent_order_id") + ":" + jsonObject.optString("order_id"));
                            }
                        }
                    }
                }

                parsingInComplete = false;
            } catch (JSONException e) {
                //Log.e("important", "exception " + Log.getStackTraceString(e));
            }
        }

        private void fetchOrders() {
            try {
//                Log.e("important", ORDER_URL + "?api_key=" + apiKey + "&access_token=" + accessToken);
                URL url = new URL(ORDER_URL + "?api_key=" + apiKey + "&access_token=" + accessToken);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                StringBuilder sb = new StringBuilder();
                InputStream stream = conn.getInputStream();
                conn.connect();
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                if ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                String modifiedString = "";
                modifiedString += sb.toString().replace("//", "");
                parseJSONAndStoreIt(modifiedString);
                stream.close();

            } catch (Exception ignored) {

            }
        }
    }

//    private class ParsePositionDataJSON {
//        String status;
//        public volatile boolean parsingInComplete = true;
//
//        public String getStatus() {
//            return status;
//        }
//
//        private void parseJSONAndStoreIt(String jsonString) {
//            try {
//                JSONObject jsonRootObject = new JSONObject(jsonString);
//                status = jsonRootObject.optString("status");
//                if (status.equals("success")) {
//                    String net = new JSONObject(jsonRootObject.optString("data")).optString("net");
//                    JSONArray jsonArray = new JSONArray(net);
//                    //Log.e("important", "found " + jsonArray.length() + " items");
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                        if (jsonObject.optString("product").equals("BO")) {
//                            if ((Float.parseFloat(jsonObject.optString("unrealised")) + Float.parseFloat(jsonObject.optString("realised"))) < 0) {
//                                losses++;
//                                lossScripts.add(jsonObject.optString("tradingsymbol"));
//                            }
//                            if ((Float.parseFloat(jsonObject.optString("unrealised")) + Float.parseFloat(jsonObject.optString("realised"))) >= 0) {
//                                profits++;
//                                profitScripts.add(jsonObject.optString("tradingsymbol"));
//                            }
//                            financial += Float.parseFloat(jsonObject.optString("unrealised")) + Float.parseFloat(jsonObject.optString("realised"));
//                        }
//                    }
//                }
//
//                parsingInComplete = false;
//            } catch (JSONException e) {
//                //Log.e("important", "exception " + Log.getStackTraceString(e));
//            }
//        }
//
//        private void fetchPositions() {
//            try {
////                Log.e("important", POSITION_URL + "?api_key=" + apiKey + "&access_token=" + accessToken);
//                URL url = new URL(POSITION_URL + "?api_key=" + apiKey + "&access_token=" + accessToken);
//                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//                conn.setReadTimeout(15000);
//                conn.setConnectTimeout(15000);
//                conn.setRequestMethod("GET");
//                conn.setDoInput(true);
//                StringBuilder sb = new StringBuilder();
//                InputStream stream = conn.getInputStream();
//                conn.connect();
//                String line;
//                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
//                if ((line = br.readLine()) != null) {
//                    sb.append(line);
//                }
//                String modifiedString = "";
//                modifiedString += sb.toString().replace("//", "");
//                parseJSONAndStoreIt(modifiedString);
//                stream.close();
//
//            } catch (Exception ignored) {
//
//            }
//        }
//    }

    public void performDeleteOperation(final String orderId, final String parentOrderId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url;
                    if (!parentOrderId.equals("null"))
                        url = new URL(DELETE_URL + orderId + "?api_key=" + apiKey + "&access_token=" + accessToken + "&parent_order_id=" + parentOrderId);
                    else
                        url = new URL(DELETE_URL + orderId + "?api_key=" + apiKey + "&access_token=" + accessToken);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("DELETE");
                    conn.setDoInput(true);
                    OutputStream os = conn.getOutputStream();
//                    conn.connect();
                    int responseCode = conn.getResponseCode();
//                    Log.e("important", responseCode+": "+conn.getResponseMessage());
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        if ((line = br.readLine()) != null) {
//                            Log.e("important", "lines are " + line);
                            ParseOrderResultDataJSON podj = new ParseOrderResultDataJSON();
                            podj.parseJSONAndStoreIt(line);
                            while (podj.parsingInComplete) ;
                            if (podj.getStatus().equals("success")) {
                                orderProcessingIncomplete = false;
                                orderProcessingSuccess = true;
                            } else {
                                orderProcessingIncomplete = false;
                                orderProcessingSuccess = false;
                            }
                        }
                    } else {
                        orderProcessingIncomplete = false;
                        orderProcessingSuccess = false;
                    }
                    os.close();
                } catch (IOException e) {
                    orderProcessingIncomplete = false;
                    orderProcessingSuccess = false;
                }
            }
        }).start();
    }

    private class ParseOrderResultDataJSON {
        String status, ordrId;
        public volatile boolean parsingInComplete = true;

        public String getStatus() {
            return status;
        }

        public String getOrdrId() {
            return ordrId;
        }

        private void parseJSONAndStoreIt(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                status = jsonObject.optString("status");
                String dataString = jsonObject.optString("data");
                if (status.equals("success")) {
                    JSONObject jsonDataObject = new JSONObject(dataString);
                    ordrId = jsonDataObject.optString("order_id");//No need for the id, just taking
                }

                parsingInComplete = false;
            } catch (JSONException e) {
                //Log.e("important", "exception " + Log.getStackTraceString(e));
            }
        }
    }
}
