package com.utilitechnology.mystockplannerandroid.prade;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;

import com.utilitechnology.mystockplannerandroid.CheckTradingToken;
import com.utilitechnology.mystockplannerandroid.R;
import com.utilitechnology.mystockplannerandroid.caching.TradingAccountSettingsAPI;
import com.utilitechnology.mystockplannerandroid.parser.ParseZerodhaAccountDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Bibaswann on 06-04-2017.
 */

public class BackTrade extends AsyncTask<Void, Void, Void> {

    private static final String BRACKET_ORDER_URL = "https://api.kite.trade/orders/bo";
    private static final String MARKET_URL = "https://api.kite.trade/instruments/NSE/";

    Context mContext;
    CheckTradingToken ctt;
    TradingAccountSettingsAPI tras;

    private static final int GRADUAL_UP = 100;
    private static final int GRADUAL_DOWN = 200;
    private static final int SHARP_UP = 300;
    private static final int SHARP_DOWN = 400;
    private static final int LOW_FLUCTUATION = 500;
    private static final int CHOPPY = 600;

    private static final double tick = 0.05;
    private static final double squareoff = 0.5;
    private static final double stoploss = 0.5;

    private static final int MARGIN = 20;
    private static final double CROSS_THRESHOLD = 0.05;

    private int charType = 0;
    int startFrom = 37;//Starts at line 7, reject first 30 minutes
    private int totalBuyable = 0;
    private int totalSellable = 0;
    private float totalCash;

    List<String> scripts = new ArrayList<>();
    List<Double> prices = new ArrayList<>();
    List<Integer> buyableIndices = new ArrayList<>();
    List<Integer> sellableIndices = new ArrayList<>();

    private volatile boolean orderProcessingIncomplete = true;
    private volatile boolean orderProcessingSuccess;

    //For changing chart source, change //Yahoo and //Google marked places, there are 2. Everything else is made compatible

    public BackTrade(Context context) {
        mContext = context;
        ctt = new CheckTradingToken(mContext);
        tras = new TradingAccountSettingsAPI(mContext);

        //Yahoo
        /*scripts.add("RELIANCE.NS");
        scripts.add("HDFCBANK.NS");
        scripts.add("ICICIBANK.NS");
        scripts.add("AXISBANK.NS");
        scripts.add("SBIN.NS");
        scripts.add("HDFC.NS");
        scripts.add("BHARATFIN.NS");
        scripts.add("INFY.NS");
        scripts.add("YESBANK.NS");
        scripts.add("MARUTI.NS");
        scripts.add("TATAMOTORS.NS");
        scripts.add("ITC.NS");
        scripts.add("TCS.NS");
        scripts.add("IDEA.NS");
        scripts.add("VEDL.NS");
        scripts.add("HINDALCO.NS");
        scripts.add("IBREALEST.NS");
        scripts.add("LT.NS");
        scripts.add("TATASTEEL.NS");
        scripts.add("RELCAPITAL.NS");
        scripts.add("GRASIM.NS");
        scripts.add("SUNTV.NS");
        scripts.add("IOC.NS");
        scripts.add("GAIL.NS");
        scripts.add("SUNPHARMA.NS");
        scripts.add("JINDALSTEL.NS");
        scripts.add("KOTAKBANK.NS");
        scripts.add("IBULHSGFIN.NS");
        scripts.add("BANKBARODA.NS");
        scripts.add("DLF.NS");
        scripts.add("INDUSINDBK.NS");
        scripts.add("BHARTIARTL.NS");
        scripts.add("HINDPETRO.NS");
        scripts.add("PNB.NS");
        scripts.add("BPCL.NS");
        scripts.add("RECLTD.NS");
        scripts.add("JETAIRWAYS.NS");
        scripts.add("INFRATEL.NS");
        scripts.add("TECHM.NS");
        scripts.add("COALINDIA.NS");
        scripts.add("BAJFINANCE.NS");
        scripts.add("CEATLTD.NS");
        scripts.add("ONGC.NS");
        scripts.add("AUROPHARMA.NS");
        scripts.add("HEROMOTOCO.NS");
        scripts.add("HDIL.NS");
        scripts.add("ADANIPORTS.NS");
        scripts.add("UPL.NS");
        scripts.add("LUPIN.NS");
        scripts.add("BHEL.NS");
        scripts.add("JUSTDIAL.NS");
        scripts.add("PFC.NS");
        scripts.add("DIVISLAB.NS");
        scripts.add("RELINFRA.NS");
        scripts.add("MRF.NS");
        scripts.add("DRREDDY.NS");
        scripts.add("HCLTECH.NS");
        scripts.add("LICHSGFIN.NS");
        scripts.add("INDIACEM.NS");
        scripts.add("POWERGRID.NS");
        scripts.add("CENTURYTEX.NS");
        scripts.add("HINDUNILVR.NS");
        scripts.add("ULTRACEMCO.NS");
        scripts.add("EICHERMOT.NS");
        scripts.add("DHFL.NS");
        scripts.add("JSWSTEEL.NS");
        scripts.add("ADANIENT.NS");
        scripts.add("WOCKPHARMA.NS");
        scripts.add("CANBK.NS");
        scripts.add("ASIANPAINT.NS");
        scripts.add("SUZLON.NS");
        scripts.add("FEDERALBNK.NS");
        scripts.add("PETRONET.NS");
        scripts.add("APOLLOTYRE.NS");
        scripts.add("HINDZINC.NS");
        scripts.add("BANKINDIA.NS");
        scripts.add("BHARATFORG.NS");
        scripts.add("NTPC.NS");
        scripts.add("ASHOKLEY.NS");
        scripts.add("ZEEL.NS");
        scripts.add("UNIONBANK.NS");
        scripts.add("JPASSOCIAT.NS");
        scripts.add("SRTRANSFIN.NS");
        scripts.add("SINTEX.NS");
        scripts.add("MOTHERSUMI.NS");
        scripts.add("WIPRO.NS");
        scripts.add("ESCORTS.NS");
        scripts.add("MFSL.NS");
        scripts.add("CIPLA.NS");
        scripts.add("KTKBANK.NS");
        scripts.add("RCOM.NS");
        scripts.add("VOLTAS.NS");
        scripts.add("TITAN.NS");
        scripts.add("ORIENTBANK.NS");
        scripts.add("CESC.NS");
        scripts.add("BIOCON.NS");
        scripts.add("CONCOR.NS");
        scripts.add("IRB.NS");
        scripts.add("JUBLFOOD.NS");
        scripts.add("ENGINERSIN.NS");
        scripts.add("IDFCBANK.NS");
        scripts.add("ABIRLANUVO.NS");
        scripts.add("TVSMOTOR.NS");
        scripts.add("IGL.NS");
        scripts.add("FORTIS.NS");
        scripts.add("APOLLOHOSP.NS");
        scripts.add("INFIBEAM.NS");
        scripts.add("ACC.NS");
        scripts.add("ARVIND.NS");
        scripts.add("INDIGO.NS");
        scripts.add("HAVELLS.NS");
        scripts.add("CADILAHC.NS");
        scripts.add("BEML.NS");
        scripts.add("JISLJALEQS.NS");
        scripts.add("BRITANNIA.NS");
        scripts.add("JUBILANT.NS");
        scripts.add("AMBUJACEM.NS");
        scripts.add("TATACOMM.NS");
        scripts.add("IDFC.NS");
        scripts.add("COLPAL.NS");
        scripts.add("DCBBANK.NS");
        scripts.add("RAYMOND.NS");
        scripts.add("BAJAJFINSV.NS");
        scripts.add("DISHTV.NS");
        scripts.add("GLENMARK.NS");
        scripts.add("TATAELXSI.NS");
        scripts.add("EXIDEIND.NS");
        scripts.add("MANAPPURAM.NS");
        scripts.add("NCC.NS");
        scripts.add("EDELWEISS.NS");
        scripts.add("ADANIPOWER.NS");
        scripts.add("KSCL.NS");
        scripts.add("NMDC.NS");
        scripts.add("TATASPONGE.NS");
        scripts.add("RAJESHEXPO.NS");
        scripts.add("UJJIVAN.NS");
        scripts.add("TATAPOWER.NS");
        scripts.add("JSWENERGY.NS");
        scripts.add("SRF.NS");
        scripts.add("DABUR.NS");
        scripts.add("ADVENZYMES.NS");
        scripts.add("AMARAJABAT.NS");
        scripts.add("MCX.NS");
        scripts.add("GMRINFRA.NS");
        scripts.add("MARICO.NS");
        scripts.add("SAIL.NS");
        scripts.add("TATACHEM.NS");
        scripts.add("BOSCHLTD.NS");
        scripts.add("NATCOPHARM.NS");
        scripts.add("CHOLAFIN.NS");
        scripts.add("SOUTHBANK.NS");
        scripts.add("CASTROLIND.NS");
        scripts.add("PEL.NS");
        scripts.add("GODREJCP.NS");
        scripts.add("BOMDYEING.NS");
        scripts.add("STAR.NS");
        scripts.add("JKTYRE.NS");
        scripts.add("NBCC.NS");
        scripts.add("SHREECEM.NS");
        scripts.add("IFCI.NS");
        scripts.add("IDBI.NS");
        scripts.add("VAKRANGEE.NS");
        scripts.add("GODREJIND.NS");
        scripts.add("BEL.NS");
        scripts.add("TATAGLOBAL.NS");
        scripts.add("BATAINDIA.NS");
        scripts.add("INDIANB.NS");
        scripts.add("PIDILITIND.NS");
        scripts.add("ALBK.NS");
        scripts.add("SIEMENS.NS");
        scripts.add("CANFINHOME.NS");
        scripts.add("CUMMINSIND.NS");
        scripts.add("HEXAWARE.NS");
        scripts.add("GRANULES.NS");
        scripts.add("RCF.NS");
        scripts.add("PVR.NS");
        scripts.add("TV18BRDCST.NS");
        scripts.add("CROMPTON.NS");
        scripts.add("EQUITAS.NS");
        scripts.add("TORNTPHARM.NS");
        scripts.add("MINDTREE.NS");
        scripts.add("DALMIABHA.NS");
        scripts.add("MUTHOOTFIN.NS");
        scripts.add("ANDHRABANK.NS");
        scripts.add("ICIL.NS");
        scripts.add("TORNTPOWER.NS");
        scripts.add("NESTLEIND.NS");
        scripts.add("RAIN.NS");
        scripts.add("NHPC.NS");
        scripts.add("SREINFRA.NS");
        scripts.add("PCJEWELLER.NS");
        scripts.add("OIL.NS");
        scripts.add("KAJARIACER.NS");
        scripts.add("CHENNPETRO.NS");
        scripts.add("AJANTPHARM.NS");
        scripts.add("EMAMILTD.NS");
        scripts.add("KWALITY.NS");
        scripts.add("LAKSHVILAS.NS");
        scripts.add("GODFRYPHLP.NS");
        scripts.add("PTC.NS");
        scripts.add("DHAMPURSUG.NS");
        scripts.add("CAPF.NS");
        scripts.add("MGL.NS");
        scripts.add("RDEL.NS");
        scripts.add("GNFC.NS");
        scripts.add("SYNDIBANK.NS");
        scripts.add("UNITECH.NS");
        scripts.add("MRPL.NS");
        scripts.add("ABAN.NS");
        scripts.add("NATIONALUM.NS");
        scripts.add("KARURVYSYA.NS");
        scripts.add("BALKRISIND.NS");
        scripts.add("DWARKESH.NS");
        scripts.add("NFL.NS");
        scripts.add("BERGEPAINT.NS");
        scripts.add("OFSS.NS");
        scripts.add("RPOWER.NS");
        scripts.add("BALRAMCHIN.NS");
        scripts.add("RAMCOCEM.NS");
        scripts.add("GSPL.NS");
        scripts.add("GODREJPROP.NS");
        scripts.add("NOCIL.NS");
        scripts.add("WABAG.NS");
        scripts.add("BAJAJELEC.NS");
        scripts.add("MERCATOR.NS");
        scripts.add("KEC.NS");
        scripts.add("UBL.NS");
        scripts.add("LALPATHLAB.NS");
        scripts.add("VIJAYABANK.NS");
        scripts.add("VADILALIND.NS");
        scripts.add("SOBHA.NS");
        scripts.add("IIFL.NS");
        scripts.add("PAGEIND.NS");
        scripts.add("OBEROIRLTY.NS");
        scripts.add("STRTECH.NS");
        scripts.add("ABB.NS");
        scripts.add("VGUARD.NS");
        scripts.add("PRAKASH.NS");
        scripts.add("GICHSGFIN.NS");
        scripts.add("JINDALPOLY.NS");
        scripts.add("GATI.NS");
        scripts.add("GSFC.NS");
        scripts.add("BEPL.NS");
        scripts.add("ALKEM.NS");
        scripts.add("FLFL.NS");
        scripts.add("REPCOHOME.NS");
        scripts.add("CUB.NS");
        scripts.add("PNCINFRA.NS");
        scripts.add("MOIL.NS");
        scripts.add("ABFRL.NS");
        scripts.add("TFCILTD.NS");
        scripts.add("KOPRAN.NS");
        scripts.add("KESORAMIND.NS");
        scripts.add("PRESTIGE.NS");
        scripts.add("MCLEODRUSS.NS");
        scripts.add("DBL.NS");
        scripts.add("RADICO.NS");
        scripts.add("GUJGASLTD.NS");
        scripts.add("OMAXE.NS");
        scripts.add("INOXWIND.NS");
        scripts.add("MOTILALOFS.NS");
        scripts.add("TRIDENT.NS");
        scripts.add("SHRIRAMCIT.NS");
        scripts.add("SUNTECK.NS");
        scripts.add("CHAMBLFERT.NS");
        scripts.add("INTELLECT.NS");
        scripts.add("IPCALAB.NS");
        scripts.add("CRISIL.NS");
        scripts.add("TRIVENI.NS");
        scripts.add("NIITTECH.NS");
        scripts.add("APARINDS.NS");
        scripts.add("BAYERCROP.NS");
        scripts.add("SCI.NS");
        scripts.add("GUJFLUORO.NS");
        scripts.add("EIDPARRY.NS");
        scripts.add("GSKCONS.NS");
        scripts.add("KEI.NS");
        scripts.add("LLOYDELENG.NS");
        scripts.add("AEGISCHEM.NS");
        scripts.add("INDHOTEL.NS");
        scripts.add("JSL.NS");
        scripts.add("COROMANDEL.NS");
        scripts.add("GRUH.NS");
        scripts.add("TRENT.NS");
        scripts.add("PARAGMILK.NS");
        scripts.add("CENTRALBK.NS");
        scripts.add("GREAVESCOT.NS");
        scripts.add("BODALCHEM.NS");
        scripts.add("BAJAJHIND.NS");
        scripts.add("STAMPEDE.NS");
        scripts.add("PIIND.NS");
        scripts.add("MPHASIS.NS");
        scripts.add("RALLIS.NS");
        scripts.add("HINDOILEXP.NS");
        scripts.add("NIITLTD.NS");
        scripts.add("SYNGENE.NS");
        scripts.add("GESHIP.NS");
        scripts.add("UFLEX.NS");
        scripts.add("WHIRLPOOL.NS");
        scripts.add("GPPL.NS");
        scripts.add("ALEMBICLTD.NS");
        scripts.add("PERSISTENT.NS");
        scripts.add("KPIT.NS");
        scripts.add("DHANBANK.NS");
        scripts.add("SPARC.NS");
        scripts.add("ORIENTCEM.NS");
        scripts.add("FEL.NS");
        scripts.add("MANINFRA.NS");
        scripts.add("GUJALKALI.NS");
        scripts.add("AMTEKAUTO.NS");
        scripts.add("BGRENERGY.NS");
        scripts.add("SUNDARMFIN.NS");
        scripts.add("CENTENKA.NS");
        scripts.add("MAXVIL.NS");
        scripts.add("ASTRAL.NS");
        scripts.add("HCG.NS");
        scripts.add("FSL.NS");
        scripts.add("SUNDRMFAST.NS");
        scripts.add("CARERATING.NS");
        scripts.add("KANSAINER.NS");
        scripts.add("ASIANTILES.NS");
        scripts.add("PFS.NS");
        scripts.add("JSLHISAR.NS");
        scripts.add("CYIENT.NS");
        scripts.add("DATAMATICS.NS");
        scripts.add("JKLAKSHMI.NS");
        scripts.add("CENTURYPLY.NS");
        scripts.add("DLINKINDIA.NS");
        scripts.add("GILLETTE.NS");
        scripts.add("JKPAPER.NS");
        scripts.add("DAAWAT.NS");
        scripts.add("INOXLEISUR.NS");
        scripts.add("KCP.NS");
        scripts.add("AARTIIND.NS");
        scripts.add("HOVS.NS");
        scripts.add("SOMANYCERA.NS");
        scripts.add("ALLCARGO.NS");
        scripts.add("TVSSRICHAK.NS");
        scripts.add("JYOTHYLAB.NS");
        scripts.add("COFFEEDAY.NS");
        scripts.add("QUICKHEAL.NS");
        scripts.add("TUBEINVEST.NS");
        scripts.add("MIRZAINT.NS");
        scripts.add("VRLLOG.NS");
        scripts.add("MAHLIFE.NS");
        scripts.add("MAWANASUG.NS");
        scripts.add("WELENT.NS");
        scripts.add("MONSANTO.NS");
        scripts.add("ASTRAMICRO.NS");
        scripts.add("CORPBANK.NS");
        scripts.add("HINDCOPPER.NS");
        scripts.add("HSCL.NS");
        scripts.add("SUPREMEIND.NS");
        scripts.add("THERMAX.NS");
        scripts.add("BLUESTARCO.NS");
        scripts.add("BAJAJHLDNG.NS");
        scripts.add("NCLIND.NS");
        scripts.add("SYMPHONY.NS");
        scripts.add("BIRLACORPN.NS");
        scripts.add("WABCOINDIA.NS");
        scripts.add("SURYAROSNI.NS");
        scripts.add("ABBOTINDIA.NS");
        scripts.add("SONASTEER.NS");
        scripts.add("RESPONIND.NS");
        scripts.add("NBVENTURES.NS");
        scripts.add("KRBL.NS");
        scripts.add("TATACOFFEE.NS");
        scripts.add("PRISMCEM.NS");
        scripts.add("GMDCLTD.NS");
        scripts.add("MINDAIND.NS");
        scripts.add("HIMATSEIDE.NS");
        scripts.add("NAUKRI.NS");
        scripts.add("NETWORK18.NS");
        scripts.add("QUESS.NS");
        scripts.add("SIGNET.NS");
        scripts.add("UFO.NS");
        scripts.add("BRIGADE.NS");
        scripts.add("BLUEDART.NS");
        scripts.add("TNPL.NS");
        scripts.add("ATUL.NS");
        scripts.add("SADBHAV.NS");
        scripts.add("HFCL.NS");
        scripts.add("TTML.NS");
        scripts.add("FIEMIND.NS");
        scripts.add("SHILPAMED.NS");
        scripts.add("PHOENIXLTD.NS");
        scripts.add("PGHH.NS");
        scripts.add("DEEPAKFERT.NS");
        scripts.add("BALMLAWRIE.NS");
        scripts.add("ADLABS.NS");
        scripts.add("DENABANK.NS");
        scripts.add("SNOWMAN.NS");
        scripts.add("AJMERA.NS");
        scripts.add("EVEREADY.NS");
        scripts.add("WSTCSTPAPR.NS");
        scripts.add("JPPOWER.NS");
        scripts.add("THOMASCOOK.NS");
        scripts.add("TTKPRESTIG.NS");
        scripts.add("UCOBANK.NS");
        scripts.add("COSMOFILMS.NS");
        scripts.add("LTI.NS");
        scripts.add("MANALIPETC.NS");
        scripts.add("GITANJALI.NS");
        scripts.add("POKARNA.NS");
        scripts.add("AVANTIFEED.NS");
        scripts.add("MAJESCO.NS");
        scripts.add("BALLARPUR.NS");
        scripts.add("SUBEX.NS");
        scripts.add("JKCEMENT.NS");
        scripts.add("REDINGTON.NS");
        scripts.add("SMLISUZU.NS");
        scripts.add("DEEPIND.NS");
        scripts.add("APLLTD.NS");
        scripts.add("SRIPIPES.NS");
        scripts.add("BYKE.NS");
        scripts.add("DCMSHRIRAM.NS");
        scripts.add("MEGH.NS");
        scripts.add("SKFINDIA.NS");
        scripts.add("BRFL.NS");
        scripts.add("TATAINVEST.NS");
        scripts.add("NUCLEUS.NS");
        scripts.add("MANPASAND.NS");
        scripts.add("GDL.NS");
        scripts.add("AIFL.NS");
        scripts.add("DCW.NS");
        scripts.add("LAXMIMACH.NS");
        scripts.add("HEIDELBERG.NS");
        scripts.add("CARBORUNIV.NS");
        scripts.add("^nsei");*/

        //Google
        scripts.add("NSE:RELIANCE");
        scripts.add("NSE:HDFCBANK");
        scripts.add("NSE:ICICIBANK");
        scripts.add("NSE:AXISBANK");
        scripts.add("NSE:SBIN");
        scripts.add("NSE:HDFC");
        scripts.add("NSE:BHARATFIN");
        scripts.add("NSE:INFY");
        scripts.add("NSE:YESBANK");
        scripts.add("NSE:MARUTI");
        scripts.add("NSE:TATAMOTORS");
        scripts.add("NSE:ITC");
        scripts.add("NSE:TCS");
        scripts.add("NSE:IDEA");
        scripts.add("NSE:VEDL");
        scripts.add("NSE:HINDALCO");
        scripts.add("NSE:IBREALEST");
        scripts.add("NSE:LT");
        scripts.add("NSE:TATASTEEL");
        scripts.add("NSE:RELCAPITAL");
        scripts.add("NSE:GRASIM");
        scripts.add("NSE:SUNTV");
        scripts.add("NSE:IOC");
        scripts.add("NSE:GAIL");
        scripts.add("NSE:SUNPHARMA");
        scripts.add("NSE:JINDALSTEL");
        scripts.add("NSE:KOTAKBANK");
        scripts.add("NSE:IBULHSGFIN");
        scripts.add("NSE:BANKBARODA");
        scripts.add("NSE:DLF");
        scripts.add("NSE:INDUSINDBK");
        scripts.add("NSE:BHARTIARTL");
        scripts.add("NSE:HINDPETRO");
        scripts.add("NSE:PNB");
        scripts.add("NSE:BPCL");
        scripts.add("NSE:RECLTD");
        scripts.add("NSE:JETAIRWAYS");
        scripts.add("NSE:INFRATEL");
        scripts.add("NSE:TECHM");
        scripts.add("NSE:COALINDIA");
        scripts.add("NSE:BAJFINANCE");
        scripts.add("NSE:CEATLTD");
        scripts.add("NSE:ONGC");
        scripts.add("NSE:AUROPHARMA");
        scripts.add("NSE:HEROMOTOCO");
        scripts.add("NSE:HDIL");
        scripts.add("NSE:ADANIPORTS");
        scripts.add("NSE:UPL");
        scripts.add("NSE:LUPIN");
        scripts.add("NSE:BHEL");
        scripts.add("NSE:JUSTDIAL");
        scripts.add("NSE:PFC");
        scripts.add("NSE:DIVISLAB");
        scripts.add("NSE:RELINFRA");
        scripts.add("NSE:MRF");
        scripts.add("NSE:DRREDDY");
        scripts.add("NSE:HCLTECH");
        scripts.add("NSE:LICHSGFIN");
        scripts.add("NSE:INDIACEM");
        scripts.add("NSE:POWERGRID");
        scripts.add("NSE:CENTURYTEX");
        scripts.add("NSE:HINDUNILVR");
        scripts.add("NSE:ULTRACEMCO");
        scripts.add("NSE:EICHERMOT");
        scripts.add("NSE:DHFL");
        scripts.add("NSE:JSWSTEEL");
        scripts.add("NSE:ADANIENT");
        scripts.add("NSE:WOCKPHARMA");
        scripts.add("NSE:CANBK");
        scripts.add("NSE:ASIANPAINT");
        scripts.add("NSE:SUZLON");
        scripts.add("NSE:FEDERALBNK");
        scripts.add("NSE:PETRONET");
        scripts.add("NSE:APOLLOTYRE");
        scripts.add("NSE:HINDZINC");
        scripts.add("NSE:BANKINDIA");
        scripts.add("NSE:BHARATFORG");
        scripts.add("NSE:NTPC");
        scripts.add("NSE:ASHOKLEY");
        scripts.add("NSE:ZEEL");
        scripts.add("NSE:UNIONBANK");
        scripts.add("NSE:JPASSOCIAT");
        scripts.add("NSE:SRTRANSFIN");
        scripts.add("NSE:SINTEX");
        scripts.add("NSE:MOTHERSUMI");
        scripts.add("NSE:WIPRO");
        scripts.add("NSE:ESCORTS");
        scripts.add("NSE:MFSL");
        scripts.add("NSE:CIPLA");
        scripts.add("NSE:KTKBANK");
        scripts.add("NSE:RCOM");
        scripts.add("NSE:VOLTAS");
        scripts.add("NSE:TITAN");
        scripts.add("NSE:ORIENTBANK");
        scripts.add("NSE:CESC");
        scripts.add("NSE:BIOCON");
        scripts.add("NSE:CONCOR");
        scripts.add("NSE:IRB");
        scripts.add("NSE:JUBLFOOD");
        scripts.add("NSE:ENGINERSIN");
        scripts.add("NSE:IDFCBANK");
        scripts.add("NSE:ABIRLANUVO");
        scripts.add("NSE:TVSMOTOR");
        scripts.add("NSE:IGL");
        scripts.add("NSE:FORTIS");
        scripts.add("NSE:APOLLOHOSP");
        scripts.add("NSE:INFIBEAM");
        scripts.add("NSE:ACC");
        scripts.add("NSE:ARVIND");
        scripts.add("NSE:INDIGO");
        scripts.add("NSE:HAVELLS");
        scripts.add("NSE:CADILAHC");
        scripts.add("NSE:BEML");
        scripts.add("NSE:JISLJALEQS");
        scripts.add("NSE:BRITANNIA");
        scripts.add("NSE:JUBILANT");
        scripts.add("NSE:AMBUJACEM");
        scripts.add("NSE:TATACOMM");
        scripts.add("NSE:IDFC");
        scripts.add("NSE:COLPAL");
        scripts.add("NSE:DCBBANK");
        scripts.add("NSE:RAYMOND");
        scripts.add("NSE:BAJAJFINSV");
        scripts.add("NSE:DISHTV");
        scripts.add("NSE:GLENMARK");
        scripts.add("NSE:TATAELXSI");
        scripts.add("NSE:EXIDEIND");
        scripts.add("NSE:MANAPPURAM");
        scripts.add("NSE:NCC");
        scripts.add("NSE:EDELWEISS");
        scripts.add("NSE:ADANIPOWER");
        scripts.add("NSE:KSCL");
        scripts.add("NSE:NMDC");
        scripts.add("NSE:TATASPONGE");
        scripts.add("NSE:RAJESHEXPO");
        scripts.add("NSE:UJJIVAN");
        scripts.add("NSE:TATAPOWER");
        scripts.add("NSE:JSWENERGY");
        scripts.add("NSE:SRF");
        scripts.add("NSE:DABUR");
        scripts.add("NSE:ADVENZYMES");
        scripts.add("NSE:AMARAJABAT");
        scripts.add("NSE:MCX");
        scripts.add("NSE:GMRINFRA");
        scripts.add("NSE:MARICO");
        scripts.add("NSE:SAIL");
        scripts.add("NSE:TATACHEM");
        scripts.add("NSE:BOSCHLTD");
        scripts.add("NSE:NATCOPHARM");
        scripts.add("NSE:CHOLAFIN");
        scripts.add("NSE:SOUTHBANK");
        scripts.add("NSE:CASTROLIND");
        scripts.add("NSE:PEL");
        scripts.add("NSE:GODREJCP");
        scripts.add("NSE:BOMDYEING");
        scripts.add("NSE:STAR");
        scripts.add("NSE:JKTYRE");
        scripts.add("NSE:NBCC");
        scripts.add("NSE:SHREECEM");
        scripts.add("NSE:IFCI");
        scripts.add("NSE:IDBI");
        scripts.add("NSE:VAKRANGEE");
        scripts.add("NSE:GODREJIND");
        scripts.add("NSE:BEL");
        scripts.add("NSE:TATAGLOBAL");
        scripts.add("NSE:BATAINDIA");
        scripts.add("NSE:INDIANB");
        scripts.add("NSE:PIDILITIND");
        scripts.add("NSE:ALBK");
        scripts.add("NSE:SIEMENS");
        scripts.add("NSE:CANFINHOME");
        scripts.add("NSE:CUMMINSIND");
        scripts.add("NSE:HEXAWARE");
        scripts.add("NSE:GRANULES");
        scripts.add("NSE:RCF");
        scripts.add("NSE:PVR");
        scripts.add("NSE:TV18BRDCST");
        scripts.add("NSE:CROMPTON");
        scripts.add("NSE:EQUITAS");
        scripts.add("NSE:TORNTPHARM");
        scripts.add("NSE:MINDTREE");
        scripts.add("NSE:DALMIABHA");
        scripts.add("NSE:MUTHOOTFIN");
        scripts.add("NSE:ANDHRABANK");
        scripts.add("NSE:ICIL");
        scripts.add("NSE:TORNTPOWER");
        scripts.add("NSE:NESTLEIND");
        scripts.add("NSE:RAIN");
        scripts.add("NSE:NHPC");
        scripts.add("NSE:SREINFRA");
        scripts.add("NSE:PCJEWELLER");
        scripts.add("NSE:OIL");
        scripts.add("NSE:KAJARIACER");
        scripts.add("NSE:CHENNPETRO");
        scripts.add("NSE:AJANTPHARM");
        scripts.add("NSE:EMAMILTD");
        scripts.add("NSE:KWALITY");
        scripts.add("NSE:LAKSHVILAS");
        scripts.add("NSE:GODFRYPHLP");
        scripts.add("NSE:PTC");
        scripts.add("NSE:DHAMPURSUG");
        scripts.add("NSE:CAPF");
        scripts.add("NSE:MGL");
        scripts.add("NSE:RDEL");
        scripts.add("NSE:GNFC");
        scripts.add("NSE:SYNDIBANK");
        scripts.add("NSE:UNITECH");
        scripts.add("NSE:MRPL");
        scripts.add("NSE:ABAN");
        scripts.add("NSE:NATIONALUM");
        scripts.add("NSE:KARURVYSYA");
        scripts.add("NSE:BALKRISIND");
        scripts.add("NSE:DWARKESH");
        scripts.add("NSE:NFL");
        scripts.add("NSE:BERGEPAINT");
        scripts.add("NSE:OFSS");
        scripts.add("NSE:RPOWER");
        scripts.add("NSE:BALRAMCHIN");
        scripts.add("NSE:RAMCOCEM");
        scripts.add("NSE:GSPL");
        scripts.add("NSE:GODREJPROP");
        scripts.add("NSE:NOCIL");
        scripts.add("NSE:WABAG");
        scripts.add("NSE:BAJAJELEC");
        scripts.add("NSE:MERCATOR");
        scripts.add("NSE:KEC");
        scripts.add("NSE:UBL");
        scripts.add("NSE:LALPATHLAB");
        scripts.add("NSE:VIJAYABANK");
        scripts.add("NSE:VADILALIND");
        scripts.add("NSE:SOBHA");
        scripts.add("NSE:IIFL");
        scripts.add("NSE:PAGEIND");
        scripts.add("NSE:OBEROIRLTY");
        scripts.add("NSE:STRTECH");
        scripts.add("NSE:ABB");
        scripts.add("NSE:VGUARD");
        scripts.add("NSE:PRAKASH");
        scripts.add("NSE:GICHSGFIN");
        scripts.add("NSE:JINDALPOLY");
        scripts.add("NSE:GATI");
        scripts.add("NSE:GSFC");
        scripts.add("NSE:BEPL");
        scripts.add("NSE:ALKEM");
        scripts.add("NSE:FLFL");
        scripts.add("NSE:REPCOHOME");
        scripts.add("NSE:CUB");
        scripts.add("NSE:PNCINFRA");
        scripts.add("NSE:MOIL");
        scripts.add("NSE:ABFRL");
        scripts.add("NSE:TFCILTD");
        scripts.add("NSE:KOPRAN");
        scripts.add("NSE:KESORAMIND");
        scripts.add("NSE:PRESTIGE");
        scripts.add("NSE:MCLEODRUSS");
        scripts.add("NSE:DBL");
        scripts.add("NSE:RADICO");
        scripts.add("NSE:GUJGASLTD");
        scripts.add("NSE:OMAXE");
        scripts.add("NSE:INOXWIND");
        scripts.add("NSE:MOTILALOFS");
        scripts.add("NSE:TRIDENT");
        scripts.add("NSE:SHRIRAMCIT");
        scripts.add("NSE:SUNTECK");
        scripts.add("NSE:CHAMBLFERT");
        scripts.add("NSE:INTELLECT");
        scripts.add("NSE:IPCALAB");
        scripts.add("NSE:CRISIL");
        scripts.add("NSE:TRIVENI");
        scripts.add("NSE:NIITTECH");
        scripts.add("NSE:APARINDS");
        scripts.add("NSE:BAYERCROP");
        scripts.add("NSE:SCI");
        scripts.add("NSE:GUJFLUORO");
        scripts.add("NSE:EIDPARRY");
        scripts.add("NSE:GSKCONS");
        scripts.add("NSE:KEI");
        scripts.add("NSE:LLOYDELENG");
        scripts.add("NSE:AEGISCHEM");
        scripts.add("NSE:INDHOTEL");
        scripts.add("NSE:JSL");
        scripts.add("NSE:COROMANDEL");
        scripts.add("NSE:GRUH");
        scripts.add("NSE:TRENT");
        scripts.add("NSE:PARAGMILK");
        scripts.add("NSE:CENTRALBK");
        scripts.add("NSE:GREAVESCOT");
        scripts.add("NSE:BODALCHEM");
        scripts.add("NSE:BAJAJHIND");
        scripts.add("NSE:STAMPEDE");
        scripts.add("NSE:PIIND");
        scripts.add("NSE:MPHASIS");
        scripts.add("NSE:RALLIS");
        scripts.add("NSE:HINDOILEXP");
        scripts.add("NSE:NIITLTD");
        scripts.add("NSE:SYNGENE");
        scripts.add("NSE:GESHIP");
        scripts.add("NSE:UFLEX");
        scripts.add("NSE:WHIRLPOOL");
        scripts.add("NSE:GPPL");
        scripts.add("NSE:ALEMBICLTD");
        scripts.add("NSE:PERSISTENT");
        scripts.add("NSE:KPIT");
        scripts.add("NSE:DHANBANK");
        scripts.add("NSE:SPARC");
        scripts.add("NSE:ORIENTCEM");
        scripts.add("NSE:FEL");
        scripts.add("NSE:MANINFRA");
        scripts.add("NSE:GUJALKALI");
        scripts.add("NSE:AMTEKAUTO");
        scripts.add("NSE:BGRENERGY");
        scripts.add("NSE:SUNDARMFIN");
        scripts.add("NSE:CENTENKA");
        scripts.add("NSE:MAXVIL");
        scripts.add("NSE:ASTRAL");
        scripts.add("NSE:HCG");
        scripts.add("NSE:FSL");
        scripts.add("NSE:SUNDRMFAST");
        scripts.add("NSE:CARERATING");
        scripts.add("NSE:KANSAINER");
        scripts.add("NSE:ASIANTILES");
        scripts.add("NSE:PFS");
        scripts.add("NSE:JSLHISAR");
        scripts.add("NSE:CYIENT");
        scripts.add("NSE:DATAMATICS");
        scripts.add("NSE:JKLAKSHMI");
        scripts.add("NSE:CENTURYPLY");
        scripts.add("NSE:DLINKINDIA");
        scripts.add("NSE:GILLETTE");
        scripts.add("NSE:JKPAPER");
        scripts.add("NSE:DAAWAT");
        scripts.add("NSE:INOXLEISUR");
        scripts.add("NSE:KCP");
        scripts.add("NSE:AARTIIND");
        scripts.add("NSE:HOVS");
        scripts.add("NSE:SOMANYCERA");
        scripts.add("NSE:ALLCARGO");
        scripts.add("NSE:TVSSRICHAK");
        scripts.add("NSE:JYOTHYLAB");
        scripts.add("NSE:COFFEEDAY");
        scripts.add("NSE:QUICKHEAL");
        scripts.add("NSE:TUBEINVEST");
        scripts.add("NSE:MIRZAINT");
        scripts.add("NSE:VRLLOG");
        scripts.add("NSE:MAHLIFE");
        scripts.add("NSE:MAWANASUG");
        scripts.add("NSE:WELENT");
        scripts.add("NSE:MONSANTO");
        scripts.add("NSE:ASTRAMICRO");
        scripts.add("NSE:CORPBANK");
        scripts.add("NSE:HINDCOPPER");
        scripts.add("NSE:HSCL");
        scripts.add("NSE:SUPREMEIND");
        scripts.add("NSE:THERMAX");
        scripts.add("NSE:BLUESTARCO");
        scripts.add("NSE:BAJAJHLDNG");
        scripts.add("NSE:NCLIND");
        scripts.add("NSE:SYMPHONY");
        scripts.add("NSE:BIRLACORPN");
        scripts.add("NSE:WABCOINDIA");
        scripts.add("NSE:SURYAROSNI");
        scripts.add("NSE:ABBOTINDIA");
        scripts.add("NSE:SONASTEER");
        scripts.add("NSE:RESPONIND");
        scripts.add("NSE:NBVENTURES");
        scripts.add("NSE:KRBL");
        scripts.add("NSE:TATACOFFEE");
        scripts.add("NSE:PRISMCEM");
        scripts.add("NSE:GMDCLTD");
        scripts.add("NSE:MINDAIND");
        scripts.add("NSE:HIMATSEIDE");
        scripts.add("NSE:NAUKRI");
        scripts.add("NSE:NETWORK18");
        scripts.add("NSE:QUESS");
        scripts.add("NSE:SIGNET");
        scripts.add("NSE:UFO");
        scripts.add("NSE:BRIGADE");
        scripts.add("NSE:BLUEDART");
        scripts.add("NSE:TNPL");
        scripts.add("NSE:ATUL");
        scripts.add("NSE:SADBHAV");
        scripts.add("NSE:HFCL");
        scripts.add("NSE:TTML");
        scripts.add("NSE:FIEMIND");
        scripts.add("NSE:SHILPAMED");
        scripts.add("NSE:PHOENIXLTD");
        scripts.add("NSE:PGHH");
        scripts.add("NSE:DEEPAKFERT");
        scripts.add("NSE:BALMLAWRIE");
        scripts.add("NSE:ADLABS");
        scripts.add("NSE:DENABANK");
        scripts.add("NSE:SNOWMAN");
        scripts.add("NSE:AJMERA");
        scripts.add("NSE:EVEREADY");
        scripts.add("NSE:WSTCSTPAPR");
        scripts.add("NSE:JPPOWER");
        scripts.add("NSE:THOMASCOOK");
        scripts.add("NSE:TTKPRESTIG");
        scripts.add("NSE:UCOBANK");
        scripts.add("NSE:COSMOFILMS");
        scripts.add("NSE:LTI");
        scripts.add("NSE:MANALIPETC");
        scripts.add("NSE:GITANJALI");
        scripts.add("NSE:POKARNA");
        scripts.add("NSE:AVANTIFEED");
        scripts.add("NSE:MAJESCO");
        scripts.add("NSE:BALLARPUR");
        scripts.add("NSE:SUBEX");
        scripts.add("NSE:JKCEMENT");
        scripts.add("NSE:REDINGTON");
        scripts.add("NSE:SMLISUZU");
        scripts.add("NSE:DEEPIND");
        scripts.add("NSE:APLLTD");
        scripts.add("NSE:SRIPIPES");
        scripts.add("NSE:BYKE");
        scripts.add("NSE:DCMSHRIRAM");
        scripts.add("NSE:MEGH");
        scripts.add("NSE:SKFINDIA");
        scripts.add("NSE:BRFL");
        scripts.add("NSE:TATAINVEST");
        scripts.add("NSE:NUCLEUS");
        scripts.add("NSE:MANPASAND");
        scripts.add("NSE:GDL");
        scripts.add("NSE:AIFL");
        scripts.add("NSE:DCW");
        scripts.add("NSE:LAXMIMACH");
        scripts.add("NSE:HEIDELBERG");
        scripts.add("NSE:CARBORUNIV");
//        scripts.add("NSE:M%26M");// TODO: 22-05-2017 check if M&M is accepted in zerodha
//        scripts.add("NSE:M%26MFIN");
//        scripts.add("NSE:NIFTY");
    }

    @Override
    protected void onPreExecute() {
        notify("Process started");
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (hasActiveInternetConnection()) {
            if (ctt.isZerodhaValid()) {
                if (ctt.getUserID().equals("RB2176")) {
                    ParseZerodhaAccountDetails pztad = new ParseZerodhaAccountDetails("https://api.kite.trade/user/margins/equity?api_key=" + mContext.getString(R.string.ZERODHA_API_KEY) + "&access_token=" + tras.readSetting("access_token"));
                    pztad.fetchJSON();
                    while (pztad.parsingInComplete) ;
                    totalCash = Float.parseFloat(pztad.getAvlC()) * MARGIN;

                    for (String script : scripts) {
                        charType = checkChart(script);
//                        Log.e("important", "chart type: "+charType);
                        switch (charType) {
                            case GRADUAL_UP:
                                buyableIndices.add(scripts.indexOf(script));
                                totalBuyable++;
//                                Log.e("important", script + " is gradual up");
                                break;
                            case GRADUAL_DOWN:
                                sellableIndices.add(scripts.indexOf(script));
                                totalSellable++;
//                                Log.e("important", script + " is gradual down");
                                break;
                            case SHARP_UP:
//                                Log.e("important", script + " is sharp up");
                                break;
                            case SHARP_DOWN:
//                                Log.e("important", script + " is sharp down");
                                break;
                            case LOW_FLUCTUATION:
//                                Log.e("important", script + " is low fluctuation");
                                break;
                            case CHOPPY:
//                                Log.e("important", script + " is choppy");
                                break;
                            default:
//                                Log.e("important", script + " is undetermined");
                                break;
                        }
                    }

                    float individualAllocated;
                    individualAllocated = totalCash / (totalBuyable + totalSellable);
                    if (totalBuyable > 0) {
                        for (int index : buyableIndices) {
                            String itsScript;
                            if (scripts.get(index).contains("."))
                                itsScript = scripts.get(index).split("\\.")[0];
                            else
                                itsScript = scripts.get(index).split(":")[1];
                            Double itsPrice = 0.0d;
                            for (int i = 0; i < 10; i++) {
                                itsPrice = getPrice(itsScript, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
                                if (itsPrice != 0.0d)
                                    break;
                            }
                            if (itsPrice == 0.0d)
                                itsPrice = prices.get(index);
                            if ((itsPrice % tick) != 0)
                                itsPrice = (((int) (itsPrice / tick)) + 1) * tick;
                            double limit = (itsPrice * squareoff) / 100;
                            if ((limit % tick) != 0)
                                limit = limit - (limit % tick);
                            double sl = (itsPrice * stoploss) / 100;
                            if ((sl % tick) != 0)
                                sl = sl - (sl % tick);
                            int quantity = (int) (individualAllocated / itsPrice);
//                                int quantity = 1;

                            if (quantity > 0) {
                                HashMap<String, String> parahm = new HashMap<String, String>();
                                //api_key and access token are to be passed with url
//                                parahm.put("api_key", mContext.getString(R.string.ZERODHA_API_KEY));
//                                parahm.put("access_token", tras.readSetting("access_token"));
                                parahm.put("tradingsymbol", itsScript);
                                parahm.put("exchange", "NSE");//No need to convert script for NSE
                                parahm.put("transaction_type", "BUY");
                                parahm.put("order_type", "LIMIT");
                                parahm.put("product", "MIS");//mis or cnc
                                parahm.put("quantity", String.valueOf(quantity));
                                if (itsPrice > 10000) {
                                    if (quantity > 100)
                                        parahm.put("disclosed_quantity", "100");
                                } else if (itsPrice > 1000) {
                                    if (quantity > 1000)
                                        parahm.put("disclosed_quantity", "1000");
                                } else if (itsPrice > 100) {
                                    if (quantity > 10000)
                                        parahm.put("disclosed_quantity", "10000");
                                } else {
                                    if (quantity > 100000)
                                        parahm.put("disclosed_quantity", "100000");
                                }
                                parahm.put("validity", "DAY");
                                parahm.put("price", String.format(new Locale("en", "IN"), "%.2f", itsPrice));
                                parahm.put("squareoff_value", String.format(new Locale("en", "IN"), "%.2f", limit));
                                parahm.put("stoploss_value", String.format(new Locale("en", "IN"), "%.2f", sl));
//                                    parahm.put("trailing_stoploss", "1");
                                performBracketOrder(parahm, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
                                while (orderProcessingIncomplete) ;
                                if (orderProcessingSuccess) {
//                                            Log.e("important", "yey! itz success, too bad i will not see this msz");
                                }
                            }
                        }
                    }
                    if (totalSellable > 0) {
                        for (int index : sellableIndices) {
                            String itsScript;
                            if (scripts.get(index).contains("."))
                                itsScript = scripts.get(index).split("\\.")[0];
                            else
                                itsScript = scripts.get(index).split(":")[1];
                            Double itsPrice = 0.0d;
                            for (int i = 0; i < 10; i++) {
                                itsPrice = getPrice(itsScript, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
                                if (itsPrice != 0.0d)
                                    break;
                            }
                            if (itsPrice == 0.0d)
                                itsPrice = prices.get(index);
                            if ((itsPrice % tick) != 0)
                                itsPrice = (((int) (itsPrice / tick)) + 1) * tick;
                            double limit = (itsPrice * squareoff) / 100;
                            if ((limit % tick) != 0)
                                limit = limit - (limit % tick);
                            double sl = (itsPrice * stoploss) / 100;
                            if ((sl % tick) != 0)
                                sl = sl - (sl % tick);
                            int quantity = (int) (individualAllocated / itsPrice);
//                                int quantity = 1;

                            if (quantity > 0) {
                                HashMap<String, String> parahm = new HashMap<String, String>();
                                //api_key and access token are to be passed with url
//                                parahm.put("api_key", mContext.getString(R.string.ZERODHA_API_KEY));
//                                parahm.put("access_token", tras.readSetting("access_token"));
                                parahm.put("tradingsymbol", itsScript);
                                parahm.put("exchange", "NSE");//No need to convert script for NSE
                                parahm.put("transaction_type", "SELL");
                                parahm.put("order_type", "LIMIT");
                                parahm.put("product", "MIS");//mis or cnc
                                parahm.put("quantity", String.valueOf(quantity));
                                if (itsPrice > 10000) {
                                    if (quantity > 100)
                                        parahm.put("disclosed_quantity", "100");
                                } else if (itsPrice > 1000) {
                                    if (quantity > 1000)
                                        parahm.put("disclosed_quantity", "1000");
                                } else if (itsPrice > 100) {
                                    if (quantity > 10000)
                                        parahm.put("disclosed_quantity", "10000");
                                } else {
                                    if (quantity > 100000)
                                        parahm.put("disclosed_quantity", "100000");
                                }
                                parahm.put("validity", "DAY");
                                parahm.put("price", String.format(new Locale("en", "IN"), "%.2f", itsPrice));
                                parahm.put("squareoff_value", String.format(new Locale("en", "IN"), "%.2f", limit));
                                parahm.put("stoploss_value", String.format(new Locale("en", "IN"), "%.2f", sl));
//                                    parahm.put("trailing_stoploss", "1");
                                performBracketOrder(parahm, mContext.getString(R.string.ZERODHA_API_KEY), tras.readSetting("access_token"));
                                while (orderProcessingIncomplete) ;
                                if (orderProcessingSuccess) {
//                                            Log.e("important", "yey! itz success, too bad i will not see this msz");
                                }
                            }
                        }
                    }
                }
//                }
            } else
                notify("Not logged in");
        } else {
            notify("No internet, act quickly");
//            retry();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        notify(totalBuyable + " items bought and " + totalSellable + " items sold today");
        if ((totalBuyable + totalSellable) > 0)
            squareOff();
        super.onPostExecute(aVoid);
    }

    private int checkChart(String script) {
        //Yahoo
//        String uri = "https://chartapi.finance.yahoo.com/instrument/1.0/" + script + "/chartdata;type=quote;range=1d/csv";
        //Google
        String chartScriptX = script.split(":")[0];
        String chartScriptQ = script.split(":")[1];
        String uri = "https://www.google.com/finance/getprices?q=" + chartScriptQ + "&x=" + chartScriptX + "&i=60&p=1d&f=d,o,h,l,c,v";

//        Log.e("important", "checkChart: from " +uri);
        try {
            //Initialization
            List<Double> chartCloseVal = new ArrayList<>();

            Double startPrice = 0d, currentPrice = 0d, movingAverage = 0d;
            Double movingSum = 0d;
            Double noAboveMa = 0d, noBelowMa = 0d;
            int timesCrossedMa = 0, firstCrossedMA = -1, lastCrossedMA = Integer.MAX_VALUE, distanceBetweenMACross;
            Double pctAboveMa, pctBelowMa;
            Double maxUp = 0d, minDown = Double.MAX_VALUE;
            Double distanceOfMaxUp = Double.MAX_VALUE, distanceOfMinDown = Double.MAX_VALUE;
            Double distanceFromMaxUpPct, distanceFromMinDownPct;
            Double distanceOfCurUp = Double.MAX_VALUE, distanceOfCurDown = Double.MAX_VALUE;
            Double distanceFromCurUpPct, distanceFromCurDownPct;
            Double maxUpPct, minDownPct, curUpDownPct;
            Double maxUpRelative = 0d, minDownRelative = 0d;

            Double maxDiffInVicinityUp = 0.0d, maxDiffInVicinityDown = 0.0d;
            Double maxDifferenceInVicinityUpRelative = 0.0d, maxDifferenceInVicinityDownRelative = 0.0d;

            InputStream stream;
            BufferedReader br;
            URL url = new URL(uri);
            stream = url.openStream();
            br = new BufferedReader(new InputStreamReader(stream));

            //Data collection
            Double numVals = 0d;
            Double counter = 0d;
            String line;
            while ((line = br.readLine()) != null) {
//                Log.e("important", line);
                if (counter == startFrom) {
                    startPrice = Double.valueOf(line.split(",")[1]);
                    chartCloseVal.add(Double.valueOf(line.split(",")[1]));
                    if (Double.valueOf(line.split(",")[2]) > maxUp) {
                        maxUp = Double.valueOf(line.split(",")[2]);
                        distanceOfMaxUp = numVals;
                    }
                    if (Double.valueOf(line.split(",")[3]) < minDown) {
                        minDown = Double.valueOf(line.split(",")[3]);
                        distanceOfMinDown = numVals;
                    }
                    numVals++;
                } else if (counter > startFrom) {
                    chartCloseVal.add(Double.valueOf(line.split(",")[1]));
                    currentPrice = Double.valueOf(line.split(",")[1]);

                    if (Double.valueOf(line.split(",")[1]) > maxUp) {
                        distanceOfMaxUp = numVals;
                        maxUp = Double.valueOf(line.split(",")[1]);
                    }
                    if (Double.valueOf(line.split(",")[1]) < minDown) {
                        distanceOfMinDown = numVals;
                        minDown = Double.valueOf(line.split(",")[1]);
                    }

                    numVals++;
                }
                counter++;
            }

            distanceFromMaxUpPct = ((numVals - distanceOfMaxUp) / numVals) * 100;
            distanceFromMinDownPct = ((numVals - distanceOfMinDown) / numVals) * 100;

            counter = 0d;
            for (Double oneClose : chartCloseVal) {
                int closeIndex = counter.intValue();
                for (int i = closeIndex + 1; i < closeIndex + 15; i++) {
                    if (chartCloseVal.size() > (i + 1)) {
                        Double thisDiff1 = ((oneClose - chartCloseVal.get(i)) / chartCloseVal.get(i)) * 100;
                        Double thisDiff2 = ((chartCloseVal.get(i) - oneClose) / oneClose) * 100;
                        if (thisDiff1 > 0) {
                            if (thisDiff1 > maxDiffInVicinityUp) {
//                            if(chartScriptQ.equals("MARUTI"))
//                                Log.e("important", "up between: "+closeIndex+" and "+i);
                                maxDiffInVicinityUp = thisDiff1;
                            }
                        }
                        if (thisDiff2 > 0) {
                            if (thisDiff2 > maxDiffInVicinityUp) {
//                            if(chartScriptQ.equals("MARUTI"))
//                                Log.e("important", "up between: "+closeIndex+" and "+i);
                                maxDiffInVicinityUp = thisDiff2;
                            }
                        }
                        if (thisDiff1 < 0) {
                            if (thisDiff1 < maxDiffInVicinityDown) {
//                            if(chartScriptQ.equals("MARUTI"))
//                                Log.e("important", "down between: "+closeIndex+" and "+i);
                                maxDiffInVicinityDown = thisDiff1;
                            }
                        }
                        if (thisDiff2 < 0) {
                            if (thisDiff1 < maxDiffInVicinityDown) {
//                            if(chartScriptQ.equals("MARUTI"))
//                                Log.e("important", "down between: "+closeIndex+" and "+i);
                                maxDiffInVicinityDown = thisDiff1;
                            }
                        }
                    }
                }
                counter++;
            }
            if (startPrice <= currentPrice) {
                Double totalPctUp = ((currentPrice - startPrice) / startPrice) * 100;
                maxDifferenceInVicinityUpRelative = (maxDiffInVicinityUp / totalPctUp) * 100;
            } else if (startPrice > currentPrice) {
                Double totalPctDown = ((startPrice - currentPrice) / startPrice) * 100;
                maxDifferenceInVicinityDownRelative = (maxDiffInVicinityDown / totalPctDown) * 100;
            }

            if (numVals > 1) {
                for (Double oneVal : chartCloseVal) {
                    movingSum += oneVal;
                }
            }

            prices.add(currentPrice);
            movingAverage = movingSum / numVals;

            maxUpPct = ((maxUp - movingAverage) / movingAverage) * 100;
            minDownPct = ((minDown - movingAverage) / movingAverage) * 100;
            curUpDownPct = ((currentPrice - movingAverage) / movingAverage) * 100;
            maxUpRelative = ((maxUpPct - curUpDownPct) / curUpDownPct) * 100;
            minDownRelative = ((minDownPct - curUpDownPct) / curUpDownPct) * 100;
            if (minDownRelative == -0)
                minDownRelative = 0d;

            counter = 0d;
            for (Double oneVal : chartCloseVal) {
                if (oneVal > movingAverage) {
                    noAboveMa++;
                } else if (oneVal < movingAverage) {
                    noBelowMa++;
                }
                if (oneVal > (movingAverage - ((movingAverage * CROSS_THRESHOLD) / 100)) && oneVal < (movingAverage + ((movingAverage * CROSS_THRESHOLD) / 100))) {
                    if (firstCrossedMA == -1)
                        firstCrossedMA = counter.intValue();
                    lastCrossedMA = counter.intValue();
                    timesCrossedMa++;
                }

                if (oneVal >= currentPrice)
                    if (distanceOfCurUp == Double.MAX_VALUE)
                        distanceOfCurUp = counter;
                if (oneVal <= currentPrice)
                    if (distanceOfCurDown == Double.MAX_VALUE)
                        distanceOfCurDown = counter;
                counter++;
            }

            distanceFromCurUpPct = ((numVals - distanceOfCurUp) / numVals) * 100;
            distanceFromCurDownPct = ((numVals - distanceOfCurDown) / numVals) * 100;

            distanceBetweenMACross = lastCrossedMA - firstCrossedMA;

            pctAboveMa = (noAboveMa / numVals) * 100;
            pctBelowMa = (noBelowMa / numVals) * 100;

//            if (chartScriptQ.equals("VEDL")) {
//                Log.e("important", "startprice is " + startPrice);
//                Log.e("important", "currentprice is " + currentPrice);
//                Log.e("important", "ma is " + movingAverage);
//                Log.e("important", "pctabovema is " + pctAboveMa);
//                Log.e("important", "pctbelowma is " + pctBelowMa);
//                Log.e("important", "distance from maxup is " + distanceFromMaxUpPct);
//                Log.e("important", "distance from mindown is " + distanceFromMinDownPct);
//                Log.e("important", "maxuprelative is " + maxUpRelative);
//                Log.e("important", "mindownrelative is " + minDownRelative);
//                Log.e("important", "ma crossed " + timesCrossedMa+ " times");
//                Log.e("important", "ma crossed distance is " + distanceBetweenMACross);
//                Log.e("important", "mindownrelative is " + minDownRelative);
//                Log.e("important", "maxdiffinvicinityup is " + maxDifferenceInVicinityUpRelativeOld);
//                Log.e("important", "maxdiffinvicinitydown is " + maxDifferenceInVicinityDownRelativeOld);
//            }

            keepRecord(script, distanceFromMaxUpPct.toString(), distanceFromMinDownPct.toString(), maxUpRelative.toString(), minDownRelative.toString(), maxDifferenceInVicinityUpRelative.toString(), maxDifferenceInVicinityDownRelative.toString(), distanceFromCurUpPct.toString(), distanceFromCurDownPct.toString(), timesCrossedMa, distanceBetweenMACross);
            //Logic application

            if (startPrice > 0d && currentPrice > 0d && movingSum > 0d && movingAverage > 0d) {
                if ((startPrice < movingAverage) && (currentPrice > movingAverage)) {
                    if (pctAboveMa <= 20) {
                        stream.close();
                        return SHARP_UP;
                    } else if (pctAboveMa > 20 && pctAboveMa < 80) {
                        if ((distanceFromMaxUpPct < 10) && (maxUpRelative > 0) && (maxUpRelative < 10) && (distanceFromCurUpPct < 50) && (maxDifferenceInVicinityUpRelative < 1000) && (distanceBetweenMACross < 50) & (timesCrossedMa < 10)) {
                            stream.close();
                            return GRADUAL_UP;
                        } else {
                            stream.close();
                            return CHOPPY;
                        }
                    } else if (pctAboveMa >= 80) {
                        stream.close();
                        return LOW_FLUCTUATION;//not sure
                    }
                } else if ((startPrice > movingAverage) && (currentPrice < movingAverage)) {
                    if (pctBelowMa <= 20) {
                        stream.close();
                        return SHARP_DOWN;
                    } else if (pctBelowMa > 20 && pctBelowMa < 80) {
                        if ((distanceFromMinDownPct < 10) && (minDownRelative > 0) && (minDownRelative < 10) && (distanceFromCurDownPct < 50) && (maxDifferenceInVicinityDownRelative > -1000) && (distanceBetweenMACross < 50) & (timesCrossedMa < 10)) {
                            stream.close();
                            return GRADUAL_DOWN;
                        } else {
                            stream.close();
                            return CHOPPY;
                        }
                    } else if (pctBelowMa >= 80) {
                        stream.close();
                        return LOW_FLUCTUATION;
                    }
                } else if ((startPrice > movingAverage) && (currentPrice > movingAverage)) {
                    stream.close();
                    return LOW_FLUCTUATION;//it can also be choppy though
                } else if ((startPrice < movingAverage) && (currentPrice < movingAverage)) {
                    stream.close();
                    return LOW_FLUCTUATION;//it can also be choppy though
                }
            }
        } catch (Exception e) {
//            Log.e("important", "The exception here is: "+Log.getStackTraceString(e));
            if (e instanceof EOFException)
                prices.add(0.0d);
            else if (e instanceof FileNotFoundException)
                notify("Chart service not working for " + chartScriptQ);
        }
        return 0;
    }

    private void keepRecord(String script, String maxupDistance, String mindownDistance, String maxUpRelative, String minDownRelative, String maxJumpUp, String maxJumpDown, String firstCrossedCurPriceUp, String firstCrossedCurPriceDown, int timesCrossed, int distanceCrossed) {
        try {
            String recUrl;
            recUrl = mContext.getString(R.string.unidom) + "api/mystockplanner/profit.php?script=" + script + "&mud=" + maxupDistance + "&mdd=" + mindownDistance + "&mur=" + maxUpRelative + "&mdr=" + minDownRelative + "&mdvu=" + maxJumpUp + "&mdvd=" + maxJumpDown + "&dcpu=" + firstCrossedCurPriceUp + "&dcpd=" + firstCrossedCurPriceDown + "&times=" + timesCrossed + "&dmc=" + distanceCrossed;

            URL url1 = new URL(recUrl);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            InputStream stream1 = conn.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(stream1));
            StringBuilder result = new StringBuilder();
            for (String line1; (line1 = reader.readLine()) != null; ) {
                result.append(line1);
            }
            stream1.close();
        } catch (Exception e) {
//            Log.e("important", Log.getStackTraceString(e));
        }
    }

    private void notify(String msg) {
//        Log.e("important", "notify: " + msg);
        int mNotificationId = 1;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_timeline_black_24dp)
                .setContentTitle("MSP Background")
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentText(msg);

        NotificationManager mNotifyMgr = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

//    private void retry() {
//        Alarm alarm = new Alarm();
//        alarm.setTradeAlarmAgain(mContext);
//    }

    private void squareOff() {
        Alarm alarm = new Alarm();
        alarm.SetSquareOffAlarm(mContext);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean hasActiveInternetConnection() {
        if (isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1000);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    private void performBracketOrder(HashMap postDataParams, final String apiKey, final String accessToken) {
        try {
            URL url = new URL(BRACKET_ORDER_URL + "?api_key=" + apiKey + "&access_token=" + accessToken + "&" + getPostData(postDataParams));
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
//            conn.connect();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                if ((line = br.readLine()) != null) {
//                    Log.e("important", "lines are " + line);
                    BackTrade.ParseOrderResultDataJSON podj = new BackTrade.ParseOrderResultDataJSON();
                    podj.parseJSONAndStoreIt(line);
                    while (podj.parsingInComplete) ;
                    if (podj.getStatus().equals("success")) {
                        orderProcessingIncomplete = false;
                        orderProcessingSuccess = true;
                    } else {
                        orderProcessingIncomplete = false;
                        orderProcessingSuccess = false;
                    }
                }
            } else {
//                Log.e("important", "responsecode is " + responseCode);
                orderProcessingIncomplete = false;
                orderProcessingSuccess = false;
            }
            os.close();

        } catch (Exception e) {
//            Log.e("important", Log.getStackTraceString(e));
            orderProcessingIncomplete = false;
            orderProcessingSuccess = false;
        }
    }

    private String getPostData(HashMap<String, String> params) {
        StringBuilder result = new StringBuilder();
        try {
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return result.toString();
    }

    private class ParseOrderResultDataJSON {
        String status, ordrId;
        public volatile boolean parsingInComplete = true;

        public String getStatus() {
            return status;
        }

        public String getOrdrId() {
            return ordrId;
        }

        private void parseJSONAndStoreIt(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                status = jsonObject.optString("status");
                String dataString = jsonObject.optString("data");
                if (status.equals("success")) {
                    JSONObject jsonDataObject = new JSONObject(dataString);
                    ordrId = jsonDataObject.optString("order_id");//No need for the id, just taking
                }

                parsingInComplete = false;
            } catch (JSONException e) {
                //Log.e("important", "exception " + Log.getStackTraceString(e));
            }
        }
    }

    public Double getPrice(final String symbol, final String apiKey, final String accessToken) {
        Double price = 0.0d;
        try {
            URL url = new URL(MARKET_URL + symbol + "?api_key=" + apiKey + "&access_token=" + accessToken);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestMethod("GET");
            con.connect();
            int response = con.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                if ((line = br.readLine()) != null) {
//                    Log.e("important", "lines in getting price " + line);
                    BackTrade.ParseMarketDataJSON pmdj = new BackTrade.ParseMarketDataJSON();
                    pmdj.parseJSONAndStoreIt(line);
                    while (pmdj.parsingInComplete) ;
                    if (pmdj.getStatus().equals("success")) {
                        price = pmdj.getLTP();
                    }
                }
            }
        } catch (Exception ex) {
        }
        return price;
    }

    private class ParseMarketDataJSON {
        String status;
        Double ltp = 0.0d;
        public volatile boolean parsingInComplete = true;

        public String getStatus() {
            return status;
        }

        public Double getLTP() {
            return ltp;
        }

        private void parseJSONAndStoreIt(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                status = jsonObject.optString("status");
                String dataString = jsonObject.optString("data");
                if (status.equals("success")) {
                    JSONObject jsonDataObject = new JSONObject(dataString);
                    ltp = Double.parseDouble(jsonDataObject.optString("last_price"));
                }

                parsingInComplete = false;
            } catch (JSONException e) {
                //Log.e("important", "exception " + Log.getStackTraceString(e));
            }
        }
    }
}
