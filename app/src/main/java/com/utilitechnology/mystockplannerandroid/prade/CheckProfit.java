package com.utilitechnology.mystockplannerandroid.prade;

import android.content.Context;
import android.os.AsyncTask;

import com.utilitechnology.mystockplannerandroid.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Bibaswann on 23-02-2017.
 */

public class CheckProfit extends AsyncTask<Void, Void, Void> {
    Context mContext;
    int date;
    List<String> timeStamps = new ArrayList<>();
    List<Integer> skipped = new ArrayList<>();
    List<String> openScripts = new ArrayList<>();
    List<Double> buyPrice = new ArrayList<>();
    List<Double> limit = new ArrayList<>();
    List<Double> sl = new ArrayList<>();
    List<String> quantity = new ArrayList<>();

    CheckProfit(Context context) {
        mContext = context;
        date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String urlString = mContext.getString(R.string.unidom)+"api/mystockplanner/profit.php?mode=read";
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            InputStream recordStream = conn.getInputStream();
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(recordStream));
            while ((line = br.readLine()) != null) {
                timeStamps.add(line.split(",")[0]);
                skipped.add(Integer.parseInt(line.split(",")[1]));
                openScripts.add(line.split(",")[2]);
                buyPrice.add(Double.parseDouble(line.split(",")[3]));
                limit.add(Double.parseDouble(line.split(",")[4]));
                sl.add(Double.parseDouble(line.split(",")[5]));
                quantity.add(line.split(",")[6]);
            }
            recordStream.close();
//            Log.e("important", "total open trade "+openScripts.size());
            for (String open : openScripts) {
//                Log.e("important", "checking script : "+open);
                String thisTimeProfitOrLoss = "na";
                Double closingPrice;
                Double sellPrice=0d;
                int index = openScripts.indexOf(open);
                int counter = 0;

                InputStream chartStream;
                BufferedReader br2;
                URL url2 = new URL("http://chartapi.finance.yahoo.com/instrument/1.0/" + open + "/chartdata;type=quote;range=1d/csv");
                chartStream = url2.openStream();
                br2 = new BufferedReader(new InputStreamReader(chartStream));

                //No need to think about previous close
                String line2;
                while ((line2 = br2.readLine()) != null) {
                    counter++;
                    if (counter >= skipped.get(index)) {
                        if (Double.valueOf(line2.split(",")[1]) > limit.get(index)) {
                            thisTimeProfitOrLoss = "profit";
                            sellPrice = limit.get(index);
                            break;
                        } else if ((Double.valueOf(line2.split(",")[1]) < sl.get(index))) {
                            thisTimeProfitOrLoss = "loss";
                            sellPrice = sl.get(index);
                            break;
                        }

                    }
                }
                closingPrice = Double.parseDouble(line2.split(",")[1]);
                if (thisTimeProfitOrLoss.equals("na")) {
                    sellPrice = closingPrice;
                }
                String profitUrl = mContext.getString(R.string.unidom)+"api/mystockplanner/profit.php?mode=update&buy_price=" + buyPrice.get(index) + "&sell_price=" + sellPrice + "&timestamp=" + timeStamps.get(index) + "&quantity=" + quantity.get(index);
                URL url3 = new URL(profitUrl);
                HttpURLConnection conn2 = (HttpURLConnection) url3.openConnection();
                conn2.setRequestMethod("GET");
                conn2.setDoInput(true);
                conn2.connect();
                int response = conn2.getResponseCode();
//                if(response==HttpURLConnection.HTTP_OK)
//                {
//                    Log.e("important", "success");
//                }
            }
        } catch (Exception e) {
//            Log.e("important",Log.getStackTraceString(e));
        }
        return null;
    }
}
